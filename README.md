# Integration of WPS in local SDI

Integration of WPS orchestrated processes in the spatial data infrastructure of Freiburg using the example of explosive ordnance disposal. A study about the acceptability and benefits of geospatial processing services in a local government structure.