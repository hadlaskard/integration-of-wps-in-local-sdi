<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ build_radio_sys2.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413895.55</gml:X><gml:Y>5317500.28</gml:Y></gml:coord>
      <gml:coord><gml:X>414340.92</gml:X><gml:Y>5317851.69</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                                        
  <gml:featureMember>
    <ogr:build_radio_sys2 fid="build_radio_sys2.0">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:25832"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>414199.18,5317526.47 414185.17,5317505.04 414192.5,5317500.28 414196.62,5317506.57 414199.84,5317504.46 414209.77,5317519.64 414208.77,5317520.29 414210.06,5317522.27 414201.37,5317527.85 414200.08,5317525.88 414199.18,5317526.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ogc_fid>5113</ogr:ogc_fid>
      <ogr:gebaeudefunktion>3010</ogr:gebaeudefunktion>
      <ogr:bezeichnung>Stefan-Meier-Straße</ogr:bezeichnung>
      <ogr:hausnummer>70</ogr:hausnummer>
      <ogr:sschl>6510</ogr:sschl>
	  <ogr:radio_buff>10</ogr:radio_buff>
    </ogr:build_radio_sys2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:build_radio_sys2 fid="build_radio_sys2.1">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:25832"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>414307.74,5317812.84 414311.53,5317810.34 414312.04,5317811.12 414315.95,5317808.54 414340.92,5317846.44 414333.32,5317851.46 414330.52,5317847.21 414330.13,5317847.47 414320.25,5317832.46 414316.64,5317834.84 414313.76,5317830.45 414317.36,5317828.1 414310.47,5317817.62 414310.77,5317817.41 414307.74,5317812.84</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ogc_fid>5135</ogr:ogc_fid>
      <ogr:gebaeudefunktion>1010</ogr:gebaeudefunktion>
      <ogr:bezeichnung>Rennweg</ogr:bezeichnung>
      <ogr:hausnummer>23</ogr:hausnummer>
      <ogr:sschl>5460</ogr:sschl>
	  <ogr:radio_buff>40</ogr:radio_buff>
    </ogr:build_radio_sys2>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:build_radio_sys2 fid="build_radio_sys2.2">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:25832"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413913.43,5317835.85 413909.02,5317839.57 413895.55,5317823.72 413906.87,5317814.19 413918.49,5317827.93 413922.3,5317824.7 413937.49,5317842.62 413926.8,5317851.69 413913.43,5317835.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ogc_fid>19355</ogr:ogc_fid>
      <ogr:gebaeudefunktion>3010</ogr:gebaeudefunktion>
      <ogr:bezeichnung>Friedhofstraße</ogr:bezeichnung>
      <ogr:hausnummer>8</ogr:hausnummer>
      <ogr:sschl>2110</ogr:sschl>
	  <ogr:radio_buff>75</ogr:radio_buff>
    </ogr:build_radio_sys2>
  </gml:featureMember>
</ogr:FeatureCollection>
