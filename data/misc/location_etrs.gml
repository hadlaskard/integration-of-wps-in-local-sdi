<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ https://gitlab.com/hadlaskard/integration-of-wps-in-local-sdi/raw/master/data/misc/location_etrs.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413478.281822533</gml:X><gml:Y>5316862.88473361</gml:Y></gml:coord>
      <gml:coord><gml:X>413478.281822533</gml:X><gml:Y>5316862.88473361</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                              
  <gml:featureMember>
    <ogr:location_etrs fid="location.0">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413478.281822533,5316862.88473361</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:location_etrs>
  </gml:featureMember>
</ogr:FeatureCollection>
