<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ wfs_pois_data_PCdMZI.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413200.322483</gml:X><gml:Y>5316662.51718</gml:Y></gml:coord>
      <gml:coord><gml:X>413778.02976</gml:X><gml:Y>5317066.305514</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                                
  <gml:featureMember>
    <ogr:pois fid="578">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413378.826728,5316773.256702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.578</ogr:gml_id>
      <ogr:ogc_fid>29</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:name>St. Klara</ogr:name>
      <ogr:bezeichnung>Kindergarten</ogr:bezeichnung>
      <ogr:kategorie>VÖ</ogr:kategorie>
      <ogr:adresse>Klarastraße 41  79106 Freiburg</ogr:adresse>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:telefon>0761/273100</ogr:telefon>
      <ogr:organisation>katholische Kirche</ogr:organisation>
      <ogr:stadtbezirk>Alt-Stühlinger</ogr:stadtbezirk>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="739">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413200.322483,5316716.573061</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.739</ogr:gml_id>
      <ogr:ogc_fid>45</ogr:ogc_fid>
      <ogr:prioritaet>469</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>schulen</ogr:poityp>
      <ogr:name>Hansjakob-Realschule</ogr:name>
      <ogr:kategorie>122667</ogr:kategorie>
      <ogr:adresse>Wannerstraße 2, 79106 Freiburg im Breisgau</ogr:adresse>
      <ogr:orig_id>45</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="837">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413778.02976,5316662.51718</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.837</ogr:gml_id>
      <ogr:ogc_fid>57</ogr:ogc_fid>
      <ogr:prioritaet>1100</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>schulen</ogr:poityp>
      <ogr:name>St. Ursula-Gymnasium</ogr:name>
      <ogr:kategorie>667815</ogr:kategorie>
      <ogr:adresse>Eisenbahnstraße 45, 79098 Freiburg im Breisgau</ogr:adresse>
      <ogr:orig_id>57</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="965">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413502.33343,5317066.305514</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.965</ogr:gml_id>
      <ogr:ogc_fid>78</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:name>Uni-Kita Zaubergarten</ogr:name>
      <ogr:bezeichnung>Kita/Krippe</ogr:bezeichnung>
      <ogr:kategorie>GT, K1 ,agm.</ogr:kategorie>
      <ogr:adresse>Agnesenstraße 4  79106 Freiburg</ogr:adresse>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:telefon>0761/274043</ogr:telefon>
      <ogr:organisation>Familienservice der Uni</ogr:organisation>
      <ogr:stadtbezirk>Eschholz</ogr:stadtbezirk>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1213">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413442.917391,5316882.056501</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1213</ogr:gml_id>
      <ogr:ogc_fid>117</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:name>Klara-Kinder, Egonstraße</ogr:name>
      <ogr:bezeichnung>Krippe</ogr:bezeichnung>
      <ogr:kategorie>VÖ, Nachmittag</ogr:kategorie>
      <ogr:adresse>Egonstraße 21  79106 Freiburg</ogr:adresse>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:telefon>01795556659</ogr:telefon>
      <ogr:organisation>Die Klara Kinder GbR</ogr:organisation>
      <ogr:ansprechpartner>Frau Reich-Disch</ogr:ansprechpartner>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1491">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413384.088871,5316851.226149</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1491</ogr:gml_id>
      <ogr:ogc_fid>179</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:name>Klara-Kinder, Guntramstraße</ogr:name>
      <ogr:bezeichnung>Krippe</ogr:bezeichnung>
      <ogr:kategorie>GT</ogr:kategorie>
      <ogr:adresse>Guntramstraße 21  79106 Freiburg</ogr:adresse>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:telefon>01795556659</ogr:telefon>
      <ogr:organisation>Die Klara Kinder GbR</ogr:organisation>
      <ogr:ansprechpartner>Frau Reich-Disch</ogr:ansprechpartner>
    </ogr:pois>
  </gml:featureMember>
</ogr:FeatureCollection>
