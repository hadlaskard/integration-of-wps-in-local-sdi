<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ db_address_data_tOkXdT.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413098.03</gml:X><gml:Y>5316535.8</gml:Y></gml:coord>
      <gml:coord><gml:X>413840.06</gml:X><gml:Y>5317231.71</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                                               
  <gml:featureMember>
    <ogr:address fid="address.0">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413519.0,5316997.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.1">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413534.28,5317024.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.2">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413528.15,5317063.31</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.3">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413515.39,5317031.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.4">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413504.2,5317013.93</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.5">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413263.36,5316616.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Amalie-Gramm-Weg</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.6">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413206.71,5317126.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.7">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413183.12,5317042.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.8">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413172.32,5317020.73</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.9">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413157.33,5317025.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>13 x</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.10">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413197.05,5317113.35</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.11">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413189.37,5317098.3</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.12">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413182.82,5317083.1</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.13">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413207.41,5317071.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.14">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413660.43,5316535.8</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bertoldstraße</ogr:street>
      <ogr:house_nr>65</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.15">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413556.43,5316609.42</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.16">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413665.02,5316755.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.17">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413677.81,5316778.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.18">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413695.61,5316809.0</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.19">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413737.19,5316765.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.20">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413718.7,5316852.38</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413757.98,5316796.6</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413626.04,5316557.19</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413641.34,5316584.08</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>2 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413791.07,5316832.49</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413600.95,5316633.97</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413648.53,5316598.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413605.35,5316644.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413658.73,5316616.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413627.84,5316684.65</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413600.35,5316679.95</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413616.94,5316710.64</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 b</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413633.43,5316741.72</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 c</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413649.73,5316771.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 d</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413662.72,5316796.6</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 e</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413681.51,5316831.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 f</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413697.21,5316856.28</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 g</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413706.61,5316877.97</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 h</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413677.62,5316644.06</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413647.63,5316716.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413725.72,5317079.82</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413475.73,5317190.75</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413447.04,5317209.13</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413534.25,5317191.5</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413519.71,5317200.37</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413504.63,5317210.03</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413491.89,5317215.16</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413481.27,5317220.81</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413464.62,5317231.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413708.5,5317090.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413686.86,5317100.74</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413661.33,5316971.34</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413517.96,5317114.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413566.76,5317059.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413529.23,5317135.4</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413503.19,5317091.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413480.41,5317102.36</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413575.62,5317081.38</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413672.36,5316990.28</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413587.0,5317102.08</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413643.05,5317012.13</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413546.48,5317097.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413634.53,5316996.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413557.91,5317118.89</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413583.94,5317050.03</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413494.3,5316812.0</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413487.26,5316816.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413457.27,5316852.98</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413475.21,5316822.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413447.62,5316858.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413447.02,5316839.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413441.73,5316887.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413435.13,5316845.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413454.92,5316908.27</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413527.39,5316795.7</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413427.13,5316849.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413432.31,5316868.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413420.04,5316853.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413425.87,5316871.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413411.24,5316856.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413416.49,5316877.06</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413402.74,5316861.99</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413408.95,5316881.13</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413392.65,5316866.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413537.68,5316811.0</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413385.25,5316871.49</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.86">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413351.4,5316909.27</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.87">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413346.65,5316892.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.88">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413338.97,5316915.87</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.89">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413338.07,5316895.27</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.90">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413358.86,5316947.06</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.91">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413330.98,5316899.37</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.92">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413323.87,5316882.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>38 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.93">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413347.76,5316950.86</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.94">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413337.87,5316956.05</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>39 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.95">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413519.11,5316800.07</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.96">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413320.57,5316927.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.97">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413306.03,5316933.36</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.98">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413291.58,5316941.26</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.99">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413250.2,5316963.15</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.100">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413282.72,5316926.38</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.101">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413242.53,5316968.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.102">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413518.89,5316820.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.103">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413247.75,5316943.47</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.104">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413233.08,5316972.82</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.105">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413235.4,5316951.15</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.106">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413223.04,5316978.23</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.107">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413219.52,5316958.43</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.108">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413211.07,5316985.3</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>55</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.109">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413199.28,5316969.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.110">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413184.43,5316999.3</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>57</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.111">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413182.62,5316979.34</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.112">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413511.19,5316804.15</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.113">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413147.7,5316993.48</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.114">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413132.83,5317000.13</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>62</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.115">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413509.3,5316825.79</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.116">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413503.29,5316808.65</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.117">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413777.38,5316666.65</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.118">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413744.99,5316634.56</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>64</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.119">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413718.5,5316642.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>66</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.120">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413703.41,5316648.26</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>68</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.121">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413410.46,5317190.35</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.122">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413353.74,5317165.9</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.123">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413327.05,5317024.41</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.124">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413351.96,5317152.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.125">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413320.57,5317012.53</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.126">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413348.26,5317143.68</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.127">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413313.77,5316999.64</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.128">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413343.26,5317131.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.129">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413308.48,5316989.64</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.130">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413339.0,5317115.99</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.131">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413302.35,5316977.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.132">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413335.56,5317103.1</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.133">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413296.8,5316966.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.134">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413311.58,5317049.12</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.135">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413307.08,5317040.72</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>22 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.136">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413292.52,5316958.9</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.137">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413302.68,5317032.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.138">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413265.19,5316909.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.139">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413295.65,5317020.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.140">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413257.8,5316894.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.141">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413289.81,5317008.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.142">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413250.91,5316881.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.143">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413396.14,5317167.03</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.144">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413283.68,5316996.54</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.145">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413245.17,5316872.12</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.146">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413277.09,5316984.44</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.147">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413241.6,5316862.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.148">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413271.59,5316974.05</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.149">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413236.5,5316852.79</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.150">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413265.64,5316962.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.151">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413230.72,5316844.47</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.152">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413244.66,5316922.49</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.153">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413239.39,5316912.54</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>38 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.154">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413226.31,5316834.2</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.155">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413370.45,5317213.25</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>4 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.156">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413233.63,5316904.35</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.157">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413221.41,5316824.4</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.158">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413228.11,5316892.93</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.159">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413217.11,5316816.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.160">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413223.37,5316884.2</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.161">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413212.61,5316808.41</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.162">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413218.95,5316874.1</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.163">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413208.71,5316800.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.164">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413211.81,5316862.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.165">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413203.01,5316793.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.166">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413382.83,5317134.66</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.167">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413205.51,5316850.28</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.168">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413200.21,5316784.52</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.169">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413200.91,5316842.8</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.170">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413196.52,5316776.62</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.171">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413196.52,5316833.8</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.172">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413184.72,5316809.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.173">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413142.2,5316751.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.174">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413363.75,5317202.16</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.175">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413135.92,5316737.18</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.176">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413134.36,5316717.42</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>64</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.177">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413373.25,5317108.3</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.178">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413364.47,5317083.34</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>7a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.179">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413360.97,5317189.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.180">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413337.44,5317044.82</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.181">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413331.17,5317033.13</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>9 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.182">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413503.68,5317161.56</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.183">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413428.07,5317082.23</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.184">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413406.63,5317094.97</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.185">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413468.55,5317146.36</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.186">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413490.58,5317141.35</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.187">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413445.76,5317159.78</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.188">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413443.42,5317071.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.189">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413428.62,5317046.77</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>5 x</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.190">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413447.76,5317114.28</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.191">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413438.82,5317063.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.192">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413425.22,5317127.77</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.193">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413445.21,5317070.99</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.194">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413832.26,5316839.19</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Friedrichstraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.195">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413562.16,5317181.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.196">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413660.12,5317193.47</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.197">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413573.36,5317190.97</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.198">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413672.52,5317176.38</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.199">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413675.01,5317186.76</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.200">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413426.23,5316978.94</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.201">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413396.44,5316963.05</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.202">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413396.45,5316921.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.203">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413391.85,5316954.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.204">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413403.79,5316910.53</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.205">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413387.75,5316946.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.206">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413365.66,5316953.05</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>14 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.207">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413407.84,5316900.68</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.208">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413383.45,5316934.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.209">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413409.24,5316892.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.210">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413379.35,5316927.37</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.211">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413419.63,5317003.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.212">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413374.65,5316918.67</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.213">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413370.36,5316874.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.214">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413366.36,5316904.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.215">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413365.56,5316865.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.216">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413353.47,5316879.31</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.217">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413361.66,5316857.89</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.218">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413348.46,5316869.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.219">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413356.25,5316850.26</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.220">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413342.77,5316862.79</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.221">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413352.76,5316840.9</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.222">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413428.23,5316932.66</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.223">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413339.91,5316854.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.224">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413346.44,5316830.93</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.225">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413335.5,5316845.44</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.226">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413365.36,5316819.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.227">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413360.86,5316811.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>33 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.228">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413327.97,5316835.1</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.229">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413357.2,5316804.38</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.230">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413352.76,5316796.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.231">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413321.67,5316824.0</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.232">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413333.77,5316804.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.233">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413315.87,5316813.11</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.234">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413327.67,5316794.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.235">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413414.24,5316992.44</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.236">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413311.37,5316803.11</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.237">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413321.87,5316782.12</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.238">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413306.78,5316794.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.239">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413315.67,5316773.62</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.240">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413303.17,5316785.86</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.241">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413309.17,5316760.93</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.242">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413298.84,5316776.98</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.243">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413299.68,5316742.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.244">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413292.98,5316769.42</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.245">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413408.7,5316944.11</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.246">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413288.78,5316760.93</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.247">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413283.88,5316752.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.248">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413294.38,5316729.94</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.249">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413280.54,5316743.04</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.250">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413289.88,5316723.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>55</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.251">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413276.38,5316735.31</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.252">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413284.41,5316714.89</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>57</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.253">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413270.48,5316725.56</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.254">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413408.54,5316981.74</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.255">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413404.8,5316937.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.256">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413401.64,5316971.95</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.257">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413401.48,5316929.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.258">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413710.11,5316605.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>2 x</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.259">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413678.37,5316558.5</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.260">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413688.26,5316576.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.261">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413705.91,5316557.49</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.262">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413709.11,5316571.18</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.263">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413660.86,5317128.36</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.264">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413633.03,5317192.97</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.265">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413619.24,5317218.36</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.266">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413655.72,5317142.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.267">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413603.91,5317179.74</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.268">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413590.37,5317209.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.269">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413583.55,5317225.22</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.270">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413650.13,5317156.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.271">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413644.02,5317168.5</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.272">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413640.2,5317177.82</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.273">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413514.5,5316950.75</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.274">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413542.89,5316891.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.275">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413492.01,5316909.76</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.276">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413537.12,5316878.74</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.277">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413488.91,5316901.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.278">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413483.71,5316894.07</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.279">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413479.81,5316886.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.280">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413492.11,5316866.18</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.281">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413475.41,5316878.28</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.282">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413522.4,5316920.56</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.283">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413462.92,5316816.5</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.284">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413470.72,5316870.78</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.285">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413458.2,5316808.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.286">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413466.83,5316862.45</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.287">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413454.55,5316801.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.288">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413445.42,5316823.6</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.289">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413451.37,5316793.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.290">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413442.82,5316814.8</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.291">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413447.27,5316785.86</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.292">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413438.03,5316805.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.293">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413510.11,5316941.05</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.294">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413443.17,5316778.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.295">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413433.23,5316796.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.296">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413438.92,5316770.72</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.297">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413428.43,5316787.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.298">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413434.43,5316763.62</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.299">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413422.13,5316780.22</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.300">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413394.21,5316790.75</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.301">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413430.23,5316756.62</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.302">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413418.33,5316772.92</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.303">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413426.39,5316748.97</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.304">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413413.93,5316764.92</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.305">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413513.1,5316907.36</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.306">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413422.39,5316741.77</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.307">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413410.74,5316754.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.308">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413418.48,5316734.15</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.309">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413405.84,5316745.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.310">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413414.56,5316726.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.311">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413400.34,5316738.73</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.312">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413410.63,5316718.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.313">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413397.04,5316728.94</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.314">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413406.58,5316711.56</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.315">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413391.79,5316719.8</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.316">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413505.81,5316932.85</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.317">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413403.04,5316703.45</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.318">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413387.74,5316712.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.319">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413366.55,5316725.34</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>51 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.320">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413398.16,5316696.75</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.321">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413383.65,5316704.35</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.322">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413394.64,5316688.75</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.323">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413379.51,5316696.48</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>55</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.324">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413390.33,5316680.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.325">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413369.61,5316680.28</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>57</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.326">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413386.01,5316672.73</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.327">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413505.9,5316894.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.328">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413380.85,5316662.96</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.329">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413501.31,5316924.66</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.330">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413552.38,5316904.76</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.331">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413496.51,5316917.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.332">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413146.66,5316851.37</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.333">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413160.68,5316845.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.334">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413167.63,5316860.98</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.335">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413157.53,5316879.48</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.336">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413172.13,5316868.98</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.337">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413146.73,5316894.37</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.338">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413176.53,5316879.68</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.339">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413563.8,5317000.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.340">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413607.05,5317018.93</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.341">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413644.83,5317071.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>12 x</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.342">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413559.31,5316968.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.343">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413706.79,5317147.02</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.344">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413714.27,5317155.06</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.345">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413610.34,5317072.81</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.346">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413571.67,5316980.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.347">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413622.97,5317093.3</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.348">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413580.15,5316993.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.349">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413686.21,5317148.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.350">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413594.26,5317003.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.351">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413697.1,5317164.18</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.352">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413567.48,5316930.95</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.353">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413551.59,5316938.85</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.354">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413609.38,5316940.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.355">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413538.2,5316944.85</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.356">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413573.16,5316951.06</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.357">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413508.91,5316959.35</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.358">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413489.81,5316969.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.359">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413474.22,5316947.65</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17 b</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.360">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413480.91,5316974.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.361">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413472.52,5316979.64</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.362">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413463.62,5316984.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.363">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413490.19,5316990.27</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.364">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413451.32,5316991.04</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.365">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413481.74,5316995.02</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.366">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413439.33,5316996.89</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.367">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413474.04,5317000.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.368">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413409.75,5317014.78</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.369">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413620.65,5316900.96</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.370">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413467.01,5317004.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.371">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413399.16,5317019.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.372">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413386.75,5317001.44</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>31 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.373">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413458.82,5317009.23</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.374">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413468.81,5317027.16</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>32 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.375">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413390.44,5317024.83</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.376">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413450.92,5317013.13</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.377">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413382.35,5317029.23</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.378">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413365.68,5316996.64</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.379">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413442.39,5317017.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.380">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413374.52,5317034.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.381">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413361.56,5317011.83</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>37 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.382">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413433.86,5317021.44</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.383">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413445.24,5317039.9</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>38 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.384">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413364.86,5317038.72</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.385">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413425.73,5317027.53</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.386">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413356.86,5317043.12</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.387">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413313.46,5317068.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.388">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413403.14,5317049.82</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.389">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413304.38,5317076.31</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.390">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413377.95,5317063.74</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.391">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413296.98,5317081.31</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.392">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413595.68,5316923.94</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.393">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413324.52,5317086.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.394">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413281.68,5317092.7</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.395">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413314.5,5317092.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.396">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413259.49,5317098.8</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.397">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413307.67,5317098.4</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.398">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413292.24,5317110.1</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>54 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.399">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413246.2,5317053.82</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>55</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.400">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413268.42,5317135.62</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.401">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413274.7,5317150.3</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.402">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413245.39,5317105.1</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>59</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.403">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413281.42,5317164.98</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.404">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413232.63,5317110.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>61</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.405">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413289.93,5317180.0</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>62</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.406">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413240.6,5317152.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>64</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.407">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413247.29,5317164.98</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>66</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.408">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413583.87,5316925.16</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.409">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413734.99,5317089.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.410">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413742.88,5317101.6</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.411">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413747.78,5317120.9</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.412">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413821.46,5316699.73</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Poststraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.413">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413828.36,5316722.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Poststraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.414">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413830.66,5316736.72</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Poststraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.415">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413789.32,5316753.49</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.416">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413777.92,5316757.67</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.417">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413759.25,5316762.95</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.418">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413840.06,5316756.11</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.419">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413826.66,5316760.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.420">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413814.07,5316764.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.421">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413317.7,5317174.73</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.422">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413326.02,5317203.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.423">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413312.47,5317157.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.424">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413308.77,5317147.68</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.425">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413300.47,5317186.97</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>5 x</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.426">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413166.34,5316960.11</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.427">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413127.74,5316954.75</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.428">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413171.72,5316919.37</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.429">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413158.93,5316924.96</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>3 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.430">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413121.24,5316938.26</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.431">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413159.26,5316944.02</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.432">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413310.39,5316903.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.433">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413252.99,5316788.02</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.434">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413237.6,5316795.41</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.435">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413239.7,5316811.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>12 x</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.436">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413242.2,5316768.82</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.437">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413227.6,5316775.62</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.438">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413228.5,5316744.83</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.439">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413215.91,5316750.53</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.440">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413299.62,5316910.73</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.441">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413292.9,5316873.26</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.442">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413283.98,5316876.99</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.443">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413280.63,5316849.23</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.444">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413271.25,5316852.82</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.445">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413272.28,5316828.34</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.446">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413258.08,5316834.54</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.447">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413262.19,5316808.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.448">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413399.85,5316648.02</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.449">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413309.7,5316695.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.450">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413301.97,5316699.26</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.451">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413294.01,5316703.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.452">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413248.7,5316727.08</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.453">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413240.43,5316731.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.454">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413199.68,5316720.73</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.455">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413195.02,5316757.83</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.456">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413166.64,5316809.3</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.457">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413153.93,5316815.7</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.458">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413129.36,5316830.95</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.459">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413116.47,5316837.42</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.460">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413353.36,5316673.16</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.461">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413098.03,5316840.58</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>31 x</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.462">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413129.94,5316771.92</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.463">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413333.3,5316682.48</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.464">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413117.71,5316780.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.465">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413326.41,5316687.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.466">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413106.18,5316788.41</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.467">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413317.0,5316691.04</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.468">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413612.35,5316887.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.469">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413585.76,5316871.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.470">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413440.58,5316642.35</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.471">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413463.61,5316636.37</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>11 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.472">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413580.57,5316864.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.473">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413577.17,5316855.38</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.474">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413402.62,5316562.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.475">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413557.62,5316844.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.476">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413564.07,5316834.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.477">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413606.66,5316910.26</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.478">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413552.94,5316810.89</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.479">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413538.18,5316783.41</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.480">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413532.54,5316771.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.481">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413524.29,5316759.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.482">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413520.55,5316749.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.483">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413511.97,5316741.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.484">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413487.7,5316728.83</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.485">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413461.81,5316712.04</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.486">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413446.32,5316696.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.487">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413439.01,5316687.94</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.488">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413597.76,5316897.76</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.489">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413433.43,5316680.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.490">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413430.43,5316671.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.491">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413425.63,5316663.56</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.492">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413415.56,5316644.03</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.493">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413421.31,5316653.49</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.494">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413594.86,5316851.78</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.495">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413574.87,5316812.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.496">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413479.01,5316696.05</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>7 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.497">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413589.16,5316881.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.498">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413451.51,5316663.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
</ogr:FeatureCollection>
