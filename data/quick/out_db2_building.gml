<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ db_building_data_rf2KLI.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413081.91</gml:X><gml:Y>5316415.43</gml:Y></gml:coord>
      <gml:coord><gml:X>413920.85</gml:X><gml:Y>5317258.38</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                                              
  <gml:featureMember>
    <ogr:building fid="building.0">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413822.64,5316692.11 413818.56,5316674.93 413813.77,5316676.13 413813.34,5316674.42 413807.51,5316675.89 413807.94,5316677.59 413801.72,5316679.14 413802.69,5316682.06 413802.5,5316682.12 413804.66,5316688.69 413810.23,5316687.48 413810.64,5316689.1 413810.88,5316690.02 413811.38,5316689.89 413811.74,5316691.29 413810.52,5316691.59 413811.38,5316694.88 413812.58,5316694.58 413822.64,5316692.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuT3</ogr:gmlid>
      <ogr:street>Poststraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>2071</ogr:use_id>
      <ogr:use>Hotel, Motel, Pensio</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>267</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.1">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413822.64,5316692.11 413812.58,5316694.58 413814.08,5316701.46 413814.54,5316703.57 413809.96,5316704.71 413813.26,5316714.7 413819.93,5316713.09 413818.81,5316708.36 413821.49,5316707.66 413826.07,5316706.47 413822.64,5316692.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuT4</ogr:gmlid>
      <ogr:street>Poststraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>215</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.2">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413831.6,5316729.69 413828.38,5316716.18 413823.78,5316717.29 413821.06,5316717.81 413819.93,5316713.09 413813.26,5316714.7 413816.59,5316724.76 413821.4,5316723.58 413821.84,5316725.25 413822.59,5316728.14 413823.04,5316728.02 413823.81,5316731.54 413831.6,5316729.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuT5</ogr:gmlid>
      <ogr:street>Poststraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>178</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.3">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413831.6,5316729.69 413823.81,5316731.54 413824.72,5316735.69 413825.06,5316737.25 413820.85,5316738.2 413823.16,5316746.36 413823.41,5316747.21 413833.34,5316744.46 413833.96,5316744.16 413834.44,5316743.66 413834.65,5316743.0 413834.61,5316742.31 413831.6,5316729.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuT0</ogr:gmlid>
      <ogr:street>Poststraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>158</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.4">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413780.86,5316746.68 413783.75,5316757.22 413796.46,5316753.71 413793.54,5316743.18 413780.86,5316746.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSU</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>3021</ogr:use_id>
      <ogr:use>Schule</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>144</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.5">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413780.86,5316746.68 413780.35,5316744.8 413769.95,5316747.68 413771.4,5316752.74 413770.82,5316752.9 413771.86,5316756.55 413772.44,5316756.38 413773.75,5316760.97 413784.0,5316758.13 413783.75,5316757.22 413780.86,5316746.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuST</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>150</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.6">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413754.61,5316766.28 413765.27,5316763.32 413763.83,5316758.22 413764.64,5316757.87 413763.62,5316754.11 413762.73,5316754.36 413760.71,5316747.24 413752.42,5316749.59 413752.46,5316749.72 413750.21,5316750.36 413751.09,5316753.57 413754.61,5316766.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSR</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>187</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.7">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413833.46,5316756.42 413836.75,5316768.54 413837.0,5316768.47 413843.43,5316766.73 413847.45,5316781.6 413840.66,5316782.95 413843.18,5316792.24 413856.94,5316789.6 413858.28,5316789.5 413856.36,5316779.9 413855.14,5316780.14 413851.3,5316765.91 413849.59,5316766.38 413848.13,5316761.01 413849.0,5316760.77 413848.72,5316759.67 413849.27,5316759.53 413848.74,5316757.46 413848.19,5316757.6 413848.16,5316757.47 413847.24,5316757.72 413845.96,5316753.0 413833.46,5316756.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSG</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>439</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.8">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413819.91,5316759.96 413819.98,5316760.21 413823.34,5316772.62 413824.65,5316777.45 413826.87,5316785.6 413826.99,5316786.06 413827.57,5316788.27 413829.34,5316794.92 413843.18,5316792.24 413840.66,5316782.95 413834.42,5316784.07 413830.82,5316770.88 413836.94,5316769.26 413837.19,5316769.19 413837.0,5316768.47 413836.75,5316768.54 413833.46,5316756.42 413833.42,5316756.28 413819.91,5316759.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSH</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>427</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.9">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413819.98,5316760.21 413816.48,5316761.16 413816.41,5316760.91 413810.63,5316762.49 413810.7,5316762.74 413807.21,5316763.66 413809.4,5316771.77 413808.25,5316772.1 413812.21,5316786.76 413810.97,5316787.09 413812.13,5316791.38 413817.02,5316789.99 413816.58,5316788.35 413820.32,5316787.35 413826.87,5316785.6 413824.65,5316777.45 413821.14,5316778.41 413820.88,5316777.45 413819.62,5316777.79 413818.46,5316773.49 413819.65,5316773.17 413819.77,5316773.61 413823.34,5316772.62 413819.98,5316760.21</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSJ</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>359</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.10">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413803.01,5316802.23 413805.8,5316811.21 413813.76,5316808.92 413811.23,5316800.18 413809.3,5316800.66 413803.01,5316802.23</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSK</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>77</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.11">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413809.3,5316800.66 413809.18,5316800.28 413808.7,5316800.42 413806.36,5316792.79 413806.84,5316792.65 413806.69,5316792.17 413800.48,5316794.1 413803.01,5316802.23 413809.3,5316800.66</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSL</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>52</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.12">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413827.57,5316788.27 413821.03,5316790.05 413821.08,5316790.24 413821.56,5316790.11 413826.27,5316806.23 413831.82,5316804.64 413829.16,5316794.93 413829.34,5316794.92 413827.57,5316788.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSM</ogr:gmlid>
      <ogr:street>Rosastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.13">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413335.13,5317215.89 413330.52,5317200.57 413320.63,5317203.58 413320.73,5317203.91 413321.06,5317203.8 413322.37,5317208.1 413322.03,5317208.2 413324.05,5317214.87 413324.4,5317214.77 413325.62,5317218.83 413335.13,5317215.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyzA</ogr:gmlid>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>162</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.14">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413308.39,5317151.65 413317.94,5317182.24 413327.42,5317179.24 413317.83,5317148.56 413315.61,5317149.26 413308.39,5317151.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzx</ogr:gmlid>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>319</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.15">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413335.13,5317215.89 413325.62,5317218.83 413326.71,5317222.41 413326.37,5317222.51 413328.01,5317227.9 413328.34,5317227.79 413329.39,5317231.24 413329.05,5317231.34 413329.14,5317231.63 413338.97,5317228.64 413335.13,5317215.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyzC</ogr:gmlid>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>134</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.16">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413315.61,5317149.26 413314.37,5317145.35 413313.41,5317145.62 413309.93,5317134.35 413305.5,5317135.75 413305.41,5317135.46 413302.51,5317136.37 413302.6,5317136.66 413299.18,5317137.74 413301.23,5317144.29 413302.17,5317144.0 413304.92,5317152.74 413308.39,5317151.65 413315.61,5317149.26</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzy</ogr:gmlid>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>176</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.17">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413296.05,5317186.3 413297.67,5317189.87 413304.46,5317186.84 413302.85,5317183.23 413299.46,5317184.79 413296.05,5317186.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzv</ogr:gmlid>
      <ogr:street>Schenkstraße</ogr:street>
      <ogr:house_nr>5 x</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.18">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413787.2,5317042.82 413788.99,5317042.42 413792.73,5317048.82 413789.18,5317050.89 413795.36,5317061.52 413796.48,5317060.87 413800.01,5317066.94 413805.62,5317076.58 413800.31,5317079.67 413804.65,5317087.1 413809.95,5317084.01 413810.86,5317085.59 413854.35,5317159.92 413868.18,5317151.89 413869.2,5317151.24 413860.77,5317136.78 413865.61,5317133.96 413866.18,5317134.93 413871.46,5317131.84 413879.96,5317126.86 413871.88,5317113.07 413863.41,5317118.0 413858.09,5317121.15 413858.71,5317122.2 413853.86,5317125.04 413836.48,5317095.34 413841.32,5317092.5 413841.92,5317093.54 413849.57,5317089.06 413855.72,5317085.45 413847.66,5317071.67 413841.52,5317075.26 413833.85,5317079.75 413834.45,5317080.78 413829.62,5317083.6 413825.75,5317076.96 413829.16,5317074.94 413834.94,5317071.6 413836.23,5317073.81 413840.42,5317071.37 413839.13,5317069.16 413845.0,5317065.74 413834.14,5317047.07 413813.09,5317010.85 413794.29,5317021.77 413794.26,5317021.72 413788.47,5317025.04 413788.39839999,5317025.22069999 413788.327899992,5317025.40199999 413788.258599993,5317025.58359999 413788.190599995,5317025.76579998 413788.123799997,5317025.94839998 413788.058199999,5317026.1314 413787.993799975,5317026.31479999 413787.930599977,5317026.49869999 413787.868699978,5317026.68299998 413787.80789998,5317026.86769998 413787.748399981,5317027.0528 413787.690199983,5317027.23829999 413787.633199984,5317027.42419999 413787.577399985,5317027.61039998 413787.522799987,5317027.79709998 413787.469499988,5317027.98399998 413787.417499989,5317028.1714 413787.366699991,5317028.35899999 413787.317099992,5317028.54709999 413787.268799993,5317028.73539998 413787.221799994,5317028.92399998 413787.176,5317029.113 413787.131499997,5317029.30219999 413787.088199998,5317029.49169999 413787.046199999,5317029.68149998 413787.0055,5317029.87149998 413786.965999976,5317030.0619 413786.927799977,5317030.25249999 413786.890899977,5317030.44329999 413786.855199978,5317030.63439998 413786.820799979,5317030.82569998 413786.78769998,5317031.0172 413786.755799981,5317031.20889999 413786.725299982,5317031.40089999 413786.695999982,5317031.59309999 413786.667899983,5317031.78539998 413786.641199984,5317031.97789998 413786.615699984,5317032.1706 413786.591599985,5317032.36349999 413786.568699986,5317032.55649999 413786.547099986,5317032.74969998 413786.526799987,5317032.94299998 413786.507699987,5317033.1364 413786.49,5317033.33 413786.472999988,5317033.52839999 413786.457299988,5317033.72689998 413786.442999989,5317033.92559998 413786.430099989,5317034.1243 413786.418499989,5317034.32309999 413786.40829999,5317034.52199999 413786.39939999,5317034.72089998 413786.39189999,5317034.91989998 413786.38569999,5317035.119 413786.38089999,5317035.31809999 413786.37739999,5317035.51719999 413786.375299991,5317035.71629998 413786.374599991,5317035.91539998 413786.375199991,5317036.1146 413786.37719999,5317036.31369999 413786.38049999,5317036.51279999 413786.38519999,5317036.71189998 413786.39119999,5317036.91099998 413786.39869999,5317037.11 413786.40739999,5317037.30889999 413786.417499989,5317037.50779999 413786.428999989,5317037.70659998 413786.441799989,5317037.90539998 413786.456,5317038.104 413786.471499988,5317038.30249999 413786.488399988,5317038.50099999 413786.506699987,5317038.69929998 413786.526199987,5317038.89749998 413786.547199986,5317039.0955 413786.569499986,5317039.29339999 413786.593099985,5317039.49119999 413786.618099984,5317039.68879998 413786.644399984,5317039.88619998 413786.672099983,5317040.0834 413786.701099982,5317040.28039999 413786.731499982,5317040.47719999 413786.763199981,5317040.67379998 413786.79629998,5317040.87019998 413786.830599979,5317041.0664 413786.866399978,5317041.26229999 413786.903399977,5317041.45799999 413786.941799976,5317041.65339998 413786.981499975,5317041.84859998 413787.022599999,5317042.0434 413787.064899998,5317042.23799999 413787.108599997,5317042.43229999 413787.153699996,5317042.62629998 413787.2,5317042.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>413803.94,5317042.27 413799.25,5317034.22 413806.51,5317029.96 413811.21,5317038.03 413803.94,5317042.27</gml:coordinates></gml:LinearRing></gml:innerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>413814.86,5317058.26 413812.32,5317053.9 413818.22,5317050.5 413820.75,5317054.86 413825.18,5317062.45 413819.29,5317065.87 413814.86,5317058.26</gml:coordinates></gml:LinearRing></gml:innerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvuE</ogr:gmlid>
      <ogr:street>Stefan-Meier-Straße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>3024</ogr:use_id>
      <ogr:use>Forschungsinstitut</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>4468</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.19">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413178.45,5316919.73 413181.99,5316917.87 413175.46,5316905.51 413145.94,5316921.17 413144.61,5316921.88 413145.32,5316923.21 413153.54,5316938.67 413152.73,5316939.11 413155.64,5316944.58 413154.89,5316944.97 413156.33,5316947.68 413157.08,5316947.28 413158.69,5316950.27 413159.48,5316949.84 413161.73,5316954.08 413160.93,5316954.51 413163.72,5316959.77 413162.97,5316960.17 413164.41,5316962.87 413165.12,5316962.49 413166.78,5316965.51 413167.58,5316965.09 413171.34,5316972.17 413170.44,5316972.64 413174.15,5316979.63 413175.06,5316979.15 413176.63,5316982.11 413177.7,5316984.09 413179.67,5316983.04 413180.66,5316982.52 413181.12,5316983.41 413187.62,5316979.96 413187.15,5316979.08 413189.94,5316977.59 413183.54,5316965.55 413180.97,5316960.45 413179.89,5316958.42 413180.69,5316958.0 413174.78,5316946.82 413173.97,5316947.25 413171.82,5316943.22 413172.62,5316942.79 413166.71,5316931.63 413165.91,5316932.05 413164.03,5316928.5 413162.76,5316928.24 413163.03,5316926.97 413164.29,5316927.24 413171.07,5316923.65 413170.59,5316922.74 413172.36,5316921.8 413172.69,5316922.43 413174.63,5316921.4 413174.3,5316920.78 413176.09,5316919.83 413176.56,5316920.73 413178.45,5316919.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiQ</ogr:gmlid>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1337</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.20">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413123.71,5316962.87 413125.91,5316967.0 413135.42,5316961.94 413127.15,5316946.37 413117.72,5316951.48 413117.79,5316951.61 413118.92,5316951.0 413120.75,5316954.33 413119.62,5316954.92 413119.69,5316955.04 413121.91,5316959.23 413121.98,5316959.36 413123.09,5316958.76 413124.75,5316962.15 413123.64,5316962.74 413123.71,5316962.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBr</ogr:gmlid>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>179</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.21">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413117.72,5316951.48 413127.15,5316946.37 413118.89,5316930.84 413109.48,5316935.85 413109.66,5316936.19 413109.72,5316936.31 413110.85,5316935.73 413112.52,5316938.96 413111.4,5316939.55 413111.47,5316939.69 413113.71,5316943.91 413113.78,5316944.04 413114.89,5316943.47 413116.52,5316946.67 413115.41,5316947.24 413115.48,5316947.37 413117.72,5316951.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBq</ogr:gmlid>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>180</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.22">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413087.91,5316899.06 413090.11,5316903.22 413099.59,5316898.17 413091.4,5316882.8 413081.91,5316887.88 413081.94,5316887.93 413081.99,5316888.02 413083.1,5316887.44 413084.84,5316890.64 413083.73,5316891.24 413083.79,5316891.35 413086.06,5316895.62 413086.12,5316895.73 413087.22,5316895.09 413088.97,5316898.35 413087.85,5316898.95 413087.91,5316899.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBi</ogr:gmlid>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>178</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.23">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413101.66,5316755.79 413121.38,5316743.96 413118.2,5316738.66 413099.1,5316750.15 413097.94,5316747.93 413085.44,5316754.61 413085.38,5316754.47 413082.09,5316756.1 413085.03,5316761.7 413100.44,5316753.46 413101.66,5316755.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjv</ogr:gmlid>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>248</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.24">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413215.92,5316907.51 413209.26,5316911.03 413204.35,5316913.65 413198.06,5316916.95 413197.99,5316916.83 413189.45,5316921.37 413183.93,5316924.31 413181.55,5316925.58 413180.28,5316923.19 413178.45,5316919.73 413176.56,5316920.73 413176.09,5316919.83 413174.3,5316920.78 413174.63,5316921.4 413172.69,5316922.43 413172.36,5316921.8 413170.59,5316922.74 413171.07,5316923.65 413164.29,5316927.24 413163.03,5316926.97 413162.76,5316928.24 413164.03,5316928.5 413165.91,5316932.05 413166.71,5316931.63 413172.62,5316942.79 413171.82,5316943.22 413173.97,5316947.25 413174.78,5316946.82 413180.69,5316958.0 413179.89,5316958.42 413180.97,5316960.45 413202.31,5316949.16 413221.78,5316938.78 413229.44,5316934.69 413224.28,5316923.46 413215.92,5316907.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiR</ogr:gmlid>
      <ogr:street>Stürtzelstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1786</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.25">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413322.36,5316893.44 413320.47,5316889.93 413304.94,5316898.19 413311.74,5316911.05 413326.35,5316903.29 413327.04,5316904.5 413328.04,5316903.97 413322.36,5316893.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe9</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>258</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.26">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413246.69,5316782.79 413253.76,5316796.01 413262.57,5316791.3 413255.5,5316778.08 413246.69,5316782.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe4</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>150</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.27">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413243.73,5316801.32 413236.75,5316788.15 413227.98,5316792.8 413234.96,5316805.97 413243.73,5316801.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twed</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>148</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.28">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413240.71,5316811.79 413239.97,5316810.38 413237.67,5316811.58 413238.41,5316812.99 413240.71,5316811.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twee</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>12 x</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>4</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.29">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413249.82,5316758.87 413249.5,5316758.27 413235.96,5316765.55 413241.18,5316775.25 413254.72,5316767.97 413252.15,5316763.2 413249.82,5316758.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe3</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.30">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413228.47,5316769.78 413215.16,5316776.68 413220.22,5316786.45 413233.53,5316779.56 413228.47,5316769.78</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tweb</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>165</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.31">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413219.0,5316742.42 413224.05,5316752.62 413227.67,5316750.77 413227.85,5316751.13 413241.65,5316743.92 413235.73,5316732.77 413232.66,5316734.5 413232.57,5316734.33 413225.41,5316738.41 413225.51,5316738.58 413222.38,5316740.36 413222.56,5316740.72 413219.0,5316742.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe2</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>238</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.32">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413219.0,5316742.42 413215.42,5316744.12 413215.25,5316743.78 413212.06,5316745.5 413211.97,5316745.33 413204.73,5316749.2 413204.82,5316749.37 413201.66,5316751.06 413206.44,5316760.32 413207.17,5316761.75 413220.61,5316754.82 413220.44,5316754.46 413224.05,5316752.62 413219.0,5316742.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twea</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>233</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.33">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413290.81,5316923.85 413295.48,5316921.36 413294.77,5316920.04 413305.17,5316914.55 413299.9,5316904.63 413285.05,5316912.68 413286.22,5316914.94 413290.81,5316923.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twei</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>197</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.34">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413304.37,5316860.07 413288.79,5316868.34 413294.03,5316878.22 413294.3,5316878.08 413309.61,5316869.8 413307.21,5316865.34 413304.37,5316860.07</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe8</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>197</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.35">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413288.81,5316880.91 413283.64,5316871.16 413267.93,5316879.39 413272.99,5316889.16 413287.63,5316881.53 413288.81,5316880.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tweh</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>196</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.36">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413293.21,5316839.37 413291.5,5316836.2 413276.17,5316844.48 413281.35,5316854.25 413296.84,5316846.12 413295.48,5316843.59 413293.21,5316839.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe7</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>195</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.37">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413270.97,5316847.34 413255.5,5316855.53 413257.82,5316860.03 413260.56,5316865.33 413276.15,5316857.07 413270.97,5316847.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tweg</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>194</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.38">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413274.32,5316834.86 413283.12,5316830.17 413276.35,5316817.47 413267.55,5316822.14 413274.32,5316834.86</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe6</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>144</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.39">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413264.05,5316840.34 413256.98,5316827.04 413248.19,5316831.73 413255.25,5316845.01 413264.05,5316840.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twef</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>150</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.40">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413270.54,5316797.39 413256.05,5316805.3 413261.3,5316815.04 413275.87,5316807.2 413275.17,5316805.9 413270.65,5316797.51 413270.54,5316797.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe5</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>184</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.41">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413236.68,5316809.73 413239.03,5316808.51 413238.27,5316807.06 413235.93,5316808.28 413236.68,5316809.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twec</ogr:gmlid>
      <ogr:street>Tellstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>4</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.42">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.61,5316658.91 413405.42,5316656.38 413404.81,5316655.23 413408.39,5316653.34 413403.51,5316643.6 413395.11,5316648.05 413400.61,5316658.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaW</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>110</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.43">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413305.21,5316695.69 413311.57,5316707.76 413311.72,5316707.9 413314.76,5316706.29 413316.96,5316705.13 413316.89,5316705.01 413319.64,5316703.51 413319.55,5316703.35 413319.75,5316703.25 413318.74,5316701.65 413318.52,5316701.25 413318.27,5316701.38 413312.94,5316691.59 413305.21,5316695.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcr</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>123</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.44">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413297.17,5316699.97 413302.81,5316710.6 413307.04,5316708.36 413307.65,5316709.46 413307.9,5316709.92 413311.57,5316707.76 413305.21,5316695.69 413297.17,5316699.97</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twct</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>117</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.45">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413293.92,5316712.77 413295.27,5316715.2 413295.88,5316716.37 413299.44,5316714.5 413299.14,5316713.93 413298.56,5316712.83 413302.81,5316710.6 413297.17,5316699.97 413289.24,5316704.18 413293.92,5316712.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcu</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.46">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413251.85,5316724.01 413243.58,5316728.4 413249.31,5316738.94 413253.45,5316736.78 413254.35,5316738.52 413258.5,5316736.37 413257.02,5316733.62 413251.85,5316724.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdY</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>121</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.47">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413243.58,5316728.4 413235.65,5316732.61 413235.73,5316732.77 413241.65,5316743.92 413242.25,5316745.05 413245.77,5316743.23 413244.77,5316741.27 413249.31,5316738.94 413243.58,5316728.4</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe0</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>117</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.48">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413212.7,5316715.51 413215.78,5316721.26 413227.5,5316714.97 413222.48,5316705.61 413221.81,5316705.97 413212.66,5316688.91 413213.37,5316688.53 413208.15,5316678.79 413206.9,5316679.47 413196.28,5316685.19 413204.88,5316700.96 413179.19,5316714.85 413177.31,5316711.37 413166.95,5316716.97 413179.69,5316740.58 413194.06,5316732.81 413190.97,5316727.11 413212.7,5316715.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TweE</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>3021</ogr:use_id>
      <ogr:use>Schule</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>1358</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.49">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413201.66,5316751.06 413201.59,5316750.92 413185.8,5316759.72 413185.03,5316761.86 413191.63,5316774.28 413201.04,5316769.18 413200.04,5316767.23 413199.46,5316766.12 413203.27,5316764.11 413202.4,5316762.44 413206.44,5316760.32 413201.66,5316751.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TweB</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>2081</ogr:use_id>
      <ogr:use>Gaststätte, Restaura</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>268</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.50">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413160.97,5316809.99 413165.85,5316818.31 413175.15,5316812.82 413170.61,5316804.29 413160.97,5316809.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twiw</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>106</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.51">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413160.97,5316809.99 413146.13,5316818.77 413150.95,5316827.1 413165.85,5316818.31 413160.97,5316809.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twiv</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>166</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.52">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413125.57,5316843.33 413141.45,5316835.68 413136.29,5316824.9 413135.98,5316825.05 413132.78,5316826.58 413123.87,5316830.85 413120.67,5316832.39 413120.38,5316832.53 413125.57,5316843.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiY</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>211</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.53">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413110.09,5316837.46 413111.1,5316839.57 413114.57,5316846.79 413115.72,5316849.19 413118.46,5316847.97 413118.03,5316847.07 413125.61,5316843.42 413125.57,5316843.33 413120.38,5316832.53 413110.09,5316837.46</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj7</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>141</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.54">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413351.54,5316684.12 413354.81,5316682.4 413355.73,5316684.14 413361.27,5316681.23 413369.44,5316696.81 413378.7,5316691.82 413365.16,5316666.44 413364.1,5316664.44 413362.09,5316665.51 413339.47,5316677.5 413337.14,5316678.75 413336.81,5316678.93 413342.14,5316689.03 413342.42,5316688.89 413344.93,5316687.58 413349.26,5316685.32 413351.54,5316684.12</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twck</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>572</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.55">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413098.47,5316839.27 413095.9,5316840.49 413098.34,5316845.63 413100.91,5316844.41 413098.47,5316839.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyB9</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>31 x</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.56">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413124.33,5316778.75 413137.95,5316769.94 413134.96,5316765.33 413131.88,5316760.58 413131.61,5316760.16 413131.28,5316760.38 413131.55,5316760.79 413118.42,5316769.28 413118.14,5316768.86 413118.0,5316768.95 413124.33,5316778.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjq</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>181</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.57">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413328.93,5316683.12 413334.86,5316694.29 413336.07,5316693.65 413335.44,5316692.42 413342.14,5316689.03 413336.81,5316678.93 413332.73,5316681.03 413332.75,5316681.08 413328.93,5316683.12</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcl</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.58">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413112.94,5316786.13 413124.33,5316778.75 413118.0,5316768.95 413117.83,5316769.06 413118.1,5316769.48 413107.03,5316776.69 413106.75,5316776.26 413106.6,5316776.36 413112.94,5316786.13</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjr</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>152</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.59">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413328.93,5316683.12 413324.97,5316685.22 413324.95,5316685.17 413321.24,5316687.14 413321.26,5316687.18 413320.99,5316687.32 413326.21,5316697.16 413327.14,5316696.68 413327.73,5316697.84 413331.87,5316695.72 413331.27,5316694.56 413332.99,5316693.67 413333.64,5316694.92 413334.86,5316694.29 413328.93,5316683.12</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcn</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>109</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.60">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413101.17,5316793.73 413112.94,5316786.13 413106.6,5316776.36 413106.45,5316776.46 413106.73,5316776.89 413095.63,5316784.08 413101.17,5316793.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjs</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>153</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.61">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413312.94,5316691.59 413318.27,5316701.38 413318.52,5316701.25 413323.01,5316698.86 413323.6,5316699.96 413325.48,5316698.96 413324.89,5316697.86 413326.21,5316697.16 413320.99,5316687.32 413312.94,5316691.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcp</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.62">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413098.47,5316839.27 413100.91,5316844.41 413111.1,5316839.57 413110.09,5316837.46 413108.64,5316834.44 413098.47,5316839.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBa</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>64</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.63">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413114.57,5316846.79 413101.8,5316852.83 413104.3,5316857.99 413111.98,5316854.55 413117.18,5316852.22 413115.72,5316849.19 413114.57,5316846.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBb</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>83</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.64">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413117.18,5316852.22 413111.98,5316854.55 413136.24,5316905.1 413141.42,5316902.71 413117.18,5316852.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBc</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>319</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.65">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413135.59,5316849.03 413134.05,5316845.95 413128.25,5316848.91 413129.23,5316850.96 413129.69,5316851.91 413135.59,5316849.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiZ</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>22</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.66">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413362.8,5316700.38 413366.15,5316698.58 413363.12,5316692.83 413348.32,5316700.83 413351.38,5316706.53 413352.12,5316706.14 413362.8,5316700.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcj</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>109</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.67">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413301.52,5316717.66 413302.99,5316720.52 413303.86,5316720.05 413306.95,5316718.41 413305.44,5316715.57 413301.52,5316717.66</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcv</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>14</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.68">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413262.91,5316744.58 413262.44,5316743.7 413254.23,5316748.0 413255.74,5316750.77 413260.56,5316748.26 413262.73,5316747.13 413263.94,5316746.5 413262.91,5316744.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdZ</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.69">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413254.23,5316748.0 413246.09,5316752.29 413247.54,5316755.04 413247.71,5316754.95 413255.74,5316750.77 413254.23,5316748.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twe1</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.70">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413340.88,5316705.64 413343.57,5316704.19 413338.62,5316694.98 413335.94,5316696.32 413340.88,5316705.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcm</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>32</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.71">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413335.94,5316696.32 413332.94,5316697.89 413337.95,5316707.23 413340.88,5316705.64 413335.94,5316696.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twco</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>36</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.72">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413324.68,5316711.92 413326.53,5316715.17 413329.46,5316713.52 413324.1,5316703.74 413321.08,5316705.45 413321.09,5316705.47 413324.68,5316711.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcq</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>38</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.73">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413171.18,5316653.11 413144.61,5316667.41 413153.44,5316683.82 413158.6,5316681.04 413160.9,5316685.34 413177.53,5316676.4 413175.21,5316672.08 413179.92,5316669.55 413171.18,5316653.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TweC</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>3210</ogr:use_id>
      <ogr:use>Sport</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>653</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.74">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413321.09,5316705.47 413317.7,5316707.26 413321.12,5316713.78 413324.68,5316711.92 413321.09,5316705.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcs</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.75">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413390.49,5316660.01 413391.81,5316662.52 413395.48,5316660.58 413395.91,5316661.4 413400.61,5316658.91 413395.11,5316648.05 413386.73,5316652.49 413390.49,5316660.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaX</ogr:gmlid>
      <ogr:street>Wannerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>112</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.76">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413612.01,5316880.29 413612.73,5316881.62 413609.22,5316883.51 413609.18,5316883.44 413608.59,5316883.76 413608.52,5316883.79 413608.83,5316884.38 413608.9,5316884.35 413612.87,5316891.79 413612.8,5316891.82 413613.1,5316892.37 413613.67,5316892.06 413621.26,5316905.94 413621.51,5316906.56 413622.05,5316906.19 413622.08,5316906.26 413631.05,5316901.51 413631.01,5316901.44 413631.64,5316901.2 413631.3,5316900.62 413631.23,5316900.67 413630.97,5316900.21 413631.27,5316900.04 413630.53,5316898.75 413630.23,5316898.92 413623.37,5316886.89 413624.47,5316886.26 413624.16,5316885.67 413624.08,5316885.7 413620.21,5316878.26 413620.28,5316878.22 413619.97,5316877.63 413619.38,5316877.95 413619.41,5316878.03 413615.89,5316879.93 413615.17,5316878.58 413612.01,5316880.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZ1</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>314</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.77">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413452.6,5316620.18 413470.02,5316653.08 413476.98,5316649.42 413459.54,5316616.5 413452.6,5316620.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZb</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>11 a</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>293</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.78">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413580.74,5316858.96 413571.0,5316864.13 413575.26,5316872.06 413584.99,5316866.92 413580.74,5316858.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7L</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>99</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.79">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413580.74,5316858.96 413576.39,5316850.82 413572.72,5316852.78 413572.61,5316852.56 413570.18,5316853.86 413570.3,5316854.08 413566.67,5316856.01 413568.6,5316859.62 413570.72,5316863.6 413571.0,5316864.13 413580.74,5316858.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7I</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.80">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413398.06,5316561.17 413390.47,5316559.27 413389.63,5316562.66 413397.73,5316564.7 413397.732399981,5316564.9 413397.737199981,5316565.1 413397.744299981,5316565.3 413397.753699981,5316565.49979999 413397.765499981,5316565.69949998 413397.77959998,5316565.89909998 413397.79599998,5316566.0984 413397.814699979,5316566.29759999 413397.835699979,5316566.49659999 413397.859099978,5316566.69519998 413397.884699978,5316566.89359998 413397.912699977,5316567.0917 413397.942999976,5316567.28949999 413397.975599975,5316567.48689999 413398.0104,5316567.68379998 413398.047599999,5316567.88039998 413398.087099998,5316568.0765 413398.128799997,5316568.27219999 413398.172799996,5316568.46729999 413398.219099994,5316568.66199998 413398.267599993,5316568.85609998 413398.318399992,5316569.0495 413398.371399991,5316569.24239999 413398.426699989,5316569.43469999 413398.484199988,5316569.62629998 413398.543999986,5316569.81719998 413398.605999985,5316570.0074 413398.670099983,5316570.1969 413398.736499981,5316570.38559999 413398.80509998,5316570.57359999 413398.875799978,5316570.76069998 413398.948799976,5316570.94699998 413399.023799999,5316571.1324 413399.101099997,5316571.31699999 413399.180499995,5316571.50059999 413399.26199999,5316571.68329998 413399.345599991,5316571.86499998 413399.431399989,5316572.0457 413399.519199987,5316572.22549999 413399.609199985,5316572.40419999 413399.701199982,5316572.58179998 413399.79529998,5316572.75839998 413399.891399977,5316572.93379998 413399.989499975,5316573.1081 413400.089699998,5316573.28129999 413400.191899995,5316573.45329999 413400.296099993,5316573.62409998 413400.40219999,5316573.79359998 413400.510399987,5316573.96189998 413400.620499984,5316574.129 413400.732499981,5316574.29469999 413400.846399979,5316574.45919999 413400.962299976,5316574.62229998 413401.08,5316574.784 413401.199599995,5316574.94439998 413401.321099992,5316575.1033 413401.444399989,5316575.26089999 413401.569499986,5316575.41689999 413401.696499982,5316575.57159999 413401.825199979,5316575.72469998 413401.955799976,5316575.87629998 413402.088,5316576.0264 413402.222099994,5316576.1749 413402.357799991,5316576.32189999 413402.495199987,5316576.46729999 413402.634399984,5316576.61099998 413402.77509998,5316576.75319998 413402.917599977,5316576.89369998 413403.061599998,5316577.0325 413403.207299995,5316577.1696 413403.354499991,5316577.30509999 413403.503399987,5316577.43879999 413403.653699983,5316577.57069999 413403.80559998,5316577.70089998 413403.958999976,5316577.82939998 413404.113899997,5316577.95599998 413404.270199993,5316578.0808 413404.427999989,5316578.20379999 413404.587199985,5316578.32489999 413404.747799981,5316578.44419999 413404.909799977,5316578.56159999 413405.073199998,5316578.67719998 413405.237799994,5316578.79079998 413405.40379999,5316578.90239998 413405.571099986,5316579.0122 413405.739599981,5316579.12 413405.909399977,5316579.22579999 413406.080399998,5316579.32959999 413406.252599994,5316579.43139999 413406.425999989,5316579.53129999 413406.600499985,5316579.62909998 413406.77619998,5316579.72479998 413406.952899976,5316579.81849998 413407.130799997,5316579.91019998 413407.309699992,5316579.99969997 413407.489599988,5316580.0872 413407.670499983,5316580.1726 413407.852399978,5316580.25579999 413408.035299999,5316580.33699999 413408.219099994,5316580.41599999 413408.40379999,5316580.49289999 413408.589399985,5316580.56759999 413408.77579998,5316580.64009998 413408.963099976,5316580.71049998 413409.151199996,5316580.77859998 413409.34,5316580.84459998 413409.529699987,5316580.90839998 413409.72,5316580.97 413409.907599977,5316581.0284 413410.095899998,5316581.0846 413410.284699993,5316581.1387 413410.474199988,5316581.19059999 413410.664299983,5316581.24029999 413410.854899978,5316581.28789999 413411.046099999,5316581.33329999 413411.237699994,5316581.37649999 413411.429899989,5316581.41749999 413411.622499984,5316581.45629999 413411.815499979,5316581.49289999 413412.0089,5316581.52729999 413412.202699995,5316581.55949999 413412.39689999,5316581.58949998 413412.591399985,5316581.61729998 413412.78619998,5316581.64279998 413412.981299975,5316581.66609998 413413.176599996,5316581.68719998 413413.372199991,5316581.70609998 413413.567899986,5316581.72269998 413413.763899981,5316581.73709998 413413.959999976,5316581.74929998 413414.156199996,5316581.75919998 413414.352499991,5316581.76689998 413414.548899986,5316581.77229998 413414.745299981,5316581.77549998 413414.941799976,5316581.77639998 413415.138299996,5316581.77509998 413415.334699992,5316581.77159998 413415.531099987,5316581.76579998 413415.727399982,5316581.75779998 413415.923599977,5316581.74749998 413416.119699997,5316581.73499998 413416.315599992,5316581.72029998 413416.511299987,5316581.70329998 413416.706799982,5316581.68409998 413416.902099977,5316581.66269998 413417.097199998,5316581.63899998 413417.291899993,5316581.61309998 413417.486399988,5316581.58499999 413417.680499983,5316581.55469999 413417.874199978,5316581.52219999 413418.067599998,5316581.48739999 413418.260599993,5316581.45049999 413418.453099989,5316581.41129999 413418.645199984,5316581.37 413418.836799979,5316581.32639999 413419.027799999,5316581.28069999 413419.218399994,5316581.23279999 413419.40839999,5316581.1827 413419.597699985,5316581.1305 413419.78649998,5316581.0761 413419.974699975,5316581.0195 413420.162199996,5316580.96079998 413420.349,5316580.9 413420.535099986,5316580.83699998 413420.720499982,5316580.77199998 413420.905099977,5316580.70479998 413421.089,5316580.63549998 413421.27199999,5316580.56409999 413421.454199989,5316580.49059999 413421.635599984,5316580.41509999 413421.816099979,5316580.33749999 413421.995599975,5316580.25779999 413422.174299996,5316580.1761 413422.35199999,5316580.0923 413422.528799987,5316580.0066 413422.704599982,5316579.91879998 413422.879299978,5316579.82899998 413423.053,5316579.73719998 413423.225699994,5316579.64349998 413423.39729999,5316579.54779999 413423.567699986,5316579.45009999 413423.737099981,5316579.35049999 413423.905299977,5316579.24899999 413424.072299998,5316579.1455 413424.238199994,5316579.0402 413424.40279999,5316578.93289998 413424.566199986,5316578.82379998 413424.728299982,5316578.71289998 413424.889199978,5316578.60009998 413425.048699999,5316578.48539999 413425.20699999,5316578.36899999 413425.363899991,5316578.25069999 413425.519399987,5316578.1307 413425.673599983,5316578.0089 413425.826299979,5316577.88539998 413425.977699975,5316577.76009998 413426.127599997,5316577.63309998 413426.27599999,5316577.50439999 413426.422999989,5316577.37399999 413426.568399986,5316577.24189999 413426.712399982,5316577.1082 413426.854799978,5316576.97279998 413426.995599975,5316576.83589998 413427.134899997,5316576.69729998 413427.272599993,5316576.55719999 413427.40869999,5316576.41549999 413427.543199986,5316576.27219999 413427.675999983,5316576.1274 413427.80709998,5316575.98109998 413427.936599976,5316575.83339998 413428.064399998,5316575.68409998 413428.190399995,5316575.53339999 413428.314799992,5316575.38129999 413428.437399989,5316575.22779999 413428.558199986,5316575.0729 413428.677299983,5316574.91659998 413428.79449998,5316574.75899998 413428.91,5316574.6 413429.025299999,5316574.43659999 413429.138699997,5316574.27189999 413429.250099994,5316574.1059 413429.359599991,5316573.93859998 413429.467199988,5316573.77 413429.572799986,5316573.60019998 413429.676399983,5316573.42919999 413429.77809998,5316573.25699999 413429.877699978,5316573.0836 413429.975299975,5316572.90909998 413430.070899998,5316572.73339998 413430.164399996,5316572.55669999 413430.255899994,5316572.37889999 413430.345299991,5316572.2 413430.432599989,5316572.0201 413430.517899987,5316571.83919998 413430.600999985,5316571.65739998 413430.681999983,5316571.47459999 413430.760899981,5316571.29079999 413430.837699979,5316571.1062 413430.912299977,5316570.92059998 413430.984699975,5316570.73429998 413431.055,5316570.54709999 413431.123099997,5316570.35899999 413431.189,5316570.1703 413431.252699994,5316569.98069998 413431.314199992,5316569.79039998 413431.373499991,5316569.59949998 413431.430599989,5316569.40779999 413431.485499988,5316569.21549999 413431.538099986,5316569.0226 413431.588499985,5316568.82909998 413431.636599984,5316568.63499998 413431.682499983,5316568.44039999 413431.726099982,5316568.24519999 413431.767399981,5316568.0496 413431.80649998,5316567.85349998 413431.843299979,5316567.65689998 413431.877799978,5316567.46 413431.909999977,5316567.26259999 413431.939899976,5316567.0649 413431.967599976,5316566.86689998 413431.992899975,5316566.66849998 413432.0159,5316566.46989999 413432.036599999,5316566.27099999 413432.055099999,5316566.0719 413432.071199998,5316565.87259998 413432.084899998,5316565.67309998 413432.096399998,5316565.47339999 413432.105599997,5316565.27369999 413432.112399997,5316565.0738 413432.116899997,5316564.87389998 413432.119099997,5316564.67399998 413432.119,5316564.474 413432.116599997,5316564.27399999 413432.111799997,5316564.0741 413432.104699997,5316563.87429998 413432.095299998,5316563.67459998 413432.083599998,5316563.47489999 413432.069499998,5316563.27549999 413432.053199999,5316563.0762 413432.034499999,5316562.87709998 413432.0136,5316562.67819998 413431.990299975,5316562.47959999 413431.964699976,5316562.28129999 413431.936799976,5316562.0833 413431.906599977,5316561.88559998 413431.874199978,5316561.68829998 413431.839399979,5316561.49129999 413431.80239998,5316561.29479999 413431.763099981,5316561.0988 413431.721499982,5316560.90319998 413431.677599983,5316560.70809998 413431.631499984,5316560.51349999 413431.583099985,5316560.31949999 413431.532499987,5316560.126 413431.479599988,5316559.93319998 413431.424499989,5316559.74099998 413431.367199991,5316559.54939999 413431.307699992,5316559.35849999 413431.245899994,5316559.1683 413431.182,5316558.97879998 413431.115799997,5316558.79009998 413431.047499999,5316558.60219998 413430.976999975,5316558.41509999 413430.904299977,5316558.22879999 413430.829399979,5316558.0433 413430.752499981,5316557.85879998 413430.673299983,5316557.67509998 413430.592099985,5316557.49239999 413430.508699987,5316557.31069999 413430.423299989,5316557.1299 413430.335699991,5316556.95009998 413430.246099994,5316556.77139998 413430.154399996,5316556.59369999 413430.060599998,5316556.41699999 413429.964799976,5316556.24149999 413429.866999978,5316556.0671 413429.767099981,5316555.89389998 413429.665299983,5316555.72179998 413429.561399986,5316555.55089999 413429.455599988,5316555.38119999 413429.347799991,5316555.21279999 413429.238099994,5316555.0456 413429.126399997,5316554.87969998 413429.0128,5316554.71519998 413428.897399977,5316554.55189999 413428.78,5316554.39 413428.660099983,5316554.22989999 413428.538399986,5316554.0711 413428.41479999,5316553.91379998 413428.289399993,5316553.75789998 413428.162299996,5316553.60349998 413428.033299999,5316553.45059999 413427.902599977,5316553.29919999 413427.770099981,5316553.1493 413427.635899984,5316553.0009 413427.499999987,5316552.85419998 413427.362399991,5316552.70899998 413427.223099994,5316552.56539999 413427.082099998,5316552.42339999 413426.939599976,5316552.28309999 413426.79539998,5316552.1445 413426.649599984,5316552.0075 413426.502199987,5316551.87219998 413426.353299991,5316551.73869998 413426.202799995,5316551.60689999 413426.050799999,5316551.47679999 413425.897299977,5316551.34849999 413425.742299981,5316551.22199999 413425.585899985,5316551.0973 413425.427999989,5316550.97449998 413425.268799993,5316550.85339998 413425.108099997,5316550.73429998 413424.946099976,5316550.61699998 413424.78269998,5316550.50149999 413424.617899984,5316550.38799999 413424.451899989,5316550.27649999 413424.284599993,5316550.1668 413424.116,5316550.0591 413423.946199976,5316549.95329998 413423.77519998,5316549.84959998 413423.602999985,5316549.74779998 413423.429599989,5316549.64799998 413423.255099994,5316549.55029999 413423.079399998,5316549.45459999 413422.902699977,5316549.36089999 413422.724799982,5316549.26929999 413422.545899986,5316549.1797 413422.36599999,5316549.0922 413422.185099995,5316549.0069 413422.0032,5316548.92359998 413421.820399979,5316548.84239998 413421.636599984,5316548.76339998 413421.451999989,5316548.68649998 413421.266399993,5316548.61179998 413421.08,5316548.53919999 413420.892799977,5316548.46879999 413420.704699982,5316548.40049999 413420.515899987,5316548.33449999 413420.326299992,5316548.27059999 413420.136,5316548.209 413419.944999976,5316548.1496 413419.753299981,5316548.0924 413419.560999986,5316548.0374 413419.36799999,5316547.98459997 413419.174499996,5316547.93409998 413418.980399975,5316547.88579998 413418.78569998,5316547.83979998 413418.590499985,5316547.79609998 413418.39479999,5316547.75459998 413418.198699995,5316547.71539998 413418.0021,5316547.67839998 413417.80499998,5316547.64379998 413417.607599985,5316547.61139998 413417.40989999,5316547.58139999 413417.211799995,5316547.55359999 413417.0134,5316547.52809999 413416.814699979,5316547.50489999 413416.615699984,5316547.48409999 413416.416599989,5316547.46549999 413416.217199994,5316547.44929999 413416.0176,5316547.43539999 413415.817999979,5316547.42379999 413415.618099984,5316547.41449999 413415.418199989,5316547.40749999 413415.218199994,5316547.40279999 413415.0182,5316547.40049999 413414.818199979,5316547.40049999 413414.618199984,5316547.40279999 413414.418199989,5316547.40739999 413414.218299994,5316547.41439999 413414.0184,5316547.42359999 413413.818699979,5316547.43519999 413413.619199984,5316547.44909999 413413.419799989,5316547.46529999 413413.220599994,5316547.48389999 413413.021699999,5316547.50469999 413412.822999979,5316547.52779999 413412.624599984,5316547.55329999 413412.426499989,5316547.58099999 413412.228699994,5316547.61109998 413412.031299999,5316547.64339998 413411.834299979,5316547.67799998 413411.637699984,5316547.71489998 413411.441499989,5316547.75409998 413411.245799994,5316547.79559998 413411.050599999,5316547.83929998 413410.855999978,5316547.88529998 413410.661799983,5316547.93349998 413410.468299988,5316547.98399998 413410.275299993,5316548.0368 413410.083,5316548.0917 413409.891299977,5316548.1489 413409.700299982,5316548.20829999 413409.51,5316548.27 413409.323599992,5316548.33219999 413409.137899997,5316548.39659999 413408.952999976,5316548.46299999 413408.768899981,5316548.53159999 413408.585599985,5316548.60239998 413408.40299999,5316548.67519998 413408.221399994,5316548.75009998 413408.040599999,5316548.82709998 413407.860699978,5316548.90609998 413407.681699983,5316548.98719998 413407.503699987,5316549.0704 413407.326599992,5316549.1556 413407.150599996,5316549.24289999 413406.975499975,5316549.33209999 413406.80149998,5316549.42339999 413406.628499984,5316549.51659999 413406.456599988,5316549.61189999 413406.285899993,5316549.70909998 413406.116199997,5316549.80819998 413405.947699976,5316549.90929998 413405.78039998,5316550.0124 413405.614299984,5316550.1173 413405.449399989,5316550.22419999 413405.285699993,5316550.33289999 413405.123299997,5316550.44349999 413404.962099976,5316550.55599999 413404.80229998,5316550.67029998 413404.643799984,5316550.78639998 413404.486599988,5316550.90439998 413404.330899992,5316551.0241 413404.176399996,5316551.1457 413404.023399999,5316551.26899999 413403.871899978,5316551.39399999 413403.721699982,5316551.52079999 413403.573099986,5316551.64929998 413403.425899989,5316551.77949998 413403.280199993,5316551.91139998 413403.136099997,5316552.0449 413402.993499975,5316552.1801 413402.852399978,5316552.31689999 413402.712999982,5316552.45529999 413402.575099985,5316552.59539999 413402.438899989,5316552.73699998 413402.304199992,5316552.88009998 413402.171299996,5316553.0248 413402.04,5316553.171 413401.910399977,5316553.31869999 413401.78249998,5316553.46789999 413401.656299983,5316553.61849998 413401.531899987,5316553.77059998 413401.40919999,5316553.92409998 413401.288299993,5316554.079 413401.169199996,5316554.23529999 413401.051899999,5316554.39299999 413400.936399976,5316554.55199999 413400.822699979,5316554.71219998 413400.710899982,5316554.87379998 413400.600999985,5316555.0367 413400.492899988,5316555.2008 413400.38669999,5316555.36619999 413400.282399993,5316555.53269999 413400.180099995,5316555.70049998 413400.079699998,5316555.86939998 413399.981199975,5316556.0394 413399.884699978,5316556.21059999 413399.79019998,5316556.38289999 413399.697599982,5316556.55619999 413399.607099985,5316556.73059998 413399.518499987,5316556.90599998 413399.431999989,5316557.0825 413399.347499991,5316557.25989999 413399.26499999,5316557.43829999 413399.184699995,5316557.61759998 413399.106299997,5316557.79779998 413399.030099999,5316557.97889998 413398.955899976,5316558.1609 413398.883799978,5316558.34369999 413398.813899979,5316558.52729999 413398.745999981,5316558.71169998 413398.680299983,5316558.89689998 413398.616699984,5316559.0828 413398.555199986,5316559.26949999 413398.495899987,5316559.45679999 413398.438799989,5316559.64479998 413398.38379999,5316559.83349998 413398.33099999,5316560.0228 413398.280299993,5316560.21259999 413398.231899994,5316560.40309999 413398.185599995,5316560.59399998 413398.141499996,5316560.78549998 413398.099699997,5316560.97749997 413398.06,5316561.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvY9</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>957</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.81">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413556.27,5316838.68 413547.82,5316843.19 413545.11,5316844.63 413540.53,5316836.18 413540.68,5316836.1 413538.99,5316832.99 413532.97,5316836.28 413544.71,5316857.6 413561.56,5316848.53 413556.27,5316838.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7t</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>303</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.82">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413504.62,5316756.73 413500.2,5316749.22 413497.42,5316751.1 413502.05,5316758.76 413504.62,5316756.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaG</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.83">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413359.63,5316499.28 413361.87,5316498.07 413360.41,5316495.41 413358.19,5316496.62 413359.63,5316499.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZj</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>17 x</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>8</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.84">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413556.27,5316838.68 413561.56,5316848.53 413561.85,5316849.07 413572.48,5316843.48 413562.37,5316824.56 413560.68,5316821.4 413560.58,5316821.21 413550.02,5316826.97 413550.11,5316827.14 413551.75,5316830.22 413556.27,5316838.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7u</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>303</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.85">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413600.68,5316923.82 413611.62,5316920.8 413612.55,5316918.64 413602.94,5316900.62 413594.73,5316905.01 413598.01,5316912.0 413599.18,5316911.22 413599.83,5316912.72 413600.06,5316912.62 413600.49,5316913.64 413598.03,5316915.44 413600.68,5316923.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7S</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>221</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.86">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413526.7,5316826.91 413537.21,5316821.32 413537.95,5316822.6 413541.06,5316820.84 413540.32,5316819.56 413544.79,5316817.2 413546.31,5316819.99 413544.99,5316820.71 413546.68,5316823.87 413548.01,5316823.17 413549.94,5316826.76 413550.02,5316826.97 413560.58,5316821.21 413560.5,5316821.0 413549.72,5316800.91 413520.96,5316816.3 413526.7,5316826.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7r</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>536</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.87">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413536.91,5316776.85 413527.25,5316781.83 413527.09,5316781.92 413527.18,5316782.09 413528.23,5316781.56 413529.17,5316781.07 413530.71,5316783.56 413531.89,5316785.47 413529.99,5316786.56 413527.82,5316787.81 413531.71,5316795.24 413543.39,5316788.98 413536.91,5316776.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tway</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>159</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.88">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413530.52,5316764.88 413520.75,5316769.93 413527.09,5316781.92 413527.25,5316781.83 413536.91,5316776.85 413530.52,5316764.88</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaz</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>150</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.89">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413524.16,5316753.08 413514.65,5316758.15 413519.12,5316766.78 413520.67,5316769.78 413520.75,5316769.93 413530.52,5316764.88 413530.46,5316764.77 413528.82,5316761.73 413524.16,5316753.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaC</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>145</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.90">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413524.16,5316753.08 413520.37,5316746.05 413516.61,5316743.34 413510.3,5316752.25 413512.2,5316753.36 413514.65,5316758.15 413524.16,5316753.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaE</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.91">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413516.61,5316743.34 413509.32,5316738.09 413507.89,5316740.06 413502.79,5316747.16 413510.3,5316752.25 413516.61,5316743.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaF</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>100</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.92">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413507.89,5316740.06 413494.24,5316730.45 413494.74,5316729.83 413481.1,5316720.28 413474.39,5316715.59 413473.8,5316716.4 413473.44,5316716.9 413472.86,5316717.72 413461.43,5316733.88 413459.41,5316736.94 413469.59,5316744.16 413471.67,5316741.17 413485.02,5316750.5 413487.05,5316747.74 413490.35,5316750.07 413492.55,5316746.96 413494.39,5316748.26 413497.41,5316744.03 413502.49,5316747.57 413502.79,5316747.16 413507.89,5316740.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaH</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>857</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.93">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413451.25,5316711.56 413451.29,5316711.59 413446.3,5316718.59 413456.6,5316726.33 413454.5,5316729.28 413461.43,5316733.88 413472.86,5316717.72 413473.44,5316716.9 413456.04,5316704.9 413455.45,5316705.72 413451.25,5316711.56</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaI</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>393</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.94">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413437.41,5316700.07 413437.77,5316700.67 413437.15,5316701.55 413440.93,5316704.2 413440.36,5316705.01 413442.04,5316706.2 413443.14,5316704.65 413446.13,5316706.77 413445.59,5316707.53 413447.47,5316708.82 413450.81,5316711.24 413450.9,5316711.34 413451.03,5316711.4 413451.25,5316711.56 413455.45,5316705.72 413456.04,5316704.9 413458.28,5316701.78 413458.08,5316701.63 413457.86,5316701.47 413454.38,5316699.0 413443.04,5316690.93 413434.74,5316695.39 413437.41,5316700.07</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaJ</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>243</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.95">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.68,5316682.77 413427.34,5316688.96 413431.63,5316697.06 413434.74,5316695.39 413443.04,5316690.93 413438.68,5316682.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaM</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>119</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.96">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413598.25,5316891.8 413588.24,5316897.08 413589.95,5316900.26 413593.0,5316905.94 413594.73,5316905.01 413602.94,5316900.62 413598.25,5316891.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7R</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>113</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.97">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.68,5316682.77 413434.34,5316674.61 413423.99,5316680.2 413426.35,5316684.36 413425.18,5316685.0 413427.34,5316688.96 413438.68,5316682.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaO</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>114</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.98">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413429.98,5316666.45 413419.76,5316672.13 413418.69,5316672.73 413420.77,5316676.66 413421.82,5316676.1 413423.99,5316680.2 413434.34,5316674.61 413429.98,5316666.45</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaQ</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>113</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.99">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413414.28,5316664.48 413416.36,5316668.42 413417.48,5316667.81 413419.76,5316672.13 413429.98,5316666.45 413425.63,5316658.3 413414.28,5316664.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaS</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>114</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.100">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413415.69,5316639.67 413412.69,5316638.73 413403.51,5316643.6 413408.39,5316653.34 413410.53,5316652.2 413414.26,5316659.24 413413.98,5316661.16 413410.91,5316662.75 413412.35,5316665.53 413414.28,5316664.48 413425.63,5316658.3 413415.69,5316639.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaU</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>282</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.101">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413585.56,5316840.51 413598.2,5316864.16 413598.11,5316864.21 413598.51,5316864.95 413598.98,5316864.76 413598.93,5316864.68 413609.54,5316858.97 413596.08,5316833.92 413585.08,5316839.83 413585.48,5316840.56 413585.56,5316840.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYi</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>353</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.102">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413593.87,5316883.59 413583.84,5316888.89 413585.63,5316892.22 413588.24,5316897.08 413598.25,5316891.8 413593.87,5316883.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7O</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>105</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.103">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413565.37,5316802.67 413578.09,5316826.53 413578.0,5316826.58 413578.4,5316827.33 413589.52,5316821.38 413576.05,5316796.04 413575.52,5316796.32 413575.56,5316796.4 413565.47,5316801.77 413565.43,5316801.69 413564.89,5316801.98 413565.28,5316802.71 413565.37,5316802.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYh</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>359</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.104">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413457.58,5316684.27 413491.15,5316708.18 413494.37,5316703.69 413497.73,5316706.05 413503.34,5316716.74 413504.47,5316716.14 413506.02,5316719.03 413511.81,5316715.95 413506.27,5316705.59 413485.76,5316667.15 413481.17,5316658.56 413475.42,5316661.64 413476.6,5316663.86 413475.44,5316664.42 413480.99,5316674.82 413466.89,5316682.32 413465.79,5316681.55 413461.6,5316678.61 413459.76,5316681.19 413457.58,5316684.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvY8</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>7 a</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1049</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.105">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413593.87,5316883.59 413589.48,5316875.39 413579.75,5316880.54 413583.84,5316888.89 413593.87,5316883.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7N</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.106">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413447.96,5316663.79 413449.34,5316666.4 413450.0,5316666.04 413454.82,5316675.13 413463.02,5316670.79 413461.74,5316668.37 413462.09,5316668.18 413461.93,5316667.88 413461.4,5316668.16 413459.9,5316665.33 413460.43,5316665.05 413460.27,5316664.75 413459.92,5316664.94 413455.12,5316655.88 413455.47,5316655.69 413455.31,5316655.39 413454.78,5316655.67 413453.28,5316652.84 413453.82,5316652.56 413453.66,5316652.27 413453.31,5316652.46 413450.88,5316647.88 413451.23,5316647.7 413451.07,5316647.4 413450.54,5316647.68 413449.04,5316644.85 413449.57,5316644.57 413449.41,5316644.27 413449.06,5316644.46 413444.24,5316635.37 413444.59,5316635.18 413444.43,5316634.88 413443.9,5316635.16 413442.41,5316632.34 413442.94,5316632.06 413442.78,5316631.76 413442.43,5316631.95 413441.15,5316629.53 413432.95,5316633.88 413437.77,5316642.96 413437.1,5316643.31 413438.48,5316645.91 413439.14,5316645.56 413448.63,5316663.44 413447.96,5316663.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYM</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>436</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.107">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413583.55,5316899.54 413578.78,5316902.06 413581.72,5316906.74 413583.05,5316908.85 413587.14,5316906.74 413585.14,5316902.73 413583.55,5316899.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7P</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>40</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.108">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413589.95,5316900.26 413588.24,5316897.08 413583.55,5316899.54 413585.14,5316902.73 413589.95,5316900.26</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7Q</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.109">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413561.72,5316863.81 413557.86,5316856.67 413557.79,5316856.54 413552.17,5316859.56 413552.24,5316859.67 413556.52,5316866.51 413556.82,5316866.35 413561.72,5316863.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7J</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>50</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.110">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413561.72,5316863.81 413556.82,5316866.35 413558.2,5316868.88 413563.1,5316866.35 413561.75,5316863.8 413561.72,5316863.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7K</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.111">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413519.12,5316780.03 413521.37,5316778.93 413518.72,5316773.91 413516.46,5316775.28 413519.12,5316780.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaA</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>14</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.112">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413509.49,5316760.9 413504.76,5316763.43 413509.61,5316771.52 413514.14,5316769.26 413509.49,5316760.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaB</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>49</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.113">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413509.49,5316760.9 413506.13,5316755.53 413504.62,5316756.73 413502.05,5316758.76 413504.76,5316763.43 413509.49,5316760.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaD</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.114">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413429.17,5316721.73 413432.67,5316728.44 413439.19,5316724.55 413435.24,5316717.95 413435.16,5316718.0 413429.17,5316721.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaL</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>56</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.115">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413435.16,5316718.0 413427.4,5316705.52 413437.41,5316700.07 413434.74,5316695.39 413431.63,5316697.06 413426.71,5316699.71 413423.65,5316701.35 413415.7,5316705.54 413416.21,5316706.34 413418.4,5316709.8 413420.2,5316712.65 413423.02,5316717.09 413426.88,5316723.16 413429.17,5316721.73 413435.16,5316718.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaK</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>264</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.116">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413426.71,5316699.71 413422.5,5316691.6 413419.37,5316693.31 413423.65,5316701.35 413426.71,5316699.71</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaN</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>32</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.117">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413416.01,5316686.99 413421.69,5316683.93 413420.65,5316682.0 413414.98,5316685.05 413416.01,5316686.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaP</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>14</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.118">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413415.37,5316674.57 413414.25,5316672.39 413409.58,5316674.89 413410.78,5316677.13 413415.37,5316674.57</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaT</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>13</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.119">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413345.84,5316478.99 413346.92,5316481.02 413346.07,5316481.47 413349.02,5316487.04 413349.87,5316486.59 413355.32,5316496.88 413371.66,5316488.21 413377.63,5316499.47 413382.05,5316497.14 413382.52,5316498.03 413393.11,5316492.41 413377.21,5316462.38 413388.86,5316456.19 413380.18,5316439.81 413380.03,5316439.88 413373.63,5316430.16 413373.76,5316430.07 413366.26,5316418.64 413366.13,5316418.73 413363.96,5316415.43 413353.0,5316422.61 413339.24,5316426.08 413331.82,5316427.95 413331.86,5316428.09 413332.3,5316427.97 413333.66,5316433.37 413332.95,5316433.55 413336.54,5316447.77 413337.24,5316447.6 413338.64,5316453.13 413338.18,5316453.25 413338.31,5316453.75 413338.77,5316453.64 413340.16,5316459.2 413339.47,5316459.37 413343.3,5316474.48 413345.72,5316479.05 413345.84,5316478.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZi</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>3030</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.120">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413415.37,5316674.57 413410.78,5316677.13 413413.55,5316682.35 413419.48,5316679.28 413416.62,5316673.88 413415.37,5316674.57</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaR</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>40</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.121">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413403.08,5316662.68 413405.74,5316667.68 413406.35,5316668.81 413412.35,5316665.53 413410.91,5316662.75 413407.79,5316664.37 413406.02,5316661.09 413403.08,5316662.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaV</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>34</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.122">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413547.13,5316523.65 413546.86,5316523.84 413551.94,5316531.25 413552.9,5316532.98 413553.49,5316534.92 413553.61,5316536.95 413553.29,5316538.99 413551.47,5316542.73 413545.76,5316548.12 413545.03,5316547.38 413534.78,5316556.91 413532.56,5316566.5 413537.66,5316576.11 413536.84,5316576.57 413548.48,5316598.64 413548.654199983,5316598.68519998 413548.827799979,5316598.73219998 413549.001,5316598.78089998 413549.173699996,5316598.83139998 413549.345899991,5316598.88359998 413549.517499987,5316598.93759998 413549.688599983,5316598.99339997 413549.859099978,5316599.0508 413550.029,5316599.11 413550.198499995,5316599.171 413550.367299991,5316599.23359999 413550.535499986,5316599.29799999 413550.702999982,5316599.36409999 413550.869799978,5316599.43199999 413551.035899999,5316599.50139999 413551.201399995,5316599.57259999 413551.366099991,5316599.64549998 413551.53,5316599.72 413551.698499982,5316599.79949998 413551.866099978,5316599.88079998 413552.032899999,5316599.96389998 413552.198799995,5316600.0488 413552.363699991,5316600.1354 413552.527699987,5316600.22379999 413552.690799983,5316600.31399999 413552.852799978,5316600.40589999 413553.0139,5316600.49959999 413553.174,5316600.59489999 413553.333,5316600.692 413553.491099988,5316600.79079998 413553.648099984,5316600.89129998 413553.80399998,5316600.99349997 413553.958799976,5316601.0974 413554.112499997,5316601.2029 413554.26499999,5316601.31 413554.41639999,5316601.41879999 413554.566599986,5316601.52919999 413554.715599982,5316601.64119998 413554.863399978,5316601.75479998 413555.01,5316601.87 413555.153299996,5316601.99559997 413555.29499999,5316602.123 413555.435199989,5316602.25199999 413555.573799986,5316602.38279999 413555.710799982,5316602.51519999 413555.846199979,5316602.64939998 413555.979899975,5316602.78509998 413556.112,5316602.92249998 413556.242299994,5316603.0614 413556.371,5316603.202 413556.497999987,5316603.34409999 413556.623199984,5316603.48779999 413556.746599981,5316603.63299998 413556.868299978,5316603.77969998 413556.988199975,5316603.92789998 413557.106299997,5316604.0775 413557.222499994,5316604.22849999 413557.336899991,5316604.38099999 413557.449399989,5316604.53479999 413557.56,5316604.69 413557.674499983,5316604.85299998 413557.78679998,5316605.0174 413557.897199977,5316605.1833 413558.0054,5316605.35049999 413558.111499997,5316605.51899999 413558.215499995,5316605.68889998 413558.317399992,5316605.86 413558.417099989,5316606.0325 413558.514699987,5316606.20609999 413558.61,5316606.381 413558.703099982,5316606.55699999 413558.79409998,5316606.73419998 413558.882799978,5316606.91259998 413558.969199975,5316607.092 413559.053399999,5316607.27249999 413559.135299997,5316607.45399999 413559.214899995,5316607.63659998 413559.292299993,5316607.82009998 413559.367299991,5316608.0046 413559.44,5316608.19 413559.505199987,5316608.36569999 413559.568199986,5316608.54219999 413559.629099984,5316608.71939998 413559.687899983,5316608.89729998 413559.744399981,5316609.076 413559.79889998,5316609.25529999 413559.851099978,5316609.43519999 413559.901099977,5316609.61579998 413559.949,5316609.797 413559.994699975,5316609.97879998 413560.038099999,5316610.1611 413560.079399998,5316610.34389999 413560.118399997,5316610.52719999 413560.155199996,5316610.71089998 413560.189699995,5316610.89509998 413560.222099994,5316611.0797 413560.252199994,5316611.26469999 413560.28,5316611.45 413560.303299992,5316611.64669998 413560.324399992,5316611.84359998 413560.343099991,5316612.0407 413560.359599991,5316612.23809999 413560.373699991,5316612.43559999 413560.38559999,5316612.63329998 413560.39519999,5316612.83109998 413560.40239999,5316613.029 413560.40739999,5316613.22699999 413560.41,5316613.425 413560.41029999,5316613.62289998 413560.40839999,5316613.82069998 413560.40409999,5316614.0185 413560.39759999,5316614.21629999 413560.38869999,5316614.41399999 413560.37759999,5316614.61149998 413560.364099991,5316614.80889998 413560.348399991,5316615.0061 413560.330299992,5316615.20319999 413560.31,5316615.4 413560.287299993,5316615.59219998 413560.262399993,5316615.78419998 413560.235299994,5316615.97589998 413560.206199995,5316616.1672 413560.174799996,5316616.35819999 413560.141399996,5316616.54889999 413560.105699997,5316616.73909998 413560.068,5316616.929 413560.028099999,5316617.1184 413559.986199975,5316617.30729999 413559.941999976,5316617.49579999 413559.895799977,5316617.68369998 413559.847499979,5316617.87119998 413559.79709998,5316618.058 413559.744599981,5316618.24429999 413559.69,5316618.43 413561.99,5316623.04 413557.56,5316625.27 413552.23,5316614.66 413552.66,5316614.44 413549.25,5316607.73 413548.84,5316607.95 413547.61,5316605.51 413545.67,5316606.49 413565.79,5316644.97 413567.71,5316643.94 413567.5,5316643.46 413573.21,5316640.45 413571.35,5316636.83 413572.23,5316636.36 413572.40459999,5316636.27049999 413572.579599985,5316636.1818 413572.754999981,5316636.0939 413572.930799976,5316636.0068 413573.107,5316635.92049998 413573.283599993,5316635.83499998 413573.460599988,5316635.75029998 413573.637999984,5316635.66639998 413573.815699979,5316635.58329999 413573.993799975,5316635.50099999 413574.172299996,5316635.41949999 413574.351199991,5316635.33889999 413574.530399987,5316635.25899999 413574.71,5316635.18 413574.889899978,5316635.1018 413575.070199998,5316635.0244 413575.250899994,5316634.94779998 413575.431899989,5316634.87209998 413575.613199985,5316634.79709998 413575.79489998,5316634.72299998 413575.976899975,5316634.64969998 413576.159199996,5316634.57729999 413576.341899991,5316634.50559999 413576.524899987,5316634.43479999 413576.708199982,5316634.36489999 413576.891799977,5316634.29579999 413577.075799998,5316634.22749999 413577.26,5316634.16 413577.441499989,5316634.0833 413577.623399984,5316634.0075 413577.80569998,5316633.93259998 413577.988299975,5316633.85859998 413578.171299996,5316633.78549998 413578.354599991,5316633.71329998 413578.538399986,5316633.64209998 413578.722399982,5316633.57169999 413578.906799977,5316633.50229999 413579.091599998,5316633.43379999 413579.276699993,5316633.36619999 413579.462099988,5316633.29949999 413579.647899984,5316633.23379999 413579.834,5316633.169 413580.020399999,5316633.1051 413580.20699999,5316633.0422 413580.39399999,5316632.98019997 413580.581299985,5316632.91909998 413580.768899981,5316632.85899998 413580.956799976,5316632.79979998 413581.144899996,5316632.74149998 413581.333399992,5316632.68419998 413581.522199987,5316632.62779998 413581.711199982,5316632.57239999 413581.900499977,5316632.51779999 413582.090099998,5316632.46429999 413582.279899993,5316632.41169999 413582.47,5316632.36 413582.658099983,5316632.31189999 413582.846399979,5316632.26449999 413583.035,5316632.21799999 413583.223699994,5316632.1723 413583.41259999,5316632.1274 413583.601699985,5316632.0833 413583.791,5316632.04 413583.980299975,5316631.99759998 413584.169799996,5316631.95589998 413584.359499991,5316631.91509998 413584.549399986,5316631.87509998 413584.739399981,5316631.83589998 413584.929599977,5316631.79759998 413585.12,5316631.76 413585.62,5316633.88 413599.79,5316626.28 413600.18,5316627.01 413602.01,5316626.03 413602.146799996,5316625.90599998 413602.281899993,5316625.78019998 413602.41539999,5316625.65259998 413602.547099986,5316625.52319999 413602.677,5316625.392 413602.80529998,5316625.25899999 413602.931699976,5316625.1243 413603.056299999,5316624.98779998 413603.179099995,5316624.84969998 413603.3,5316624.71 413603.41469999,5316624.56549999 413603.526899987,5316624.41919999 413603.636599984,5316624.27089999 413603.743799981,5316624.1208 413603.848499979,5316623.96899998 413603.950599976,5316623.81529998 413604.05,5316623.66 413604.146699996,5316623.50319999 413604.240699994,5316623.34469999 413604.332099992,5316623.1847 413604.420699989,5316623.0232 413604.506599987,5316622.86019998 413604.589699985,5316622.69579998 413604.67,5316622.53 413604.739299981,5316622.36299999 413604.80569998,5316622.19489999 413604.869299978,5316622.0257 413604.930099976,5316621.85539998 413604.987899975,5316621.68419998 413605.042799999,5316621.51189999 413605.094799998,5316621.33879999 413605.143899996,5316621.1648 413605.19,5316620.99 413605.233099994,5316620.81459998 413605.273199993,5316620.63839998 413605.310399992,5316620.46159999 413605.344499991,5316620.28419999 413605.37569999,5316620.1063 413605.40379999,5316619.92779998 413605.428899989,5316619.74889998 413605.450999989,5316619.56959999 413605.47,5316619.39 413605.485499988,5316619.1957 413605.497299987,5316619.0011 413605.505399987,5316618.80639998 413605.509899987,5316618.61149998 413605.510699987,5316618.41649999 413605.507799987,5316618.22159999 413605.501199987,5316618.0268 413605.490899988,5316617.83209998 413605.476999988,5316617.63769998 413605.459499988,5316617.44359999 413605.438199989,5316617.24979999 413605.41339999,5316617.0565 413605.38479999,5316616.86359998 413605.352699991,5316616.67139998 413605.316899992,5316616.47969999 413605.277499993,5316616.28879999 413605.234599994,5316616.0987 413605.188,5316615.90939998 413605.138,5316615.721 413605.084399998,5316615.53349999 413605.027199999,5316615.34709999 413604.966499976,5316615.1617 413604.902299977,5316614.97759998 413604.834699979,5316614.79469998 413604.763599981,5316614.61309998 413604.689099983,5316614.43289999 413604.611199985,5316614.25409999 413604.529999987,5316614.0768 413604.445399989,5316613.90109998 413604.357599991,5316613.72699998 413604.266499993,5316613.55459999 413604.172099996,5316613.38389999 413604.074499998,5316613.2151 413603.973799975,5316613.0481 413603.869899978,5316612.88299998 413603.762999981,5316612.72 413603.652999984,5316612.55899999 413603.54,5316612.4 413592.69,5316592.11 413592.559599986,5316591.95849998 413592.426999989,5316591.80889998 413592.292099993,5316591.66139998 413592.155099996,5316591.51589999 413592.0158,5316591.37249999 413591.874499978,5316591.23119999 413591.731,5316591.092 413591.585399985,5316590.95489998 413591.437699989,5316590.82009998 413591.288099993,5316590.68749998 413591.136399997,5316590.55719999 413590.982899975,5316590.42909999 413590.827399979,5316590.30339999 413590.67,5316590.18 413590.528399987,5316590.0756 413590.38539999,5316589.97299998 413590.241199994,5316589.87229998 413590.095699998,5316589.77329998 413589.948999976,5316589.67619998 413589.801,5316589.581 413589.651999984,5316589.48769999 413589.501799987,5316589.39639999 413589.350499991,5316589.30689999 413589.198099995,5316589.21929999 413589.044599999,5316589.1337 413588.89,5316589.05 413573.4,5316560.29 413572.42,5316558.13 413569.32,5316549.89 413569.21,5316548.31 413569.41,5316546.89 413569.99,5316545.59 413572.02,5316543.61 413576.3,5316541.31 413579.38,5316539.69 413579.547699986,5316539.58089999 413579.719999982,5316539.47919999 413579.896699977,5316539.38509999 413580.077299998,5316539.29899999 413580.261499993,5316539.22089999 413580.449,5316539.151 413580.639099984,5316539.0895 413580.831799979,5316539.0364 413581.026499999,5316538.99179997 413581.223099994,5316538.95579998 413581.420999989,5316538.92849998 413581.62,5316538.91 413581.816399979,5316538.90149998 413582.013,5316538.90149998 413582.209499995,5316538.91 413582.40529999,5316538.92689998 413582.600299985,5316538.95229998 413582.794,5316538.986 413582.986199975,5316539.0281 413583.176299996,5316539.0784 413583.364099991,5316539.1368 413583.549199986,5316539.20339999 413583.731299982,5316539.27779999 413583.91,5316539.36 413584.081099998,5316539.44449999 413584.248599994,5316539.53589999 413584.41219999,5316539.63419998 413584.571599986,5316539.73909998 413584.726499982,5316539.85049998 413584.876699978,5316539.96819998 413585.022,5316540.092 413585.162,5316540.22169999 413585.296599992,5316540.35699999 413585.425499989,5316540.49769999 413585.548499986,5316540.64359998 413585.665399983,5316540.79449998 413585.77589998,5316540.95 413585.88,5316541.11 413590.01,5316547.35 413596.07,5316557.16 413596.49,5316557.04 413596.40469999,5316556.88339998 413596.334099992,5316556.71959998 413596.278799993,5316556.55009999 413596.239199994,5316556.37619999 413596.215699995,5316556.19939999 413596.208399995,5316556.0212 413596.217499994,5316555.84309998 413596.242799994,5316555.66649998 413596.284,5316555.493 413596.340899991,5316555.32389999 413596.41319999,5316555.1607 413596.500099987,5316555.0048 413596.600899985,5316554.85749998 413596.714799982,5316554.72 413596.840699979,5316554.59349998 413596.977699975,5316554.47909999 413597.124499997,5316554.37769999 413597.28,5316554.29 413597.451599989,5316554.21349999 413597.629899984,5316554.1539 413597.813099979,5316554.1119 413597.999399975,5316554.0877 413598.187299995,5316554.0817 413598.374799991,5316554.0938 413598.560299986,5316554.124 413598.742,5316554.172 413598.918299977,5316554.23729999 413599.087399998,5316554.31939999 413599.247799994,5316554.41739999 413599.39789999,5316554.53039999 413599.536499986,5316554.65749998 413599.662099983,5316554.79729998 413599.77359998,5316554.94859998 413599.87,5316555.11 413600.18,5316554.78 413594.92,5316544.77 413590.42,5316536.32 413590.18,5316535.3 413590.4,5316534.22 413590.94,5316534.37 413590.74,5316533.7 413591.52,5316532.98 413592.37,5316532.52 413588.81,5316522.31 413588.632199984,5316522.39869999 413588.452499989,5316522.48349999 413588.27099999,5316522.56439999 413588.087899998,5316522.64149998 413587.903199977,5316522.71469998 413587.716899982,5316522.78399998 413587.529299987,5316522.84929998 413587.340199991,5316522.91059998 413587.15,5316522.96789998 413586.958499976,5316523.0211 413586.765999981,5316523.0702 413586.572499986,5316523.1152 413586.37799999,5316523.1562 413586.182799995,5316523.193 413585.986799975,5316523.22559999 413585.79009998,5316523.25409999 413585.592899985,5316523.27829999 413585.39519999,5316523.29839999 413585.197099995,5316523.31429999 413584.998799975,5316523.32599999 413584.80019998,5316523.33349999 413584.601599985,5316523.33669999 413584.40289999,5316523.33579999 413584.204199995,5316523.33059999 413584.0057,5316523.32119999 413583.80749998,5316523.30759999 413583.609599985,5316523.28979999 413583.41209999,5316523.26779999 413583.215199995,5316523.24159999 413583.018799999,5316523.2112 413582.823099979,5316523.1767 413582.628199984,5316523.138 413582.434199989,5316523.0952 413582.241099994,5316523.0483 413582.049,5316522.99729997 413581.858099978,5316522.94219998 413581.668399983,5316522.88309998 413581.48,5316522.82 413581.292899993,5316522.75289998 413581.107399997,5316522.68189998 413580.923299977,5316522.60689999 413580.740999981,5316522.52799999 413580.560299986,5316522.44529999 413580.38139999,5316522.35869999 413580.204399995,5316522.26839999 413580.029399999,5316522.1743 413579.856399978,5316522.0765 413579.685499983,5316521.97509998 413579.516799987,5316521.87009998 413579.350299991,5316521.76149998 413579.186199995,5316521.64939998 413579.024599999,5316521.53389999 413578.865399978,5316521.41489999 413578.708799982,5316521.29259999 413578.554799986,5316521.167 413578.40349999,5316521.0382 413578.25499999,5316520.90619998 413578.109299997,5316520.77099998 413577.966499976,5316520.63279998 413577.826699979,5316520.49159999 413577.689899983,5316520.34749999 413577.556199986,5316520.20039999 413577.425699989,5316520.0506 413577.298299992,5316519.89809998 413577.174299996,5316519.74279998 413577.053499999,5316519.58499999 413576.936099976,5316519.42459999 413576.822199979,5316519.26179999 413576.711799982,5316519.0966 413576.604799985,5316518.92909998 413576.501499987,5316518.75939998 413576.40179999,5316518.58749999 413576.305699992,5316518.41359999 413576.213399995,5316518.23759999 413576.124799997,5316518.0597 413576.04,5316517.88 413575.958899976,5316517.69759998 413575.881799978,5316517.51349999 413575.80859998,5316517.32779999 413575.739399981,5316517.1406 413575.674199983,5316516.95199998 413575.612999985,5316516.76199998 413575.555899986,5316516.57069999 413575.502899987,5316516.37829999 413575.453999989,5316516.1848 413575.40929999,5316515.99029997 413575.368699991,5316515.79479998 413575.332299992,5316515.59859999 413575.300099992,5316515.40159999 413575.272099993,5316515.204 413575.248299994,5316515.0058 413575.228799994,5316514.80719998 413575.213499995,5316514.60819998 413575.202499995,5316514.40889999 413575.195699995,5316514.20939999 413575.193199995,5316514.0098 413575.194899995,5316513.81019998 413575.200899995,5316513.61069998 413575.211099995,5316513.41139999 413575.225599994,5316513.21229999 413575.244399994,5316513.0136 413575.267299993,5316512.81529998 413575.294499993,5316512.61759998 413575.32599999,5316512.42049999 413575.361599991,5316512.22409999 413575.40139999,5316512.0285 413575.445299989,5316511.83379998 413575.493399988,5316511.64009998 413575.545699986,5316511.44749999 413575.602,5316511.256 413575.662399983,5316511.0658 413575.726899982,5316510.87689998 413575.79529998,5316510.68939998 413575.867799978,5316510.50339999 413575.944199976,5316510.31899999 413576.024499999,5316510.1363 413576.108699997,5316509.95529998 413576.196699995,5316509.77619998 413576.288599993,5316509.59899998 413576.38419999,5316509.42379999 413576.483499988,5316509.25069999 413576.586499985,5316509.0797 413576.693099983,5316508.91099998 413576.80329998,5316508.74449998 413576.917099977,5316508.58049999 413577.034299999,5316508.41889999 413577.154899996,5316508.25989999 413577.278899993,5316508.1035 413577.40619999,5316507.94979998 413577.536699986,5316507.79879998 413577.670499983,5316507.65059998 413577.80729998,5316507.50539999 413577.947299976,5316507.36299999 413578.090199998,5316507.22369999 413578.236099994,5316507.0875 413578.38489999,5316506.95439998 413578.536399986,5316506.82459998 413578.690699983,5316506.69799998 413578.847699979,5316506.57469999 413579.0073,5316506.45479999 413579.169299996,5316506.33829999 413579.333899992,5316506.22529999 413579.500799987,5316506.1159 413579.67,5316506.01 413578.87,5316504.47 413578.689299983,5316504.55059999 413578.506899987,5316504.62729998 413578.322899992,5316504.70009998 413578.137399997,5316504.76899998 413577.950499976,5316504.83389998 413577.762199981,5316504.89479998 413577.572699986,5316504.95179998 413577.38199999,5316505.0047 413577.190199995,5316505.0535 413576.997499975,5316505.0983 413576.80379998,5316505.1389 413576.609399985,5316505.1754 413576.41419999,5316505.20779999 413576.218299994,5316505.23609999 413576.021899999,5316505.26019999 413575.824999979,5316505.28009999 413575.627799984,5316505.29579999 413575.430199989,5316505.30739999 413575.232499994,5316505.31469999 413575.034599999,5316505.31779999 413574.836799979,5316505.31679999 413574.638899984,5316505.31149999 413574.441299989,5316505.30209999 413574.243899994,5316505.28839999 413574.046799999,5316505.27059999 413573.850199979,5316505.24859999 413573.653999983,5316505.22239999 413573.458499988,5316505.19209999 413573.263599993,5316505.1576 413573.069599998,5316505.119 413572.876299978,5316505.0763 413572.684099983,5316505.0295 413572.492899988,5316504.97859997 413572.302799992,5316504.92369998 413572.113899997,5316504.86469998 413571.926299977,5316504.80179998 413571.74,5316504.735 413571.555199986,5316504.66419998 413571.371899991,5316504.58949998 413571.190299995,5316504.51079999 413571.0104,5316504.42839999 413570.832299979,5316504.34209999 413570.655999983,5316504.25209999 413570.481699988,5316504.1583 413570.309499992,5316504.0609 413570.139299996,5316503.95979998 413569.971299975,5316503.85509998 413569.80559998,5316503.74689998 413569.642199984,5316503.63519998 413569.481299988,5316503.52009999 413569.322799992,5316503.40159999 413569.166799996,5316503.27969999 413569.0135,5316503.1546 413568.862899978,5316503.0262 413568.714999982,5316502.89459998 413568.569999986,5316502.76 413568.427799989,5316502.62229998 413568.288599993,5316502.48159999 413568.152399996,5316502.33799999 413568.0193,5316502.1915 413567.889399978,5316502.0423 413567.762599981,5316501.89029998 413567.639099984,5316501.73559998 413567.518899987,5316501.57839999 413567.40209999,5316501.41869999 413567.288599993,5316501.25649999 413567.178699996,5316501.0919 413567.072299998,5316500.92499998 413566.969399975,5316500.75599998 413566.870099978,5316500.58469999 413566.77449998,5316500.41149999 413566.682599983,5316500.23619999 413566.594499985,5316500.059 413566.51,5316499.88 413566.428799989,5316499.69759998 413566.351499991,5316499.51339999 413566.278299993,5316499.32759999 413566.209,5316499.1403 413566.143799996,5316498.95149998 413566.082599998,5316498.76139998 413566.025599999,5316498.57009999 413565.972599975,5316498.37749999 413565.923799977,5316498.18389999 413565.879099978,5316497.98919997 413565.838599979,5316497.79369998 413565.80229998,5316497.59729998 413565.770199981,5316497.40019999 413565.742399981,5316497.2024 413565.718799982,5316497.0041 413565.699399982,5316496.80529998 413565.684399983,5316496.60619998 413565.673499983,5316496.40679999 413565.666999983,5316496.20719999 413565.664699983,5316496.0075 413565.666699983,5316495.80779998 413565.672999983,5316495.60819998 413565.683599983,5316495.40879999 413565.698399982,5316495.2096 413565.717499982,5316495.0108 413565.740899981,5316494.81249998 413565.768499981,5316494.61469998 413565.80029998,5316494.41749999 413565.836299979,5316494.22109999 413565.876599978,5316494.0255 413565.920999977,5316493.83079998 413565.969599976,5316493.63709998 413566.022299999,5316493.44449999 413566.079,5316493.253 413566.139799996,5316493.0627 413566.204799995,5316492.87389998 413566.273799993,5316492.68639998 413566.346899991,5316492.50049999 413566.423899989,5316492.31619999 413566.504799987,5316492.1336 413566.589599985,5316491.95269998 413566.678299983,5316491.77369998 413566.77079998,5316491.59669998 413566.867099978,5316491.42169999 413566.966999976,5316491.24879999 413567.070699998,5316491.078 413567.178,5316490.90959998 413567.288899993,5316490.74339998 413567.40329999,5316490.57969999 413567.521299987,5316490.41849999 413567.642599984,5316490.25979999 413567.767299981,5316490.1038 413567.895299977,5316489.95039998 413568.026599999,5316489.79989998 413568.161099996,5316489.65219998 413568.298699992,5316489.50739999 413568.439399989,5316489.36559999 413568.582999985,5316489.22679999 413568.729699982,5316489.0912 413568.879199978,5316488.95869998 413569.031499999,5316488.82949998 413569.186499995,5316488.70349998 413569.344199991,5316488.58089999 413569.504499987,5316488.46169999 413569.667299983,5316488.34599999 413569.832499979,5316488.23379999 413570.0002,5316488.1251 413570.17,5316488.02 413569.93,5316487.51 413572.5,5316486.15 413571.83,5316484.95 413566.38,5316487.78 413562.77,5316480.85 413567.78,5316478.21 413566.38,5316475.77 413561.44,5316478.39 413557.88,5316471.71 413554.36,5316474.34 413554.191399995,5316474.43579999 413554.0193,5316474.52499999 413553.843699979,5316474.60729998 413553.665099983,5316474.68269998 413553.483599988,5316474.75089998 413553.299499992,5316474.81199998 413553.113199997,5316474.86569998 413552.924999977,5316474.91209998 413552.735,5316474.951 413552.543799986,5316474.98229998 413552.351499991,5316475.0062 413552.158399996,5316475.0224 413551.964799976,5316475.0311 413551.770999981,5316475.0321 413551.577299985,5316475.0255 413551.38409999,5316475.0113 413551.191499995,5316474.98939998 413551.0,5316474.96 413550.814099979,5316474.92339998 413550.629799984,5316474.87969998 413550.447199989,5316474.82889998 413550.266799993,5316474.77129998 413550.088599998,5316474.70679998 413549.913099977,5316474.63559998 413549.740399981,5316474.55769999 413549.570799986,5316474.47319999 413549.40449999,5316474.38239999 413549.241799994,5316474.28529999 413549.083,5316474.182 413548.928199977,5316474.0727 413548.77769998,5316473.95769998 413548.631799984,5316473.83689998 413548.490499988,5316473.71069998 413548.354099991,5316473.57919999 413548.222899994,5316473.44259999 413548.097,5316473.30099999 413547.976599975,5316473.1548 413547.861799978,5316473.0041 413547.752899981,5316472.84909998 413547.65,5316472.69 413541.13,5316462.06 413535.41,5316465.06 413540.74,5316474.5 413568.86,5316519.51 413571.4,5316523.72 413571.487899988,5316523.87649998 413571.569599986,5316524.0364 413571.644799984,5316524.19939999 413571.713499982,5316524.36529999 413571.77559998,5316524.53369999 413571.830899979,5316524.70449998 413571.879399978,5316524.87739998 413571.921,5316525.052 413571.955599976,5316525.22809999 413571.983299975,5316525.40549999 413572.0038,5316525.58379999 413572.0173,5316525.76289998 413572.023699999,5316525.94229998 413572.022899999,5316526.1218 413572.015,5316526.30109999 413572.0,5316526.48 413571.981999975,5316526.64819998 413571.957699976,5316526.81559998 413571.926999977,5316526.98189997 413571.89,5316527.147 413571.846699979,5316527.31059999 413571.79719998,5316527.47249999 413571.741599981,5316527.63239998 413571.68,5316527.79 413571.610199985,5316527.95659998 413571.533899987,5316528.1203 413571.451399989,5316528.28089999 413571.362699991,5316528.43829999 413571.26799999,5316528.59199999 413571.167399996,5316528.74199998 413571.061,5316528.888 413570.948999976,5316529.0298 413570.831699979,5316529.1671 413570.708999982,5316529.29979999 413570.581399985,5316529.42759999 413570.448799989,5316529.55029999 413570.311599992,5316529.66789998 413570.17,5316529.78 413564.89,5316532.59 413562.12,5316533.58 413561.48,5316533.62 413559.59,5316533.11 413558.39,5316532.17 413550.76,5316521.19 413550.47,5316521.39 413550.559299986,5316521.55839999 413550.632299984,5316521.73449998 413550.688399983,5316521.91669998 413550.726999982,5316522.1033 413550.747899981,5316522.29279999 413550.750799981,5316522.48339999 413550.735699981,5316522.67339998 413550.702799982,5316522.86119998 413550.652399983,5316523.045 413550.584799985,5316523.22319999 413550.500799987,5316523.39429999 413550.40099999,5316523.55669999 413550.286299993,5316523.70899998 413550.157799996,5316523.84969998 413550.0166,5316523.97779998 413549.864,5316524.092 413549.701299982,5316524.1913 413549.529899987,5316524.27479999 413549.351399991,5316524.34179999 413549.167399996,5316524.39159999 413548.979499975,5316524.42389999 413548.78939998,5316524.43829999 413548.598799985,5316524.43479999 413548.40939999,5316524.41319999 413548.222799994,5316524.37399999 413548.040799999,5316524.31729999 413547.864899978,5316524.24369999 413547.696799982,5316524.1538 413547.537899986,5316524.0485 413547.38959999,5316523.92869998 413547.253199994,5316523.79549998 413547.13,5316523.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>413543.19,5316551.87 413545.65,5316556.35 413541.48,5316558.63 413539.03,5316554.16 413543.19,5316551.87</gml:coordinates></gml:LinearRing></gml:innerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvY5</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>5287</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.123">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413486.79,5317215.24 413474.03,5317223.17 413479.45,5317231.84 413492.15,5317223.87 413486.79,5317215.24</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyyW</ogr:gmlid>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>153</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.124">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413384.01,5317143.58 413394.2,5317139.84 413377.39,5317094.72 413367.23,5317098.53 413384.01,5317143.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzt</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>522</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.125">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413875.44,5316836.57 413893.71,5316831.53 413895.37,5316831.09 413891.05,5316820.25 413890.26,5316820.47 413888.29,5316814.18 413873.89,5316819.82 413873.12,5316820.12 413858.34,5316824.32 413858.27,5316824.34 413862.59,5316840.11 413870.89,5316837.81 413875.44,5316836.57</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL001000gVUtP</ogr:gmlid>
      <ogr:street>Friedrichstraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>554</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.126">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413643.31,5317163.2 413652.56,5317167.46 413653.8,5317168.03 413653.91,5317167.78 413652.67,5317167.21 413653.95,5317164.3 413655.19,5317164.87 413658.87,5317156.78 413657.61,5317156.2 413658.86,5317153.56 413660.12,5317154.14 413660.21,5317153.96 413658.95,5317153.38 413649.83,5317149.18 413643.31,5317163.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6V</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.127">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413639.31,5317172.46 413648.75,5317176.72 413650.15,5317173.57 413651.63,5317174.33 413652.86,5317173.97 413654.32,5317170.79 413652.95,5317169.78 413653.8,5317168.03 413652.56,5317167.46 413643.31,5317163.2 413639.11,5317172.37 413639.31,5317172.46</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6M</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>119</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.128">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413430.98,5316761.0 413439.19,5316756.29 413435.95,5316750.0 413436.96,5316749.48 413436.28,5316748.15 413427.0,5316753.56 413430.98,5316761.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbf</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>82</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.129">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413440.6,5316748.88 413439.09,5316749.88 413442.11,5316754.62 413445.74,5316752.54 413442.59,5316747.57 413440.6,5316748.88</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbh</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.130">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413485.65,5316974.39 413479.6,5316963.44 413479.4,5316963.07 413471.62,5316967.25 413477.84,5316978.72 413485.65,5316974.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8U</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.131">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413374.65,5317076.65 413413.57,5317052.81 413408.0,5317043.63 413369.02,5317067.47 413374.65,5317076.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzs</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>491</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.132">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413730.65,5317114.51 413729.11,5317111.27 413724.07,5317113.58 413725.65,5317116.89 413730.65,5317114.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6P</ogr:gmlid>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>20</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.133">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413584.99,5316866.92 413575.26,5316872.06 413579.75,5316880.54 413589.48,5316875.39 413584.99,5316866.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7M</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>106</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.134">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413541.26,5317042.87 413543.33,5317041.74 413543.46,5317042.08 413552.81,5317036.84 413550.34,5317032.45 413549.96,5317032.71 413520.23,5316979.14 413520.49,5316978.99 413518.06,5316974.59 413508.46,5316979.87 413508.62,5316980.11 413506.61,5316981.23 413541.26,5317042.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7e</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>911</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.135">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413536.62,5317073.24 413524.44,5317051.61 413512.22,5317058.5 413524.42,5317080.13 413536.62,5317073.24</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8s</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>3065</ogr:use_id>
      <ogr:use>Kindergarten, Kinder</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>348</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.136">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413513.24,5317023.75 413503.32,5317029.27 413504.77,5317031.86 413498.78,5317035.13 413497.53,5317032.96 413495.92,5317033.86 413495.41,5317034.16 413496.61,5317036.33 413492.02,5317038.91 413497.01,5317047.25 413497.67,5317048.35 413519.91,5317036.01 413513.24,5317023.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8m</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>322</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.137">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413501.32,5317025.85 413503.32,5317029.27 413513.24,5317023.75 413511.27,5317020.22 413502.55,5317004.32 413498.76,5317006.42 413495.1,5317008.45 413494.78,5317008.62 413494.68,5317008.68 413492.07,5317010.13 413496.95,5317018.46 413501.32,5317025.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8k</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>259</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.138">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413495.8,5317054.45 413488.48,5317058.55 413499.32,5317077.86 413506.62,5317073.76 413495.8,5317054.45</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8t</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>3065</ogr:use_id>
      <ogr:use>Kindergarten, Kinder</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.139">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413496.95,5317018.46 413492.07,5317010.13 413486.4,5317013.28 413482.93,5317015.2 413479.12,5317017.31 413482.88,5317023.68 413484.2,5317025.88 413496.95,5317018.46</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8l</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>145</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.140">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413267.6,5316626.83 413270.97,5316625.0 413259.67,5316603.8 413258.36,5316604.51 413258.274299993,5316604.33569999 413258.177299995,5316604.1675 413258.069199998,5316604.0061 413257.950599976,5316603.85239998 413257.821899979,5316603.70689998 413257.683799983,5316603.57029999 413257.536899986,5316603.44329999 413257.38179999,5316603.32649999 413257.219199994,5316603.22019999 413257.049899999,5316603.1251 413256.874499978,5316603.0416 413256.694,5316602.97 413256.509199987,5316602.91069998 413256.320799992,5316602.86379998 413256.129699997,5316602.82959998 413255.936799976,5316602.80829998 413255.742799981,5316602.79989998 413255.548699986,5316602.80459998 413255.355399991,5316602.82209998 413255.163699996,5316602.85259998 413254.974499975,5316602.89589998 413254.78849998,5316602.95169998 413254.606799985,5316603.0198 413254.43,5316603.1 413254.265199993,5316603.18889999 413254.106499997,5316603.28809999 413253.954399976,5316603.39739999 413253.80979998,5316603.51629999 413253.673099983,5316603.64419998 413253.544999986,5316603.78069998 413253.425899989,5316603.92519998 413253.316399992,5316604.077 413253.216899995,5316604.23559999 413253.127899997,5316604.40029999 413253.049699999,5316604.57039999 413252.982599975,5316604.74519998 413252.927,5316604.924 413252.883099978,5316605.1059 413252.850899978,5316605.29029999 413252.830699979,5316605.47629999 413252.822499979,5316605.66329998 413252.826399979,5316605.85039998 413252.842399979,5316606.0368 413252.870299978,5316606.22189999 413252.910099977,5316606.40469999 413252.961699976,5316606.58469999 413253.024699999,5316606.76089998 413253.099,5316606.93259998 413253.184199995,5316607.0993 413253.28,5316607.26 413251.96,5316607.95 413252.07,5316608.15 413245.86,5316611.5 413245.53,5316610.89 413244.14,5316611.64 413244.47,5316612.26 413241.88,5316613.65 413241.58,5316613.1 413236.75,5316615.72 413237.05,5316616.27 413231.01,5316619.53 413229.88,5316617.45 413230.23,5316617.26 413228.81,5316614.64 413218.52,5316620.22 413217.83,5316618.93 413201.92,5316627.54 413205.29,5316633.86 413213.06,5316629.71 413213.28,5316630.12 413213.137899997,5316630.26059999 413213.0031,5316630.40829999 413212.875999978,5316630.56269999 413212.756999981,5316630.72339998 413212.646399984,5316630.88989998 413212.544399986,5316631.0619 413212.451299989,5316631.23879999 413212.367399991,5316631.42029999 413212.292899993,5316631.60589998 413212.228,5316631.795 413212.172899996,5316631.98709998 413212.127599997,5316632.1818 413212.092299998,5316632.37859999 413212.067099998,5316632.57689999 413212.052099999,5316632.77619998 413212.047299999,5316632.97599998 413212.052699999,5316633.1759 413212.068299998,5316633.37509999 413212.094099998,5316633.57339999 413212.13,5316633.77 413212.175399996,5316633.94049998 413212.227199994,5316634.1092 413212.285499993,5316634.27579999 413212.35,5316634.44 413212.420899989,5316634.60199999 413212.497999987,5316634.76109998 413212.580999985,5316634.91719998 413212.67,5316635.07 413213.36,5316635.77 413211.2,5316637.92 413210.51,5316637.22 413209.92,5316638.47 413210.85,5316638.61 413210.43,5316641.51 413209.5,5316641.37 413209.73,5316642.72 413210.58,5316642.28 413211.97,5316644.9 413211.12,5316645.34 413212.15,5316646.28 413212.56,5316645.43 413215.2,5316646.69 413214.79,5316647.54 413216.25,5316647.87 413216.42,5316647.12 413218.82,5316646.68 413219.14,5316647.55 413219.211899995,5316647.72779998 413219.293599993,5316647.90139998 413219.38479999,5316648.0701 413219.485399988,5316648.23339999 413219.594899985,5316648.39089999 413219.713,5316648.542 413219.839299979,5316648.68619998 413219.973499975,5316648.82319998 413220.115099997,5316648.95239998 413220.263699993,5316649.0735 413220.418799989,5316649.1862 413220.58,5316649.29 413220.739399981,5316649.37959999 413220.903499977,5316649.46039999 413221.071699998,5316649.53219999 413221.243499994,5316649.59479999 413221.418499989,5316649.64799998 413221.596099985,5316649.69169998 413221.77569998,5316649.72569998 413221.957,5316649.75 413222.139299996,5316649.76439998 413222.322099992,5316649.76899998 413222.504899987,5316649.76359998 413222.687199983,5316649.74839998 413222.868299978,5316649.72329998 413223.047899999,5316649.68849998 413223.225299994,5316649.64399998 413223.4,5316649.59 413224.2,5316650.92 413225.63,5316650.13 413225.31,5316649.37 413228.87,5316647.48 413228.938899976,5316647.64729998 413229.0195,5316647.80929998 413229.111199997,5316647.96519998 413229.213799995,5316648.1143 413229.326499992,5316648.25579999 413229.449,5316648.389 413229.580499985,5316648.51319999 413229.720499982,5316648.62779998 413229.868099978,5316648.73229998 413230.022799999,5316648.82619998 413230.183699995,5316648.90889998 413230.35,5316648.98 413230.528599987,5316649.0367 413230.710899982,5316649.0802 413230.895899977,5316649.1105 413231.082499998,5316649.1272 413231.269899993,5316649.1304 413231.457,5316649.12 413231.642899984,5316649.0961 413231.826599979,5316649.0587 413232.0071,5316649.0081 413232.183399995,5316648.94449998 413232.354699991,5316648.86839998 413232.52,5316648.78 413233.71,5316650.98 413244.89,5316644.95 413243.46,5316642.31 413243.11,5316642.5 413242.0,5316640.41 413248.05,5316637.13 413248.35,5316637.67 413253.2,5316635.04 413252.91,5316634.5 413255.52,5316633.08 413255.85,5316633.7 413257.25,5316632.95 413256.91,5316632.35 413263.15,5316628.97 413263.26,5316629.18 413264.0,5316628.78 413264.121699997,5316628.91199998 413264.254299994,5316629.0332 413264.39659999,5316629.1427 413264.547899986,5316629.23949999 413264.706799982,5316629.32319999 413264.872199978,5316629.39299999 413265.043099999,5316629.44839999 413265.218,5316629.489 413265.39569999,5316629.51449999 413265.574999985,5316629.52489999 413265.754499981,5316629.51979999 413265.932899976,5316629.49949999 413266.108899997,5316629.46409999 413266.281299993,5316629.41379999 413266.448699989,5316629.34889999 413266.61,5316629.27 413266.760499981,5316629.176 413266.902699977,5316629.0697 413267.035499999,5316628.95199998 413267.158,5316628.82359998 413267.269299993,5316628.68539998 413267.368699991,5316628.53829999 413267.455499989,5316628.38349999 413267.529,5316628.222 413267.588799985,5316628.0549 413267.634399984,5316627.88339998 413267.665599983,5316627.70869998 413267.681999983,5316627.53199999 413267.683599983,5316627.35449999 413267.670399983,5316627.1776 413267.642499984,5316627.0023 413267.6,5316626.83</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TweF</ogr:gmlid>
      <ogr:street>Amalie-Gramm-Weg</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>3041</ogr:use_id>
      <ogr:use>Kirche</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>1664</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.141">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413159.71,5317021.91 413157.57,5317022.93 413158.84,5317025.61 413160.99,5317024.59 413159.71,5317021.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj3</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>13 x</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>7</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.142">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413191.22,5317081.1 413186.14,5317070.99 413176.38,5317075.91 413197.52,5317117.88 413199.13,5317117.07 413207.85,5317134.37 413217.58,5317129.44 413217.52,5317129.31 413214.82,5317123.95 413212.5,5317119.33 413208.86,5317112.1 413207.25,5317112.91 413203.62,5317105.7 413198.5,5317095.54 413191.22,5317081.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjb</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>725</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.143">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413230.78,5317067.58 413224.61,5317054.79 413222.59,5317055.77 413222.16,5317054.89 413204.2,5317063.59 413204.63,5317064.47 413202.59,5317065.46 413208.75,5317078.2 413219.89,5317072.83 413220.55,5317074.19 413222.5,5317073.24 413221.85,5317071.88 413223.0,5317071.3 413223.06,5317071.27 413230.78,5317067.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twji</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>369</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.144">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413198.74,5317067.47 413196.23,5317062.32 413191.04,5317064.8 413193.59,5317069.95 413198.74,5317067.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj9</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>33</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.145">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413198.5,5317095.54 413200.11,5317094.73 413192.84,5317080.29 413191.22,5317081.1 413198.5,5317095.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twja</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.146">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413203.62,5317105.7 413207.25,5317112.91 413208.86,5317112.1 413205.23,5317104.89 413203.62,5317105.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjc</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.147">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413212.5,5317119.33 413214.1,5317118.52 413210.47,5317111.29 413208.86,5317112.1 413212.5,5317119.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjd</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.148">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413217.52,5317129.31 413222.77,5317126.67 413220.06,5317121.32 413214.82,5317123.95 413217.52,5317129.31</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twje</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.149">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413202.97,5317039.36 413203.2,5317039.84 413199.63,5317041.55 413203.22,5317049.02 413206.78,5317047.31 413207.0,5317047.77 413227.4,5317037.99 413223.36,5317029.57 413202.97,5317039.36</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj6</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>244</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.150">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413222.88,5317045.67 413222.85,5317045.62 413229.73,5317042.24 413209.23,5316999.37 413181.89,5317012.73 413181.87,5317012.97 413178.72,5317014.5 413180.34,5317017.89 413181.25,5317017.46 413191.6,5317038.96 413192.48,5317038.54 413194.3,5317042.43 413193.48,5317042.86 413197.93,5317052.1 413197.82,5317052.15 413200.88,5317058.48 413223.77,5317047.52 413222.88,5317045.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj5</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1619</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.151">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413225.52,5317051.1 413197.61,5317064.66 413206.29,5317082.64 413210.31,5317080.68 413209.6,5317079.17 413218.85,5317074.7 413219.63,5317076.31 413222.78,5317074.79 413223.35,5317075.98 413224.96,5317075.2 413223.06,5317071.27 413223.0,5317071.3 413221.85,5317071.88 413222.5,5317073.24 413220.55,5317074.19 413219.89,5317072.83 413208.75,5317078.2 413202.59,5317065.46 413204.63,5317064.47 413204.2,5317063.59 413222.16,5317054.89 413222.59,5317055.77 413224.61,5317054.79 413230.78,5317067.58 413232.94,5317066.49 413225.52,5317051.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjh</ogr:gmlid>
      <ogr:street>Antoniterstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>204</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.152">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413557.56,5316604.69 413557.449399989,5316604.53479999 413557.336899991,5316604.38099999 413557.222499994,5316604.22849999 413557.106299997,5316604.0775 413556.988199975,5316603.92789998 413556.868299978,5316603.77969998 413556.746599981,5316603.63299998 413556.623199984,5316603.48779999 413556.497999987,5316603.34409999 413556.371,5316603.202 413556.242299994,5316603.0614 413556.112,5316602.92249998 413555.979899975,5316602.78509998 413555.846199979,5316602.64939998 413555.710799982,5316602.51519999 413555.573799986,5316602.38279999 413555.435199989,5316602.25199999 413555.29499999,5316602.123 413555.153299996,5316601.99559997 413555.01,5316601.87 413554.863399978,5316601.75479998 413554.715599982,5316601.64119998 413554.566599986,5316601.52919999 413554.41639999,5316601.41879999 413554.26499999,5316601.31 413554.112499997,5316601.2029 413553.958799976,5316601.0974 413553.80399998,5316600.99349997 413553.648099984,5316600.89129998 413553.491099988,5316600.79079998 413553.333,5316600.692 413553.174,5316600.59489999 413553.0139,5316600.49959999 413552.852799978,5316600.40589999 413552.690799983,5316600.31399999 413552.527699987,5316600.22379999 413552.363699991,5316600.1354 413552.198799995,5316600.0488 413552.032899999,5316599.96389998 413551.866099978,5316599.88079998 413551.698499982,5316599.79949998 413551.53,5316599.72 413551.366099991,5316599.64549998 413551.201399995,5316599.57259999 413551.035899999,5316599.50139999 413550.869799978,5316599.43199999 413550.702999982,5316599.36409999 413550.535499986,5316599.29799999 413550.367299991,5316599.23359999 413550.198499995,5316599.171 413550.029,5316599.11 413549.859099978,5316599.0508 413549.688599983,5316598.99339997 413549.517499987,5316598.93759998 413549.345899991,5316598.88359998 413549.173699996,5316598.83139998 413549.001,5316598.78089998 413548.827799979,5316598.73219998 413548.654199983,5316598.68519998 413548.48,5316598.64 413547.05,5316604.41 413547.61,5316605.51 413548.84,5316607.95 413549.25,5316607.73 413552.66,5316614.44 413552.23,5316614.66 413557.56,5316625.27 413561.99,5316623.04 413559.69,5316618.43 413559.744599981,5316618.24429999 413559.79709998,5316618.058 413559.847499979,5316617.87119998 413559.895799977,5316617.68369998 413559.941999976,5316617.49579999 413559.986199975,5316617.30729999 413560.028099999,5316617.1184 413560.068,5316616.929 413560.105699997,5316616.73909998 413560.141399996,5316616.54889999 413560.174799996,5316616.35819999 413560.206199995,5316616.1672 413560.235299994,5316615.97589998 413560.262399993,5316615.78419998 413560.287299993,5316615.59219998 413560.31,5316615.4 413560.330299992,5316615.20319999 413560.348399991,5316615.0061 413560.364099991,5316614.80889998 413560.37759999,5316614.61149998 413560.38869999,5316614.41399999 413560.39759999,5316614.21629999 413560.40409999,5316614.0185 413560.40839999,5316613.82069998 413560.41029999,5316613.62289998 413560.41,5316613.425 413560.40739999,5316613.22699999 413560.40239999,5316613.029 413560.39519999,5316612.83109998 413560.38559999,5316612.63329998 413560.373699991,5316612.43559999 413560.359599991,5316612.23809999 413560.343099991,5316612.0407 413560.324399992,5316611.84359998 413560.303299992,5316611.64669998 413560.28,5316611.45 413560.252199994,5316611.26469999 413560.222099994,5316611.0797 413560.189699995,5316610.89509998 413560.155199996,5316610.71089998 413560.118399997,5316610.52719999 413560.079399998,5316610.34389999 413560.038099999,5316610.1611 413559.994699975,5316609.97879998 413559.949,5316609.797 413559.901099977,5316609.61579998 413559.851099978,5316609.43519999 413559.79889998,5316609.25529999 413559.744399981,5316609.076 413559.687899983,5316608.89729998 413559.629099984,5316608.71939998 413559.568199986,5316608.54219999 413559.505199987,5316608.36569999 413559.44,5316608.19 413559.367299991,5316608.0046 413559.292299993,5316607.82009998 413559.214899995,5316607.63659998 413559.135299997,5316607.45399999 413559.053399999,5316607.27249999 413558.969199975,5316607.092 413558.882799978,5316606.91259998 413558.79409998,5316606.73419998 413558.703099982,5316606.55699999 413558.61,5316606.381 413558.514699987,5316606.20609999 413558.417099989,5316606.0325 413558.317399992,5316605.86 413558.215499995,5316605.68889998 413558.111499997,5316605.51899999 413558.0054,5316605.35049999 413557.897199977,5316605.1833 413557.78679998,5316605.0174 413557.674499983,5316604.85299998 413557.56,5316604.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvY6</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>192</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.153">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413676.24,5316794.06 413676.47,5316794.48 413687.15,5316788.76 413662.14,5316742.08 413656.33,5316745.19 413651.49,5316747.79 413676.24,5316794.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZ4</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>641</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.154">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413693.33,5316797.11 413692.09,5316797.76 413681.25,5316803.56 413692.2,5316823.95 413703.0,5316818.17 413704.33,5316817.5 413700.65,5316810.61 413700.56,5316810.66 413693.33,5316797.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZ8</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>317</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.155">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413751.09,5316753.57 413747.88,5316754.71 413744.66,5316756.14 413741.37,5316757.6 413740.76,5316756.22 413737.31,5316758.14 413737.75,5316758.93 413733.11,5316761.57 413734.6,5316764.62 413734.34,5316764.76 413734.48,5316765.02 413733.04,5316765.8 413735.21,5316769.82 413736.65,5316769.04 413736.79,5316769.3 413737.05,5316769.16 413739.4,5316773.34 413750.63,5316767.38 413754.61,5316766.28 413751.09,5316753.57</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSP</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>254</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.156">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413751.22,5316780.06 413758.85,5316808.15 413762.6,5316807.11 413763.19,5316809.27 413766.86,5316808.29 413767.66,5316811.16 413766.5,5316811.48 413769.83,5316823.58 413769.94,5316823.98 413797.99,5316816.29 413794.18,5316803.97 413798.21,5316803.04 413791.88,5316781.0 413795.48,5316780.07 413795.56,5316780.37 413805.57,5316777.95 413801.69,5316764.91 413800.87,5316765.14 413792.11,5316767.52 413782.74,5316770.07 413755.47,5316777.49 413752.07,5316778.47 413751.22,5316780.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSO</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1921</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.157">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413629.54,5316538.19 413617.47,5316544.61 413631.77,5316571.71 413632.04,5316571.56 413632.37,5316571.38 413643.66,5316565.31 413643.82,5316565.23 413631.25,5316541.47 413629.54,5316538.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUm</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>418</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.158">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413685.96,5316568.18 413686.27,5316567.99 413675.95,5316548.82 413675.12,5316549.26 413673.92,5316547.0 413675.54,5316546.14 413670.17,5316529.5 413653.57,5316534.86 413653.39,5316534.34 413650.6,5316535.25 413650.62,5316535.3 413647.52,5316536.3 413642.72,5316537.84 413642.88,5316538.12 413632.63,5316541.48 413636.65,5316548.99 413638.12,5316548.31 413642.09,5316555.83 413643.55,5316555.41 413648.96,5316553.85 413654.11,5316563.29 413646.09,5316567.57 413644.63,5316564.81 413643.82,5316565.23 413643.66,5316565.31 413632.37,5316571.38 413633.83,5316574.24 413633.1,5316574.78 413633.25,5316575.06 413635.89,5316580.0 413642.12,5316591.69 413642.55,5316591.46 413654.36,5316585.12 413654.36,5316585.07 413685.96,5316568.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUk</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>2 a</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1853</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.159">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413788.77,5316830.95 413789.91,5316835.12 413791.36,5316840.42 413801.27,5316837.69 413801.86,5316839.61 413807.02,5316838.16 413805.73,5316833.44 413805.26,5316833.56 413803.3,5316826.38 413802.01,5316821.67 413787.33,5316825.69 413788.62,5316830.4 413788.77,5316830.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSN</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>244</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.160">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413600.18,5316627.01 413599.79,5316626.28 413585.62,5316633.88 413585.12,5316631.76 413584.929599977,5316631.79759998 413584.739399981,5316631.83589998 413584.549399986,5316631.87509998 413584.359499991,5316631.91509998 413584.169799996,5316631.95589998 413583.980299975,5316631.99759998 413583.791,5316632.04 413583.601699985,5316632.0833 413583.41259999,5316632.1274 413583.223699994,5316632.1723 413583.035,5316632.21799999 413582.846399979,5316632.26449999 413582.658099983,5316632.31189999 413582.47,5316632.36 413582.279899993,5316632.41169999 413582.090099998,5316632.46429999 413581.900499977,5316632.51779999 413581.711199982,5316632.57239999 413581.522199987,5316632.62779998 413581.333399992,5316632.68419998 413581.144899996,5316632.74149998 413580.956799976,5316632.79979998 413580.768899981,5316632.85899998 413580.581299985,5316632.91909998 413580.39399999,5316632.98019997 413580.20699999,5316633.0422 413580.020399999,5316633.1051 413579.834,5316633.169 413579.647899984,5316633.23379999 413579.462099988,5316633.29949999 413579.276699993,5316633.36619999 413579.091599998,5316633.43379999 413578.906799977,5316633.50229999 413578.722399982,5316633.57169999 413578.538399986,5316633.64209998 413578.354599991,5316633.71329998 413578.171299996,5316633.78549998 413577.988299975,5316633.85859998 413577.80569998,5316633.93259998 413577.623399984,5316634.0075 413577.441499989,5316634.0833 413577.26,5316634.16 413577.075799998,5316634.22749999 413576.891799977,5316634.29579999 413576.708199982,5316634.36489999 413576.524899987,5316634.43479999 413576.341899991,5316634.50559999 413576.159199996,5316634.57729999 413575.976899975,5316634.64969998 413575.79489998,5316634.72299998 413575.613199985,5316634.79709998 413575.431899989,5316634.87209998 413575.250899994,5316634.94779998 413575.070199998,5316635.0244 413574.889899978,5316635.1018 413574.71,5316635.18 413574.530399987,5316635.25899999 413574.351199991,5316635.33889999 413574.172299996,5316635.41949999 413573.993799975,5316635.50099999 413573.815699979,5316635.58329999 413573.637999984,5316635.66639998 413573.460599988,5316635.75029998 413573.283599993,5316635.83499998 413573.107,5316635.92049998 413572.930799976,5316636.0068 413572.754999981,5316636.0939 413572.579599985,5316636.1818 413572.40459999,5316636.27049999 413572.23,5316636.36 413571.35,5316636.83 413573.21,5316640.45 413567.5,5316643.46 413567.71,5316643.94 413574.89,5316657.38 413578.81,5316655.33 413584.42,5316652.32 413584.99,5316653.01 413594.78,5316647.78 413594.31,5316646.88 413604.77,5316641.49 413606.07,5316640.81 413605.31,5316639.39 413606.45,5316638.74 413600.18,5316627.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYY</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>2071</ogr:use_id>
      <ogr:use>Hotel, Motel, Pensio</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>633</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.161">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413578.81,5316655.33 413576.11,5316656.76 413578.69,5316661.49 413608.61,5316645.4 413606.17,5316640.8 413606.07,5316640.81 413604.77,5316641.49 413594.31,5316646.88 413594.78,5316647.78 413584.99,5316653.01 413584.42,5316652.32 413578.81,5316655.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZ7</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>172</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.162">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413651.28,5316608.0 413662.49,5316628.9 413662.86,5316628.71 413663.01,5316628.63 413674.84,5316622.33 413670.6,5316614.39 413673.25,5316612.98 413675.15,5316616.57 413693.49,5316607.05 413695.79,5316611.17 413700.48,5316608.68 413704.83,5316606.36 413706.45,5316605.5 413694.81,5316584.72 413684.19,5316590.4 413686.56,5316594.63 413668.62,5316604.28 413668.9,5316604.8 413666.24,5316606.22 413663.65,5316601.37 413651.28,5316608.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUj</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>936</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.163">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413638.42,5316711.72 413631.11,5316698.06 413637.22,5316694.78 413624.18,5316670.52 413604.84,5316680.88 413594.74,5316662.1 413594.72,5316662.12 413650.26,5316765.76 413667.82,5316798.54 413676.24,5316794.06 413651.49,5316747.79 413656.33,5316745.19 413650.31,5316733.9 413645.36,5316736.56 413633.42,5316714.36 413638.42,5316711.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZ6</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>3090</ogr:use_id>
      <ogr:use>Empfangsgebäude</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1773</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.164">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413594.72,5316662.12 413592.91,5316658.77 413580.7,5316665.31 413634.27,5316765.29 413638.02,5316772.3 413680.47,5316851.45 413680.7,5316851.33 413681.66,5316850.81 413692.4,5316845.02 413692.66,5316844.88 413670.96,5316804.4 413668.51,5316799.82 413667.82,5316798.54 413650.26,5316765.76 413594.72,5316662.12</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZa</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 f</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>2928</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.165">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413680.7,5316851.33 413680.84,5316851.6 413681.92,5316851.02 413692.05,5316869.92 413688.86,5316871.59 413703.99,5316899.8 413718.05,5316892.26 413714.74,5316886.09 413703.15,5316864.43 413704.89,5316863.51 413704.78,5316863.3 413711.44,5316859.72 413722.51,5316880.37 413733.22,5316874.65 413733.02,5316874.35 413734.25,5316873.68 413709.57,5316827.66 413708.29,5316828.33 413708.19,5316828.06 413697.52,5316833.75 413708.56,5316854.36 413701.9,5316857.94 413694.29,5316844.3 413692.54,5316845.28 413692.4,5316845.02 413681.66,5316850.81 413680.7,5316851.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZ9</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>7 h</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1581</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.166">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413678.42,5316657.6 413690.71,5316654.58 413689.85,5316651.98 413695.59,5316650.08 413695.81,5316650.01 413692.53,5316640.13 413686.62,5316629.29 413699.38,5316622.3 413698.75,5316621.11 413679.74,5316631.52 413674.84,5316622.33 413663.01,5316628.63 413662.86,5316628.71 413677.47,5316656.09 413678.42,5316657.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUh</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>646</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.167">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413650.31,5316733.9 413654.25,5316731.77 413650.05,5316724.15 413653.55,5316722.22 413645.78,5316707.72 413638.42,5316711.72 413633.42,5316714.36 413645.36,5316736.56 413650.31,5316733.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvZ5</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>318</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.168">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413520.73,5316635.03 413518.67,5316636.11 413521.11,5316640.72 413523.16,5316639.63 413520.73,5316635.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYg</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>12</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.169">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413614.48,5316867.93 413619.9,5316865.08 413616.88,5316859.38 413611.45,5316862.24 413614.48,5316867.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYj</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>40</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.170">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413700.48,5316608.68 413702.18,5316611.99 413706.63,5316609.71 413704.83,5316606.36 413700.48,5316608.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUi</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.171">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413519.28,5316515.93 413522.06,5316514.44 413520.56,5316511.67 413517.79,5316513.16 413519.28,5316515.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYb</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2612</ogr:use_id>
      <ogr:use>Toilette</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>10</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.172">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413515.54,5316509.05 413518.28,5316507.54 413516.77,5316504.79 413514.03,5316506.3 413515.54,5316509.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYc</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2612</ogr:use_id>
      <ogr:use>Toilette</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>10</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.173">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413541.48,5316558.63 413545.65,5316556.35 413543.19,5316551.87 413539.03,5316554.16 413541.48,5316558.63</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYa</ogr:gmlid>
      <ogr:street>Bismarckallee</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2412</ogr:use_id>
      <ogr:use>Wartehalle</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.174">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413433.65,5317218.84 413445.86,5317211.27 413446.2,5317211.82 413446.6,5317212.47 413446.63,5317212.45 413450.41,5317210.02 413449.7,5317208.88 413474.67,5317193.39 413475.38,5317194.54 413479.25,5317192.14 413478.54,5317190.99 413490.73,5317183.42 413483.68,5317172.06 413426.6,5317207.5 413433.65,5317218.84</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzf</ogr:gmlid>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>910</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.175">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413521.83,5317209.81 413547.0,5317194.09 413541.25,5317184.89 413534.78,5317188.94 413534.68,5317188.77 413532.33,5317190.24 413532.43,5317190.41 413519.81,5317198.29 413519.7,5317198.12 413517.33,5317199.57 413517.45,5317199.77 413504.77,5317207.67 413504.67,5317207.51 413502.37,5317208.97 413502.48,5317209.15 413499.15,5317211.22 413496.43,5317212.9 413496.32,5317212.98 413494.73,5317210.43 413494.48,5317210.59 413501.57,5317222.0 413501.78,5317222.33 413502.15,5317222.11 413504.86,5317220.41 413519.71,5317211.13 413521.83,5317209.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyz9</ogr:gmlid>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>580</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.176">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413486.79,5317215.24 413492.15,5317223.87 413493.69,5317222.9 413496.63,5317227.26 413497.8,5317227.53 413500.42,5317226.01 413500.69,5317224.91 413499.64,5317223.27 413501.57,5317222.0 413494.48,5317210.59 413494.41,5317210.48 413494.32,5317210.54 413486.79,5317215.24</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyyX</ogr:gmlid>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>128</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.177">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413470.17,5317225.55 413466.69,5317227.99 413466.46,5317227.64 413455.53,5317234.92 413456.76,5317236.65 413454.59,5317240.88 413476.64,5317252.04 413481.02,5317243.33 413473.04,5317239.27 413472.64,5317237.08 413479.89,5317232.55 413479.45,5317231.84 413474.03,5317223.17 413470.17,5317225.55</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyyV</ogr:gmlid>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>402</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.178">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413697.49,5317094.19 413694.08,5317096.05 413693.46,5317094.91 413675.52,5317104.67 413681.23,5317115.24 413684.86,5317113.28 413699.29,5317105.47 413698.49,5317104.01 413701.72,5317102.26 413697.49,5317094.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6Q</ogr:gmlid>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>281</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.179">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413684.86,5317113.28 413690.38,5317123.82 413697.19,5317120.11 413706.59,5317137.72 413708.2,5317139.47 413711.57,5317136.73 413719.76,5317146.66 413719.66,5317146.74 413719.92,5317147.28 413726.43,5317144.61 413729.27,5317143.4 413725.68,5317135.83 413723.03,5317130.22 413718.37,5317120.36 413713.95,5317111.01 413709.69,5317102.0 413703.2,5317105.08 413701.72,5317102.26 413698.49,5317104.01 413699.29,5317105.47 413684.86,5317113.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6R</ogr:gmlid>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>873</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.180">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413486.76,5317167.58 413466.4,5317180.26 413466.0,5317179.6 413420.84,5317207.67 413429.77,5317222.05 413446.2,5317211.82 413446.23,5317211.8 413446.63,5317212.45 413447.43,5317213.73 413480.04,5317193.42 413480.19,5317193.65 413496.62,5317183.41 413486.76,5317167.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzg</ogr:gmlid>
      <ogr:street>Breisacher Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1422</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.181">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413659.36,5316961.29 413650.76,5316965.89 413669.32,5317000.59 413677.8,5316995.83 413659.36,5316961.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw76</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>382</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.182">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413582.13,5317051.92 413582.73,5317052.9 413587.3,5317051.33 413586.77,5317049.37 413595.06,5317044.62 413595.31,5317044.82 413595.69,5317044.59 413589.9,5317034.1 413589.49,5317034.28 413589.56,5317034.56 413590.23,5317035.8 413586.22,5317037.92 413585.67,5317036.8 413580.67,5317039.55 413577.19,5317041.54 413563.69,5317049.34 413560.34,5317051.24 413555.57,5317054.19 413556.26,5317055.44 413552.17,5317057.74 413551.47,5317056.55 413551.26,5317056.24 413550.96,5317056.43 413556.48,5317066.56 413556.91,5317066.34 413556.89,5317065.93 413565.06,5317061.47 413565.61,5317062.24 413570.13,5317060.76 413569.68,5317058.88 413582.13,5317051.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7f</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>511</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.183">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413512.35,5317114.73 413533.77,5317153.51 413542.43,5317148.64 413520.87,5317109.98 413512.35,5317114.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzm</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>436</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.184">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413470.23,5317111.47 413512.24,5317088.02 413506.96,5317078.55 413464.96,5317102.01 413470.23,5317111.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8q</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>521</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.185">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413578.83,5317077.65 413570.47,5317082.29 413591.85,5317120.85 413600.21,5317116.22 413578.83,5317077.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzi</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>421</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.186">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413640.32,5317016.99 413648.69,5317012.29 413629.29,5316978.13 413620.87,5316982.81 413640.32,5317016.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw74</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>378</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.187">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413549.8,5317093.83 413541.45,5317098.47 413562.82,5317137.03 413571.18,5317132.4 413549.8,5317093.83</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzn</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>421</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.188">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413493.19,5317078.84 413498.19,5317075.98 413486.92,5317055.91 413481.88,5317058.74 413493.19,5317078.84</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8r</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>133</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.189">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413571.95,5317124.89 413574.98,5317130.3 413588.06,5317122.97 413585.03,5317117.55 413571.95,5317124.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzj</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>93</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.190">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413665.34,5317002.72 413662.36,5316997.35 413649.46,5317004.58 413652.45,5317009.86 413665.34,5317002.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw75</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>90</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.191">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413500.03,5316812.23 413495.97,5316804.4 413488.08,5316808.93 413492.1,5316816.48 413500.03,5316812.23</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twao</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>79</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.192">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413492.1,5316816.48 413488.08,5316808.93 413482.65,5316812.19 413483.31,5316813.38 413482.43,5316813.98 413482.86,5316814.83 413480.54,5316816.04 413483.3,5316821.2 413492.1,5316816.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twan</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>76</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.193">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413467.06,5316857.18 413463.1,5316849.76 413460.09,5316848.82 413450.02,5316854.19 413454.26,5316862.12 413457.46,5316860.41 413458.09,5316861.61 413457.74,5316861.79 413457.9,5316862.09 413467.06,5316857.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9n</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>137</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.194">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413483.3,5316821.2 413480.54,5316816.04 413479.74,5316814.55 413478.5,5316812.22 413475.36,5316814.36 413477.19,5316817.38 413473.36,5316819.42 413472.89,5316818.62 413472.05,5316819.01 413470.94,5316817.13 413463.1,5316821.13 413466.81,5316828.2 413469.38,5316828.67 413483.3,5316821.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twam</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>154</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.195">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413450.02,5316854.19 413442.08,5316858.43 413441.83,5316858.58 413442.68,5316860.15 413446.93,5316867.97 413447.09,5316867.89 413448.29,5316867.25 413447.78,5316866.29 413454.12,5316862.81 413454.52,5316862.6 413454.26,5316862.12 413450.02,5316854.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9o</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>90</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.196">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413440.15,5316844.19 413453.77,5316836.93 413454.69,5316833.9 413450.53,5316826.12 413441.22,5316831.1 413440.55,5316831.46 413442.24,5316834.62 413436.64,5316837.6 413440.15,5316844.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbA</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>2081</ogr:use_id>
      <ogr:use>Gaststätte, Restaura</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>170</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.197">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.67,5316848.7 413440.15,5316844.19 413436.64,5316837.6 413435.15,5316834.8 413434.88,5316834.95 413431.07,5316837.07 413430.49,5316836.02 413429.23,5316836.65 413426.19,5316838.17 413431.67,5316848.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twde</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.198">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413436.04,5316889.16 413437.93,5316892.71 413436.61,5316893.4 413438.51,5316896.95 413438.07,5316897.19 413441.44,5316903.52 413441.88,5316903.28 413443.76,5316906.82 413445.06,5316906.12 413446.94,5316909.64 413447.38,5316909.41 413449.33,5316913.07 413448.47,5316915.9 413444.72,5316917.95 413444.49,5316917.51 413440.95,5316919.39 413440.24,5316918.05 413436.7,5316919.93 413436.46,5316919.49 413430.15,5316922.85 413430.38,5316923.29 413426.85,5316925.16 413427.56,5316926.49 413424.04,5316928.36 413429.14,5316937.86 413462.77,5316919.97 413464.25,5316919.18 413463.46,5316917.7 413445.56,5316884.13 413436.04,5316889.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8J</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>790</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.199">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.73,5316799.53 413531.71,5316795.24 413527.82,5316787.81 413527.29,5316786.76 413526.87,5316785.96 413522.44,5316788.26 413521.52,5316786.5 413517.86,5316788.4 413518.79,5316790.15 413523.73,5316799.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaw</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.200">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.67,5316848.7 413426.19,5316838.17 413422.49,5316840.15 413423.05,5316841.21 413418.95,5316843.4 413424.02,5316852.77 413431.67,5316848.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdc</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.201">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413434.12,5316862.67 413420.01,5316870.2 413420.86,5316871.78 413425.68,5316880.82 413439.78,5316873.29 413434.97,5316864.26 413434.12,5316862.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8K</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>192</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.202">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413417.02,5316856.51 413424.02,5316852.77 413418.95,5316843.4 413418.38,5316842.35 413416.42,5316843.41 413416.1,5316842.83 413414.35,5316843.76 413411.06,5316845.49 413411.66,5316846.6 413417.02,5316856.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd9</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.203">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413409.15,5316860.69 413417.02,5316856.51 413411.66,5316846.6 413409.35,5316847.85 413403.67,5316851.13 413409.15,5316860.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd8</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>101</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.204">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413397.4,5316866.95 413409.15,5316860.69 413403.67,5316851.13 413403.07,5316850.09 413401.05,5316846.41 413396.18,5316849.07 413397.63,5316851.83 413394.81,5316853.32 413395.39,5316854.42 413391.77,5316856.34 413397.4,5316866.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd7</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.205">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413416.96,5316887.89 413416.7,5316885.64 413425.68,5316880.82 413420.86,5316871.78 413420.01,5316870.2 413402.0,5316879.79 413402.089899998,5316879.96199998 413402.178599996,5316880.1346 413402.266099993,5316880.30779999 413402.352399991,5316880.48169999 413402.437499989,5316880.65609998 413402.521299987,5316880.83109998 413402.603899985,5316881.0068 413402.685299983,5316881.183 413402.765399981,5316881.35969999 413402.844299979,5316881.53699999 413402.921899977,5316881.71489998 413402.998299975,5316881.89329998 413403.073399998,5316882.0723 413403.147199996,5316882.25179999 413403.219799994,5316882.43169999 413403.291199993,5316882.61219998 413403.361199991,5316882.79319998 413403.429999989,5316882.97469997 413403.497499987,5316883.1567 413403.563699986,5316883.33909999 413403.628699984,5316883.52199999 413403.692299983,5316883.70529998 413403.754699981,5316883.88909998 413403.815799979,5316884.0733 413403.875499978,5316884.25789999 413403.934,5316884.443 413403.991199975,5316884.62849998 413404.047,5316884.81429998 413404.101599997,5316885.0006 413404.154799996,5316885.18719999 413404.206799995,5316885.37419999 413404.257399993,5316885.56149999 413404.306699992,5316885.74929998 413404.354699991,5316885.93729998 413404.40129999,5316886.1257 413404.446599989,5316886.31439999 413404.490599988,5316886.50339999 413404.533299987,5316886.69269998 413404.574699986,5316886.88239998 413404.614699984,5316887.0723 413404.653299983,5316887.26239999 413404.690699983,5316887.45289999 413404.726599982,5316887.64359998 413404.761299981,5316887.83459998 413404.79459998,5316888.0257 413404.826599979,5316888.21719999 413404.857199978,5316888.40879999 413404.886399978,5316888.60069998 413404.914399977,5316888.79269998 413404.940899976,5316888.98499998 413404.966099976,5316889.1774 413404.99,5316889.37 413406.86,5316889.17 413416.96,5316887.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8L</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>257</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.206">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413389.42,5316871.19 413397.4,5316866.95 413391.77,5316856.34 413391.3,5316855.46 413387.23,5316857.58 413387.81,5316858.69 413383.93,5316860.72 413389.42,5316871.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd6</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>112</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.207">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413382.28,5316874.99 413389.42,5316871.19 413383.93,5316860.72 413380.41,5316862.68 413381.19,5316864.09 413378.99,5316865.42 413377.65,5316866.14 413382.28,5316874.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd4</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>88</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.208">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413350.98,5316891.73 413346.43,5316883.19 413346.15,5316883.34 413344.44,5316884.25 413342.56,5316885.15 413342.25,5316884.6 413341.69,5316884.5 413338.92,5316886.06 413343.93,5316895.5 413350.98,5316891.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdg</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>81</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.209">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413355.62,5316904.55 413344.27,5316910.6 413332.19,5316917.05 413332.65,5316917.91 413338.31,5316928.52 413345.21,5316924.83 413344.65,5316923.77 413346.35,5316922.86 413346.92,5316923.91 413353.8,5316920.23 413352.37,5316917.56 413354.89,5316916.22 413355.09,5316916.59 413357.62,5316915.24 413357.42,5316914.87 413360.31,5316913.33 413360.29,5316913.29 413355.62,5316904.55</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9q</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>317</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.210">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413335.96,5316899.75 413343.93,5316895.5 413338.92,5316886.06 413338.06,5316884.44 413330.18,5316888.83 413335.96,5316899.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdi</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>112</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.211">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413370.94,5316959.59 413362.56,5316943.8 413359.85,5316942.96 413331.64,5316958.0 413330.83,5316960.72 413334.61,5316967.81 413337.32,5316968.62 413356.62,5316958.27 413357.07,5316957.09 413358.38,5316957.54 413359.23,5316959.17 413357.68,5316959.98 413360.43,5316965.26 413370.94,5316959.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9t</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>516</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.212">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413335.96,5316899.75 413330.18,5316888.83 413325.98,5316891.1 413326.15,5316891.38 413322.36,5316893.44 413328.04,5316903.97 413335.96,5316899.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdk</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>110</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.213">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413324.34,5316877.5 413318.8,5316880.57 413321.47,5316885.61 413327.06,5316882.65 413325.86,5316880.38 413324.34,5316877.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdl</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>38 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>36</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.214">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413515.8,5316803.77 413523.73,5316799.53 413518.79,5316790.15 413514.42,5316792.45 413513.48,5316790.93 413509.97,5316792.8 413510.8,5316794.37 413515.8,5316803.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twau</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.215">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413315.51,5316939.01 413316.24,5316940.34 413323.17,5316936.63 413322.61,5316935.57 413324.29,5316934.67 413324.86,5316935.72 413331.76,5316932.03 413326.11,5316921.43 413325.63,5316920.54 413313.59,5316926.96 413310.01,5316928.87 413315.51,5316939.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9r</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>228</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.216">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413310.01,5316928.87 413298.54,5316934.99 413303.27,5316943.8 413303.98,5316945.11 413308.0,5316942.98 413308.71,5316944.32 413313.7,5316941.68 413312.99,5316940.35 413315.51,5316939.01 413310.01,5316928.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twak</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>158</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.217">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413298.54,5316934.99 413283.55,5316942.99 413282.8,5316945.65 413288.43,5316956.22 413298.14,5316951.05 413296.24,5316947.5 413300.04,5316945.5 413303.27,5316943.8 413298.54,5316934.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9Z</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>231</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.218">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413245.28,5316963.38 413250.99,5316973.96 413254.39,5316972.13 413253.78,5316970.98 413257.83,5316968.81 413258.45,5316969.95 413258.63,5316969.85 413257.45,5316967.65 413252.95,5316959.29 413245.28,5316963.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi5</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.219">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413290.81,5316923.85 413286.22,5316914.94 413281.31,5316917.59 413279.53,5316914.36 413277.99,5316915.18 413277.04,5316913.39 413276.43,5316912.26 413267.63,5316916.99 413267.55,5316917.03 413274.68,5316930.43 413276.93,5316931.26 413290.81,5316923.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twek</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
      <ogr:use_id>2081</ogr:use_id>
      <ogr:use>Gaststätte, Restaura</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>250</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.220">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413236.94,5316967.82 413236.6,5316967.97 413243.01,5316979.93 413243.04,5316979.98 413243.52,5316979.71 413242.8,5316978.39 413248.36,5316975.36 413249.08,5316976.67 413251.69,5316975.26 413250.99,5316973.96 413245.28,5316963.38 413236.94,5316967.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi6</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>124</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.221">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413517.54,5316831.75 413519.22,5316830.85 413519.79,5316831.92 413521.2,5316831.16 413520.63,5316830.1 413525.26,5316827.63 413526.37,5316827.03 413526.7,5316826.91 413520.96,5316816.3 413520.68,5316816.47 413511.85,5316821.19 413517.54,5316831.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7p</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>126</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.222">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413241.8,5316949.93 413255.47,5316942.66 413256.21,5316940.32 413254.27,5316936.68 413254.72,5316936.44 413252.32,5316931.94 413251.87,5316932.18 413249.96,5316928.58 413238.85,5316934.5 413239.0,5316934.72 413241.19,5316933.56 413243.53,5316937.93 413237.46,5316941.17 413235.94,5316938.31 413235.69,5316938.44 413241.8,5316949.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi7</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>224</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.223">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413221.81,5316989.92 413222.27,5316990.95 413239.9,5316981.58 413242.63,5316980.13 413243.01,5316979.93 413236.6,5316967.97 413216.77,5316978.51 413222.12,5316989.77 413221.81,5316989.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twis</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>310</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.224">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413230.81,5316955.79 413241.8,5316949.93 413235.69,5316938.44 413235.44,5316938.57 413236.14,5316939.9 413232.71,5316941.72 413232.0,5316940.39 413228.14,5316942.44 413228.84,5316943.76 413225.59,5316945.48 413224.9,5316944.16 413224.69,5316944.26 413230.81,5316955.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi8</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>151</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.225">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413211.04,5316966.34 413230.81,5316955.79 413224.69,5316944.26 413224.46,5316944.39 413224.95,5316945.33 413205.4,5316955.68 413211.04,5316966.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiE</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>270</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.226">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413210.01,5316995.74 413215.64,5316992.97 413218.32,5316991.64 413221.71,5316989.97 413221.81,5316989.92 413222.12,5316989.77 413216.77,5316978.51 413216.41,5316978.76 413213.06,5316980.55 413204.65,5316985.02 413204.41,5316985.1 413209.74,5316996.03 413210.01,5316995.74</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiJ</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>55</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>170</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.227">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413211.04,5316966.34 413205.4,5316955.68 413184.57,5316966.81 413183.82,5316965.4 413183.54,5316965.55 413189.94,5316977.59 413190.08,5316977.52 413211.04,5316966.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiP</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>289</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.228">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413204.41,5316985.1 413163.63,5317006.84 413160.32,5317008.63 413165.97,5317019.27 413168.06,5317018.16 413168.59,5317017.87 413169.17,5317018.92 413168.68,5317019.19 413183.75,5317050.54 413187.03,5317057.36 413197.82,5317052.15 413197.93,5317052.1 413193.48,5317042.86 413194.3,5317042.43 413192.48,5317038.54 413191.6,5317038.96 413181.25,5317017.46 413180.34,5317017.89 413178.72,5317014.5 413178.67,5317014.39 413176.85,5317015.26 413176.13,5317013.91 413178.85,5317012.46 413196.75,5317002.93 413197.22,5317003.83 413201.07,5317001.76 413200.61,5317000.88 413209.74,5316996.03 413204.41,5316985.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj4</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>57</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1131</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.229">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413507.88,5316808.02 413515.8,5316803.77 413510.8,5316794.37 413506.5,5316796.63 413505.69,5316795.18 413502.05,5316797.04 413507.88,5316808.02</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twas</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.230">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413141.06,5316999.01 413156.48,5316990.86 413155.32,5316988.68 413156.45,5316988.08 413155.11,5316985.54 413155.02,5316985.59 413153.44,5316982.58 413152.39,5316983.14 413151.44,5316981.35 413143.63,5316985.48 413143.52,5316985.54 413144.15,5316986.64 413140.76,5316988.44 413140.18,5316987.31 413140.07,5316987.37 413136.17,5316989.43 413141.06,5316999.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBt</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>190</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.231">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413125.99,5317006.99 413141.06,5316999.01 413136.17,5316989.43 413136.15,5316989.44 413136.01,5316989.52 413136.58,5316990.63 413133.41,5316992.36 413132.82,5316991.26 413132.72,5316991.31 413128.41,5316993.55 413128.25,5316993.63 413128.84,5316994.74 413125.6,5316996.44 413125.03,5316995.33 413124.96,5316995.37 413121.21,5316997.27 413120.3,5316995.48 413120.21,5316995.53 413125.99,5317006.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBs</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>62</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>174</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.232">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413109.84,5317013.18 413124.79,5317005.28 413125.71,5317007.14 413125.99,5317006.99 413120.21,5316995.53 413120.02,5316995.64 413119.91,5316995.69 413120.13,5316996.13 413116.28,5316998.18 413116.0,5316997.65 413115.87,5316997.71 413112.16,5316999.67 413112.04,5316999.74 413112.32,5317000.27 413108.93,5317002.2 413108.65,5317001.67 413108.55,5317001.72 413105.06,5317003.58 413109.84,5317013.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyBw</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>64</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>181</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.233">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413511.85,5316821.19 413503.03,5316825.92 413508.74,5316836.48 413514.45,5316833.41 413515.02,5316834.46 413516.43,5316833.7 413515.86,5316832.65 413517.54,5316831.75 413511.85,5316821.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7n</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>122</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.234">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413507.88,5316808.02 413502.05,5316797.04 413498.51,5316798.95 413499.26,5316800.51 413495.11,5316802.74 413495.97,5316804.4 413500.03,5316812.23 413507.88,5316808.02</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaq</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.235">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413230.62,5317022.03 413219.92,5316999.52 413213.19,5317002.77 413221.23,5317019.53 413223.95,5317025.18 413230.62,5317022.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiL</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>185</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.236">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413411.14,5316837.51 413414.77,5316835.67 413410.03,5316826.94 413403.01,5316830.62 413403.03,5316830.66 413407.68,5316839.26 413411.14,5316837.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twda</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>78</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.237">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413411.14,5316837.51 413407.68,5316839.26 413411.06,5316845.49 413414.35,5316843.76 413411.14,5316837.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdb</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>27</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.238">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413242.63,5316980.13 413239.9,5316981.58 413241.97,5316985.54 413240.65,5316986.23 413238.53,5316989.65 413246.39,5317004.42 413252.67,5317001.11 413249.58,5316994.78 413249.8,5316994.65 413242.63,5316980.13</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twit</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>152</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.239">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413213.0,5317001.79 413219.33,5316998.72 413218.11,5316996.17 413211.5,5316999.28 413212.77,5317001.9 413213.0,5317001.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiK</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>21</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.240">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413512.92,5316779.03 413504.99,5316783.43 413507.72,5316788.58 413515.72,5316784.34 413512.92,5316779.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twav</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>54</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.241">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413457.9,5316862.09 413457.74,5316861.79 413458.09,5316861.61 413457.46,5316860.41 413454.26,5316862.12 413454.52,5316862.6 413454.12,5316862.81 413452.64,5316867.04 413454.36,5316867.7 413456.0,5316868.33 413456.49,5316866.71 413457.9,5316862.09</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9m</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.242">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413375.68,5316845.16 413376.11,5316845.79 413378.77,5316850.86 413383.2,5316848.53 413385.75,5316853.33 413389.2,5316851.5 413383.64,5316840.96 413375.68,5316845.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd5</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>79</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.243">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413277.04,5316913.39 413278.75,5316913.29 413282.32,5316911.49 413281.32,5316909.63 413278.06,5316911.39 413276.43,5316912.26 413277.04,5316913.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twej</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>11</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.244">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413450.13,5316873.61 413453.0,5316872.09 413454.36,5316867.7 413452.64,5316867.04 413451.34,5316868.96 413448.47,5316870.48 413450.13,5316873.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9p</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.245">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413325.86,5316880.38 413333.74,5316876.32 413332.05,5316873.13 413327.86,5316875.36 413324.23,5316877.28 413324.34,5316877.5 413325.86,5316880.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdj</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.246">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413516.46,5316775.28 413515.45,5316773.48 413513.05,5316775.1 413516.61,5316781.42 413519.12,5316780.03 413516.46,5316775.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twax</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>21</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.247">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413526.11,5316840.1 413528.5,5316838.81 413527.32,5316836.61 413521.74,5316839.54 413522.96,5316841.8 413526.11,5316840.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7q</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.248">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413499.79,5316792.81 413502.3,5316791.39 413498.57,5316784.24 413496.05,5316785.77 413499.79,5316792.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twat</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>23</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.249">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413516.77,5316845.14 413513.54,5316839.49 413511.09,5316840.82 413514.18,5316846.54 413514.47,5316846.38 413516.77,5316845.14</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7o</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.250">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413496.05,5316785.77 413493.35,5316787.41 413497.02,5316794.28 413499.79,5316792.81 413496.05,5316785.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twar</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.251">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413418.32,5316943.61 413419.39,5316945.67 413423.23,5316943.64 413423.40189999,5316943.55929999 413423.576699985,5316943.48499999 413423.754099981,5316943.41729999 413423.933899976,5316943.35619999 413424.115799997,5316943.30189999 413424.299699992,5316943.25429999 413424.485099988,5316943.21369999 413424.671999983,5316943.1799 413424.859999978,5316943.1531 413425.048899999,5316943.1333 413425.238299994,5316943.1206 413425.428099989,5316943.1148 413425.617999984,5316943.1162 413425.80769998,5316943.1246 413425.997,5316943.14 413426.185599995,5316943.1624 413426.373199991,5316943.1919 413426.559599986,5316943.22829999 413426.744599981,5316943.27159999 413426.927699977,5316943.32179999 413427.108899997,5316943.37869999 413427.287899993,5316943.44239999 413427.464399988,5316943.51259999 413427.638099984,5316943.58939999 413427.80889998,5316943.67249998 413427.976399975,5316943.76199998 413428.140499996,5316943.85759998 413428.30099999,5316943.95919998 413428.457499988,5316944.0668 413428.61,5316944.18 413428.759899981,5316944.30969999 413428.904699977,5316944.44509999 413429.044099999,5316944.58599999 413429.177899996,5316944.73219998 413429.30599999,5316944.88349998 413429.428099989,5316945.0396 413429.543999986,5316945.20039999 413429.653699983,5316945.36549999 413429.756799981,5316945.53479999 413429.853299978,5316945.70799998 413429.942999976,5316945.88469998 413430.025699999,5316946.0648 413430.101399997,5316946.24799999 413430.17,5316946.434 413430.231299994,5316946.62249998 413430.285199993,5316946.81329998 413430.331699992,5316947.006 413430.370699991,5316947.2003 413430.40209999,5316947.39599999 413430.425899989,5316947.59279998 413430.441999989,5316947.79039998 413430.450399989,5316947.98839998 413430.451199989,5316948.1866 413430.444299989,5316948.38469999 413430.429599989,5316948.58239998 413430.40739999,5316948.77939998 413430.37749999,5316948.97539998 413430.34,5316949.17 413426.62,5316952.92 413426.75,5316953.25 413475.61,5316927.36 413453.06,5316884.9 413455.45,5316883.63 413446.93,5316867.97 413442.68,5316860.15 413434.97,5316864.26 413420.86,5316871.78 413404.41,5316880.55 413404.492999988,5316880.72719998 413404.574599985,5316880.90509998 413404.654999983,5316881.0835 413404.734199981,5316881.26239999 413404.811999979,5316881.44199999 413404.888599978,5316881.62209998 413404.963799976,5316881.80269998 413405.037799999,5316881.98389998 413405.110499997,5316882.1656 413405.181899995,5316882.34779999 413405.25199999,5316882.53049999 413405.320799992,5316882.71369998 413405.38819999,5316882.89739998 413405.454399989,5316883.0815 413405.519199987,5316883.26619999 413405.582699985,5316883.45129999 413405.644899984,5316883.63679998 413405.705699982,5316883.82279998 413405.765299981,5316884.0092 413405.823499979,5316884.19609999 413405.880299978,5316884.38329999 413405.935799976,5316884.57099999 413405.99,5316884.759 413406.042799999,5316884.94739998 413406.094299998,5316885.1362 413406.144399996,5316885.32539999 413406.193199995,5316885.51489999 413406.240599994,5316885.70479998 413406.286699993,5316885.89499998 413406.331399992,5316886.0855 413406.374799991,5316886.27629999 413406.416699989,5316886.46749999 413406.457299988,5316886.65889998 413406.496599987,5316886.85059998 413406.534399986,5316887.0426 413406.570899986,5316887.23489999 413406.606099985,5316887.42739999 413406.639799984,5316887.62019998 413406.672199983,5316887.81319998 413406.703199982,5316888.0064 413406.732799981,5316888.19989999 413406.760999981,5316888.39349999 413406.78779998,5316888.58739998 413406.813299979,5316888.78139998 413406.837299979,5316888.97559998 413406.86,5316889.17 413406.883199978,5316889.36399999 413406.905099977,5316889.55809999 413406.925699977,5316889.75229998 413406.945099976,5316889.94669998 413406.963099976,5316890.1413 413406.979799975,5316890.33589999 413406.995299975,5316890.53059999 413407.0094,5316890.72549998 413407.022299999,5316890.92039998 413407.033799999,5316891.1154 413407.044099999,5316891.31049999 413407.053,5316891.50569999 413407.060699998,5316891.70089998 413407.067,5316891.89609998 413407.072099998,5316892.0914 413407.075899998,5316892.28669999 413407.078399998,5316892.48199999 413407.079499998,5316892.67739998 413407.079399998,5316892.87269998 413407.078,5316893.0681 413407.075299998,5316893.26339999 413407.071199998,5316893.45869999 413407.065899998,5316893.65399998 413407.059299999,5316893.84929998 413407.051399999,5316894.0444 413407.042199999,5316894.23959999 413407.031699999,5316894.43469999 413407.019899999,5316894.62959998 413407.0068,5316894.82459998 413406.992399975,5316895.0194 413406.976699975,5316895.21409999 413406.959699976,5316895.40869999 413406.941399976,5316895.60319998 413406.921899977,5316895.79759998 413406.900999977,5316895.99179997 413406.878899978,5316896.1859 413406.855399978,5316896.37989999 413406.830699979,5316896.57359999 413406.80469998,5316896.76729998 413406.77739998,5316896.96069998 413406.748799981,5316897.1539 413406.719,5316897.347 413406.687899983,5316897.53989999 413406.655499983,5316897.73259998 413406.621699984,5316897.92499998 413406.586699985,5316898.1173 413406.550499986,5316898.30919999 413406.512899987,5316898.50099999 413406.474099988,5316898.69249998 413406.433999989,5316898.88369998 413406.39259999,5316899.0747 413406.35,5316899.26529999 413406.306099992,5316899.45569999 413406.26099999,5316899.64579998 413406.214499995,5316899.83559998 413406.166799996,5316900.0251 413406.117899997,5316900.21429999 413406.067699998,5316900.40309999 413406.0162,5316900.59159999 413405.963499976,5316900.77969998 413405.909599977,5316900.96749998 413405.854399978,5316901.1549 413405.79789998,5316901.34199999 413405.740199981,5316901.52869999 413405.681299983,5316901.71499998 413405.621199984,5316901.90089998 413405.559799986,5316902.0863 413405.497099987,5316902.27139999 413405.433299989,5316902.45609999 413405.368199991,5316902.64029998 413405.301899992,5316902.82409998 413405.234299994,5316903.0074 413405.165599996,5316903.1903 413405.095599998,5316903.37279999 413405.024499999,5316903.55469999 413404.952099976,5316903.73619998 413404.878499978,5316903.91719998 413404.80369998,5316904.0977 413404.727699982,5316904.27769999 413404.650499984,5316904.45719999 413404.572199986,5316904.63619998 413404.492599988,5316904.81469998 413404.41189999,5316904.99259998 413404.33,5316905.17 413404.246799994,5316905.34669999 413404.162399996,5316905.52279999 413404.076799998,5316905.69829998 413403.990099975,5316905.87329998 413403.902199977,5316906.0477 413403.813199979,5316906.22159999 413403.723099982,5316906.39479999 413403.631799984,5316906.56749999 413403.539299986,5316906.73949998 413403.445799989,5316906.91089998 413403.351099991,5316907.0817 413403.255199994,5316907.25189999 413403.158299996,5316907.42139999 413403.060199998,5316907.59029999 413402.961099976,5316907.75859998 413402.860799978,5316907.92619998 413402.759399981,5316908.0931 413402.656999983,5316908.25929999 413402.553399986,5316908.42489999 413402.448699989,5316908.58979999 413402.343,5316908.754 413402.236199994,5316908.91749998 413402.128299997,5316909.0803 413402.0193,5316909.24239999 413401.909199977,5316909.40379999 413401.79809998,5316909.56439999 413401.685899983,5316909.72439998 413401.572699986,5316909.88349998 413401.458399988,5316910.0419 413401.343099991,5316910.1996 413401.226799994,5316910.35649999 413401.109399997,5316910.51259999 413400.990999975,5316910.66799998 413400.871599978,5316910.82249998 413400.751099981,5316910.97629998 413400.629699984,5316911.1293 413400.507199987,5316911.28139999 413400.38369999,5316911.43279999 413400.259299993,5316911.58329999 413400.133799997,5316911.73309998 413400.0074,5316911.88189998 413399.88,5316912.03 413399.751899981,5316912.1768 413399.622799984,5316912.32269999 413399.492799988,5316912.46769999 413399.361799991,5316912.61189999 413399.229799994,5316912.75509998 413399.096799998,5316912.89749998 413398.962999976,5316913.039 413398.828099979,5316913.1797 413398.692399982,5316913.31939999 413398.555699986,5316913.45819999 413398.418099989,5316913.59599999 413398.279599993,5316913.73299998 413398.140099996,5316913.86899998 413397.999799975,5316914.0041 413397.858499978,5316914.1383 413397.716399982,5316914.27149999 413397.573399986,5316914.40379999 413397.429499989,5316914.53509999 413397.284699993,5316914.66539998 413397.139099996,5316914.79479998 413396.992599975,5316914.92319998 413396.845199979,5316915.0506 413396.697,5316915.177 413396.547999986,5316915.30239999 413396.39809999,5316915.42689999 413396.247399994,5316915.55029999 413396.095799998,5316915.67269998 413395.943499976,5316915.79409998 413395.79029998,5316915.91449998 413395.636399984,5316916.0339 413395.481599988,5316916.1522 413395.326099992,5316916.26949999 413395.169799996,5316916.38579999 413395.0127,5316916.50099999 413394.854899978,5316916.61509998 413394.696299982,5316916.72819998 413394.536899986,5316916.84029998 413394.37679999,5316916.95119998 413394.215899995,5316917.0611 413394.054399999,5316917.1699 413393.892099977,5316917.27769999 413393.729099982,5316917.38429999 413393.565299986,5316917.48989999 413393.40089999,5316917.59439998 413393.235799994,5316917.69769998 413393.07,5316917.8 413398.05,5316927.2 413401.79,5316934.26 413405.54,5316941.34 413409.29,5316948.41 413418.32,5316943.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8I</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>4399</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.252">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413353.42,5316922.88 413318.77,5316941.44 413327.55,5316957.82 413362.14,5316939.3 413353.42,5316922.88</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9s</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>730</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.253">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413526.7,5316826.91 413531.7,5316836.89 413549.94,5316826.76 413548.01,5316823.17 413546.68,5316823.87 413544.99,5316820.71 413546.31,5316819.99 413544.79,5316817.2 413540.32,5316819.56 413541.06,5316820.84 413537.95,5316822.6 413537.21,5316821.32 413526.7,5316826.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7s</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>217</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.254">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413245.09,5317032.14 413238.54,5317018.3 413230.62,5317022.03 413223.95,5317025.18 413231.72,5317041.33 413233.3,5317044.63 413247.7,5317037.89 413247.79,5317037.85 413245.09,5317032.14</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiM</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>347</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.255">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413262.85,5317022.7 413245.09,5317032.14 413247.79,5317037.85 413247.7,5317037.89 413247.76,5317038.02 413250.92,5317044.72 413269.08,5317035.07 413263.06,5317022.59 413262.85,5317022.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiN</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>283</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.256">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413269.08,5317035.07 413273.0,5317043.18 413281.63,5317038.55 413287.63,5317035.34 413279.14,5317019.49 413274.63,5317011.11 413270.12,5317013.52 413267.17,5317008.04 413267.03,5317008.12 413265.8,5317005.79 413262.36,5317007.62 413257.17,5317010.38 413257.34,5317010.73 413257.13,5317010.84 413262.85,5317022.7 413263.06,5317022.59 413269.08,5317035.07</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiO</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>533</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.257">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413487.53,5316803.72 413493.78,5316800.18 413490.39,5316793.65 413483.58,5316797.77 413485.09,5316800.05 413487.53,5316803.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twap</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>55</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.258">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413425.72,5316818.53 413418.09,5316822.61 413422.23,5316830.57 413427.09,5316827.96 413427.99,5316829.7 413427.46,5316829.98 413430.23,5316835.29 413428.9,5316836.01 413429.23,5316836.65 413430.49,5316836.02 413431.07,5316837.07 413434.88,5316834.95 413434.69,5316834.59 413430.91,5316827.82 413430.85,5316827.72 413430.92,5316827.68 413428.57,5316823.62 413428.67,5316823.57 413425.73,5316818.52 413425.72,5316818.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdd</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>120</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.259">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413320.05,5316947.35 413308.46,5316953.46 413309.42,5316955.24 413309.65,5316955.66 413311.62,5316959.35 413312.26,5316960.53 413319.76,5316956.5 413321.06,5316958.93 413321.17,5316959.14 413322.98,5316958.19 413325.3,5316956.99 413320.05,5316947.35</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twal</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>119</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.260">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413805.71,5316669.15 413816.4,5316665.6 413808.97,5316643.34 413804.66,5316644.78 413804.41,5316644.04 413801.08,5316645.14 413801.33,5316645.89 413796.97,5316647.35 413802.2,5316663.07 413796.98,5316664.82 413799.14,5316671.35 413805.71,5316669.15</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuT1</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>338</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.261">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413764.48,5316685.81 413765.53,5316689.0 413791.07,5316680.63 413794.82,5316691.93 413793.98,5316692.17 413808.1,5316734.55 413809.02,5316734.24 413810.3,5316738.55 413793.54,5316743.18 413796.46,5316753.71 413823.16,5316746.36 413820.85,5316738.2 413820.32,5316736.69 413819.13,5316732.66 413818.66,5316731.07 413817.11,5316726.37 413816.59,5316724.76 413813.26,5316714.7 413809.96,5316704.71 413809.3,5316702.69 413807.07,5316695.94 413805.18,5316690.27 413804.66,5316688.69 413802.5,5316682.12 413802.69,5316682.06 413801.72,5316679.14 413800.42,5316675.2 413799.14,5316671.35 413796.98,5316664.82 413795.77,5316661.18 413795.58,5316661.24 413794.31,5316658.26 413759.25,5316669.89 413759.78,5316671.48 413756.69,5316672.51 413753.22,5316673.66 413759.24,5316687.53 413759.48,5316687.46 413759.62,5316687.68 413761.44,5316686.98 413764.48,5316685.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSZ</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
      <ogr:use_id>3021</ogr:use_id>
      <ogr:use>Schule</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1823</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.262">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413749.61,5316563.18 413752.7,5316572.44 413753.31,5316572.24 413768.14,5316617.09 413773.76,5316615.19 413776.98,5316624.96 413792.01,5316619.93 413828.83,5316607.79 413834.35,5316606.01 413831.13,5316596.25 413836.74,5316594.4 413818.85,5316540.23 413804.14,5316545.09 413804.0,5316544.68 413798.45,5316546.52 413798.55,5316546.86 413786.64,5316550.78 413782.42,5316552.19 413764.99,5316558.03 413749.61,5316563.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>413797.65,5316599.77 413794.13,5316589.04 413788.58,5316590.89 413781.32,5316568.99 413787.28,5316567.09 413791.58,5316565.7 413797.27,5316563.73 413804.43,5316585.67 413799.22,5316587.37 413802.78,5316598.12 413797.65,5316599.77</gml:coordinates></gml:LinearRing></gml:innerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuU5</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>4305</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.263">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413735.85,5316639.27 413755.41,5316632.73 413751.03,5316619.63 413739.44,5316623.5 413735.77,5316624.73 413731.48,5316626.16 413735.85,5316639.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUa</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>64</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>285</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.264">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413710.31,5316647.76 413727.83,5316641.96 413723.97,5316630.38 413718.25,5316632.31 413714.98,5316622.61 413714.92,5316622.42 413708.37,5316625.93 413711.27,5316634.68 413706.54,5316636.25 413706.47,5316636.27 413710.31,5316647.76</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUe</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>66</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>296</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.265">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413697.11,5316653.92 413710.93,5316649.32 413710.31,5316647.76 413706.47,5316636.27 413706.54,5316636.25 413705.92,5316634.6 413702.44,5316628.05 413699.38,5316622.3 413686.62,5316629.29 413692.53,5316640.13 413695.81,5316650.01 413697.11,5316653.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUg</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>68</ogr:house_nr>
      <ogr:use_id>3010</ogr:use_id>
      <ogr:use>Verwaltung</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>408</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.266">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413734.46,5316576.44 413731.5,5316577.52 413713.73,5316583.82 413718.25,5316599.33 413717.88,5316599.49 413718.59,5316601.83 413718.94,5316601.69 413724.37,5316599.89 413722.08,5316592.98 413735.05,5316588.38 413733.52,5316583.69 413743.61,5316580.18 413746.52,5316579.32 413744.52,5316573.26 413741.63,5316574.21 413734.46,5316576.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUb</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>347</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.267">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413799.14,5316671.35 413800.42,5316675.2 413807.19,5316673.59 413805.71,5316669.15 413799.14,5316671.35</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuT2</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>30</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.268">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413754.77,5316605.82 413752.13,5316606.67 413753.86,5316611.89 413756.49,5316611.02 413754.77,5316605.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUc</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.269">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413746.52,5316579.32 413743.61,5316580.18 413743.64,5316580.28 413751.82,5316605.0 413754.74,5316604.02 413746.52,5316579.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuU9</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>80</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.270">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413751.82,5316605.0 413743.64,5316580.28 413743.61,5316580.18 413733.52,5316583.69 413735.05,5316588.38 413741.62,5316608.53 413751.82,5316605.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUd</ogr:gmlid>
      <ogr:street>Eisenbahnstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>281</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.271">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413179.92,5316669.55 413184.06,5316667.33 413188.5,5316664.94 413190.55,5316668.74 413198.71,5316664.35 413196.67,5316660.57 413197.12,5316660.33 413188.29,5316643.91 413189.2,5316643.42 413179.35,5316624.72 413178.45,5316625.2 413173.65,5316616.18 413174.33,5316615.81 413169.19,5316606.14 413168.45,5316606.54 413163.87,5316597.88 413152.98,5316603.66 413155.12,5316607.7 413129.21,5316621.46 413127.23,5316617.7 413116.81,5316623.22 413129.42,5316647.0 413144.06,5316639.22 413145.74,5316642.37 413167.3,5316630.98 413177.22,5316649.86 413171.18,5316653.11 413179.92,5316669.55</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TweD</ogr:gmlid>
      <ogr:street>Engelbergerstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>3021</ogr:use_id>
      <ogr:use>Schule</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>2152</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.272">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413349.74,5317179.47 413358.88,5317176.61 413358.82,5317176.41 413357.38,5317171.72 413352.84,5317157.18 413354.62,5317156.63 413354.54,5317156.38 413342.16,5317160.28 413342.23,5317160.51 413343.79,5317160.03 413344.12,5317161.12 413345.93,5317167.02 413348.24,5317174.61 413349.69,5317179.29 413349.74,5317179.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7V</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>197</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.273">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413323.22,5317021.44 413327.46,5317029.39 413338.5,5317023.46 413336.42,5317019.58 413335.1,5317020.29 413333.15,5317016.11 413323.22,5317021.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa6</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>106</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.274">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413343.78,5317150.33 413344.29,5317151.9 413339.91,5317153.32 413339.99,5317153.55 413341.28,5317157.55 413342.16,5317160.28 413354.54,5317156.38 413353.86,5317154.24 413354.0,5317154.19 413353.85,5317153.71 413354.8,5317153.41 413354.83,5317153.5 413355.22,5317153.38 413355.19,5317153.28 413355.28,5317153.25 413355.16,5317152.87 413355.06,5317152.9 413354.28,5317150.42 413354.37,5317150.39 413354.25,5317150.0 413354.15,5317150.03 413354.12,5317149.94 413353.73,5317150.06 413353.76,5317150.16 413352.82,5317150.46 413352.66,5317149.97 413352.52,5317150.02 413351.82,5317147.81 413343.78,5317150.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7X</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.275">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413314.75,5317005.56 413323.22,5317021.44 413333.15,5317016.11 413324.64,5317000.25 413314.75,5317005.56</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa8</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>202</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.276">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413349.14,5317139.23 413348.04,5317139.56 413337.25,5317142.93 413336.73,5317143.09 413337.58,5317145.86 413338.81,5317149.86 413338.89,5317150.12 413343.3,5317148.77 413343.78,5317150.33 413351.82,5317147.81 413351.11,5317145.55 413351.25,5317145.51 413351.1,5317145.02 413352.05,5317144.73 413352.08,5317144.82 413352.47,5317144.7 413352.44,5317144.6 413352.54,5317144.57 413352.41,5317144.18 413352.32,5317144.21 413351.54,5317141.74 413351.64,5317141.71 413351.52,5317141.32 413351.42,5317141.35 413351.39,5317141.25 413351.0,5317141.37 413351.03,5317141.47 413350.08,5317141.77 413349.93,5317141.28 413349.79,5317141.33 413349.14,5317139.23</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7W</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.277">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413309.61,5316995.92 413314.75,5317005.56 413324.64,5317000.25 413327.78,5316998.57 413323.93,5316991.29 413325.27,5316990.58 413324.0,5316988.18 413309.61,5316995.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaa</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>166</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.278">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413337.25,5317142.93 413348.04,5317139.56 413349.14,5317139.23 413349.01,5317138.82 413344.2,5317123.46 413343.06,5317123.82 413332.3,5317127.22 413337.25,5317142.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8w</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>206</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.279">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413309.61,5316995.92 413324.0,5316988.18 413324.57,5316987.87 413319.97,5316979.33 413319.68,5316978.78 413317.18,5316980.14 413316.21,5316978.4 413316.05,5316978.05 413315.94,5316977.8 413315.89,5316977.83 413314.59,5316978.52 413303.49,5316984.45 413303.62,5316984.69 413304.74,5316986.79 413309.61,5316995.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twad</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>213</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.280">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413332.3,5317127.22 413343.06,5317123.82 413344.2,5317123.46 413339.4,5317108.19 413339.32,5317107.92 413338.14,5317108.29 413328.79,5317111.22 413327.38,5317111.65 413332.3,5317127.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8u</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>204</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.281">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413296.91,5316972.11 413303.49,5316984.45 413314.59,5316978.52 413312.74,5316975.1 413314.04,5316974.4 413311.13,5316968.99 413309.81,5316969.7 413307.95,5316966.21 413296.91,5316972.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaf</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>185</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.282">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413336.08,5317097.6 413329.31,5317099.73 413329.22,5317099.46 413325.51,5317100.63 413325.6,5317100.9 413328.79,5317111.22 413338.14,5317108.29 413339.32,5317107.92 413336.08,5317097.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8z</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>120</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.283">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413292.67,5316964.17 413296.91,5316972.11 413307.95,5316966.21 413303.74,5316958.27 413302.41,5316958.98 413292.67,5316964.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twah</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>113</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.284">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413316.01,5317052.46 413311.43,5317043.86 413300.83,5317049.53 413305.46,5317058.1 413306.65,5317057.47 413316.01,5317052.46</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhT</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>117</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.285">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413306.84,5317035.26 413296.88,5317040.6 413296.27,5317040.92 413300.83,5317049.53 413311.43,5317043.86 413306.84,5317035.26</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhU</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>22 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>117</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.286">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413288.43,5316956.22 413292.67,5316964.17 413302.41,5316958.98 413300.09,5316954.66 413301.41,5316953.95 413299.46,5316950.34 413298.14,5316951.05 413288.43,5316956.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaj</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>105</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.287">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413306.84,5317035.26 413306.86,5317035.25 413302.64,5317027.31 413302.6,5317027.33 413292.64,5317032.66 413293.52,5317034.31 413289.46,5317036.48 413291.87,5317040.98 413295.93,5317038.82 413296.88,5317040.6 413306.84,5317035.26</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhW</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>125</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.288">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413259.95,5316902.49 413267.63,5316916.99 413276.43,5316912.26 413278.06,5316911.39 413277.91,5316911.11 413277.82,5316911.17 413276.65,5316911.79 413275.53,5316909.74 413276.66,5316909.11 413271.2,5316899.33 413270.18,5316899.9 413269.0,5316897.91 413270.08,5316897.33 413270.01,5316897.15 413259.95,5316902.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twel</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>183</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.289">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413302.64,5317027.31 413302.47,5317026.99 413301.27,5317024.74 413294.14,5317011.44 413294.12,5317011.45 413284.12,5317016.81 413291.26,5317030.1 413292.48,5317032.37 413292.64,5317032.66 413302.6,5317027.33 413302.64,5317027.31</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhX</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>204</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.290">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413270.01,5316897.15 413269.89,5316896.97 413268.8,5316897.55 413267.68,5316895.46 413268.8,5316894.86 413263.66,5316884.96 413262.27,5316885.68 413261.07,5316883.39 413252.24,5316888.14 413259.95,5316902.49 413270.01,5316897.15</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twem</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>180</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.291">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413294.14,5317011.44 413289.66,5317003.05 413289.64,5317003.06 413280.26,5317008.09 413279.64,5317008.42 413284.12,5317016.81 413294.12,5317011.45 413294.14,5317011.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhY</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.292">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413246.01,5316876.51 413252.2,5316888.16 413252.24,5316888.14 413261.07,5316883.39 413262.46,5316882.65 413260.06,5316878.11 413261.33,5316877.44 413258.8,5316872.73 413257.41,5316873.46 413256.16,5316871.1 413246.01,5316876.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twen</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>161</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.293">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413412.35,5317199.99 413421.85,5317194.76 413398.73,5317152.55 413389.26,5317157.75 413412.35,5317199.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzo</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>521</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.294">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413289.66,5317003.05 413282.68,5316990.08 413281.47,5316987.82 413281.37,5316987.64 413272.0,5316992.67 413272.02,5316992.71 413272.1,5316992.86 413273.31,5316995.11 413278.95,5317005.64 413280.09,5317005.02 413281.31,5317007.31 413280.17,5317007.92 413280.26,5317008.09 413289.64,5317003.06 413289.66,5317003.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi1</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>183</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.295">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413246.01,5316876.51 413256.16,5316871.1 413257.55,5316870.35 413255.69,5316866.79 413254.27,5316867.53 413252.01,5316863.21 413241.87,5316868.74 413246.01,5316876.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twep</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.296">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413277.14,5316979.7 413259.24,5316989.29 413259.09,5316989.37 413259.18,5316989.54 413266.9,5316985.39 413271.0,5316993.25 413272.02,5316992.71 413272.0,5316992.67 413281.37,5316987.64 413277.14,5316979.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi2</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.297">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413236.67,5316858.97 413241.87,5316868.74 413252.01,5316863.21 413250.02,5316859.35 413251.4,5316858.64 413251.72,5316858.45 413249.71,5316854.73 413247.98,5316855.66 413246.82,5316853.49 413236.67,5316858.97</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tweq</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>136</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.298">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413270.15,5316966.56 413262.74,5316970.53 413261.85,5316971.01 413263.16,5316973.38 413262.66,5316973.65 413265.08,5316978.04 413265.58,5316977.76 413266.28,5316979.02 413256.42,5316984.32 413259.09,5316989.37 413259.24,5316989.29 413277.14,5316979.7 413270.15,5316966.56</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi3</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>206</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.299">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413231.53,5316849.31 413236.67,5316858.97 413246.82,5316853.49 413245.54,5316851.01 413247.15,5316850.13 413245.18,5316846.39 413244.88,5316846.56 413243.56,5316847.24 413241.67,5316843.64 413240.82,5316844.12 413231.53,5316849.31</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twer</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>135</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.300">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413266.63,5316958.93 413266.2,5316959.16 413264.14,5316955.3 413261.79,5316954.57 413252.95,5316959.29 413257.45,5316967.65 413260.45,5316966.09 413262.74,5316970.53 413270.15,5316966.56 413268.14,5316962.79 413268.58,5316962.55 413266.63,5316958.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi4</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>154</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.301">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413227.22,5316841.21 413231.53,5316849.31 413240.82,5316844.12 413238.61,5316839.77 413240.04,5316839.03 413238.23,5316835.49 413227.22,5316841.21</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twes</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.302">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413247.3,5316923.09 413245.05,5316918.84 413244.83,5316918.96 413243.35,5316916.25 413232.23,5316922.14 413230.37,5316923.12 413232.73,5316927.38 413234.43,5316926.44 413235.39,5316928.1 413236.96,5316930.97 413238.27,5316930.25 413240.09,5316933.52 413238.74,5316934.23 413238.85,5316934.5 413249.96,5316928.58 413247.09,5316923.2 413247.3,5316923.09</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi9</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>182</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.303">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413239.12,5316908.31 413228.53,5316913.95 413228.92,5316914.7 413231.16,5316919.01 413232.07,5316918.54 413233.44,5316921.18 413232.54,5316921.65 413232.09,5316921.88 413232.23,5316922.14 413243.35,5316916.25 413239.12,5316908.31</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twic</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>38 a</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>105</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.304">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413220.94,5316829.39 413227.22,5316841.21 413238.23,5316835.49 413232.77,5316825.32 413231.45,5316826.03 413230.63,5316824.43 413220.94,5316829.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twet</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>163</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.305">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413362.02,5317221.17 413356.34,5317222.69 413351.81,5317223.9 413356.21,5317240.24 413365.88,5317237.53 413364.76,5317233.38 413365.24,5317233.25 413362.02,5317221.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyzM</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>176</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.306">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413362.02,5317221.17 413373.82,5317217.74 413370.85,5317208.25 413368.17,5317208.99 413359.06,5317211.84 413352.9,5317213.8 413356.34,5317222.69 413362.02,5317221.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyzO</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>4 a</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>181</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.307">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413233.48,5316897.73 413222.92,5316903.38 413228.14,5316913.19 413228.53,5316913.95 413239.12,5316908.31 413233.48,5316897.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twib</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>144</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.308">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413216.92,5316821.82 413220.94,5316829.39 413230.63,5316824.43 413231.95,5316823.76 413230.07,5316820.15 413228.7,5316820.89 413226.47,5316816.63 413216.92,5316821.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tweu</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>101</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.309">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413228.35,5316888.02 413218.16,5316893.46 413217.75,5316893.68 413221.24,5316900.26 413222.7,5316903.03 413222.89,5316903.39 413222.92,5316903.38 413233.48,5316897.73 413233.29,5316897.37 413231.82,5316894.6 413228.35,5316888.02</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twid</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>132</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.310">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413212.67,5316813.84 413216.92,5316821.82 413226.47,5316816.63 413224.26,5316812.4 413225.53,5316811.68 413223.59,5316807.99 413212.67,5316813.84</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twev</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.311">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413223.18,5316878.31 413213.02,5316883.72 413218.16,5316893.46 413228.35,5316888.02 413223.18,5316878.31</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twii</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>127</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.312">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413208.47,5316805.95 413212.67,5316813.84 413223.59,5316807.99 413221.6,5316804.21 413220.33,5316804.84 413218.06,5316800.77 413208.47,5316805.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twew</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.313">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413218.4,5316869.34 413208.61,5316874.55 413208.28,5316874.73 413213.02,5316883.72 413223.18,5316878.31 413218.4,5316869.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twij</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>117</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.314">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413204.17,5316797.86 413208.47,5316805.95 413218.06,5316800.77 413215.74,5316796.59 413216.95,5316795.93 413214.98,5316792.09 413204.17,5316797.86</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twex</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>105</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.315">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413218.4,5316869.34 413209.94,5316853.47 413200.14,5316858.69 413208.61,5316874.55 413218.4,5316869.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twik</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>200</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.316">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413199.98,5316790.0 413204.17,5316797.86 413214.98,5316792.09 413213.2,5316788.6 413211.88,5316789.3 413209.51,5316784.88 413199.98,5316790.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twey</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.317">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413209.94,5316853.47 413208.76,5316851.25 413208.97,5316851.14 413208.71,5316850.66 413210.02,5316849.95 413208.6,5316847.28 413207.29,5316847.97 413207.04,5316847.49 413206.82,5316847.61 413205.69,5316845.52 413197.1,5316850.1 413197.86,5316851.51 413194.06,5316853.5 413194.49,5316854.33 413196.99,5316859.07 413197.55,5316860.06 413200.14,5316858.69 413209.94,5316853.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twin</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>125</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.318">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413199.98,5316790.0 413209.51,5316784.88 413207.26,5316780.8 413208.55,5316780.05 413206.51,5316776.14 413195.76,5316782.03 413199.98,5316790.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twez</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.319">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413205.69,5316845.52 413204.52,5316843.31 413204.74,5316843.2 413204.48,5316842.71 413205.79,5316842.01 413204.37,5316839.35 413203.05,5316840.07 413202.79,5316839.58 413202.57,5316839.7 413201.42,5316837.55 413189.03,5316844.17 413189.53,5316845.12 413192.11,5316849.77 413192.58,5316850.63 413196.32,5316848.61 413197.1,5316850.1 413205.69,5316845.52</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twip</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>125</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.320">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413191.63,5316774.28 413195.76,5316782.03 413206.51,5316776.14 413204.69,5316772.57 413203.28,5316773.33 413201.04,5316769.18 413191.63,5316774.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TweA</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>100</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.321">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413201.42,5316837.55 413196.33,5316827.91 413186.24,5316833.32 413189.94,5316840.32 413185.22,5316842.8 413186.5,5316845.47 413189.01,5316844.13 413189.03,5316844.17 413201.42,5316837.55</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhL</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>141</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.322">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413175.15,5316812.82 413175.35,5316812.71 413176.87,5316811.85 413183.19,5316823.7 413191.67,5316819.17 413180.6,5316798.41 413172.32,5316803.28 413170.81,5316804.17 413170.61,5316804.29 413175.15,5316812.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhK</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>243</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.323">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413134.96,5316765.33 413138.75,5316762.93 413148.53,5316757.7 413141.07,5316743.63 413131.28,5316748.89 413136.05,5316757.87 413131.88,5316760.58 413134.96,5316765.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjk</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>204</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.324">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413368.17,5317208.99 413363.84,5317195.27 413355.51,5317197.87 413354.76,5317198.11 413355.98,5317202.02 413357.83,5317207.93 413359.06,5317211.84 413368.17,5317208.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyyT</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>137</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.325">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413131.28,5316748.89 413141.07,5316743.63 413133.57,5316729.52 413133.46,5316729.58 413124.14,5316735.15 413124.01,5316735.22 413131.28,5316748.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjj</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>175</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.326">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413124.14,5316735.15 413133.46,5316729.58 413132.65,5316728.05 413131.77,5316726.38 413133.66,5316725.36 413139.85,5316722.03 413134.01,5316711.04 413117.2,5316719.98 413116.33,5316720.44 413123.59,5316734.1 413124.14,5316735.15</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwjH</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>64</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>292</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.327">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413365.22,5317093.77 413376.57,5317089.59 413364.53,5317069.98 413357.95,5317074.02 413365.22,5317093.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL001000g5ClW</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>7a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>216</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.328">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413355.27,5317197.1 413355.51,5317197.87 413363.84,5317195.27 413364.6,5317195.03 413360.37,5317181.47 413358.94,5317176.8 413358.88,5317176.61 413349.74,5317179.47 413349.8,5317179.67 413351.25,5317184.34 413353.41,5317191.21 413355.27,5317197.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzw</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>184</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.329">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413332.33,5317038.51 413337.85,5317048.86 413339.92,5317052.76 413340.8,5317054.4 413342.42,5317053.51 413343.65,5317052.84 413353.95,5317047.19 413348.82,5317037.83 413346.06,5317039.35 413343.75,5317038.66 413341.13,5317033.78 413332.33,5317038.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa3</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>234</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.330">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413327.46,5317029.39 413332.33,5317038.51 413341.13,5317033.78 413343.35,5317032.59 413341.48,5317029.08 413338.5,5317023.46 413327.46,5317029.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa4</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>9 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>129</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.331">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413191.67,5316895.11 413193.4,5316898.66 413195.72,5316903.4 413201.6,5316900.3 413203.05,5316898.02 413199.64,5316890.87 413191.67,5316895.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twif</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>81</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.332">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413235.39,5316928.1 413234.43,5316926.44 413232.73,5316927.38 413230.37,5316923.12 413226.96,5316924.93 413225.95,5316925.46 413229.41,5316931.46 413235.39,5316928.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twia</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>38</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.333">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413338.17,5316980.63 413330.56,5316984.65 413331.76,5316986.85 413333.96,5316985.65 413338.0,5316993.07 413343.4,5316990.26 413340.21,5316984.39 413338.17,5316980.63</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twac</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2111</ogr:use_id>
      <ogr:use>Fabrik</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>73</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.334">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413191.67,5316895.11 413199.64,5316890.87 413202.92,5316889.11 413202.84,5316888.97 413202.77,5316888.85 413202.83,5316888.65 413196.12,5316886.55 413195.18,5316889.6 413192.16,5316888.65 413191.21,5316891.7 413188.16,5316890.76 413187.18,5316893.81 413184.14,5316892.89 413183.04,5316896.14 413187.24,5316897.43 413187.25,5316897.46 413191.67,5316895.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiD</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>79</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.335">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413262.36,5317007.62 413258.6,5316999.8 413253.42,5317002.62 413257.17,5317010.38 413262.36,5317007.62</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhZ</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>51</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.336">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413270.12,5317013.52 413274.63,5317011.11 413275.43,5317010.68 413272.49,5317005.19 413267.17,5317008.04 413270.12,5317013.52</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twi0</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>38</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.337">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413197.79,5316868.96 413194.05,5316861.92 413188.7,5316864.77 413192.46,5316871.82 413197.79,5316868.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twil</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>48</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.338">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413193.42,5316873.62 413197.16,5316880.65 413202.47,5316877.82 413198.73,5316870.75 413193.42,5316873.62</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twim</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>48</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.339">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413185.22,5316842.8 413179.52,5316830.85 413172.47,5316834.61 413179.5,5316849.21 413186.5,5316845.47 413185.22,5316842.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhM</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>129</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.340">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413173.5,5316827.44 413176.27,5316832.42 413181.25,5316829.68 413178.5,5316824.7 413173.5,5316827.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhJ</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>32</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.341">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413137.95,5316769.94 413151.25,5316762.82 413148.53,5316757.7 413138.75,5316762.93 413134.96,5316765.33 413137.95,5316769.94</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjl</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>88</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.342">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413339.72,5317002.35 413347.71,5316998.19 413343.4,5316990.26 413338.0,5316993.07 413335.4,5316994.47 413339.72,5317002.35</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa9</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>81</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.343">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413328.97,5316991.15 413326.31,5316992.56 413329.07,5316997.92 413331.76,5316996.43 413328.97,5316991.15</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twab</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>18</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.344">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413328.46,5316971.75 413330.38,5316975.28 413333.9,5316973.37 413331.98,5316969.84 413328.46,5316971.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twae</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.345">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413309.65,5316955.66 413307.6,5316956.76 413309.58,5316960.44 413311.62,5316959.35 413309.65,5316955.66</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twai</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>10</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.346">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413265.8,5316880.53 413267.85,5316879.43 413263.18,5316870.41 413261.07,5316871.52 413265.8,5316880.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tweo</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.347">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413188.02,5316852.6 413182.52,5316855.5 413183.43,5316857.38 413188.98,5316854.41 413188.02,5316852.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twio</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>13</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.348">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413351.82,5317016.31 413346.24,5317019.31 413348.26,5317023.39 413346.25,5317024.39 413348.28,5317028.34 413350.2,5317027.31 413352.6,5317026.02 413360.6,5317021.87 413358.03,5317017.14 413353.55,5317019.58 413351.82,5317016.31</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa5</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>95</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.349">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413310.36,5317123.82 413307.67,5317127.21 413327.82,5317150.67 413336.73,5317143.09 413337.25,5317142.93 413332.3,5317127.22 413327.38,5317111.65 413327.01,5317111.77 413324.22,5317113.77 413316.9,5317119.02 413314.08,5317115.11 413313.05,5317113.68 413309.85,5317109.19 413309.67,5317109.33 413309.82,5317109.53 413309.52,5317109.75 413302.24,5317099.65 413287.8,5317109.91 413282.97,5317113.63 413286.63,5317118.01 413287.77,5317119.37 413294.29,5317127.16 413300.86,5317123.95 413306.78,5317119.67 413306.915799977,5317119.52899999 413307.061399998,5317119.39809999 413307.215699995,5317119.27769999 413307.37819999,5317119.1685 413307.547999986,5317119.071 413307.724199982,5317118.98569997 413307.905899977,5317118.91299998 413308.092399998,5317118.85329998 413308.282499993,5317118.80689998 413308.475499988,5317118.77389998 413308.670299983,5317118.75469998 413308.866,5317118.749 413309.061699998,5317118.75709998 413309.256399994,5317118.77919998 413309.448999989,5317118.81489998 413309.638599984,5317118.86409998 413309.824199979,5317118.92649998 413310.0051,5317119.0019 413310.180099995,5317119.0897 413310.348599991,5317119.1897 413310.509499987,5317119.30139999 413310.662199983,5317119.42399999 413310.80589998,5317119.55719999 413310.94,5317119.7 413311.062499998,5317119.85309998 413311.173799996,5317120.0145 413311.273399993,5317120.1834 413311.360699991,5317120.35889999 413311.435499989,5317120.54019999 413311.497299987,5317120.72629998 413311.545699986,5317120.91629998 413311.580599985,5317121.1092 413311.601699985,5317121.30409999 413311.608999985,5317121.5 413311.602399985,5317121.69599998 413311.582,5317121.891 413311.547899986,5317122.0841 413311.500099987,5317122.27429999 413311.438999989,5317122.46059999 413311.364899991,5317122.64219998 413311.278099993,5317122.81799998 413311.17899999,5317122.98729998 413311.068299998,5317123.1491 413310.946399976,5317123.30269999 413310.813899979,5317123.44729999 413310.671499983,5317123.58209999 413310.519999987,5317123.70659998 413310.36,5317123.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8v</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1056</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.350">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413117.2,5316719.98 413115.54,5316716.87 413104.53,5316722.72 413109.32,5316731.76 413113.8,5316729.39 413118.34,5316737.89 413121.5,5316736.22 413121.09,5316735.43 413123.59,5316734.1 413124.14,5316735.15 413133.46,5316729.58 413132.65,5316728.05 413134.53,5316727.06 413133.66,5316725.36 413139.85,5316722.03 413134.01,5316711.04 413117.2,5316719.98</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwjI</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>477</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.351">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413204.38,5316900.8 413204.34,5316900.82 413205.29,5316902.79 413209.12,5316910.74 413209.26,5316911.03 413215.92,5316907.51 413215.76,5316907.2 413210.8,5316897.37 413204.38,5316900.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twie</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>84</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.352">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413205.29,5316902.79 413189.67,5316910.75 413185.18,5316913.04 413189.14,5316921.39 413189.39,5316921.26 413189.45,5316921.37 413197.99,5316916.83 413198.06,5316916.95 413204.35,5316913.65 413209.26,5316911.03 413209.12,5316910.74 413205.29,5316902.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twih</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>209</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.353">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413204.38,5316900.8 413210.8,5316897.37 413210.69,5316897.15 413209.54,5316894.81 413208.9,5316893.53 413205.93,5316887.51 413205.85,5316887.36 413202.84,5316888.97 413202.92,5316889.11 413199.64,5316890.87 413203.05,5316898.02 413203.1,5316898.12 413204.24,5316900.51 413204.38,5316900.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twig</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>80</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.354">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413346.24,5317019.31 413351.82,5317016.31 413349.23,5317011.36 413353.64,5317009.08 413352.02,5317006.1 413341.9,5317011.42 413346.24,5317019.31</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa7</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>74</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.355">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413321.06,5316958.93 413317.32,5316960.93 413324.17,5316973.42 413332.0,5316969.3 413328.39,5316962.66 413326.19,5316963.86 413322.98,5316958.19 413321.17,5316959.14 413321.06,5316958.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twag</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>109</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.356">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413273.0,5317043.18 413276.91,5317051.31 413283.47,5317047.79 413285.81,5317046.53 413281.63,5317038.55 413273.0,5317043.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhV</ogr:gmlid>
      <ogr:street>Eschholzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>90</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.357">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413395.45,5317107.94 413433.12,5317085.07 413428.08,5317076.76 413390.42,5317099.63 413395.45,5317107.94</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzr</ogr:gmlid>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>428</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.358">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413517.07,5317167.42 413491.94,5317126.11 413482.5,5317131.86 413487.11,5317139.46 413487.18,5317139.41 413488.31,5317141.27 413487.05,5317142.04 413484.99,5317143.29 413486.2,5317145.27 413488.25,5317144.02 413489.53,5317143.25 413490.7,5317145.15 413490.61,5317145.2 413499.5,5317159.8 413499.58,5317159.75 413500.73,5317161.64 413499.45,5317162.42 413497.37,5317163.69 413498.59,5317165.66 413500.67,5317164.4 413501.95,5317163.62 413503.09,5317165.5 413503.01,5317165.55 413507.63,5317173.15 413517.07,5317167.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzh</ogr:gmlid>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>551</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.359">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413472.91,5317150.43 413467.21,5317141.06 413459.58,5317145.68 413458.85,5317144.5 413457.67,5317142.55 413455.66,5317143.76 413456.84,5317145.7 413457.57,5317146.9 413437.66,5317158.98 413436.84,5317157.6 413435.67,5317155.66 413433.65,5317156.89 413434.82,5317158.82 413435.65,5317160.2 413428.01,5317164.83 413433.79,5317174.18 413472.91,5317150.43</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzp</ogr:gmlid>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>521</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.360">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413430.97,5317054.34 413436.07,5317062.78 413438.44,5317066.69 413445.17,5317077.83 413467.58,5317064.29 413467.87,5317064.61 413470.65,5317062.94 413471.65,5317064.61 413476.9,5317061.5 413475.91,5317059.83 413497.01,5317047.25 413492.02,5317038.91 413490.79,5317036.86 413482.84,5317041.4 413484.89,5317044.48 413483.48,5317045.32 413483.18,5317044.83 413469.26,5317053.21 413469.02,5317052.9 413451.86,5317062.88 413451.1,5317061.52 413451.92,5317061.04 413451.03,5317059.36 413448.79,5317060.53 413446.99,5317057.41 413446.52,5317056.58 413444.79,5317053.55 413447.74,5317051.9 413444.55,5317046.31 413443.98,5317046.65 413433.94,5317052.58 413430.97,5317054.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8o</ogr:gmlid>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>844</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.361">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413429.49,5317045.22 413428.71,5317043.93 413425.69,5317045.69 413430.95,5317054.35 413430.97,5317054.34 413433.94,5317052.58 413429.49,5317045.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8n</ogr:gmlid>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>5 x</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.362">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413415.08,5317140.12 413452.7,5317117.29 413447.62,5317109.0 413409.95,5317131.83 413415.08,5317140.12</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzq</ogr:gmlid>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>429</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.363">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413451.38,5317042.29 413444.55,5317046.31 413447.74,5317051.9 413449.89,5317055.67 413446.99,5317057.41 413448.79,5317060.53 413451.03,5317059.36 413451.92,5317061.04 413451.1,5317061.52 413451.86,5317062.88 413469.02,5317052.9 413469.26,5317053.21 413483.18,5317044.83 413483.48,5317045.32 413484.89,5317044.48 413482.84,5317041.4 413490.79,5317036.86 413495.41,5317034.16 413495.92,5317033.86 413490.12,5317024.18 413483.85,5317027.82 413481.76,5317024.34 413474.85,5317028.42 413466.79,5317033.18 413462.96,5317035.44 413459.04,5317037.75 413456.86,5317039.04 413451.38,5317042.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8p</ogr:gmlid>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>869</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.364">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413917.8,5316933.17 413917.35,5316933.28 413917.24,5316932.79 413905.91,5316935.52 413906.03,5316936.0 413905.2,5316936.2 413897.47,5316903.7 413897.0,5316903.81 413896.34,5316900.77 413877.6,5316905.27 413878.3,5316908.23 413877.84,5316908.34 413885.61,5316940.86 413881.57,5316942.01 413880.11,5316937.77 413853.08,5316945.5 413854.06,5316949.91 413850.06,5316951.05 413839.3,5316919.02 413828.07,5316922.81 413829.39,5316926.72 413829.26,5316926.76 413838.75,5316954.87 413838.62,5316954.91 413839.81,5316959.07 413840.93,5316962.99 413842.15,5316967.23 413842.28,5316967.19 413849.12,5316996.14 413849.25,5316996.1 413850.2,5317000.12 413861.73,5316997.36 413853.91,5316964.51 413857.93,5316963.36 413861.25,5316968.98 413885.56,5316962.01 413885.44,5316955.48 413889.49,5316954.31 413900.09,5316985.97 413900.55,5316985.81 413901.57,5316988.81 413919.76,5316982.63 413918.78,5316979.67 413919.26,5316979.51 413908.54,5316947.9 413909.35,5316947.63 413909.5,5316948.11 413920.57,5316944.46 413920.41,5316943.98 413920.85,5316943.84 413917.8,5316933.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tvux</ogr:gmlid>
      <ogr:street>Friedrichstraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
      <ogr:use_id>3021</ogr:use_id>
      <ogr:use>Schule</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>3677</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.365">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413832.44,5316813.19 413819.5,5316816.76 413820.61,5316820.79 413819.45,5316821.11 413820.33,5316824.32 413821.49,5316824.0 413822.49,5316827.64 413822.1,5316827.74 413822.37,5316828.7 413821.59,5316828.92 413822.34,5316831.62 413823.11,5316831.4 413823.39,5316832.42 413823.77,5316832.32 413826.48,5316842.23 413839.42,5316838.69 413832.44,5316813.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuSI</ogr:gmlid>
      <ogr:street>Friedrichstraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>363</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.366">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413850.99,5316876.4 413852.67,5316881.58 413860.56,5316879.0 413858.88,5316873.83 413850.99,5316876.4</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tvuw</ogr:gmlid>
      <ogr:street>Friedrichstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>45</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.367">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413870.89,5316837.81 413875.44,5316836.57 413893.71,5316831.53 413890.26,5316820.47 413888.29,5316814.18 413873.89,5316819.82 413872.85,5316816.08 413868.04,5316817.43 413861.73,5316819.3 413857.28,5316820.41 413858.34,5316824.32 413858.27,5316824.34 413862.59,5316840.11 413870.89,5316837.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL001000gVUtO</ogr:gmlid>
      <ogr:street>Friedrichstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>604</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.368">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413428.26,5316968.73 413420.3,5316972.99 413427.85,5316987.15 413429.12,5316986.48 413430.9,5316989.85 413432.33,5316989.09 413435.62,5316987.29 413433.85,5316983.94 413435.81,5316982.89 413428.26,5316968.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8E</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>165</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.369">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.95,5316965.02 413396.9,5316957.43 413388.55,5316961.93 413389.28,5316963.29 413385.18,5316965.5 413388.41,5316971.76 413400.95,5316965.02</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9C</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.370">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413398.51,5316910.83 413398.38179999,5316910.97669998 413398.252599994,5316911.1226 413398.122299997,5316911.26749999 413397.990999975,5316911.41149999 413397.858799978,5316911.55449999 413397.725499982,5316911.69669998 413397.591199985,5316911.83779998 413397.455899988,5316911.97809998 413397.319599992,5316912.1174 413397.182399995,5316912.25569999 413397.044099999,5316912.39299999 413396.904999977,5316912.52939999 413396.764799981,5316912.66469998 413396.623699984,5316912.79909998 413396.481699988,5316912.93249998 413396.338699991,5316913.0649 413396.194799995,5316913.1963 413396.05,5316913.32659999 413395.904199977,5316913.45589999 413395.757599981,5316913.58419999 413395.609999985,5316913.71149998 413395.461599988,5316913.83769998 413395.312199992,5316913.96289998 413395.162,5316914.087 413395.0109,5316914.21 413394.859,5316914.332 413394.706199982,5316914.45289999 413394.552499986,5316914.57279999 413394.39799999,5316914.69149998 413394.242599994,5316914.80919998 413394.086499998,5316914.92569998 413393.929499977,5316915.0412 413393.771599981,5316915.1556 413393.612999985,5316915.26879999 413393.453599989,5316915.38089999 413393.293399993,5316915.49189999 413393.132499997,5316915.60169998 413392.970699975,5316915.71039998 413392.80819998,5316915.81799998 413392.644999984,5316915.92439998 413392.480999988,5316916.0297 413392.316199992,5316916.1338 413392.150699996,5316916.23669999 413391.984499975,5316916.33849999 413391.817599979,5316916.43909999 413391.649999984,5316916.53849999 413391.481699988,5316916.63679998 413391.312699992,5316916.73379998 413391.143,5316916.82969998 413390.972699975,5316916.92429998 413390.80169998,5316917.0177 413390.63,5316917.11 413396.44,5316928.06 413396.46,5316928.05 413398.05,5316927.2 413407.04,5316922.43 413405.98,5316920.43 413406.115599997,5316920.28949999 413406.250499994,5316920.1484 413406.38469999,5316920.0065 413406.518199987,5316919.86399998 413406.650999984,5316919.72079998 413406.783,5316919.577 413406.914299977,5316919.43249999 413407.044899999,5316919.28729999 413407.174799996,5316919.1414 413407.303899992,5316918.99489998 413407.432299989,5316918.84779998 413407.56,5316918.7 413399.88,5316912.03 413398.51,5316910.83</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8O</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>160</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.371">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413396.9,5316957.43 413392.01,5316948.26 413391.93,5316948.3 413381.06,5316954.14 413377.93,5316955.83 413381.63,5316962.68 413387.29,5316959.6 413388.55,5316961.93 413396.9,5316957.43</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9D</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>149</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.372">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413361.59,5316862.79 413365.54,5316870.2 413373.25,5316866.26 413374.2,5316865.75 413374.18,5316865.24 413373.67,5316864.33 413372.36,5316861.54 413374.23,5316860.51 413372.27,5316856.95 413361.59,5316862.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd2</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>94</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.373">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413351.67,5316872.88 413342.96,5316877.53 413346.15,5316883.34 413346.43,5316883.19 413350.98,5316891.73 413357.95,5316888.01 413358.57,5316885.82 413351.67,5316872.88</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdf</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>156</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.374">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413407.56,5316918.7 413407.688899983,5316918.55059999 413407.816999979,5316918.40069999 413407.944399976,5316918.25 413408.071,5316918.0988 413408.196899995,5316917.94689998 413408.32199999,5316917.79439998 413408.446299989,5316917.64119998 413408.569899986,5316917.48749999 413408.692799982,5316917.33309999 413408.814799979,5316917.1781 413408.936099976,5316917.0226 413409.056599999,5316916.86639998 413409.176299996,5316916.70959998 413409.295299993,5316916.55219999 413409.41349999,5316916.39429999 413409.530799987,5316916.23569999 413409.647399984,5316916.0766 413409.763199981,5316915.91689998 413409.878199978,5316915.75659998 413409.992399975,5316915.59579999 413410.105799997,5316915.43439999 413410.218399994,5316915.27239999 413410.330199992,5316915.1099 413410.441199989,5316914.94679998 413410.551399986,5316914.78319998 413410.660699983,5316914.61899998 413410.769299981,5316914.45429999 413410.877,5316914.289 413410.983899975,5316914.1232 413411.09,5316913.95689998 413411.195199995,5316913.79 413411.299699992,5316913.62259998 413411.40319999,5316913.45469999 413411.505999987,5316913.28629999 413411.607899985,5316913.1174 413411.708999982,5316912.94799998 413411.80919998,5316912.77799998 413411.908599977,5316912.60759998 413412.0071,5316912.43669999 413412.104799997,5316912.26529999 413412.201599995,5316912.0934 413412.297599992,5316911.92099998 413412.39269999,5316911.74819998 413412.486899988,5316911.57489999 413412.580299985,5316911.40109999 413412.672799983,5316911.22689999 413412.764499981,5316911.0522 413412.855299978,5316910.87699998 413412.945199976,5316910.70139998 413413.034199999,5316910.52539999 413413.122399997,5316910.34889999 413413.209699995,5316910.172 413413.296099993,5316909.99459997 413413.38159999,5316909.81679998 413413.466199988,5316909.63859998 413413.55,5316909.46 413404.33,5316905.17 413402.67,5316904.4 413402.588399985,5316904.57389999 413402.505699987,5316904.74719998 413402.421699989,5316904.92 413402.336499991,5316905.0921 413402.250099994,5316905.26369999 413402.162599996,5316905.43469999 413402.073799998,5316905.60499998 413401.983899975,5316905.77469998 413401.892799977,5316905.94379998 413401.80049998,5316906.1123 413401.706999982,5316906.28009999 413401.612399985,5316906.44729999 413401.516599987,5316906.61379998 413401.419699989,5316906.77959998 413401.321599992,5316906.94469998 413401.222299994,5316907.1092 413401.121899997,5316907.27299999 413401.020399999,5316907.43599999 413400.917799977,5316907.59839998 413400.814,5316907.76 413400.709099982,5316907.92089998 413400.603099985,5316908.0811 413400.495999987,5316908.24049999 413400.38769999,5316908.39919999 413400.278399993,5316908.55709999 413400.168,5316908.71429998 413400.056499999,5316908.87059998 413399.943899976,5316909.0262 413399.830199979,5316909.1811 413399.715399982,5316909.33509999 413399.599599985,5316909.48829999 413399.482699988,5316909.64069998 413399.364799991,5316909.79229998 413399.245799994,5316909.94309998 413399.125699997,5316910.093 413399.0046,5316910.24209999 413398.882499978,5316910.39039999 413398.759399981,5316910.53779999 413398.635199984,5316910.68429998 413398.51,5316910.83 413399.88,5316912.03 413407.56,5316918.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8N</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>112</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.375">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413391.93,5316948.3 413389.01,5316942.88 413386.35,5316942.07 413379.03,5316946.01 413378.2,5316948.76 413381.06,5316954.14 413391.93,5316948.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9v</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.376">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413371.84,5316936.77 413374.75,5316942.25 413377.46,5316943.05 413384.78,5316939.13 413385.6,5316936.42 413382.72,5316930.99 413371.84,5316936.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9u</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.377">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413404.99,5316889.37 413405.0143,5316889.56599999 413405.037099999,5316889.76219998 413405.058499999,5316889.95859998 413405.078499998,5316890.1551 413405.097099998,5316890.35179999 413405.114299997,5316890.54859999 413405.130099997,5316890.74549998 413405.144399996,5316890.94249998 413405.157399996,5316891.1396 413405.168899996,5316891.33679999 413405.17899999,5316891.53409999 413405.187699995,5316891.73139998 413405.195,5316891.92879998 413405.200899995,5316892.1262 413405.205399995,5316892.32369999 413405.208399995,5316892.52119999 413405.21,5316892.71879998 413405.210199995,5316892.91629998 413405.209,5316893.1138 413405.206399995,5316893.31129999 413405.202399995,5316893.50879999 413405.196899995,5316893.70629998 413405.19,5316893.90369998 413405.181699995,5316894.101 413405.172,5316894.29829999 413405.160899996,5316894.49559999 413405.148399996,5316894.69269998 413405.134399997,5316894.88969998 413405.119099997,5316895.0867 413405.102299997,5316895.28349999 413405.084099998,5316895.48019999 413405.064499998,5316895.67669998 413405.043499999,5316895.87309998 413405.021099999,5316896.0694 413404.997299975,5316896.26549999 413404.972099975,5316896.46139999 413404.945399976,5316896.65709998 413404.917399977,5316896.85269998 413404.888,5316897.048 413404.857199978,5316897.24309999 413404.824999979,5316897.43799999 413404.79129998,5316897.63269998 413404.756299981,5316897.82709998 413404.719899982,5316898.0213 413404.682099983,5316898.21519999 413404.642899984,5316898.40879999 413404.602299985,5316898.60209998 413404.560299986,5316898.79519998 413404.516899987,5316898.98789997 413404.472199988,5316899.1803 413404.426099989,5316899.37239999 413404.37859999,5316899.56409999 413404.329699992,5316899.75559998 413404.279499993,5316899.94659998 413404.227899994,5316900.1373 413404.174899996,5316900.32759999 413404.120599997,5316900.51749999 413404.064899998,5316900.70709998 413404.0078,5316900.89619998 413403.949399976,5316901.0849 413403.889599978,5316901.27319999 413403.828499979,5316901.46109999 413403.765999981,5316901.64849998 413403.702199982,5316901.83539998 413403.637099984,5316902.0219 413403.570599986,5316902.20799999 413403.502799987,5316902.39349999 413403.433699989,5316902.57859999 413403.363199991,5316902.76309998 413403.291399993,5316902.94709998 413403.218299994,5316903.1307 413403.143899996,5316903.31369999 413403.068199998,5316903.49609999 413402.991099975,5316903.67799998 413402.912799977,5316903.85939998 413402.833099979,5316904.0402 413402.752199981,5316904.22039999 413402.67,5316904.4 413404.33,5316905.17 413413.55,5316909.46 413413.633799984,5316909.27909999 413413.716699982,5316909.0978 413413.79869998,5316908.91609998 413413.879799978,5316908.73389998 413413.959899976,5316908.55139999 413414.039199999,5316908.36849999 413414.117499997,5316908.1851 413414.194899995,5316908.0014 413414.271299993,5316907.81729998 413414.346899991,5316907.63279998 413414.421499989,5316907.44789999 413414.495099987,5316907.26269999 413414.567899986,5316907.0771 413414.639699984,5316906.89109998 413414.710599982,5316906.70469998 413414.78049998,5316906.51799999 413414.849499979,5316906.33099999 413414.917499977,5316906.1436 413414.984599975,5316905.95589998 413415.050799999,5316905.76779998 413415.116,5316905.57939999 413415.180199995,5316905.39069999 413415.243499994,5316905.20159999 413415.305899992,5316905.0123 413415.367299991,5316904.82259998 413415.427699989,5316904.63259998 413415.487199988,5316904.44239999 413415.545699986,5316904.25179999 413415.603299985,5316904.0609 413415.659899983,5316903.86979998 413415.715499982,5316903.67829998 413415.770199981,5316903.48659999 413415.823899979,5316903.29459999 413415.876599978,5316903.1023 413415.928399977,5316902.90979998 413415.979199975,5316902.71699998 413416.029,5316902.524 413416.077799998,5316902.33069999 413416.125699997,5316902.1372 413416.172599996,5316901.94349998 413416.218499994,5316901.74949998 413416.263499993,5316901.55539999 413416.307399992,5316901.36089999 413416.350399991,5316901.1663 413416.39239999,5316900.97139998 413416.433399989,5316900.77639998 413416.473499988,5316900.58109999 413416.512499987,5316900.38569999 413416.550599986,5316900.19 413416.587699985,5316899.99409997 413416.623799984,5316899.79809998 413416.658899983,5316899.60189999 413416.692999983,5316899.40549999 413416.726199982,5316899.209 413416.758299981,5316899.0122 413416.78949998,5316898.81539998 413416.819599979,5316898.61829998 413416.848799979,5316898.42109999 413416.876999978,5316898.22379999 413416.904199977,5316898.0264 413416.930399976,5316897.82879998 413416.955599976,5316897.63099998 413416.979799975,5316897.43319999 413417.003,5316897.23519999 413417.025199999,5316897.0371 413417.046399999,5316896.83889998 413417.066599998,5316896.64059998 413417.085799998,5316896.44219999 413417.104,5316896.24369999 413417.121199997,5316896.0451 413417.137399997,5316895.84649998 413417.152599996,5316895.64769998 413417.166799996,5316895.44889999 413417.18,5316895.25 413417.374399991,5316895.22899999 413417.565599986,5316895.1877 413417.751299981,5316895.1265 413417.929599977,5316895.046 413418.098299998,5316894.94709998 413418.255699994,5316894.83099998 413418.4,5316894.699 413418.529499987,5316894.55249999 413418.642899984,5316894.39319999 413418.738799981,5316894.22279999 413418.816199979,5316894.0432 413418.874299978,5316893.85649998 413418.912399977,5316893.66469998 413418.93,5316893.47 413418.926099977,5316893.27349999 413418.901199977,5316893.0786 413418.855799978,5316892.88749998 413418.79009998,5316892.70229998 413418.705099982,5316892.52509999 413418.601699985,5316892.35809999 413418.481,5316892.203 413418.344499991,5316892.0617 413418.193699995,5316891.93579998 413418.030299999,5316891.82659998 413417.856199978,5316891.73559998 413417.673399983,5316891.66359998 413417.483899988,5316891.61149998 413417.29,5316891.58 413417.282099993,5316891.39489999 413417.273199993,5316891.20979999 413417.263499993,5316891.0247 413417.252799994,5316890.83969998 413417.241299994,5316890.65479998 413417.228799994,5316890.46989999 413417.215499995,5316890.28509999 413417.201199995,5316890.1003 413417.186099995,5316889.91559998 413417.17,5316889.731 413417.153,5316889.54649999 413417.135199997,5316889.36199999 413417.116399997,5316889.1777 413417.096799998,5316888.99339997 413417.076199998,5316888.80919998 413417.054799999,5316888.62519998 413417.032399999,5316888.44119999 413417.0092,5316888.25729999 413416.984999975,5316888.0736 413416.96,5316887.89 413406.86,5316889.17 413404.99,5316889.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8M</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>230</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.378">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413382.77,5316930.96 413378.66,5316923.29 413368.75,5316928.53 413370.92,5316932.47 413369.89,5316933.09 413371.84,5316936.77 413382.72,5316930.99 413382.77,5316930.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9y</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.379">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413418.22,5316997.37 413412.63,5317000.49 413414.96,5317004.87 413413.27,5317005.78 413416.9,5317012.6 413424.24,5317008.62 413418.22,5316997.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9I</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.380">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413378.66,5316923.29 413373.34,5316913.3 413364.27,5316918.1 413364.72,5316919.09 413363.17,5316919.93 413364.14,5316921.73 413363.91,5316921.85 413365.19,5316924.22 413363.76,5316924.98 413366.22,5316929.72 413366.28,5316929.84 413367.02,5316929.44 413368.75,5316928.53 413378.66,5316923.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9x</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>146</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.381">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413365.54,5316870.2 413370.18,5316878.96 413373.15,5316879.86 413382.28,5316874.99 413377.65,5316866.14 413374.2,5316867.95 413373.25,5316866.26 413365.54,5316870.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd3</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>141</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.382">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413373.34,5316913.3 413366.59,5316900.67 413364.24,5316899.95 413355.62,5316904.55 413360.29,5316913.29 413361.96,5316912.4 413363.39,5316914.9 413362.79,5316915.3 413364.27,5316918.1 413373.34,5316913.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9w</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>2081</ogr:use_id>
      <ogr:use>Gaststätte, Restaura</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>173</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.383">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413357.35,5316854.83 413361.59,5316862.79 413372.27,5316856.95 413371.07,5316854.8 413373.67,5316853.3 413370.67,5316847.81 413368.63,5316848.89 413357.35,5316854.83</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd1</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>127</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.384">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413342.96,5316877.53 413351.67,5316872.88 413347.44,5316864.94 413336.22,5316870.91 413336.35,5316871.16 413337.49,5316870.56 413339.32,5316873.9 413338.18,5316874.5 413340.07,5316877.95 413341.99,5316876.94 413342.45,5316877.8 413342.96,5316877.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdh</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.385">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413353.12,5316846.9 413357.35,5316854.83 413368.63,5316848.89 413367.67,5316847.08 413371.22,5316845.22 413369.32,5316841.17 413365.82,5316843.02 413364.71,5316840.91 413364.61,5316840.96 413364.56,5316840.86 413363.5,5316841.42 413353.12,5316846.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd0</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>134</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.386">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413336.22,5316870.91 413347.44,5316864.94 413343.4,5316857.32 413331.68,5316863.45 413331.81,5316863.7 413332.84,5316863.16 413334.72,5316866.93 413333.71,5316867.45 413335.73,5316871.17 413336.22,5316870.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdn</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>110</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.387">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413347.95,5316837.19 413353.12,5316846.9 413363.5,5316841.42 413358.35,5316831.7 413347.95,5316837.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcV</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>129</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.388">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413331.68,5316863.45 413343.4,5316857.32 413338.97,5316849.02 413327.26,5316855.03 413329.27,5316858.86 413330.26,5316858.35 413332.57,5316862.67 413331.56,5316863.2 413331.68,5316863.45</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdp</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>119</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.389">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413342.03,5316826.22 413347.95,5316837.19 413358.25,5316831.74 413358.8,5316831.46 413356.5,5316827.11 413357.92,5316826.37 413354.46,5316819.75 413349.32,5316822.4 413349.23,5316822.23 413347.72,5316821.13 413346.0,5316822.01 413345.9,5316823.95 413345.99,5316824.13 413342.03,5316826.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcT</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>171</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.390">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413327.26,5316855.03 413338.97,5316849.02 413334.27,5316840.19 413332.95,5316837.73 413332.83,5316837.5 413322.08,5316843.18 413322.2,5316843.41 413323.52,5316845.9 413321.68,5316846.88 413323.73,5316850.75 413324.72,5316850.23 413325.63,5316849.75 413328.04,5316854.31 413327.13,5316854.78 413327.26,5316855.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twds</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>167</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.391">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413357.41,5316808.37 413365.44,5316823.65 413373.84,5316819.24 413370.54,5316812.95 413371.84,5316812.27 413371.97,5316811.82 413370.86,5316809.69 413370.4,5316809.55 413369.11,5316810.22 413365.81,5316803.95 413357.41,5316808.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcQ</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>33 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.392">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413322.08,5316843.18 413332.83,5316837.5 413328.55,5316829.47 413327.2,5316826.92 413316.43,5316832.58 413317.79,5316835.15 413315.77,5316836.22 413317.45,5316839.41 413319.5,5316838.33 413322.08,5316843.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdt</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>154</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.393">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413365.81,5316803.95 413362.64,5316797.88 413363.94,5316797.2 413364.06,5316796.69 413363.12,5316794.86 413362.65,5316794.73 413361.34,5316795.41 413358.04,5316789.14 413349.63,5316793.55 413357.41,5316808.37 413365.81,5316803.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcP</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>164</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.394">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413316.43,5316832.58 413327.2,5316826.92 413321.56,5316816.34 413309.37,5316822.74 413309.5,5316822.98 413310.79,5316822.3 413313.11,5316826.57 413311.82,5316827.25 413314.09,5316831.57 413315.5,5316830.83 413316.43,5316832.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdv</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>155</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.395">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413328.3,5316800.3 413334.24,5316811.47 413338.18,5316809.39 413338.27,5316809.57 413339.94,5316810.57 413341.66,5316809.66 413341.73,5316807.73 413341.64,5316807.55 413346.73,5316804.84 413343.25,5316798.3 413341.82,5316799.05 413339.35,5316794.4 413338.96,5316794.61 413328.3,5316800.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcU</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>176</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.396">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413309.37,5316822.74 413321.56,5316816.34 413316.21,5316806.3 413315.95,5316805.83 413304.55,5316811.84 413303.59,5316812.35 413303.72,5316812.59 413305.18,5316811.82 413306.42,5316814.03 413304.96,5316814.8 413307.23,5316818.85 413308.52,5316818.17 413310.53,5316821.82 413309.24,5316822.5 413309.37,5316822.74</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdw</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>155</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.397">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413322.66,5316789.72 413327.91,5316799.59 413328.3,5316800.3 413338.96,5316794.61 413337.53,5316791.93 413334.49,5316786.19 413333.53,5316784.4 413322.66,5316789.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcL</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>142</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.398">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413418.22,5316997.37 413412.42,5316986.61 413405.42,5316990.4 413407.82,5316995.17 413409.38,5316994.38 413409.91,5316995.37 413412.63,5317000.49 413418.22,5316997.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9H</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>87</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.399">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413304.55,5316811.84 413315.95,5316805.83 413311.46,5316797.39 413301.29,5316802.81 413303.54,5316806.81 413302.16,5316807.58 413304.55,5316811.84</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdz</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>117</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.400">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413316.54,5316778.24 413322.66,5316789.72 413333.53,5316784.4 413331.0,5316779.64 413333.03,5316778.56 413330.79,5316774.34 413328.76,5316775.42 413327.4,5316772.86 413316.54,5316778.24</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcI</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.401">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413307.03,5316789.07 413294.91,5316795.46 413297.04,5316799.26 413298.76,5316798.3 413301.29,5316802.81 413311.46,5316797.39 413307.03,5316789.07</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdB</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>118</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.402">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413311.84,5316769.42 413316.54,5316778.24 413327.4,5316772.86 413328.51,5316772.26 413327.35,5316770.09 413328.62,5316769.41 413327.86,5316767.98 413325.42,5316769.28 413324.42,5316767.41 413322.5,5316763.81 413321.58,5316764.28 413311.84,5316769.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcE</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>130</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.403">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413307.03,5316789.07 413302.55,5316780.65 413292.26,5316786.09 413294.76,5316790.54 413292.78,5316791.65 413294.91,5316795.46 413307.03,5316789.07</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdD</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>120</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.404">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413311.84,5316769.42 413321.58,5316764.28 413318.54,5316758.53 413319.13,5316758.21 413317.48,5316755.12 413316.89,5316755.43 413314.42,5316750.82 413313.23,5316748.55 413313.15,5316748.39 413303.39,5316753.55 413303.47,5316753.71 413304.63,5316755.9 413311.84,5316769.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcC</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>201</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.405">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413297.81,5316771.75 413287.65,5316777.14 413289.67,5316780.92 413288.14,5316781.75 413290.88,5316786.82 413292.26,5316786.09 413302.55,5316780.65 413297.81,5316771.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdF</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>126</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.406">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413303.39,5316753.55 413313.15,5316748.39 413313.81,5316748.02 413313.7,5316747.82 413312.55,5316745.63 413304.26,5316729.95 413293.7,5316735.36 413302.07,5316751.1 413303.29,5316753.36 413303.39,5316753.55</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcA</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>243</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.407">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413297.81,5316771.75 413293.57,5316763.81 413282.73,5316769.57 413285.16,5316773.89 413283.82,5316774.64 413284.5,5316775.87 413285.77,5316778.14 413287.65,5316777.14 413297.81,5316771.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdI</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>116</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.408">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413403.93,5316942.2 413407.68,5316949.26 413409.29,5316948.41 413418.32,5316943.61 413414.56,5316936.55 413405.54,5316941.34 413403.93,5316942.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8R</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.409">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413293.57,5316763.81 413288.88,5316755.01 413278.84,5316760.33 413281.33,5316764.76 413280.34,5316765.31 413282.73,5316769.57 413293.57,5316763.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdM</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>118</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.410">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413288.88,5316755.01 413284.19,5316746.2 413274.5,5316751.34 413271.91,5316752.72 413271.67,5316752.85 413274.14,5316757.28 413275.72,5316756.41 413278.13,5316760.72 413278.84,5316760.33 413288.88,5316755.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdP</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>132</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.411">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413293.7,5316735.36 413304.26,5316729.95 413304.62,5316729.77 413303.35,5316727.34 413302.29,5316727.9 413299.56,5316722.69 413298.33,5316723.33 413289.7,5316727.86 413289.75,5316727.94 413293.7,5316735.36</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcy</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.412">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413279.92,5316738.2 413269.22,5316743.75 413269.12,5316743.81 413271.41,5316747.98 413272.33,5316747.46 413274.5,5316751.34 413284.19,5316746.2 413279.92,5316738.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdT</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>106</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.413">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413289.7,5316727.86 413298.33,5316723.33 413296.68,5316720.17 413295.03,5316719.46 413293.35,5316716.25 413292.93,5316716.46 413285.71,5316720.36 413289.66,5316727.77 413289.7,5316727.86</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcx</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>55</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>79</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.414">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413275.69,5316730.25 413264.82,5316735.91 413269.22,5316743.75 413279.92,5316738.2 413275.69,5316730.25</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdW</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>109</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.415">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413289.24,5316704.18 413281.85,5316708.1 413280.95,5316711.47 413285.71,5316720.36 413292.93,5316716.46 413291.2,5316713.14 413292.21,5316712.6 413292.64,5316713.44 413293.92,5316712.77 413289.24,5316704.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcw</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>57</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>125</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.416">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413275.69,5316730.25 413269.35,5316718.34 413265.19,5316716.94 413251.85,5316724.01 413257.02,5316733.62 413261.97,5316730.84 413264.82,5316735.91 413275.69,5316730.25</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdX</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>263</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.417">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413407.08,5316976.49 413397.47,5316981.6 413397.54,5316981.72 413399.14,5316984.65 413398.19,5316985.18 413398.25,5316985.3 413399.68,5316987.92 413400.64,5316987.39 413403.0,5316991.69 413403.37,5316991.5 413405.14,5316990.56 413405.42,5316990.4 413412.42,5316986.61 413412.51,5316986.64 413407.08,5316976.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9F</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>128</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.418">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.18,5316935.12 413403.93,5316942.2 413405.54,5316941.34 413414.56,5316936.55 413410.8,5316929.48 413401.79,5316934.26 413400.18,5316935.12</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8Q</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.419">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413407.08,5316976.49 413400.95,5316965.02 413388.41,5316971.76 413388.51,5316971.95 413389.01,5316971.68 413394.95,5316982.72 413395.11,5316983.01 413397.54,5316981.72 413397.47,5316981.6 413407.08,5316976.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9A</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>178</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.420">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413396.44,5316928.06 413400.18,5316935.12 413401.79,5316934.26 413410.8,5316929.48 413407.04,5316922.43 413398.05,5316927.2 413396.46,5316928.05 413396.44,5316928.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8P</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.421">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413369.84,5316994.47 413373.25,5316992.8 413370.88,5316988.29 413385.3,5316980.56 413379.45,5316969.65 413376.97,5316970.96 413375.82,5316968.79 413365.24,5316974.49 413366.38,5316976.67 413360.89,5316979.6 413368.96,5316994.95 413369.84,5316994.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9B</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>315</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.422">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413365.21,5316833.43 413363.08,5316834.57 413366.02,5316840.09 413368.16,5316838.96 413365.21,5316833.43</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcX</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.423">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413372.32,5316827.11 413377.61,5316824.27 413376.43,5316822.04 413371.12,5316824.85 413372.32,5316827.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcZ</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.424">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413346.53,5316751.16 413343.71,5316746.32 413324.47,5316756.9 413326.89,5316761.5 413334.94,5316757.26 413341.4,5316753.85 413346.53,5316751.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcD</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>119</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.425">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413366.22,5316929.72 413360.56,5316932.7 413360.62,5316932.82 413363.17,5316937.54 413365.65,5316936.2 413364.25,5316933.81 413368.24,5316931.73 413367.02,5316929.44 413366.28,5316929.84 413366.22,5316929.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9z</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>27</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.426">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413334.49,5316786.19 413336.73,5316785.0 413337.15,5316785.79 413337.92,5316785.96 413338.46,5316785.67 413341.31,5316784.16 413339.64,5316781.08 413337.02,5316782.51 413333.53,5316784.4 413334.49,5316786.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcM</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>20</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.427">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413336.83,5316760.76 413343.17,5316756.99 413341.4,5316753.85 413334.94,5316757.26 413336.83,5316760.76</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcG</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>28</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.428">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413346.85,5316750.99 413346.53,5316751.16 413341.4,5316753.85 413343.17,5316756.99 413346.39,5316762.66 413351.76,5316759.78 413346.85,5316750.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcH</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>62</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.429">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413327.85,5316717.48 413322.95,5316720.1 413333.11,5316737.82 413337.94,5316735.26 413337.4,5316734.31 413332.98,5316726.52 413328.62,5316718.79 413327.85,5316717.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcB</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>113</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.430">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413262.52,5316752.02 413249.82,5316758.87 413252.15,5316763.2 413264.86,5316756.46 413263.28,5316753.46 413262.52,5316752.02</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdU</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>72</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.431">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413374.21,5316799.53 413370.74,5316793.44 413368.68,5316789.83 413365.9,5316791.34 413371.42,5316801.0 413374.21,5316799.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcR</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.432">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413371.42,5316801.0 413376.88,5316810.58 413379.66,5316809.08 413374.21,5316799.53 413371.42,5316801.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcS</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.433">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413346.39,5316762.66 413343.17,5316764.38 413344.61,5316767.07 413353.46,5316762.83 413351.76,5316759.78 413346.39,5316762.66</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcK</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>32</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.434">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413290.94,5316804.85 413292.34,5316807.57 413297.01,5316805.09 413295.59,5316802.46 413290.94,5316804.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdA</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.435">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413289.22,5316790.47 413288.06,5316788.31 413284.71,5316790.08 413285.85,5316792.25 413289.22,5316790.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdC</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>9</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.436">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413276.82,5316776.74 413279.27,5316775.36 413277.64,5316772.29 413275.17,5316773.6 413276.82,5316776.74</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdH</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>10</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.437">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413261.51,5316769.53 413262.92,5316772.22 413265.53,5316770.78 413264.11,5316768.15 413261.51,5316769.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdJ</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>9</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.438">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413264.11,5316768.15 413265.53,5316770.78 413265.69,5316771.08 413269.87,5316768.39 413268.67,5316765.74 413264.11,5316768.15</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdK</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.439">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413306.95,5316718.41 413303.86,5316720.05 413308.18,5316727.95 413311.19,5316726.41 413306.95,5316718.41</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcz</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.440">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413262.73,5316747.13 413260.56,5316748.26 413261.05,5316749.22 413262.52,5316752.02 413263.31,5316751.6 413264.74,5316750.84 413262.73,5316747.13</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdQ</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>11</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.441">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413263.31,5316751.6 413262.52,5316752.02 413263.28,5316753.46 413264.08,5316753.04 413263.31,5316751.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdR</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>1</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.442">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413262.91,5316744.58 413263.94,5316746.5 413266.49,5316745.18 413265.42,5316743.22 413262.91,5316744.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdV</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>6</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.443">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413398.25,5316985.3 413398.19,5316985.18 413399.14,5316984.65 413397.54,5316981.72 413395.11,5316983.01 413394.95,5316982.72 413392.15,5316984.22 413393.83,5316987.39 413398.25,5316985.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9G</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>21</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.444">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.25,5316967.15 413428.26,5316968.73 413420.3,5316972.99 413427.85,5316987.15 413429.12,5316986.48 413430.9,5316989.85 413432.33,5316989.09 413435.62,5316987.29 413436.26,5316986.94 413440.79,5316984.43 413439.26,5316981.64 413431.25,5316967.15</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8F</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>244</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.445">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413345.18,5316778.08 413341.58,5316780.03 413343.78,5316784.08 413346.0,5316782.89 413345.77,5316782.46 413347.16,5316781.71 413345.18,5316778.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcN</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>18</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.446">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413313.76,5316861.97 413307.91,5316850.67 413301.21,5316854.21 413304.37,5316860.07 413307.21,5316865.34 413310.15,5316863.83 413313.76,5316861.97</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdq</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>95</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.447">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413318.01,5316854.93 413319.98,5316858.77 413323.71,5316856.86 413321.74,5316853.02 413318.01,5316854.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdr</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>18</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.448">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413265.9,5316788.7 413270.54,5316797.39 413270.65,5316797.51 413284.2,5316790.35 413279.35,5316781.56 413265.9,5316788.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdE</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>153</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.449">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413262.92,5316772.22 413261.51,5316769.53 413256.88,5316771.98 413261.64,5316780.81 413266.23,5316778.36 413262.92,5316772.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdL</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>52</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.450">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413252.15,5316763.2 413254.72,5316767.97 413256.88,5316771.98 413261.51,5316769.53 413264.11,5316768.15 413268.67,5316765.74 413269.49,5316765.3 413264.86,5316756.46 413252.15,5316763.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdN</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>143</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.451">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413274.12,5316759.48 413269.25,5316762.06 413270.64,5316764.69 413275.52,5316762.1 413274.12,5316759.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdO</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.452">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413264.08,5316753.04 413263.28,5316753.46 413264.86,5316756.46 413271.67,5316752.85 413271.91,5316752.72 413270.31,5316749.77 413264.08,5316753.04</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdS</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>27</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.453">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413384.58,5316817.85 413379.49,5316820.54 413384.9,5316830.12 413390.02,5316827.42 413389.4,5316826.2 413384.58,5316817.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcW</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>64</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.454">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413347.16,5316781.71 413350.84,5316788.47 413360.65,5316783.2 413355.0,5316772.77 413345.18,5316778.08 413347.16,5316781.71</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcO</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>132</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.455">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413275.17,5316773.6 413266.23,5316778.36 413261.64,5316780.81 413265.9,5316788.7 413279.35,5316781.56 413278.18,5316779.33 413276.82,5316776.74 413275.17,5316773.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwdG</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>137</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.456">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413375.82,5316968.79 413370.94,5316959.59 413360.43,5316965.26 413365.24,5316974.49 413375.82,5316968.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9E</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>125</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.457">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413327.86,5316875.36 413323.72,5316867.6 413314.8,5316872.27 413317.67,5316877.91 413314.86,5316879.53 413316.01,5316881.65 413324.1,5316877.35 413324.23,5316877.28 413327.86,5316875.36</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdm</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.458">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413368.16,5316838.96 413380.39,5316832.5 413377.48,5316826.91 413374.42,5316828.51 413374.16,5316828.02 413368.31,5316831.15 413368.13,5316831.24 413368.39,5316831.73 413365.21,5316833.43 413368.16,5316838.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcY</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.459">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413323.72,5316867.6 413319.18,5316859.18 413313.76,5316861.97 413310.15,5316863.83 413314.8,5316872.27 413323.72,5316867.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdo</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.460">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413295.48,5316843.59 413307.27,5316837.4 413301.58,5316826.83 413294.2,5316830.71 413297.54,5316837.12 413293.21,5316839.37 413295.48,5316843.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdu</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>124</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.461">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413301.58,5316826.83 413302.65,5316826.26 413297.22,5316815.7 413296.86,5316815.89 413284.15,5316822.57 413286.14,5316826.26 413286.68,5316827.26 413291.18,5316824.91 413294.2,5316830.71 413301.58,5316826.83</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdx</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>141</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.462">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413296.86,5316815.89 413292.34,5316807.57 413282.43,5316812.84 413285.65,5316818.71 413282.91,5316820.25 413284.15,5316822.57 413296.86,5316815.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdy</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>114</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.463">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413355.51,5316766.5 413334.52,5316777.87 413337.02,5316782.51 413339.64,5316781.08 413341.58,5316780.03 413345.18,5316778.08 413355.0,5316772.77 413358.08,5316771.1 413356.27,5316767.86 413355.51,5316766.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcJ</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>126</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.464">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413324.42,5316767.41 413336.83,5316760.76 413334.94,5316757.26 413326.89,5316761.5 413322.5,5316763.81 413324.42,5316767.41</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcF</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>57</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.465">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413679.11,5317177.44 413686.9,5317173.98 413684.09,5317168.99 413676.94,5317172.16 413679.11,5317177.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6z</ogr:gmlid>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>47</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.466">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413655.99,5317196.54 413665.31,5317192.91 413662.04,5317184.49 413652.74,5317188.14 413655.99,5317196.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6w</ogr:gmlid>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>90</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.467">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413676.03,5317178.21 413678.54,5317177.2 413676.56,5317172.27 413674.16,5317173.3 413673.08,5317170.46 413663.84,5317174.05 413665.95,5317179.43 413667.84,5317178.65 413668.83,5317181.11 413676.03,5317178.21</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6B</ogr:gmlid>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.468">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413668.83,5317181.11 413672.29,5317189.7 413679.64,5317186.74 413676.03,5317178.21 413668.83,5317181.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6C</ogr:gmlid>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>73</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.469">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413659.77,5317178.65 413661.95,5317184.26 413664.78,5317183.2 413662.6,5317177.57 413662.51,5317177.6 413659.77,5317178.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6x</ogr:gmlid>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>18</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.470">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413662.51,5317177.6 413660.43,5317172.44 413660.12,5317172.56 413660.2,5317172.8 413654.33,5317175.05 413656.2,5317180.0 413659.77,5317178.65 413662.51,5317177.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6A</ogr:gmlid>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>36</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.471">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413519.71,5317211.13 413523.69,5317217.51 413506.02,5317228.54 413515.03,5317242.92 413517.75,5317241.23 413518.54,5317242.48 413524.54,5317238.75 413523.75,5317237.48 413559.36,5317215.26 413571.9,5317224.77 413574.78,5317217.94 413577.42,5317219.08 413578.39,5317216.8 413575.74,5317215.64 413578.7,5317208.62 413566.73,5317199.57 413568.67,5317197.15 413566.69,5317195.65 413564.75,5317198.08 413556.13,5317191.54 413553.34,5317193.28 413553.55,5317193.57 413548.67,5317196.63 413551.08,5317200.4 413525.79,5317216.19 413521.83,5317209.81 413519.71,5317211.13</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyz8</ogr:gmlid>
      <ogr:street>Gärtnerweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1406</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.472">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413706.63,5316609.71 413712.59,5316606.67 413710.82,5316603.18 413706.45,5316605.5 413704.83,5316606.36 413706.63,5316609.71</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUf</ogr:gmlid>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>2 x</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>26</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.473">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413710.42,5316542.53 413722.69,5316579.92 413722.52,5316580.45 413734.46,5316576.44 413732.05,5316569.11 413729.77,5316562.18 413722.05,5316538.71 413714.18,5316541.3 413710.42,5316542.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUq</ogr:gmlid>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>3210</ogr:use_id>
      <ogr:use>Sport</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>487</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.474">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413648.87,5316604.11 413649.04,5316604.02 413651.06,5316607.84 413651.17,5316607.78 413651.28,5316608.0 413663.65,5316601.37 413684.19,5316590.4 413694.81,5316584.72 413694.64,5316584.4 413695.05,5316584.19 413686.47,5316568.16 413686.07,5316568.38 413685.96,5316568.18 413654.36,5316585.07 413654.36,5316585.12 413642.55,5316591.46 413642.65,5316591.65 413642.56,5316591.7 413644.57,5316595.51 413644.39,5316595.61 413644.428199989,5316595.80119998 413644.468599988,5316595.99189997 413644.511299987,5316596.1821 413644.556199986,5316596.37179999 413644.603299985,5316596.56089999 413644.652599983,5316596.74949998 413644.704199982,5316596.93749998 413644.757999981,5316597.1249 413644.813899979,5316597.31159999 413644.872099978,5316597.49769999 413644.932399976,5316597.68309998 413644.994899975,5316597.86769998 413645.059599999,5316598.0516 413645.126399997,5316598.23469999 413645.195399995,5316598.41709999 413645.266499993,5316598.59859999 413645.339799991,5316598.77919998 413645.41509999,5316598.95899998 413645.492599988,5316599.1379 413645.572199986,5316599.31589999 413645.653799983,5316599.49289999 413645.737499981,5316599.66889998 413645.823299979,5316599.84399998 413645.911199977,5316600.018 413646.001,5316600.191 413646.092899998,5316600.36299999 413646.186799995,5316600.53379999 413646.282799993,5316600.70359998 413646.38069999,5316600.87219998 413646.480599988,5316601.0396 413646.582399985,5316601.20589999 413646.686199983,5316601.37089999 413646.79199998,5316601.53469999 413646.899599977,5316601.69729998 413647.0092,5316601.85859998 413647.120599997,5316602.0186 413647.23399999,5316602.1772 413647.349099991,5316602.33449999 413647.466199988,5316602.49049999 413647.584999985,5316602.64509998 413647.705599982,5316602.79819998 413647.828099979,5316602.95 413647.952299976,5316603.1003 413648.078299998,5316603.24909999 413648.20599999,5316603.39639999 413648.335399992,5316603.54219999 413648.466599988,5316603.68649998 413648.599399985,5316603.82919998 413648.733899981,5316603.97039998 413648.87,5316604.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUl</ogr:gmlid>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>941</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.475">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413710.42,5316542.53 413710.02,5316542.33 413703.67,5316544.42 413702.83,5316541.86 413693.13,5316545.05 413695.02,5316550.8 413697.0,5316556.8 413701.06,5316555.48 413701.88,5316557.99 413700.37,5316558.49 413703.26,5316567.34 413704.78,5316566.84 413710.35,5316583.92 413710.73,5316584.08 413722.52,5316580.45 413722.69,5316579.92 413710.42,5316542.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuUo</ogr:gmlid>
      <ogr:street>Hans-Sachs-Gasse</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>601</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.476">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413476.64,5317252.04 413489.15,5317258.38 413494.64,5317247.6 413498.03,5317240.84 413491.1,5317237.79 413486.79,5317246.26 413481.02,5317243.33 413476.64,5317252.04</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyyU</ogr:gmlid>
      <ogr:street>Hohenzollernstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>211</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.477">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413656.15,5317135.03 413665.43,5317139.3 413666.67,5317139.88 413667.13,5317140.09 413668.63,5317136.68 413669.38,5317136.96 413670.68,5317134.05 413670.53,5317133.98 413670.86,5317133.17 413673.71,5317130.87 413666.5,5317122.14 413662.96,5317120.36 413656.15,5317135.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6S</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.478">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413634.33,5317183.37 413626.04,5317201.59 413635.98,5317206.18 413644.4,5317187.89 413634.33,5317183.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6v</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>221</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.479">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413607.44,5317242.08 413608.64,5317242.44 413618.02,5317245.3 413632.0,5317212.4 413621.85,5317208.12 413607.44,5317242.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6k</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>400</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.480">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413649.83,5317149.18 413658.95,5317153.38 413660.21,5317153.96 413660.3,5317153.76 413659.04,5317153.19 413660.39,5317150.18 413661.65,5317150.76 413665.29,5317142.81 413664.05,5317142.23 413665.32,5317139.54 413666.56,5317140.12 413666.67,5317139.88 413665.43,5317139.3 413656.15,5317135.03 413649.83,5317149.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6U</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.481">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413608.58,5317174.6 413597.14,5317169.37 413593.78,5317176.87 413591.51,5317181.92 413600.96,5317186.42 413603.71,5317185.49 413608.58,5317174.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzb</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>172</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.482">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413583.0,5317233.68 413596.72,5317200.97 413582.76,5317194.28 413570.08,5317184.65 413559.62,5317173.35 413548.83,5317180.15 413554.6,5317189.37 413557.82,5317187.33 413562.93,5317192.8 413566.69,5317195.65 413568.67,5317197.15 413577.23,5317203.65 413582.78,5317206.31 413578.39,5317216.8 413577.42,5317219.08 413572.84,5317229.85 413583.0,5317233.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyza</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>844</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.483">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413566.87,5317242.74 413578.22,5317246.91 413579.77,5317242.61 413579.89,5317242.65 413581.57,5317238.0 413581.45,5317237.96 413583.0,5317233.68 413572.84,5317229.85 413571.28,5317229.26 413571.18,5317229.52 413572.58,5317230.03 413571.14,5317234.0 413569.74,5317233.49 413568.07,5317238.11 413567.96,5317238.37 413569.37,5317238.88 413567.94,5317242.83 413566.54,5317242.32 413566.44,5317242.58 413566.87,5317242.74</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyz7</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>165</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.484">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413639.31,5317172.46 413634.33,5317183.37 413644.4,5317187.89 413645.31,5317188.3 413650.18,5317177.36 413648.75,5317176.72 413639.31,5317172.46</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6D</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>144</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.485">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413597.14,5317169.37 413597.26,5317169.12 413594.1,5317167.7 413593.68,5317168.63 413590.77,5317167.34 413588.19,5317173.19 413591.21,5317174.52 413590.82,5317175.54 413593.78,5317176.87 413597.14,5317169.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzc</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>49</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.486">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413590.82,5317175.54 413591.21,5317174.52 413588.19,5317173.19 413585.58,5317179.11 413588.57,5317180.52 413590.82,5317175.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzd</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>22</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.487">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413591.51,5317181.92 413593.78,5317176.87 413590.82,5317175.54 413588.57,5317180.52 413591.51,5317181.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyze</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>18</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.488">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413532.48,5317253.76 413537.59,5317255.32 413545.26,5317234.8 413540.22,5317232.94 413532.48,5317253.76</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyz5</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>118</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.489">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413670.15,5317141.48 413673.63,5317143.08 413678.49,5317138.97 413678.41,5317138.87 413676.2,5317136.29 413670.15,5317141.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6T</ogr:gmlid>
      <ogr:street>Hugstetter Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.490">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413519.58,5316955.56 413513.95,5316944.99 413503.89,5316950.61 413498.3,5316953.74 413493.73,5316956.29 413499.54,5316966.68 413499.63,5316966.63 413519.58,5316955.56</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8Y</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>275</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.491">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413545.21,5316898.7 413554.92,5316893.47 413548.11,5316881.18 413538.43,5316886.48 413545.21,5316898.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7F</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>155</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.492">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413492.44,5316904.73 413483.41,5316909.54 413485.58,5316913.5 413484.47,5316914.11 413486.5,5316917.9 413496.6,5316912.5 413492.44,5316904.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw98</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>95</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.493">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413538.43,5316886.48 413548.11,5316881.18 413539.98,5316866.52 413530.32,5316871.88 413538.43,5316886.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7E</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>185</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.494">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413488.29,5316896.96 413478.14,5316902.37 413480.15,5316906.14 413481.27,5316905.54 413483.41,5316909.54 413492.44,5316904.73 413488.29,5316896.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9a</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.495">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.05,5316889.0 413475.02,5316893.82 413470.99,5316895.97 413467.63,5316897.77 413469.51,5316901.3 413474.56,5316898.6 413476.33,5316898.98 413478.14,5316902.37 413488.29,5316896.96 413484.05,5316889.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9d</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>133</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.496">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.05,5316889.0 413479.8,5316881.05 413469.63,5316886.48 413471.68,5316890.28 413472.8,5316889.68 413475.02,5316893.82 413484.05,5316889.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9f</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.497">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413503.03,5316825.92 413475.61,5316840.6 413474.81,5316843.17 413483.74,5316859.88 413485.24,5316859.08 413497.16,5316881.36 413506.56,5316876.33 413495.98,5316856.49 413490.65,5316846.49 413492.83,5316845.32 413502.08,5316840.39 413503.8,5316839.47 413509.05,5316849.31 413514.18,5316846.54 413511.09,5316840.82 413508.74,5316836.48 413503.03,5316825.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7i</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1020</ogr:use_id>
      <ogr:use>Wohnheim</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>845</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.498">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413475.56,5316873.11 413466.46,5316877.98 413468.69,5316882.11 413467.59,5316882.71 413469.63,5316886.48 413479.8,5316881.05 413475.56,5316873.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9h</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.499">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.18,5316929.49 413528.39,5316926.73 413532.9,5316924.32 413525.0,5316909.53 413515.26,5316914.74 413523.18,5316929.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7B</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>185</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.500">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413458.87,5316813.22 413463.1,5316821.13 413470.94,5316817.13 413472.42,5316816.37 413471.31,5316814.41 413472.45,5316813.74 413471.75,5316812.57 413470.56,5316813.03 413468.17,5316808.39 413458.87,5316813.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbw</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.501">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.31,5316865.16 413461.02,5316870.68 413463.08,5316874.44 413464.18,5316873.83 413464.96,5316875.25 413466.46,5316877.98 413475.56,5316873.11 413471.31,5316865.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9j</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>99</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.502">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413455.04,5316806.05 413458.87,5316813.22 413468.17,5316808.39 413467.51,5316807.21 413468.54,5316806.56 413467.87,5316805.3 413466.81,5316805.91 413464.23,5316801.15 413455.04,5316806.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbu</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>87</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.503">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.31,5316865.16 413467.06,5316857.18 413457.9,5316862.09 413460.08,5316866.22 413458.98,5316866.91 413461.02,5316870.68 413471.31,5316865.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9l</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>100</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.504">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413451.01,5316798.51 413455.04,5316806.05 413464.23,5316801.15 413461.67,5316796.21 413462.82,5316795.62 413462.21,5316794.45 413461.04,5316795.07 413460.27,5316793.58 413451.01,5316798.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbs</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.505">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413446.81,5316819.17 413438.41,5316823.74 413438.98,5316824.79 413438.11,5316825.27 413439.64,5316828.14 413441.22,5316831.1 413450.53,5316826.12 413446.81,5316819.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbB</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>82</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.506">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413446.99,5316790.97 413451.01,5316798.51 413460.27,5316793.58 413459.75,5316792.38 413460.82,5316791.74 413460.21,5316790.57 413459.07,5316791.26 413456.0,5316786.16 413446.99,5316790.97</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbq</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.507">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413441.85,5316809.89 413430.51,5316815.96 413431.54,5316817.87 413434.46,5316816.31 413438.41,5316823.74 413446.81,5316819.17 413441.85,5316809.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbD</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.508">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413442.88,5316783.29 413446.99,5316790.97 413456.0,5316786.16 413457.16,5316785.55 413458.34,5316784.92 413455.17,5316779.59 413454.27,5316780.05 413453.83,5316779.17 413452.6,5316779.8 413451.92,5316778.46 413442.88,5316783.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbo</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>106</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.509">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413441.85,5316809.89 413436.89,5316800.61 413426.93,5316805.95 413424.37,5316807.32 413420.93,5316809.16 413420.79,5316809.24 413425.72,5316818.53 413425.73,5316818.52 413430.51,5316815.96 413441.85,5316809.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbF</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>192</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.510">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413509.24,5316936.17 413499.1,5316941.57 413503.89,5316950.61 413513.95,5316944.99 413509.24,5316936.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw90</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>116</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.511">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.79,5316775.64 413442.88,5316783.29 413451.92,5316778.46 413451.22,5316777.11 413452.37,5316776.52 413451.79,5316775.41 413450.64,5316775.99 413447.97,5316770.75 413447.1,5316771.21 413438.79,5316775.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbm</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.512">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413436.89,5316800.61 413431.97,5316791.56 413422.24,5316796.73 413421.84,5316796.94 413423.91,5316800.84 413424.08,5316800.75 413426.93,5316805.95 413436.89,5316800.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbH</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>118</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.513">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.79,5316775.64 413447.1,5316771.21 413443.84,5316764.85 413444.8,5316764.37 413444.13,5316763.06 413434.91,5316768.35 413438.79,5316775.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbk</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>81</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.514">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.97,5316791.56 413427.61,5316783.4 413416.64,5316789.26 413417.15,5316790.2 413418.41,5316789.52 413420.85,5316794.12 413417.91,5316795.68 413419.3,5316798.29 413421.84,5316796.94 413422.24,5316796.73 413431.97,5316791.56</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbK</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>113</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.515">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413430.98,5316761.0 413434.91,5316768.35 413444.13,5316763.06 413443.56,5316761.98 413442.56,5316762.52 413440.75,5316759.18 413439.19,5316756.29 413430.98,5316761.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbi</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>80</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.516">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413423.37,5316775.47 413414.77,5316780.01 413413.62,5316780.62 413417.27,5316787.49 413416.04,5316788.14 413416.64,5316789.26 413427.61,5316783.4 413423.37,5316775.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbO</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>101</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.517">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413399.28,5316792.92 413398.45,5316793.36 413394.44,5316785.84 413394.12,5316786.02 413392.01,5316782.06 413384.13,5316786.28 413381.61,5316787.63 413384.2,5316792.45 413385.86,5316791.56 413389.18,5316797.77 413387.51,5316798.66 413390.1,5316803.49 413400.55,5316797.89 413400.53,5316797.84 413401.68,5316797.23 413399.28,5316792.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbL</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>208</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.518">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413419.17,5316767.61 413410.63,5316772.14 413411.41,5316773.63 413410.34,5316774.19 413410.9,5316775.26 413411.97,5316774.7 413414.77,5316780.01 413423.37,5316775.47 413419.17,5316767.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbQ</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>88</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.519">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413422.95,5316745.98 413427.0,5316753.56 413436.28,5316748.15 413435.81,5316747.24 413434.84,5316747.74 413431.55,5316741.33 413422.95,5316745.98</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbd</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>83</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.520">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413419.17,5316767.61 413414.87,5316759.57 413406.35,5316764.11 413408.25,5316767.67 413409.26,5316769.57 413408.23,5316770.12 413408.8,5316771.19 413409.83,5316770.64 413410.63,5316772.14 413419.17,5316767.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbU</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>89</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.521">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413515.26,5316914.74 413525.0,5316909.53 413518.45,5316897.27 413508.72,5316902.48 413515.26,5316914.74</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7C</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>153</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.522">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413422.95,5316745.98 413431.55,5316741.33 413429.16,5316736.79 413428.28,5316735.08 413429.29,5316734.56 413428.64,5316733.27 413418.93,5316738.45 413422.95,5316745.98</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbb</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>85</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.523">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413414.87,5316759.57 413409.28,5316749.09 413400.03,5316753.95 413399.66,5316754.14 413400.91,5316756.27 413399.86,5316756.8 413401.74,5316760.44 413402.79,5316759.9 413405.28,5316764.68 413406.35,5316764.11 413414.87,5316759.57</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbZ</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>134</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.524">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413414.93,5316730.95 413418.93,5316738.45 413428.64,5316733.27 413428.02,5316732.09 413426.86,5316732.69 413426.02,5316731.08 413423.65,5316726.48 413414.93,5316730.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twba</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>84</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.525">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413405.02,5316741.11 413394.74,5316746.59 413395.33,5316747.7 413396.35,5316747.16 413400.03,5316753.95 413409.28,5316749.09 413405.02,5316741.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc1</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.526">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413410.88,5316723.38 413414.93,5316730.95 413423.65,5316726.48 413424.71,5316725.94 413424.06,5316724.68 413422.8,5316725.33 413419.52,5316718.95 413410.88,5316723.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb9</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>85</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.527">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.74,5316733.1 413392.32,5316737.63 413391.38,5316738.14 413395.17,5316744.93 413394.15,5316745.46 413394.74,5316746.59 413405.02,5316741.11 413400.74,5316733.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc3</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.528">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413410.88,5316723.38 413419.52,5316718.95 413416.22,5316712.52 413417.15,5316712.05 413417.61,5316712.95 413418.5,5316712.5 413418.91,5316713.31 413420.2,5316712.65 413418.4,5316709.8 413416.53,5316710.85 413406.9,5316715.92 413410.88,5316723.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb7</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.529">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.74,5316733.1 413396.02,5316724.27 413387.45,5316728.72 413392.32,5316737.63 413400.74,5316733.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc5</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.530">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413406.9,5316715.92 413416.53,5316710.85 413418.4,5316709.8 413416.21,5316706.34 413413.99,5316707.55 413413.22,5316706.13 413414.2,5316705.6 413412.76,5316702.99 413402.83,5316708.29 413406.9,5316715.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb6</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.531">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413391.39,5316715.59 413382.17,5316720.54 413381.68,5316720.8 413382.14,5316721.62 413383.25,5316721.01 413385.83,5316725.75 413387.45,5316728.72 413396.02,5316724.27 413391.39,5316715.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc9</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.532">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413509.24,5316936.17 413505.0,5316928.24 413494.88,5316933.63 413497.28,5316938.13 413499.1,5316941.57 413509.24,5316936.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw92</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.533">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413398.75,5316700.67 413402.83,5316708.29 413412.76,5316702.99 413413.72,5316702.47 413411.76,5316698.9 413410.89,5316699.38 413408.69,5316695.33 413398.75,5316700.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb5</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.534">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413391.39,5316715.59 413387.06,5316707.48 413377.7,5316712.54 413376.68,5316713.09 413377.13,5316713.92 413378.23,5316713.32 413379.5,5316715.65 413382.17,5316720.54 413391.39,5316715.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcb</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.535">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413370.91,5316726.57 413369.85,5316724.6 413368.42,5316725.37 413366.7,5316722.07 413351.52,5316730.22 413353.39,5316733.7 413357.49,5316731.5 413358.43,5316733.27 413370.91,5316726.57</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcc</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>51 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.536">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413394.68,5316693.04 413398.75,5316700.67 413408.69,5316695.33 413406.53,5316691.36 413407.44,5316690.86 413405.48,5316687.24 413404.52,5316687.76 413394.68,5316693.04</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb3</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.537">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413382.95,5316699.77 413373.61,5316704.77 413369.82,5316706.79 413368.19,5316707.66 413369.84,5316710.76 413371.28,5316710.01 413372.35,5316709.45 413375.26,5316707.91 413377.7,5316712.54 413387.06,5316707.48 413382.95,5316699.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcd</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.538">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413394.68,5316693.04 413404.52,5316687.76 413402.29,5316683.66 413403.32,5316683.1 413401.48,5316679.74 413390.6,5316685.41 413394.68,5316693.04</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb2</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>100</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.539">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413382.95,5316699.77 413378.7,5316691.82 413369.44,5316696.81 413370.51,5316698.86 413373.61,5316704.77 413382.95,5316699.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twci</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>55</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>95</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.540">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413386.51,5316677.77 413390.6,5316685.41 413401.48,5316679.74 413399.49,5316676.07 413398.65,5316676.53 413396.48,5316672.54 413386.51,5316677.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb1</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>56</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.541">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413386.51,5316677.77 413396.48,5316672.54 413397.41,5316672.05 413395.48,5316668.52 413391.57,5316670.65 413390.58,5316668.81 413390.99,5316668.59 413391.31,5316667.53 413390.7,5316666.37 413390.3,5316666.29 413389.48,5316666.11 413382.21,5316669.7 413386.51,5316677.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaZ</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>58</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.542">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413508.72,5316902.48 413518.45,5316897.27 413510.55,5316882.48 413500.83,5316887.69 413508.72,5316902.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7D</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>185</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.543">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413376.04,5316658.14 413382.21,5316669.7 413389.48,5316666.11 413390.3,5316666.29 413387.71,5316661.54 413390.49,5316660.01 413386.73,5316652.49 413376.04,5316658.14</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaY</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>143</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.544">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413500.74,5316920.26 413491.75,5316925.04 413490.64,5316925.64 413493.1,5316930.27 413494.88,5316933.63 413505.0,5316928.24 413500.74,5316920.26</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw94</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.545">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413545.21,5316898.7 413553.35,5316913.42 413563.15,5316908.32 413554.92,5316893.47 413545.21,5316898.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7G</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>187</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.546">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413496.6,5316912.5 413486.5,5316917.9 413488.33,5316921.32 413488.47,5316921.58 413489.59,5316920.98 413491.75,5316925.04 413500.74,5316920.26 413496.6,5316912.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw97</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>95</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.547">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413455.57,5316883.8 413457.29,5316887.02 413464.34,5316883.25 413462.63,5316880.03 413461.6,5316880.58 413460.29,5316881.28 413455.57,5316883.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9g</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.548">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413429.16,5316736.79 413431.11,5316735.8 413433.42,5316740.31 413434.88,5316739.52 413436.83,5316738.46 413432.29,5316731.27 413428.64,5316733.27 413429.29,5316734.56 413428.28,5316735.08 413429.16,5316736.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbc</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.549">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413354.06,5316709.77 413348.88,5316712.55 413351.13,5316716.8 413356.32,5316714.02 413354.06,5316709.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcf</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>28</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.550">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413365.81,5316706.05 413363.85,5316702.44 413362.8,5316700.38 413352.12,5316706.14 413354.06,5316709.77 413356.32,5316714.02 413365.25,5316709.24 413367.02,5316708.29 413365.81,5316706.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcg</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.551">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.23,5316939.29 413487.27,5316937.67 413482.84,5316929.79 413479.82,5316931.4 413484.23,5316939.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw93</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.552">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.87,5316846.93 413521.53,5316842.57 413516.77,5316845.14 413514.47,5316846.38 413516.82,5316850.73 413523.87,5316846.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7k</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>40</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.553">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.5,5316804.8 413480.91,5316807.21 413480.62,5316806.86 413476.92,5316809.45 413478.69,5316812.09 413486.81,5316806.54 413488.59,5316805.32 413487.53,5316803.72 413484.94,5316805.46 413484.5,5316804.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twby</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>33</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.554">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413402.12,5316776.65 413394.03,5316780.98 413396.95,5316786.15 413404.99,5316781.73 413402.12,5316776.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbM</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>54</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.555">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413399.28,5316792.92 413401.68,5316797.23 413406.66,5316794.56 413404.21,5316790.17 413399.23,5316792.83 413399.28,5316792.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbN</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>28</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.556">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413402.65,5316766.08 413399.88,5316760.87 413394.66,5316763.67 413397.42,5316768.86 413402.65,5316766.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbY</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.557">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413356.27,5316767.86 413358.08,5316771.1 413358.34,5316771.56 413362.23,5316769.37 413365.4,5316775.35 413361.66,5316777.48 413363.83,5316781.33 413366.2,5316785.49 413368.98,5316784.03 413378.95,5316778.71 413380.64,5316777.81 413379.41,5316775.52 413379.51,5316775.46 413372.47,5316762.35 413372.37,5316762.41 413370.92,5316759.71 413356.27,5316767.86</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbV</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>3044</ogr:use_id>
      <ogr:use>Gemeindehaus, Küster</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>310</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.558">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413366.95,5316751.09 413356.96,5316756.79 413359.74,5316761.63 413354.44,5316764.58 413355.51,5316766.5 413356.27,5316767.86 413370.92,5316759.71 413372.46,5316758.85 413367.76,5316750.63 413366.95,5316751.09</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbW</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>3044</ogr:use_id>
      <ogr:use>Gemeindehaus, Küster</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>140</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.559">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413488.33,5316921.32 413484.43,5316923.75 413485.14,5316925.23 413483.52,5316926.1 413484.91,5316928.69 413488.83,5316926.6 413490.64,5316925.64 413491.75,5316925.04 413489.59,5316920.98 413488.47,5316921.58 413488.33,5316921.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw96</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>33</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.560">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413388.86,5316773.42 413380.45,5316757.64 413382.88,5316756.34 413376.05,5316743.57 413365.85,5316749.03 413366.95,5316751.09 413367.76,5316750.63 413372.46,5316758.85 413370.92,5316759.71 413372.37,5316762.41 413372.47,5316762.35 413379.51,5316775.46 413379.41,5316775.52 413380.64,5316777.81 413388.86,5316773.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbX</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>3065</ogr:use_id>
      <ogr:use>Kindergarten, Kinder</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>324</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.561">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413456.05,5316873.34 413459.18,5316871.66 413456.49,5316866.71 413456.0,5316868.33 413454.36,5316867.7 413453.0,5316872.09 413450.13,5316873.61 413451.33,5316875.86 413456.05,5316873.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9k</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>34</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.562">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413402.64,5316662.93 413398.35,5316665.26 413400.94,5316670.2 413405.74,5316667.68 413403.08,5316662.68 413402.64,5316662.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb0</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>30</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.563">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413470.99,5316895.97 413469.46,5316893.09 413462.52,5316896.8 413464.05,5316899.68 413467.63,5316897.77 413470.99,5316895.97</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9e</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>26</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.564">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.87,5316846.93 413528.46,5316844.45 413526.11,5316840.1 413522.96,5316841.8 413521.53,5316842.57 413523.87,5316846.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7l</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>26</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.565">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413470.1,5316798.01 413469.57,5316798.3 413473.46,5316805.05 413476.26,5316803.03 413473.53,5316798.38 413473.7,5316798.28 413472.73,5316796.61 413470.1,5316798.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbv</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>26</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.566">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413465.96,5316790.54 413470.1,5316798.01 413472.73,5316796.61 413468.43,5316789.23 413465.96,5316790.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbt</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.567">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413465.96,5316790.54 413468.43,5316789.23 413464.14,5316781.82 413461.58,5316783.18 413465.96,5316790.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbr</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.568">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413418.88,5316816.17 413416.5,5316811.69 413409.38,5316815.47 413411.8,5316820.04 413418.88,5316816.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbG</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>41</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.569">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413452.63,5316768.27 413456.97,5316775.77 413459.56,5316774.39 413454.92,5316767.05 413452.63,5316768.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbn</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.570">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413416.07,5316800.0 413410.28,5316803.09 413410.23,5316803.12 413412.03,5316806.5 413417.87,5316803.39 413416.07,5316800.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbI</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.571">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413448.14,5316760.77 413452.63,5316768.27 413454.92,5316767.05 413450.2,5316759.59 413448.14,5316760.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbl</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>22</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.572">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.96,5316746.58 413440.6,5316748.88 413442.59,5316747.57 413441.16,5316745.3 413438.96,5316746.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbg</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>7</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.573">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413441.16,5316745.3 413436.83,5316738.46 413434.88,5316739.52 413438.96,5316746.58 413441.16,5316745.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbe</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.574">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413395.7,5316774.38 413396.4,5316775.68 413404.31,5316771.45 413403.61,5316770.15 413395.7,5316774.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbS</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>13</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.575">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413402.65,5316766.08 413397.42,5316768.86 413392.23,5316771.65 413392.87,5316772.81 413393.92,5316772.23 413395.24,5316774.62 413395.7,5316774.38 413403.61,5316770.15 413408.25,5316767.67 413406.35,5316764.11 413405.28,5316764.68 413402.65,5316766.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbT</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>61</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.576">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413391.4,5316758.48 413394.12,5316757.05 413389.89,5316749.17 413387.17,5316750.62 413391.4,5316758.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc0</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>27</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.577">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413389.89,5316749.17 413385.42,5316741.34 413382.68,5316742.82 413387.17,5316750.62 413389.89,5316749.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc2</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>28</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.578">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413383.66,5316726.96 413374.69,5316731.62 413376.22,5316734.58 413385.17,5316729.91 413383.66,5316726.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc7</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>34</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.579">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413385.83,5316725.75 413383.66,5316726.96 413385.17,5316729.91 413387.45,5316728.72 413385.83,5316725.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc8</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>8</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.580">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413368.36,5316717.58 413369.63,5316719.97 413376.17,5316716.49 413374.92,5316714.04 413368.36,5316717.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twca</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>20</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.581">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413369.82,5316706.79 413368.61,5316704.55 413365.81,5316706.05 413367.02,5316708.29 413368.19,5316707.66 413369.82,5316706.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twch</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>8</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.582">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413493.24,5316944.69 413488.62,5316947.15 413489.61,5316948.92 413493.64,5316956.12 413493.73,5316956.29 413498.3,5316953.74 413493.24,5316944.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8Z</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>55</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.583">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413493.24,5316944.69 413496.95,5316942.71 413495.11,5316939.26 413490.48,5316941.67 413486.66,5316943.65 413488.62,5316947.15 413493.24,5316944.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw91</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>38</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.584">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413318.54,5316545.77 413321.08,5316544.31 413318.13,5316538.59 413315.22,5316540.1 413310.94,5316542.31 413309.66,5316539.88 413300.61,5316544.68 413301.96,5316547.15 413302.92,5316548.96 413300.91,5316550.19 413340.64,5316624.65 413357.76,5316615.51 413351.0,5316603.07 413349.24,5316603.97 413318.54,5316545.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7h</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>1584</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.585">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413480.77,5316920.96 413480.99,5316920.84 413479.05,5316917.2 413477.59,5316917.95 413477.0,5316916.79 413473.45,5316918.6 413475.97,5316923.53 413480.77,5316920.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw99</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.586">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413442.11,5316754.62 413439.19,5316756.29 413440.75,5316759.18 413441.75,5316758.64 413442.04,5316759.17 413444.77,5316757.14 413446.87,5316759.81 413447.38,5316759.5 413448.14,5316760.77 413450.2,5316759.59 413445.74,5316752.54 413442.11,5316754.62</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbj</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>41</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.587">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413351.13,5316716.8 413353.3,5316720.74 413367.38,5316713.22 413365.25,5316709.24 413356.32,5316714.02 413351.13,5316716.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twce</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>72</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.588">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413483.58,5316797.77 413476.26,5316803.03 413473.46,5316805.05 413474.86,5316807.22 413477.31,5316805.54 413482.46,5316801.98 413485.09,5316800.05 413483.58,5316797.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbx</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>34</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.589">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413482.46,5316801.98 413483.06,5316802.81 413484.5,5316804.8 413484.94,5316805.46 413487.53,5316803.72 413485.09,5316800.05 413482.46,5316801.98</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbz</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>14</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.590">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413358.43,5316733.27 413360.85,5316737.77 413375.41,5316729.97 413373.0,5316725.45 413370.91,5316726.57 413358.43,5316733.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc6</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>85</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.591">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413416.01,5316686.99 413414.98,5316685.05 413413.78,5316682.78 413409.68,5316684.99 413413.78,5316692.59 413417.83,5316690.41 413416.01,5316686.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb4</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>40</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.592">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413469.51,5316901.3 413467.63,5316897.77 413464.05,5316899.68 413465.78,5316903.29 413469.51,5316901.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9c</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>17</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.593">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413392.01,5316782.06 413393.36,5316781.34 413388.86,5316773.42 413380.64,5316777.81 413378.95,5316778.71 413382.46,5316785.32 413383.36,5316784.84 413384.13,5316786.28 413392.01,5316782.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbR</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.594">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.96,5316915.67 413477.08,5316912.94 413472.92,5316905.17 413472.89,5316905.13 413467.95,5316907.84 413471.96,5316915.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9b</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>51</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.595">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413540.09,5316851.39 413529.95,5316856.83 413532.74,5316862.14 413542.88,5316856.72 413540.09,5316851.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7m</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>69</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.596">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413456.05,5316873.34 413451.33,5316875.86 413455.57,5316883.8 413460.29,5316881.28 413456.05,5316873.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9i</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>48</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.597">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413439.64,5316828.14 413435.82,5316830.18 413433.67,5316826.14 413430.92,5316827.68 413430.85,5316827.72 413430.91,5316827.82 413434.69,5316834.59 413440.55,5316831.46 413441.22,5316831.1 413439.64,5316828.14</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbC</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>39</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.598">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.54,5316817.87 413430.51,5316815.96 413425.73,5316818.52 413428.67,5316823.57 413433.24,5316821.05 413431.54,5316817.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbE</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.599">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413461.58,5316783.18 413464.14,5316781.82 413459.56,5316774.39 413456.97,5316775.77 413461.58,5316783.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbp</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.600">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413424.37,5316807.32 413419.3,5316798.29 413416.07,5316800.0 413417.87,5316803.39 413420.93,5316809.16 413424.37,5316807.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbJ</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>39</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.601">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413404.99,5316781.73 413406.49,5316784.38 413409.06,5316783.03 413407.86,5316780.75 413409.3,5316779.99 413406.27,5316774.45 413402.12,5316776.65 413404.99,5316781.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbP</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>37</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.602">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413385.42,5316741.34 413389.29,5316739.26 413386.25,5316733.7 413379.53,5316737.37 413382.68,5316742.82 413385.42,5316741.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc4</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>48</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.603">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413480.77,5316920.96 413475.97,5316923.53 413478.22,5316927.93 413477.96,5316928.07 413478.86,5316929.68 413479.82,5316931.4 413482.84,5316929.79 413484.91,5316928.69 413483.52,5316926.1 413480.77,5316920.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw95</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>49</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.604">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413495.98,5316856.49 413509.12,5316849.44 413509.05,5316849.31 413503.8,5316839.47 413502.08,5316840.39 413504.24,5316844.46 413495.0,5316849.37 413492.83,5316845.32 413490.65,5316846.49 413495.98,5316856.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7j</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1020</ogr:use_id>
      <ogr:use>Wohnheim</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>121</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.605">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413134.08,5316861.05 413142.23,5316857.07 413143.68,5316860.04 413148.71,5316857.6 413147.26,5316854.61 413149.43,5316853.55 413146.33,5316847.11 413136.87,5316851.74 413130.99,5316854.62 413134.08,5316861.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiW</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>141</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.606">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413173.0,5316852.3 413162.48,5316830.18 413153.43,5316834.46 413164.13,5316857.05 413164.16,5316857.14 413164.19,5316857.1 413173.04,5316852.39 413173.0,5316852.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiC</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>249</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.607">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413164.19,5316857.1 413168.22,5316865.48 413177.05,5316860.78 413173.04,5316852.39 413164.19,5316857.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twix</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>93</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.608">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413161.25,5316888.6 413165.7,5316886.44 413162.52,5316880.17 413161.4,5316880.72 413160.2,5316878.2 413159.75,5316878.42 413155.43,5316869.33 413151.85,5316871.2 413141.56,5316876.6 413149.95,5316894.08 413161.25,5316888.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiT</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>321</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.609">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413172.1,5316873.59 413180.85,5316868.88 413181.24,5316868.67 413177.05,5316860.78 413168.22,5316865.48 413168.25,5316865.54 413166.51,5316866.48 413167.78,5316869.08 413169.25,5316868.28 413172.1,5316873.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twiy</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.610">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413129.23,5316850.96 413122.94,5316853.98 413141.5,5316892.69 413140.73,5316893.05 413143.46,5316898.73 413150.29,5316895.44 413151.12,5316897.18 413151.38,5316897.05 413149.95,5316894.08 413141.56,5316876.6 413138.75,5316870.76 413136.33,5316865.72 413134.08,5316861.05 413130.99,5316854.62 413129.69,5316851.91 413129.23,5316850.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj8</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>351</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.611">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413172.1,5316873.59 413172.0,5316873.64 413178.22,5316886.67 413187.35,5316882.42 413180.85,5316868.88 413172.1,5316873.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiA</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>148</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.612">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413136.87,5316851.74 413135.59,5316849.03 413129.69,5316851.91 413130.99,5316854.62 413136.87,5316851.74</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiU</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>20</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.613">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413143.62,5316868.18 413141.2,5316863.12 413136.33,5316865.72 413138.75,5316870.76 413143.62,5316868.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiV</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.614">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413151.85,5316871.2 413150.42,5316868.42 413145.08,5316871.22 413143.62,5316868.18 413138.75,5316870.76 413141.56,5316876.6 413151.85,5316871.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiX</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>55</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.615">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413173.0,5316852.3 413175.85,5316850.81 413161.73,5316821.04 413151.59,5316826.73 413152.46,5316828.19 413151.37,5316828.27 413150.87,5316827.42 413147.48,5316829.43 413161.26,5316858.57 413164.13,5316857.05 413164.16,5316857.14 413164.19,5316857.1 413173.04,5316852.39 413173.0,5316852.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiB</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>538</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.616">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413181.24,5316868.67 413183.6,5316867.4 413179.47,5316859.56 413177.05,5316860.78 413181.24,5316868.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twiz</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.617">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413151.38,5316897.05 413153.99,5316902.47 413165.27,5316897.04 413168.67,5316895.34 413169.17,5316893.93 413167.15,5316893.22 413164.17,5316894.72 413161.25,5316888.6 413149.95,5316894.08 413151.38,5316897.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiS</ogr:gmlid>
      <ogr:street>Kleineschholzweg</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>128</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.618">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413607.01,5316481.72 413607.35,5316482.33 413605.69,5316483.22 413609.86,5316491.09 413608.78,5316491.66 413614.22,5316501.97 413615.29,5316501.41 413619.46,5316509.26 413621.33,5316508.27 413622.79,5316511.07 413682.54,5316491.69 413681.69,5316489.18 413686.92,5316487.5 413687.071399998,5316487.61939998 413687.225899994,5316487.73469998 413687.38339999,5316487.84589998 413687.543899986,5316487.95279998 413687.707099982,5316488.0554 413687.872999978,5316488.1536 413688.041499999,5316488.24739999 413688.212399995,5316488.33659999 413688.38559999,5316488.42119999 413688.560999986,5316488.50119999 413688.738499981,5316488.57639999 413688.917999977,5316488.64689998 413689.099199997,5316488.71259998 413689.282199993,5316488.77339998 413689.466699988,5316488.82929998 413689.652599983,5316488.88029998 413689.839899979,5316488.92629998 413690.028299999,5316488.96729998 413690.217699995,5316489.0032 413690.408,5316489.034 413690.598999985,5316489.0597 413690.79059998,5316489.0803 413690.982699975,5316489.0959 413691.175199996,5316489.1063 413691.367799991,5316489.1116 413691.560599986,5316489.1117 413691.753199981,5316489.1067 413691.945699976,5316489.0965 413692.137799997,5316489.0812 413692.329499992,5316489.0608 413692.520499987,5316489.0353 413692.710799982,5316489.0047 413692.900199977,5316488.96899998 413693.088599998,5316488.92829998 413693.275799993,5316488.88259998 413693.461799988,5316488.83189998 413693.646299984,5316488.77629998 413693.829299979,5316488.71569998 413694.0105,5316488.65029998 413694.19,5316488.58 413694.372499991,5316488.49939999 413694.552699986,5316488.41379999 413694.730499982,5316488.32339999 413694.905799977,5316488.22799999 413695.078299998,5316488.1279 413695.24799999,5316488.023 413695.41479999,5316487.91349998 413695.578499985,5316487.79949998 413695.739,5316487.681 413695.896099977,5316487.55819999 413696.049799999,5316487.43099999 413696.2,5316487.29979999 413696.346499991,5316487.1644 413696.489099988,5316487.025 413696.627999984,5316486.88179998 413696.762799981,5316486.73479998 413696.893499977,5316486.58419999 413697.02,5316486.43 413697.138199996,5316486.28019999 413697.252499994,5316486.1273 413697.362599991,5316485.97149998 413697.468599988,5316485.81279998 413697.570399986,5316485.65129998 413697.667899983,5316485.48729999 413697.760999981,5316485.32069999 413697.849699979,5316485.1517 413697.933799976,5316484.98039998 413698.0135,5316484.80699998 413698.088499998,5316484.63149998 413698.158799996,5316484.45409999 413698.224499994,5316484.27489999 413698.285299993,5316484.094 413698.341399991,5316483.91159998 413698.39269999,5316483.72769998 413698.438999989,5316483.54259999 413698.480499988,5316483.35629999 413698.517,5316483.169 413698.548599986,5316482.98079997 413698.575099985,5316482.79179998 413698.596699985,5316482.60219998 413698.613299984,5316482.41199999 413698.624799984,5316482.22149999 413698.631299984,5316482.0308 413698.632699984,5316481.84 413698.629099984,5316481.64919998 413698.620499984,5316481.45849999 413698.606799985,5316481.26819999 413698.588099985,5316481.0782 413698.564399986,5316480.88889998 413698.535699986,5316480.70019998 413698.502099987,5316480.51229999 413698.463399988,5316480.32539999 413698.419899989,5316480.1396 413698.371399991,5316479.95499998 413698.318099992,5316479.77179998 413698.26,5316479.59 413698.196499995,5316479.41859999 413698.128899997,5316479.24879999 413698.057299999,5316479.0806 413697.981599975,5316478.91419998 413697.901999977,5316478.74969998 413697.818499979,5316478.58709999 413697.731099982,5316478.42659999 413697.639899984,5316478.26819999 413697.545,5316478.112 413697.446299989,5316477.95799998 413697.343899991,5316477.80649998 413697.237899994,5316477.65739998 413697.128399997,5316477.51089999 413697.0154,5316477.36709999 413696.898999977,5316477.22599999 413696.77919998,5316477.0877 413696.656199983,5316476.95239998 413696.53,5316476.82 413696.39429999,5316476.69179998 413696.255499994,5316476.56699999 413696.113699997,5316476.44559999 413695.968999976,5316476.32769999 413695.821399979,5316476.21339999 413695.671099983,5316476.1028 413695.518099987,5316475.99589998 413695.362499991,5316475.89269998 413695.204399995,5316475.79339998 413695.044,5316475.698 413694.881299978,5316475.60649998 413694.716299982,5316475.51909999 413694.549299986,5316475.43569999 413694.38019999,5316475.35649999 413694.209299995,5316475.28139999 413694.036599999,5316475.21059999 413693.862199978,5316475.1439 413693.686299983,5316475.0816 413693.508799987,5316475.0236 413693.33,5316474.97 413692.27,5316471.71 413690.73,5316466.93 413690.48,5316466.15 413686.43,5316453.65 413683.37,5316444.21 413680.61,5316435.7 413668.36,5316442.14 413666.92,5316439.4 413665.72,5316440.03 413664.41,5316437.64 413664.36,5316437.54 413663.85,5316437.93 413644.03,5316453.13 413641.21,5316447.84 413639.36,5316444.36 413639.3,5316444.39 413636.85,5316445.66 413637.2,5316446.29 413634.61,5316447.67 413634.442499989,5316447.59689999 413634.272899993,5316447.52859999 413634.101499997,5316447.46509999 413633.928399977,5316447.40649999 413633.753599981,5316447.35289999 413633.577399985,5316447.30419999 413633.39989999,5316447.26049999 413633.221299994,5316447.22189999 413633.041599999,5316447.1884 413632.861,5316447.16 413632.679799983,5316447.1368 413632.497999987,5316447.1186 413632.315699992,5316447.1057 413632.133199997,5316447.0979 413631.950499976,5316447.0953 413631.767799981,5316447.0979 413631.585299985,5316447.1057 413631.40299999,5316447.1186 413631.221199994,5316447.1367 413631.04,5316447.16 413630.848499979,5316447.1915 413630.657999983,5316447.22879999 413630.468799988,5316447.27199999 413630.280899993,5316447.32089999 413630.094699998,5316447.37549999 413629.910199977,5316447.43579999 413629.727599982,5316447.50169999 413629.547199986,5316447.57309999 413629.36899999,5316447.65 413629.193199995,5316447.73239998 413629.02,5316447.82 413628.849499979,5316447.91289998 413628.681899983,5316448.011 413628.517399987,5316448.1142 413628.356199991,5316448.22239999 413628.198299995,5316448.33549999 413628.043899999,5316448.45329999 413627.893299977,5316448.57579999 413627.746399981,5316448.70279998 413627.603499985,5316448.83429998 413627.464599988,5316448.97009998 413627.33,5316449.11 413627.20799999,5316449.24689999 413627.089899998,5316449.38729999 413626.975999975,5316449.53099999 413626.866199978,5316449.67789998 413626.760699981,5316449.82799998 413626.659499983,5316449.98089998 413626.562799986,5316450.1368 413626.470599988,5316450.29529999 413626.38299999,5316450.45639999 413626.3,5316450.62 413626.221799994,5316450.78589998 413626.148299996,5316450.95399998 413626.079799998,5316451.1241 413626.0161,5316451.29609999 413625.957399976,5316451.46989999 413625.903699977,5316451.64519998 413625.855099978,5316451.82209998 413625.81159998,5316452.0003 413625.77319998,5316452.1796 413625.74,5316452.36 413624.65,5316452.93 413624.657199983,5316452.73419998 413624.655199983,5316452.53829999 413624.644299984,5316452.34269999 413624.624299984,5316452.1478 413624.595399985,5316451.95399998 413624.557499986,5316451.76179998 413624.510799987,5316451.57149999 413624.455399989,5316451.38359999 413624.39129999,5316451.1985 413624.318799992,5316451.0165 413624.238,5316450.838 413624.149,5316450.66339998 413624.052,5316450.49309999 413623.947299976,5316450.32739999 413623.834999979,5316450.1668 413623.715399982,5316450.0115 413623.588799985,5316449.86199998 413623.455399989,5316449.71839998 413623.315499992,5316449.58109999 413623.169399996,5316449.45039999 413623.0175,5316449.32669999 413622.86,5316449.21 413622.706899982,5316449.1101 413622.549699986,5316449.0169 413622.38859999,5316448.93049998 413622.223899994,5316448.85119998 413622.056,5316448.779 413621.885399978,5316448.71419998 413621.712199982,5316448.65679998 413621.536699986,5316448.60689999 413621.359199991,5316448.56459999 413621.18,5316448.53 413621.0068,5316448.50479999 413620.832599979,5316448.48679999 413620.657899983,5316448.47599999 413620.482899988,5316448.47239999 413620.307899992,5316448.47609999 413620.133099997,5316448.48699999 413619.959,5316448.505 413619.78549998,5316448.53029999 413619.613199985,5316448.56269999 413619.442499989,5316448.60229998 413619.273499993,5316448.64889998 413619.106499997,5316448.70249998 413618.941999976,5316448.76289998 413618.78,5316448.83 413617.7,5316449.51 413615.4,5316450.61 413615.74,5316451.17 413613.13,5316452.53 413613.06,5316452.58 413615.21,5316456.66 413615.367299991,5316456.78349998 413615.522699987,5316456.90919998 413615.676399983,5316457.0371 413615.828199979,5316457.1672 413615.978199975,5316457.29939999 413616.126199997,5316457.43379999 413616.272399993,5316457.57019999 413616.416499989,5316457.70879998 413616.558699986,5316457.84939998 413616.698799982,5316457.99199997 413616.836899979,5316458.1366 413616.972899975,5316458.28309999 413617.106799997,5316458.43159999 413617.238599994,5316458.58199999 413617.368199991,5316458.73419998 413617.495599987,5316458.88829998 413617.620799984,5316459.0442 413617.743699981,5316459.2018 413617.864399978,5316459.36119999 413617.982799975,5316459.52229999 413618.098899997,5316459.68509998 413618.212699995,5316459.84949998 413618.324099992,5316460.0156 413618.433099989,5316460.1832 413618.539699986,5316460.35229999 413618.643899984,5316460.52299999 413618.745599981,5316460.69509998 413618.844799979,5316460.86869998 413618.941599976,5316461.0436 413619.035899999,5316461.21989999 413619.127599997,5316461.39759999 413619.216799995,5316461.57649999 413619.303399992,5316461.75669998 413619.38739999,5316461.93809998 413619.468899988,5316462.1207 413619.547699986,5316462.30449999 413619.623899984,5316462.48929999 413619.697399982,5316462.67529998 413619.768299981,5316462.86219998 413619.836499979,5316463.0502 413619.901999977,5316463.23909999 413619.964799976,5316463.42889999 413620.024899999,5316463.61959998 413620.082199998,5316463.81109998 413620.136799997,5316464.0034 413620.188699995,5316464.19649999 413620.237799994,5316464.39039999 413620.284099993,5316464.58489999 413620.327599992,5316464.78 413620.368399991,5316464.97569998 413620.40629999,5316465.172 413620.441399989,5316465.36889999 413620.473799988,5316465.56619999 413620.503299987,5316465.76389998 413620.529899987,5316465.96209998 413620.553799986,5316466.1606 413620.574799986,5316466.35939999 413620.592899985,5316466.55849999 413620.608199985,5316466.75789998 413620.620699984,5316466.95739998 413620.630299984,5316467.1571 413620.636999984,5316467.35689999 413620.640899984,5316467.55679999 413620.641999984,5316467.75679998 413620.640199984,5316467.95669998 413620.635499984,5316468.1566 413620.627999984,5316468.35639999 413620.617599984,5316468.55599999 413620.604299985,5316468.75549998 413620.588299985,5316468.95479998 413620.569299986,5316469.1539 413620.547599986,5316469.35259999 413620.522999987,5316469.55099999 413620.495499988,5316469.74909998 413620.465299988,5316469.94669998 413620.432199989,5316470.1439 413620.39629999,5316470.34059999 413620.357599991,5316470.53669999 413620.316099992,5316470.73229998 413620.271799993,5316470.92729998 413620.224699994,5316471.1216 413620.174899996,5316471.31519999 413620.122299997,5316471.50809999 413620.066899998,5316471.70019998 413620.0088,5316471.89149998 413619.948,5316472.082 413619.884499978,5316472.27159999 413619.818199979,5316472.46019999 413619.749299981,5316472.64789998 413619.677699983,5316472.83459998 413619.603399985,5316473.0202 413619.526499987,5316473.20479999 413619.446999989,5316473.38819999 413619.364899991,5316473.57049999 413619.280099993,5316473.75159998 413619.192799995,5316473.93139998 413619.102899997,5316474.11 413619.0105,5316474.28729999 413618.915599977,5316474.46329999 413618.818099979,5316474.63779998 413618.718199982,5316474.81099998 413618.615799984,5316474.98269998 413618.510999987,5316475.153 413618.40369999,5316475.32169999 413618.29399999,5316475.48889999 413618.182,5316475.65449998 413618.067599998,5316475.81849998 413617.950899976,5316475.98079997 413617.831899979,5316476.1414 413617.710599982,5316476.30039999 413617.586999985,5316476.45749999 413617.461199988,5316476.61289998 413617.333199992,5316476.76649998 413617.20299999,5316476.91829998 413617.070699998,5316477.0681 413616.936199976,5316477.21609999 413616.79959998,5316477.36209999 413616.660999983,5316477.50619999 413616.520299987,5316477.64819998 413616.37759999,5316477.78829998 413616.232899994,5316477.92619998 413616.086299998,5316478.0621 413615.937699976,5316478.1959 413615.78719998,5316478.32759999 413615.634899984,5316478.45709999 413615.480699988,5316478.58439999 413615.324799992,5316478.70949998 413615.167,5316478.83229998 413615.0075,5316478.95289998 413614.846399979,5316479.0712 413614.683499983,5316479.18719999 413614.518999987,5316479.30079999 413614.352899991,5316479.41209999 413614.185199995,5316479.52099999 413614.016,5316479.62749998 413613.845299979,5316479.73149998 413613.673099983,5316479.83309998 413613.499499987,5316479.93229998 413613.324399992,5316480.0289 413613.148099996,5316480.1231 413612.970299975,5316480.21469999 413612.79129998,5316480.30369999 413612.611099985,5316480.39019999 413612.429599989,5316480.47409999 413612.24699999,5316480.55549999 413612.063199998,5316480.63419998 413611.878299978,5316480.71019998 413611.692299983,5316480.78359998 413611.505299987,5316480.85439998 413611.317299992,5316480.92239998 413611.128299997,5316480.98779998 413610.938499976,5316481.0505 413610.747799981,5316481.1104 413610.556199986,5316481.1676 413610.363799991,5316481.22209999 413610.170699996,5316481.27379999 413609.976799975,5316481.32279999 413609.78229998,5316481.36899999 413609.587099985,5316481.41239999 413609.39139999,5316481.45299999 413609.195,5316481.49079999 413608.998199975,5316481.52579999 413608.80089998,5316481.55799999 413608.603099985,5316481.58729998 413608.40489999,5316481.61389998 413608.206399995,5316481.63759998 413608.0076,5316481.65839998 413607.80839998,5316481.67639998 413607.609099985,5316481.69159998 413607.40949999,5316481.70389998 413607.209799995,5316481.71339998 413607.01,5316481.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuV1</ogr:gmlid>
      <ogr:street>Konrad-Adenauer-Platz</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>3036</ogr:use_id>
      <ogr:use>Veranstaltungsgebäud</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>4261</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.619">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413616.33,5316530.87 413616.450899989,5316530.90599998 413616.573,5316530.938 413616.696099982,5316530.96599998 413616.82,5316530.99 413617.24,5316532.8 413618.59,5316532.35 413618.48,5316532.91 413626.56,5316529.95 413624.01,5316522.05 413615.71,5316524.42 413616.57,5316525.2 413615.38,5316527.07 413614.7,5316525.8 413613.62,5316523.77 413634.66,5316512.73 413649.98,5316504.73 413683.27,5316493.85 413697.89,5316489.11 413697.1,5316486.89 413697.16,5316486.87 413701.47,5316485.53 413700.27,5316483.19 413700.7,5316483.05 413698.06,5316476.35 413696.53,5316476.82 413694.08,5316477.56 413692.2,5316471.74 413692.27,5316471.71 413693.89,5316471.19 413692.35,5316466.4 413690.73,5316466.93 413690.66,5316466.95 413690.41,5316466.19 413690.48,5316466.15 413692.03,5316465.3 413691.37,5316464.03 413692.32,5316463.5 413688.69,5316452.39 413686.43,5316453.65 413686.33,5316453.7 413683.26,5316444.27 413683.37,5316444.21 413686.3,5316442.7 413681.46,5316433.47 413679.1,5316434.72 413678.65,5316433.89 413675.98,5316433.11 413673.17,5316434.61 413672.48,5316433.37 413664.41,5316437.64 413663.85,5316437.93 413654.67,5316442.78 413653.83,5316441.2 413641.21,5316447.84 413641.12,5316447.89 413639.3,5316444.39 413637.7,5316441.3 413622.86,5316449.21 413622.706899982,5316449.1101 413622.549699986,5316449.0169 413622.38859999,5316448.93049998 413622.223899994,5316448.85119998 413622.056,5316448.779 413621.885399978,5316448.71419998 413621.712199982,5316448.65679998 413621.536699986,5316448.60689999 413621.359199991,5316448.56459999 413621.18,5316448.53 413621.0068,5316448.50479999 413620.832599979,5316448.48679999 413620.657899983,5316448.47599999 413620.482899988,5316448.47239999 413620.307899992,5316448.47609999 413620.133099997,5316448.48699999 413619.959,5316448.505 413619.78549998,5316448.53029999 413619.613199985,5316448.56269999 413619.442499989,5316448.60229998 413619.273499993,5316448.64889998 413619.106499997,5316448.70249998 413618.941999976,5316448.76289998 413618.78,5316448.83 413617.7,5316449.51 413615.4,5316450.61 413615.74,5316451.17 413613.13,5316452.53 413613.06,5316452.58 413597.56,5316458.97 413596.6,5316459.52 413596.09,5316458.52 413566.15,5316447.5 413573.8,5316462.35 413572.32,5316463.14 413570.53,5316464.09 413570.88,5316464.8 413557.88,5316471.71 413561.44,5316478.39 413566.38,5316475.77 413567.78,5316478.21 413562.77,5316480.85 413566.38,5316487.78 413571.83,5316484.95 413572.5,5316486.15 413569.93,5316487.51 413570.17,5316488.02 413570.0002,5316488.1251 413569.832499979,5316488.23379999 413569.667299983,5316488.34599999 413569.504499987,5316488.46169999 413569.344199991,5316488.58089999 413569.186499995,5316488.70349998 413569.031499999,5316488.82949998 413568.879199978,5316488.95869998 413568.729699982,5316489.0912 413568.582999985,5316489.22679999 413568.439399989,5316489.36559999 413568.298699992,5316489.50739999 413568.161099996,5316489.65219998 413568.026599999,5316489.79989998 413567.895299977,5316489.95039998 413567.767299981,5316490.1038 413567.642599984,5316490.25979999 413567.521299987,5316490.41849999 413567.40329999,5316490.57969999 413567.288899993,5316490.74339998 413567.178,5316490.90959998 413567.070699998,5316491.078 413566.966999976,5316491.24879999 413566.867099978,5316491.42169999 413566.77079998,5316491.59669998 413566.678299983,5316491.77369998 413566.589599985,5316491.95269998 413566.504799987,5316492.1336 413566.423899989,5316492.31619999 413566.346899991,5316492.50049999 413566.273799993,5316492.68639998 413566.204799995,5316492.87389998 413566.139799996,5316493.0627 413566.079,5316493.253 413566.022299999,5316493.44449999 413565.969599976,5316493.63709998 413565.920999977,5316493.83079998 413565.876599978,5316494.0255 413565.836299979,5316494.22109999 413565.80029998,5316494.41749999 413565.768499981,5316494.61469998 413565.740899981,5316494.81249998 413565.717499982,5316495.0108 413565.698399982,5316495.2096 413565.683599983,5316495.40879999 413565.672999983,5316495.60819998 413565.666699983,5316495.80779998 413565.664699983,5316496.0075 413565.666999983,5316496.20719999 413565.673499983,5316496.40679999 413565.684399983,5316496.60619998 413565.699399982,5316496.80529998 413565.718799982,5316497.0041 413565.742399981,5316497.2024 413565.770199981,5316497.40019999 413565.80229998,5316497.59729998 413565.838599979,5316497.79369998 413565.879099978,5316497.98919997 413565.923799977,5316498.18389999 413565.972599975,5316498.37749999 413566.025599999,5316498.57009999 413566.082599998,5316498.76139998 413566.143799996,5316498.95149998 413566.209,5316499.1403 413566.278299993,5316499.32759999 413566.351499991,5316499.51339999 413566.428799989,5316499.69759998 413566.51,5316499.88 413566.594499985,5316500.059 413566.682599983,5316500.23619999 413566.77449998,5316500.41149999 413566.870099978,5316500.58469999 413566.969399975,5316500.75599998 413567.072299998,5316500.92499998 413567.178699996,5316501.0919 413567.288599993,5316501.25649999 413567.40209999,5316501.41869999 413567.518899987,5316501.57839999 413567.639099984,5316501.73559998 413567.762599981,5316501.89029998 413567.889399978,5316502.0423 413568.0193,5316502.1915 413568.152399996,5316502.33799999 413568.288599993,5316502.48159999 413568.427799989,5316502.62229998 413568.569999986,5316502.76 413568.714999982,5316502.89459998 413568.862899978,5316503.0262 413569.0135,5316503.1546 413569.166799996,5316503.27969999 413569.322799992,5316503.40159999 413569.481299988,5316503.52009999 413569.642199984,5316503.63519998 413569.80559998,5316503.74689998 413569.971299975,5316503.85509998 413570.139299996,5316503.95979998 413570.309499992,5316504.0609 413570.481699988,5316504.1583 413570.655999983,5316504.25209999 413570.832299979,5316504.34209999 413571.0104,5316504.42839999 413571.190299995,5316504.51079999 413571.371899991,5316504.58949998 413571.555199986,5316504.66419998 413571.74,5316504.735 413571.926299977,5316504.80179998 413572.113899997,5316504.86469998 413572.302799992,5316504.92369998 413572.492899988,5316504.97859997 413572.684099983,5316505.0295 413572.876299978,5316505.0763 413573.069599998,5316505.119 413573.263599993,5316505.1576 413573.458499988,5316505.19209999 413573.653999983,5316505.22239999 413573.850199979,5316505.24859999 413574.046799999,5316505.27059999 413574.243899994,5316505.28839999 413574.441299989,5316505.30209999 413574.638899984,5316505.31149999 413574.836799979,5316505.31679999 413575.034599999,5316505.31779999 413575.232499994,5316505.31469999 413575.430199989,5316505.30739999 413575.627799984,5316505.29579999 413575.824999979,5316505.28009999 413576.021899999,5316505.26019999 413576.218299994,5316505.23609999 413576.41419999,5316505.20779999 413576.609399985,5316505.1754 413576.80379998,5316505.1389 413576.997499975,5316505.0983 413577.190199995,5316505.0535 413577.38199999,5316505.0047 413577.572699986,5316504.95179998 413577.762199981,5316504.89479998 413577.950499976,5316504.83389998 413578.137399997,5316504.76899998 413578.322899992,5316504.70009998 413578.506899987,5316504.62729998 413578.689299983,5316504.55059999 413578.87,5316504.47 413579.67,5316506.01 413579.500799987,5316506.1159 413579.333899992,5316506.22529999 413579.169299996,5316506.33829999 413579.0073,5316506.45479999 413578.847699979,5316506.57469999 413578.690699983,5316506.69799998 413578.536399986,5316506.82459998 413578.38489999,5316506.95439998 413578.236099994,5316507.0875 413578.090199998,5316507.22369999 413577.947299976,5316507.36299999 413577.80729998,5316507.50539999 413577.670499983,5316507.65059998 413577.536699986,5316507.79879998 413577.40619999,5316507.94979998 413577.278899993,5316508.1035 413577.154899996,5316508.25989999 413577.034299999,5316508.41889999 413576.917099977,5316508.58049999 413576.80329998,5316508.74449998 413576.693099983,5316508.91099998 413576.586499985,5316509.0797 413576.483499988,5316509.25069999 413576.38419999,5316509.42379999 413576.288599993,5316509.59899998 413576.196699995,5316509.77619998 413576.108699997,5316509.95529998 413576.024499999,5316510.1363 413575.944199976,5316510.31899999 413575.867799978,5316510.50339999 413575.79529998,5316510.68939998 413575.726899982,5316510.87689998 413575.662399983,5316511.0658 413575.602,5316511.256 413575.545699986,5316511.44749999 413575.493399988,5316511.64009998 413575.445299989,5316511.83379998 413575.40139999,5316512.0285 413575.361599991,5316512.22409999 413575.32599999,5316512.42049999 413575.294499993,5316512.61759998 413575.267299993,5316512.81529998 413575.244399994,5316513.0136 413575.225599994,5316513.21229999 413575.211099995,5316513.41139999 413575.200899995,5316513.61069998 413575.194899995,5316513.81019998 413575.193199995,5316514.0098 413575.195699995,5316514.20939999 413575.202499995,5316514.40889999 413575.213499995,5316514.60819998 413575.228799994,5316514.80719998 413575.248299994,5316515.0058 413575.272099993,5316515.204 413575.300099992,5316515.40159999 413575.332299992,5316515.59859999 413575.368699991,5316515.79479998 413575.40929999,5316515.99029997 413575.453999989,5316516.1848 413575.502899987,5316516.37829999 413575.555899986,5316516.57069999 413575.612999985,5316516.76199998 413575.674199983,5316516.95199998 413575.739399981,5316517.1406 413575.80859998,5316517.32779999 413575.881799978,5316517.51349999 413575.958899976,5316517.69759998 413576.04,5316517.88 413576.124799997,5316518.0597 413576.213399995,5316518.23759999 413576.305699992,5316518.41359999 413576.40179999,5316518.58749999 413576.501499987,5316518.75939998 413576.604799985,5316518.92909998 413576.711799982,5316519.0966 413576.822199979,5316519.26179999 413576.936099976,5316519.42459999 413577.053499999,5316519.58499999 413577.174299996,5316519.74279998 413577.298299992,5316519.89809998 413577.425699989,5316520.0506 413577.556199986,5316520.20039999 413577.689899983,5316520.34749999 413577.826699979,5316520.49159999 413577.966499976,5316520.63279998 413578.109299997,5316520.77099998 413578.25499999,5316520.90619998 413578.40349999,5316521.0382 413578.554799986,5316521.167 413578.708799982,5316521.29259999 413578.865399978,5316521.41489999 413579.024599999,5316521.53389999 413579.186199995,5316521.64939998 413579.350299991,5316521.76149998 413579.516799987,5316521.87009998 413579.685499983,5316521.97509998 413579.856399978,5316522.0765 413580.029399999,5316522.1743 413580.204399995,5316522.26839999 413580.38139999,5316522.35869999 413580.560299986,5316522.44529999 413580.740999981,5316522.52799999 413580.923299977,5316522.60689999 413581.107399997,5316522.68189998 413581.292899993,5316522.75289998 413581.48,5316522.82 413581.668399983,5316522.88309998 413581.858099978,5316522.94219998 413582.049,5316522.99729997 413582.241099994,5316523.0483 413582.434199989,5316523.0952 413582.628199984,5316523.138 413582.823099979,5316523.1767 413583.018799999,5316523.2112 413583.215199995,5316523.24159999 413583.41209999,5316523.26779999 413583.609599985,5316523.28979999 413583.80749998,5316523.30759999 413584.0057,5316523.32119999 413584.204199995,5316523.33059999 413584.40289999,5316523.33579999 413584.601599985,5316523.33669999 413584.80019998,5316523.33349999 413584.998799975,5316523.32599999 413585.197099995,5316523.31429999 413585.39519999,5316523.29839999 413585.592899985,5316523.27829999 413585.79009998,5316523.25409999 413585.986799975,5316523.22559999 413586.182799995,5316523.193 413586.37799999,5316523.1562 413586.572499986,5316523.1152 413586.765999981,5316523.0702 413586.958499976,5316523.0211 413587.15,5316522.96789998 413587.340199991,5316522.91059998 413587.529299987,5316522.84929998 413587.716899982,5316522.78399998 413587.903199977,5316522.71469998 413588.087899998,5316522.64149998 413588.27099999,5316522.56439999 413588.452499989,5316522.48349999 413588.632199984,5316522.39869999 413588.81,5316522.31 413592.37,5316532.52 413592.65,5316533.1 413593.25,5316535.04 413607.3,5316527.65 413609.49,5316531.67 413609.579899985,5316531.83029998 413609.677399983,5316531.98609998 413609.78219998,5316532.1371 413609.894199977,5316532.28279999 413610.013,5316532.423 413610.138399997,5316532.55729999 413610.270099993,5316532.68549998 413610.40789999,5316532.80719998 413610.551199986,5316532.92209998 413610.7,5316533.03 413610.843399979,5316533.1185 413610.990399975,5316533.2007 413611.140899996,5316533.27659999 413611.294499993,5316533.34579999 413611.450999989,5316533.40829999 413611.61,5316533.464 413611.771099981,5316533.51269999 413611.934199976,5316533.55449999 413612.099,5316533.58909999 413612.26499999,5316533.61659998 413612.432199989,5316533.63689998 413612.6,5316533.65 413612.79359998,5316533.65319998 413612.987099975,5316533.64619998 413613.179899995,5316533.62899998 413613.371599991,5316533.60169998 413613.561499986,5316533.56439999 413613.749299981,5316533.51719999 413613.934299976,5316533.46019999 413614.116099997,5316533.39349999 413614.294099993,5316533.31739999 413614.467899988,5316533.23199999 413614.636899984,5316533.1377 413614.80079998,5316533.0346 413614.959,5316532.923 413615.111199997,5316532.80329998 413615.256799994,5316532.67569998 413615.39559999,5316532.54069999 413615.526999987,5316532.39849999 413615.650899984,5316532.24969999 413615.766699981,5316532.0945 413615.874199978,5316531.93349998 413615.973099975,5316531.76709998 413616.063199998,5316531.59559998 413616.144099996,5316531.41969999 413616.215699995,5316531.23979999 413616.277699993,5316531.0564 413616.33,5316530.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TuV2</ogr:gmlid>
      <ogr:street>Konrad-Adenauer-Platz</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>8326</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.620">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413584.05,5317022.41 413545.56,5316975.73 413537.32,5316982.58 413540.43,5316986.38 413541.08,5316985.87 413542.67,5316984.63 413557.49,5317002.84 413555.93,5317004.12 413555.45,5317004.51 413557.68,5317007.18 413558.04,5317006.88 413559.68,5317005.52 413574.79,5317023.75 413573.15,5317025.08 413572.81,5317025.38 413575.68,5317029.07 413584.05,5317022.41</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7g</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>518</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.621">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413641.37,5317069.27 413646.12,5317074.95 413649.12,5317072.45 413644.39,5317066.76 413641.37,5317069.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw70</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>12 x</ogr:house_nr>
      <ogr:use_id>2523</ogr:use_id>
      <ogr:use>Umformer</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.622">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413553.16,5316959.78 413556.47,5316967.7 413556.85,5316967.55 413562.49,5316974.38 413569.58,5316968.56 413565.73,5316963.94 413567.85,5316963.08 413570.83,5316961.86 413571.09,5316961.75 413566.97,5316951.66 413566.67,5316951.79 413563.7,5316953.02 413555.43,5316956.46 413552.31,5316957.75 413553.16,5316959.78</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw79</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>240</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.623">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413708.2,5317139.47 413702.24,5317144.33 413708.65,5317152.02 413717.54,5317148.02 413718.88,5317147.41 413719.66,5317146.74 413719.76,5317146.66 413711.57,5317136.73 413708.2,5317139.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6Y</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>139</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.624">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413715.4,5317160.17 413722.82,5317153.97 413721.74,5317152.69 413722.87,5317151.76 413720.3,5317148.61 413719.75,5317149.06 413718.94,5317149.73 413717.54,5317148.02 413708.65,5317152.02 413715.4,5317160.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6E</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>95</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.625">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413720.87,5317166.8 413729.44,5317159.64 413723.92,5317153.05 413722.82,5317153.97 413715.4,5317160.17 413720.87,5317166.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6J</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.626">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413573.77,5316988.68 413581.96,5316981.93 413570.35,5316967.93 413569.58,5316968.56 413562.49,5316974.38 413562.19,5316974.63 413573.77,5316988.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7a</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>193</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.627">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413607.75,5317061.63 413599.45,5317066.27 413620.74,5317104.77 413629.1,5317100.14 413607.75,5317061.63</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzk</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>420</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.628">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413573.77,5316988.68 413582.71,5316999.51 413584.91,5316997.7 413592.36,5316991.56 413592.39,5316991.53 413589.48,5316988.14 413589.31,5316988.27 413587.99,5316989.28 413581.96,5316981.93 413573.77,5316988.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7b</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>157</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.629">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413693.55,5317154.93 413681.41,5317140.22 413672.7,5317147.41 413684.89,5317162.09 413693.55,5317154.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6X</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>215</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.630">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413584.91,5316997.7 413610.28,5317028.52 413617.7,5317022.15 413592.36,5316991.56 413584.91,5316997.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw72</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>387</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.631">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413693.55,5317154.93 413684.89,5317162.09 413696.98,5317176.78 413705.68,5317169.64 413693.55,5317154.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6y</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>214</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.632">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413586.28,5316968.06 413580.23,5316971.6 413581.41,5316973.62 413588.19,5316977.29 413590.98,5316975.62 413587.55,5316969.86 413587.44,5316969.93 413586.28,5316968.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7c</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>51</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.633">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413595.41,5316983.07 413594.84,5316982.12 413590.79,5316985.28 413588.88,5316987.62 413589.31,5316988.27 413589.48,5316988.14 413592.39,5316991.53 413598.96,5316989.03 413595.41,5316983.07</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7d</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>51</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.634">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413613.97,5317101.47 413600.9,5317108.67 413603.88,5317114.11 413616.96,5317106.91 413613.97,5317101.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzl</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>93</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.635">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413667.39,5317163.8 413669.33,5317168.43 413679.08,5317164.36 413677.16,5317159.74 413667.39,5317163.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6W</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>53</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.636">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413632.12,5317014.34 413619.39,5317021.54 413622.35,5317026.91 413635.27,5317019.68 413632.12,5317014.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw73</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.637">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413729.27,5317143.4 413726.43,5317144.61 413727.51,5317147.32 413730.4,5317146.26 413729.27,5317143.4</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6F</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>9</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.638">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413559.94,5316936.94 413562.21,5316936.12 413570.27,5316933.26 413575.57,5316931.35 413572.1,5316920.89 413555.95,5316926.61 413559.94,5316936.94</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7x</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.639">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413544.68,5316943.36 413549.11,5316941.49 413556.87,5316938.22 413559.94,5316936.94 413555.95,5316926.61 413540.19,5316933.26 413544.68,5316943.36</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7y</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.640">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413581.83,5316956.9 413586.96,5316955.56 413586.8,5316954.96 413586.3,5316953.03 413609.09,5316947.09 413609.58,5316948.99 413609.53,5316949.0 413609.7,5316949.63 413612.69,5316948.85 413612.54,5316948.22 413612.48,5316948.23 413611.98,5316946.34 413634.82,5316940.38 413635.32,5316942.3 413635.48,5316942.91 413640.22,5316941.67 413637.57,5316931.64 413578.47,5316946.99 413581.83,5316956.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw71</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>509</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.641">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413528.16,5316938.87 413533.32,5316948.58 413541.36,5316944.92 413544.68,5316943.36 413540.19,5316933.26 413533.51,5316936.38 413528.16,5316938.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7A</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>142</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.642">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413566.97,5316951.66 413571.09,5316961.75 413577.59,5316959.03 413581.94,5316957.22 413581.83,5316956.9 413578.47,5316946.99 413578.43,5316946.88 413566.97,5316951.66</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw77</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>132</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.643">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413485.65,5316974.39 413496.35,5316968.45 413499.36,5316966.78 413499.45,5316966.73 413499.54,5316966.68 413493.73,5316956.29 413493.64,5316956.12 413493.44,5316956.22 413490.46,5316957.78 413479.6,5316963.44 413485.65,5316974.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8X</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17 a</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>195</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.644">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413465.27,5316955.64 413472.72,5316951.51 413473.23,5316952.42 413475.99,5316950.88 413475.49,5316949.98 413482.86,5316945.88 413477.99,5316937.08 413471.41,5316940.7 413470.34,5316939.62 413466.67,5316941.66 413467.03,5316943.14 413460.47,5316946.81 413461.57,5316948.77 413465.27,5316955.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8W</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17 b</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>212</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.645">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413469.01,5316983.63 413475.31,5316980.13 413477.59,5316978.86 413477.84,5316978.72 413471.62,5316967.25 413471.48,5316967.33 413469.12,5316968.65 413468.21,5316969.16 413468.95,5316970.55 413463.61,5316973.59 413469.01,5316983.63</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8T</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>122</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.646">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413469.01,5316983.63 413463.61,5316973.59 413441.54,5316985.79 413441.31,5316985.92 413446.74,5316995.99 413469.01,5316983.63</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8G</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>291</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.647">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413485.02,5316991.21 413490.5,5317000.88 413494.48,5316998.85 413498.94,5316996.4 413493.42,5316986.55 413485.02,5316991.21</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7Y</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.648">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413485.02,5316991.21 413476.67,5316995.95 413482.1,5317005.63 413490.5,5317000.88 413485.02,5316991.21</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw81</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.649">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413432.33,5316989.09 413433.32,5316991.0 413430.67,5316992.44 413435.37,5317000.89 413435.99,5317001.95 413437.13,5317001.32 413446.74,5316995.99 413441.31,5316985.92 413437.01,5316988.29 413436.26,5316986.94 413435.62,5316987.29 413432.33,5316989.09</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8D</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>147</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.650">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413476.62,5316995.86 413469.08,5317000.05 413474.52,5317009.07 413474.32,5317009.19 413474.53,5317009.55 413481.09,5317006.19 413481.85,5317007.54 413482.86,5317006.97 413482.1,5317005.63 413476.67,5316995.95 413476.62,5316995.86</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw82</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.651">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413416.92,5317012.64 413416.9,5317012.6 413413.27,5317005.78 413411.04,5317006.99 413409.72,5317004.57 413408.1,5317005.46 413407.26,5317003.91 413402.36,5317006.59 413403.2,5317008.14 413399.03,5317010.4 413404.02,5317019.72 413416.92,5317012.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9J</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>158</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.652">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413469.08,5317000.05 413461.22,5317004.42 413467.15,5317014.86 413471.07,5317012.7 413470.33,5317011.46 413471.75,5317010.65 413474.32,5317009.19 413474.52,5317009.07 413469.08,5317000.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw85</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.653">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413395.59,5317024.35 413404.02,5317019.72 413399.03,5317010.4 413398.78,5317010.54 413396.81,5317011.64 413396.27,5317010.68 413390.06,5317014.13 413395.59,5317024.35</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9M</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>109</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.654">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413384.97,5317004.33 413385.23,5317004.18 413390.55,5317001.33 413389.06,5316998.5 413389.5,5316998.27 413387.17,5316993.85 413386.73,5316994.09 413385.52,5316991.79 413380.13,5316994.67 413379.85,5316994.77 413384.97,5317004.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9N</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>31 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>71</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.655">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413453.34,5317008.79 413458.45,5317018.05 413462.57,5317015.89 413463.13,5317016.96 413467.15,5317014.86 413461.22,5317004.42 413453.34,5317008.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw88</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.656">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413463.25,5317026.77 413465.91,5317031.59 413466.79,5317033.18 413474.85,5317028.42 413471.39,5317022.33 413463.25,5317026.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw89</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>32 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>67</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.657">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413387.1,5317029.01 413395.59,5317024.35 413390.06,5317014.13 413383.86,5317017.57 413384.4,5317018.53 413382.25,5317019.69 413387.1,5317029.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9O</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>109</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.658">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413445.46,5317013.16 413451.22,5317023.58 413455.09,5317021.27 413454.52,5317020.24 413458.45,5317018.05 413453.34,5317008.79 413445.46,5317013.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8a</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>101</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.659">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413379.53,5317033.16 413387.1,5317029.01 413382.25,5317019.69 413380.78,5317020.48 413378.16,5317021.89 413377.31,5317020.35 413373.72,5317022.33 413374.42,5317023.64 413379.53,5317033.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9R</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>99</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.660">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413360.89,5316979.6 413360.58,5316979.01 413354.71,5316982.23 413363.75,5316999.42 413369.65,5316996.26 413368.96,5316994.95 413360.89,5316979.6</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9U</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>130</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.661">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413445.46,5317013.16 413437.67,5317017.65 413443.52,5317028.17 413443.61,5317028.33 413447.85,5317025.95 413447.69,5317025.67 413451.22,5317023.58 413445.46,5317013.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8d</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>109</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.662">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413369.93,5317038.44 413371.98,5317037.33 413379.53,5317033.16 413374.42,5317023.64 413372.74,5317024.56 413372.02,5317023.27 413367.48,5317025.78 413368.2,5317027.07 413366.64,5317027.89 413364.78,5317028.97 413364.54,5317029.11 413369.68,5317038.57 413369.93,5317038.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9W</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>129</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.663">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413353.64,5317009.08 413357.35,5317015.89 413367.19,5317010.36 413360.66,5316998.39 413356.24,5316990.28 413353.19,5316991.88 413353.26,5316992.01 413352.83,5316992.25 413357.22,5317000.27 413350.81,5317003.87 413352.02,5317006.1 413353.64,5317009.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9Y</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>37 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>191</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.664">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413436.05,5317032.29 413441.06,5317029.53 413443.27,5317028.31 413443.52,5317028.17 413437.67,5317017.65 413437.42,5317017.79 413435.27,5317018.97 413430.24,5317021.76 413436.05,5317032.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8f</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.665">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413447.71,5317035.69 413440.32,5317040.03 413440.91,5317041.08 413441.84,5317042.78 413443.98,5317046.65 413444.55,5317046.31 413451.38,5317042.29 413449.07,5317038.14 413447.71,5317035.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8g</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>38 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>65</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.666">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413361.83,5317042.88 413369.68,5317038.57 413364.54,5317029.11 413364.42,5317028.88 413360.23,5317031.16 413359.64,5317030.09 413359.51,5317029.85 413355.8,5317031.86 413361.83,5317042.88</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa0</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>105</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.667">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413430.24,5317021.76 413417.91,5317028.55 413423.83,5317039.06 413424.88,5317038.48 413428.14,5317036.67 413436.05,5317032.29 413430.24,5317021.76</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8h</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.668">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413353.95,5317047.19 413361.83,5317042.88 413355.8,5317031.86 413351.58,5317034.2 413352.31,5317035.52 413348.57,5317037.37 413348.82,5317037.83 413353.95,5317047.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa2</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.669">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413308.95,5317075.06 413321.08,5317066.41 413321.23,5317066.42 413321.26,5317066.28 413321.94,5317063.56 413321.98,5317063.4 413321.83,5317063.35 413316.01,5317052.46 413306.65,5317057.47 413308.38,5317060.65 413303.51,5317064.05 413305.16,5317066.34 413303.58,5317067.45 413308.95,5317075.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhS</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>239</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.670">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413301.48,5317080.39 413308.95,5317075.06 413303.58,5317067.45 413301.95,5317065.14 413301.28,5317064.19 413297.1,5317067.18 413297.88,5317068.28 413294.8,5317070.48 413294.01,5317069.38 413293.8,5317069.53 413301.48,5317080.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhN</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>117</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.671">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413286.82,5317074.5 413294.57,5317085.3 413301.48,5317080.39 413293.8,5317069.53 413290.3,5317072.02 413291.09,5317073.14 413287.83,5317075.47 413287.03,5317074.35 413286.82,5317074.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhP</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.672">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413591.42,5316926.42 413600.68,5316923.82 413598.03,5316915.44 413597.46,5316913.65 413595.79,5316914.08 413595.77,5316914.05 413592.38,5316914.92 413592.0,5316913.48 413588.15,5316914.49 413588.24,5316914.86 413588.49,5316915.78 413591.42,5316926.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7U</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.673">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413317.3,5317088.74 413322.87,5317096.53 413326.14,5317094.19 413327.65,5317094.45 413329.22,5317099.46 413329.31,5317099.73 413336.08,5317097.6 413334.83,5317093.61 413335.98,5317093.27 413334.61,5317088.88 413333.93,5317089.03 413332.71,5317085.14 413332.25,5317085.34 413331.08,5317081.62 413328.48,5317080.75 413317.3,5317088.74</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8y</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>200</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.674">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413285.06,5317079.26 413282.8,5317080.88 413281.91,5317079.6 413268.75,5317089.2 413271.49,5317094.99 413274.01,5317100.28 413291.14,5317087.8 413294.09,5317085.65 413294.34,5317085.47 413294.57,5317085.3 413286.82,5317074.5 413286.61,5317074.65 413288.27,5317076.97 413288.03,5317077.14 413285.06,5317079.26</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twiq</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>283</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.675">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413309.97,5317093.98 413317.18,5317104.0 413324.42,5317098.69 413322.87,5317096.53 413317.3,5317088.74 413309.97,5317093.98</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8A</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>111</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.676">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413251.36,5317104.66 413259.11,5317100.92 413259.48,5317101.69 413259.66,5317101.6 413260.8,5317103.1 413261.78,5317105.12 413263.67,5317104.22 413262.69,5317102.19 413262.21,5317100.38 413262.39,5317100.29 413262.02,5317099.52 413266.77,5317097.25 413271.49,5317094.99 413266.55,5317084.8 413261.82,5317087.08 413254.57,5317090.58 413252.18,5317085.64 413252.02,5317085.31 413252.63,5317085.02 413250.07,5317079.68 413241.37,5317083.93 413244.07,5317089.55 413246.44,5317094.42 413251.36,5317104.66</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiI</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>374</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.677">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413302.65,5317099.22 413309.85,5317109.19 413317.18,5317104.0 413309.97,5317093.98 413302.65,5317099.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8B</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>54</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>111</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.678">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413573.72,5316964.78 413578.37,5316972.68 413580.23,5316971.6 413586.28,5316968.06 413584.17,5316964.18 413584.05,5316964.25 413584.53,5316965.13 413582.14,5316966.57 413581.62,5316965.71 413579.15,5316961.62 413573.72,5316964.78</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw78</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>68</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.679">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413287.8,5317109.91 413287.657799983,5317110.0409 413287.519699987,5317110.176 413287.38569999,5317110.31529999 413287.25599999,5317110.45849999 413287.130699997,5317110.60569998 413287.01,5317110.75659998 413286.893899977,5317110.91099998 413286.78249998,5317111.069 413286.675999983,5317111.23019999 413286.574399985,5317111.39459999 413286.477899988,5317111.56199999 413286.38649999,5317111.73229998 413286.300299992,5317111.90519998 413286.219399994,5317112.0807 413286.143799996,5317112.25859999 413286.073699998,5317112.43869999 413286.0091,5317112.62079998 413285.950099976,5317112.80479998 413285.896599977,5317112.99049998 413285.848899979,5317113.1778 413285.80679998,5317113.36639999 413285.770499981,5317113.55619999 413285.74,5317113.747 413285.715299982,5317113.93869998 413285.696399982,5317114.131 413285.683399983,5317114.32379999 413285.676199983,5317114.51689999 413285.674899983,5317114.71009998 413285.679399983,5317114.90329998 413285.689899983,5317115.0963 413285.706099982,5317115.28879999 413285.728299982,5317115.48079999 413285.756199981,5317115.67199998 413285.78989998,5317115.86229998 413285.829399979,5317116.0514 413285.874599978,5317116.23929999 413285.925499977,5317116.42569999 413285.982099975,5317116.61049998 413286.044199999,5317116.79349998 413286.111799997,5317116.97449998 413286.18499999,5317117.1534 413286.263499993,5317117.33 413286.347299991,5317117.50409999 413286.436399989,5317117.67559998 413286.530699987,5317117.84419998 413286.63,5317118.01 413286.692699982,5317118.1065 413286.757,5317118.202 413286.822799979,5317118.29649999 413286.89,5317118.39 413286.988899975,5317118.52189999 413287.091,5317118.65119998 413287.196399995,5317118.77799998 413287.305,5317118.902 413287.416799989,5317119.0234 413287.531599987,5317119.1419 413287.649399984,5317119.25739999 413287.77,5317119.37 413287.919999977,5317119.50129999 413288.074,5317119.62789998 413288.231899994,5317119.74959998 413288.39359999,5317119.86629998 413288.558799986,5317119.97789998 413288.727499982,5317120.0843 413288.899299977,5317120.1853 413289.074299998,5317120.28089999 413289.252199994,5317120.37099999 413289.432799989,5317120.45539999 413289.615899984,5317120.53419999 413289.80139998,5317120.60719998 413289.989199975,5317120.67439998 413290.178899995,5317120.73559998 413290.370499991,5317120.79089998 413290.563699986,5317120.84009998 413290.758299981,5317120.88329998 413290.954199976,5317120.92039998 413291.151199996,5317120.95129998 413291.349,5317120.976 413291.547499986,5317120.99449997 413291.746499981,5317121.0068 413291.945799976,5317121.0129 413292.145099996,5317121.0127 413292.344399991,5317121.0063 413292.543299986,5317120.99369998 413292.741799981,5317120.97479998 413292.939599976,5317120.94969998 413293.136499997,5317120.91849998 413293.332299992,5317120.88109998 413293.526899987,5317120.83759998 413293.719999982,5317120.78799998 413293.911399977,5317120.73239998 413294.101099997,5317120.67089998 413294.288699993,5317120.60339998 413294.474099988,5317120.53009999 413294.657099983,5317120.45099999 413294.837499979,5317120.36629999 413295.0152,5317120.27589999 413295.19,5317120.18 413305.59,5317112.6 413309.52,5317109.75 413309.82,5317109.53 413309.67,5317109.33 413309.85,5317109.19 413302.65,5317099.22 413302.24,5317099.65 413298.29,5317102.5 413287.8,5317109.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8x</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>54 a</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>284</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.680">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413241.69,5317062.14 413246.65,5317059.76 413245.08,5317056.47 413251.85,5317053.24 413254.38,5317052.03 413250.92,5317044.72 413247.76,5317038.02 413247.7,5317037.89 413233.3,5317044.63 413236.56,5317051.44 413240.1,5317058.82 413241.69,5317062.14</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiF</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>55</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>269</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.681">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413241.4,5317109.47 413245.05,5317107.66 413245.3,5317108.18 413248.01,5317106.87 413247.78,5317106.34 413251.36,5317104.66 413246.44,5317094.42 413245.18,5317095.05 413244.52,5317093.7 413237.21,5317097.34 413237.88,5317098.68 413236.61,5317099.31 413236.57,5317099.33 413236.87,5317099.94 413241.4,5317109.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj2</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>59</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>138</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.682">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413299.46,5317184.79 413299.16,5317184.11 413299.08,5317183.94 413298.32,5317184.28 413296.79,5317180.85 413297.55,5317180.52 413297.48,5317180.35 413294.46,5317173.59 413294.38,5317173.41 413293.63,5317173.75 413292.16,5317170.45 413292.92,5317170.12 413292.83,5317169.92 413292.54,5317169.27 413292.43,5317169.32 413291.66,5317169.66 413291.58,5317169.48 413291.02,5317169.72 413289.45,5317166.01 413290.01,5317165.77 413289.93,5317165.59 413287.04,5317158.78 413286.97,5317158.62 413286.41,5317158.86 413283.07,5317151.04 413283.64,5317150.8 413283.57,5317150.64 413280.67,5317143.82 413280.6,5317143.65 413280.01,5317143.9 413276.67,5317135.98 413277.24,5317135.74 413277.17,5317135.58 413274.25,5317128.73 413274.19,5317128.58 413273.63,5317128.82 413272.03,5317125.05 413272.59,5317124.81 413272.5,5317124.6 413272.45,5317124.48 413263.23,5317128.4 413266.07,5317135.08 413265.91,5317135.15 413266.76,5317137.14 413266.91,5317137.07 413272.47,5317150.17 413272.32,5317150.23 413273.16,5317152.22 413273.32,5317152.15 413278.82,5317165.11 413278.67,5317165.18 413279.51,5317167.16 413279.66,5317167.09 413282.51,5317173.78 413283.39,5317173.38 413286.35,5317180.06 413286.2,5317180.13 413287.08,5317182.11 413287.23,5317182.04 413290.28,5317188.91 413296.05,5317186.3 413299.46,5317184.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tyzu</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>60</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>644</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.683">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413236.87,5317099.94 413222.87,5317106.66 413227.4,5317116.18 413241.4,5317109.47 413236.87,5317099.94</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjg</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>61</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>164</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.684">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413236.87,5317153.38 413237.86,5317155.14 413238.0,5317155.06 413243.93,5317165.62 413243.78,5317165.7 413244.76,5317167.45 413244.91,5317167.36 413250.79,5317177.82 413250.64,5317177.9 413251.62,5317179.64 413251.77,5317179.56 413254.53,5317184.47 413255.38,5317183.96 413260.42,5317192.36 413260.28,5317192.45 413261.29,5317194.13 413260.15,5317194.82 413264.44,5317201.96 413269.88,5317198.68 413268.69,5317196.7 413273.35,5317193.98 413264.2,5317178.64 413263.33,5317179.12 413243.23,5317143.39 413234.28,5317148.43 413237.01,5317153.29 413236.87,5317153.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TyyS</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>70</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>632</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.685">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413580.05,5316929.94 413591.42,5316926.42 413588.49,5316915.78 413576.79,5316919.43 413572.3,5316920.83 413572.1,5316920.89 413575.57,5316931.35 413575.82,5316931.27 413580.05,5316929.94</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7w</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.686">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413380.07,5317015.5 413384.09,5317013.33 413380.61,5317006.67 413376.59,5317008.81 413380.07,5317015.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9P</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>34</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.687">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413568.89,5316886.28 413563.79,5316878.12 413559.01,5316870.48 413556.52,5316866.51 413552.24,5316859.67 413552.17,5316859.56 413547.25,5316862.23 413572.97,5316903.23 413577.63,5316900.23 413573.83,5316894.17 413568.89,5316886.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7H</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>268</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.688">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413368.66,5317013.06 413364.15,5317015.48 413367.72,5317022.12 413372.24,5317019.68 413368.66,5317013.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9X</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>39</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.689">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413279.72,5317075.37 413275.25,5317068.1 413266.66,5317073.46 413271.14,5317080.72 413279.72,5317075.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twir</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>86</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.690">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413213.72,5317087.8 413223.22,5317083.26 413220.37,5317077.32 413210.85,5317081.85 413213.72,5317087.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twjf</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>70</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.691">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413572.97,5316903.23 413577.09,5316909.8 413581.72,5316906.74 413578.78,5316902.06 413577.63,5316900.23 413572.97,5316903.23</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7v</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>43</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.692">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413241.37,5317083.93 413250.07,5317079.68 413241.69,5317062.14 413240.1,5317058.82 413236.56,5317051.44 413233.3,5317044.63 413231.72,5317041.33 413231.7,5317041.34 413222.88,5317045.67 413223.77,5317047.52 413224.41,5317048.84 413225.52,5317051.1 413232.94,5317066.49 413241.37,5317083.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj0</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>415</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.693">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413273.27,5317063.32 413273.441599989,5317063.37879999 413273.612599985,5317063.43899999 413273.78319998,5317063.50059999 413273.953299976,5317063.56369999 413274.122799997,5317063.62809998 413274.291699993,5317063.69399998 413274.460199988,5317063.76129998 413274.628,5317063.83 413274.79539998,5317063.90009998 413274.962199976,5317063.97169998 413275.128399997,5317064.0446 413275.29399999,5317064.119 413275.458999988,5317064.1947 413275.623299984,5317064.27169999 413275.78699998,5317064.35019999 413275.95,5317064.43 413276.099199997,5317064.50819999 413276.247699994,5317064.58779998 413276.39549999,5317064.66869998 413276.542399986,5317064.75109998 413276.688599983,5317064.83489998 413276.834,5317064.92 413276.978799975,5317065.0066 413277.122699997,5317065.0946 413277.265799993,5317065.18389999 413277.40809999,5317065.27459999 413277.549499986,5317065.36659999 413277.69,5317065.46 413280.99,5317063.72 413280.64,5317063.0 413280.89,5317062.88 413279.58,5317060.05 413278.67,5317058.08 413275.78,5317051.92 413273.82,5317052.98 413269.89,5317044.81 413273.0,5317043.18 413269.08,5317035.07 413250.92,5317044.72 413254.38,5317052.03 413258.66,5317061.08 413256.25,5317062.37 413259.84,5317070.24 413273.27,5317063.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twiu</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>557</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.694">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.18,5316929.49 413523.22,5316929.57 413524.44,5316931.85 413528.16,5316938.87 413533.51,5316936.38 413529.65,5316929.1 413528.43,5316926.8 413528.39,5316926.73 413523.18,5316929.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7z</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>63</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.695">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413441.54,5316985.79 413463.61,5316973.59 413452.77,5316953.48 413448.69,5316945.89 413439.87,5316950.61 413439.74,5316950.27 413433.99,5316953.26 413436.57,5316957.95 413433.37,5316959.71 413433.09,5316959.22 413431.76,5316959.95 413431.67,5316959.83 413428.22,5316961.65 413431.25,5316967.15 413439.26,5316981.64 413440.79,5316984.43 413441.54,5316985.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8H</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>764</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.696">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.75,5317010.65 413475.5,5317017.16 413478.15,5317015.67 413477.46,5317014.5 413474.53,5317009.55 413474.32,5317009.19 413471.75,5317010.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw86</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>23</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.697">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413429.49,5317045.22 413433.94,5317052.58 413443.98,5317046.65 413441.84,5317042.78 413440.65,5317043.43 413435.01,5317046.51 413433.05,5317043.2 413429.49,5317045.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8j</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>70</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.698">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413463.61,5316973.59 413468.95,5316970.55 413468.21,5316969.16 413466.74,5316966.39 413470.16,5316964.64 413468.03,5316960.82 413465.27,5316955.64 413461.57,5316948.77 413452.77,5316953.48 413463.61,5316973.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8S</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>209</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.699">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413251.85,5317053.24 413256.25,5317062.37 413258.66,5317061.08 413254.38,5317052.03 413251.85,5317053.24</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiG</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>28</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.700">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.62,5317010.1 413481.12,5317011.99 413481.28,5317012.26 413482.93,5317015.2 413486.4,5317013.28 413484.62,5317010.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw83</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.701">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413477.46,5317014.5 413478.15,5317015.67 413479.12,5317017.31 413482.93,5317015.2 413481.28,5317012.26 413477.46,5317014.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw84</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.702">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413396.98,5317002.16 413395.18,5317003.1 413398.58,5317009.34 413399.88,5317008.61 413398.02,5317005.05 413398.48,5317004.81 413396.98,5317002.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9K</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>12</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.703">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413462.96,5317035.44 413461.38,5317032.76 413457.52,5317034.99 413459.04,5317037.75 413462.96,5317035.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8b</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>14</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.704">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413449.07,5317038.14 413451.94,5317036.55 413450.51,5317033.96 413447.62,5317035.53 413447.71,5317035.69 413449.07,5317038.14</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8e</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>10</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.705">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413350.2,5317027.31 413351.34,5317029.42 413353.74,5317028.11 413352.6,5317026.02 413350.2,5317027.31</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twa1</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>7</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.706">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413294.84,5317058.28 413293.34,5317055.35 413288.28,5317057.91 413289.69,5317060.88 413294.84,5317058.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhO</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.707">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413287.11,5317066.55 413284.5,5317061.13 413280.89,5317062.88 413283.48,5317068.3 413287.11,5317066.55</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwhQ</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.708">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413583.05,5316908.85 413584.38,5316910.96 413590.97,5316907.21 413590.88,5316907.07 413589.71,5316905.41 413587.14,5316906.74 413583.05,5316908.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7T</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>18</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.709">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413468.03,5316960.82 413482.87,5316952.66 413480.42,5316947.99 413484.11,5316945.88 413483.13,5316944.13 413486.04,5316942.55 413478.8,5316929.92 413457.31,5316941.04 413460.47,5316946.81 413467.03,5316943.14 413466.67,5316941.66 413470.34,5316939.62 413471.41,5316940.7 413477.99,5316937.08 413482.86,5316945.88 413475.49,5316949.98 413475.99,5316950.88 413473.23,5316952.42 413472.72,5316951.51 413465.27,5316955.64 413468.03,5316960.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8V</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>282</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.710">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413252.18,5317085.64 413254.57,5317090.58 413261.82,5317087.08 413266.55,5317084.8 413264.14,5317079.9 413252.18,5317085.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwiH</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>73</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.711">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413223.06,5317071.27 413224.96,5317075.2 413236.61,5317099.31 413237.88,5317098.68 413237.21,5317097.34 413244.52,5317093.7 413245.18,5317095.05 413246.44,5317094.42 413244.07,5317089.55 413241.37,5317083.93 413232.94,5317066.49 413230.78,5317067.58 413223.06,5317071.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twj1</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>329</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.712">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413352.61,5316977.82 413354.02,5316980.39 413359.67,5316977.35 413358.28,5316974.82 413352.61,5316977.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9T</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.713">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413324.22,5317113.77 413321.41,5317109.88 413314.08,5317115.11 413316.9,5317119.02 413324.22,5317113.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8C</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>43</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.714">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413495.1,5317008.45 413498.76,5317006.42 413497.11,5317003.5 413493.22,5317005.71 413493.11,5317005.76 413493.01,5317005.82 413494.68,5317008.68 413494.78,5317008.62 413495.1,5317008.45</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw80</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.715">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413463.25,5317026.77 413460.79,5317022.31 413457.93,5317023.88 413463.05,5317033.17 413465.91,5317031.59 413463.25,5317026.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8c</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.716">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413380.61,5317006.67 413384.97,5317004.33 413379.85,5316994.77 413376.91,5316996.36 413376.47,5316995.55 413373.84,5316996.98 413374.27,5316997.79 413371.36,5316999.36 413372.48,5317001.38 413376.24,5317008.17 413376.59,5317008.81 413380.61,5317006.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9Q</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>106</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.717">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413371.36,5316999.36 413370.6,5316997.98 413364.62,5317001.18 413366.42,5317004.46 413367.23,5317004.03 413367.28,5317004.14 413372.48,5317001.38 413371.36,5316999.36</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9V</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>26</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.718">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413502.55,5317004.32 413503.18,5317003.97 413498.94,5316996.4 413494.48,5316998.85 413497.11,5317003.5 413498.76,5317006.42 413502.55,5317004.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7Z</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>44</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.719">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413396.98,5317002.16 413406.51,5316997.13 413403.37,5316991.5 413403.0,5316991.69 413392.04,5316997.51 413393.3,5316999.85 413393.11,5316999.95 413394.89,5317003.26 413395.18,5317003.1 413396.98,5317002.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9L</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>84</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.720">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.39,5317022.33 413474.85,5317028.42 413481.76,5317024.34 413482.88,5317023.68 413479.12,5317017.31 413478.15,5317015.67 413475.5,5317017.16 413474.75,5317017.58 413475.88,5317019.72 413471.39,5317022.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw87</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>74</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.721">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413376.24,5317008.17 413370.34,5317011.07 413374.07,5317018.06 413376.69,5317016.67 413378.68,5317020.5 413380.32,5317019.62 413380.78,5317020.48 413382.25,5317019.69 413380.07,5317015.5 413376.59,5317008.81 413376.24,5317008.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9S</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>68</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.722">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413440.65,5317043.43 413439.73,5317041.72 413437.42,5317037.49 413431.68,5317040.89 413433.05,5317043.2 413435.01,5317046.51 413440.65,5317043.43</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8i</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>44</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.723">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413705.58,5317100.19 413721.4,5317091.73 413721.25,5317091.46 413727.0,5317088.84 413733.06,5317102.11 413742.61,5317097.56 413732.25,5317075.03 413716.6,5317082.19 413716.74,5317082.49 413700.58,5317091.09 413697.17,5317092.9 413696.89,5317093.05 413697.49,5317094.19 413701.72,5317102.26 413702.0,5317102.11 413705.58,5317100.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6Z</ogr:gmlid>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>562</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.724">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413768.89,5317167.4 413761.84,5317141.59 413759.88,5317135.98 413749.62,5317138.71 413750.13,5317140.33 413739.0,5317143.29 413742.02,5317152.79 413753.17,5317150.0 413758.23,5317170.09 413768.89,5317167.4</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6I</ogr:gmlid>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>466</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.725">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413736.03,5317111.95 413746.86,5317106.8 413746.77,5317106.6 413745.76,5317104.41 413742.61,5317097.56 413733.06,5317102.11 413731.58,5317102.82 413734.85,5317109.53 413735.92,5317111.72 413736.03,5317111.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6O</ogr:gmlid>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>123</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.726">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413746.86,5317106.8 413736.03,5317111.95 413737.74,5317115.74 413739.51,5317114.94 413739.88,5317115.75 413732.64,5317119.02 413737.68,5317130.17 413739.43,5317134.05 413748.2,5317130.09 413748.61,5317130.98 413748.04,5317131.24 413749.68,5317134.87 413758.09,5317131.35 413746.86,5317106.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6K</ogr:gmlid>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>404</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.727">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413737.68,5317130.17 413732.64,5317119.02 413739.88,5317115.75 413739.51,5317114.94 413737.74,5317115.74 413736.03,5317111.95 413730.65,5317114.51 413725.65,5317116.89 413724.58,5317117.4 413718.57,5317120.27 413718.37,5317120.36 413723.03,5317130.22 413725.68,5317135.83 413737.68,5317130.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6L</ogr:gmlid>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>261</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.728">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413737.51,5317157.47 413738.17,5317157.22 413732.22,5317142.17 413729.27,5317143.4 413730.4,5317146.26 413727.51,5317147.32 413728.33,5317149.37 413731.67,5317157.78 413732.33,5317159.44 413737.51,5317157.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6G</ogr:gmlid>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.729">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413724.58,5317117.4 413720.3,5317107.95 413713.95,5317111.01 413718.37,5317120.36 413718.57,5317120.27 413724.58,5317117.4</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw6N</ogr:gmlid>
      <ogr:street>Marchstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>72</ogr:area>
    </ogr:building>
  </gml:featureMember>
</ogr:FeatureCollection>
