<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ wfs_pois_data_uMlpz6.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413185.460789</gml:X><gml:Y>5316529.275981</gml:Y></gml:coord>
      <gml:coord><gml:X>413791.723075</gml:X><gml:Y>5317135.595947</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                              
  <gml:featureMember>
    <ogr:pois fid="37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413601.711943,5316705.485081</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.37</ogr:gml_id>
      <ogr:ogc_fid>2</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>wc</ogr:poityp>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="153">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413791.723075,5316828.350475</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.153</ogr:gml_id>
      <ogr:ogc_fid>6</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_touren</ogr:poityp>
      <ogr:kategorie>Stadt und Münsterführung</ogr:kategorie>
      <ogr:name>Dr. Elisabeth Glück</ogr:name>
      <ogr:bezeichnung>Mit dem Fokus auf Kunstgeschichte und Stadtgeschichte (Schwerpunkt: Mittelalter bis Barock) bekommen Sie einen systematischen Einblick in die Entwicklung des städtischen Lebens in Freiburg, geführt von einer gebürtigen Freiburgerin, einem echten ‚Bobbele‘ (wie die in Freiburg Geborenen mundartlich genannt werden). Auch in einem neuzeitlichen Stadtbild lassen sich offene und verdeckte Spuren der Vergangenheit finden, die Freiburg zu dem machten, was es heute ist: Eine liebenswerte, lebendige und kunsthistorisch reiche Zähringer-Stadt. Diese Spuren aufzudecken und zu einem Ganzen zusammenzufügen ist das Ziel dieser Führungen.</ogr:bezeichnung>
      <ogr:adresse>Bismarckallee 22 79098 Freiburg</ogr:adresse>
      <ogr:url>www.stadtführung-in-freiburg.de</ogr:url>
      <ogr:telefon>0761 15648060</ogr:telefon>
      <ogr:ansprechpartner>Dr. Elisabeth Glück</ogr:ansprechpartner>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="184">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413314.946562,5316529.275981</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.184</ogr:gml_id>
      <ogr:ogc_fid>7</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>parken</ogr:poityp>
      <ogr:kategorie>Tiefgarage</ogr:kategorie>
      <ogr:name>Parkaus Stühlinger Kirchplatz</ogr:name>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="253">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413303.560004,5316596.301431</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.253</ogr:gml_id>
      <ogr:ogc_fid>10</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>brunnen</ogr:poityp>
      <ogr:name>Marienbrunnen</ogr:name>
      <ogr:orig_id>2456</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="259">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413721.134936,5316727.450371</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.259</ogr:gml_id>
      <ogr:ogc_fid>10</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>parken</ogr:poityp>
      <ogr:kategorie>Tiefgarage</ogr:kategorie>
      <ogr:name>Volksbank-Parkhaus</ogr:name>
      <ogr:bezeichnung>P3</ogr:bezeichnung>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="268">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413291.70313,5316666.014289</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.268</ogr:gml_id>
      <ogr:ogc_fid>11</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>wc</ogr:poityp>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="272">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413704.058235,5316644.123003</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.272</ogr:gml_id>
      <ogr:ogc_fid>11</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_kulinarik</ogr:poityp>
      <ogr:kategorie>Bar, Café, Restaurant</ogr:kategorie>
      <ogr:name>Baltino</ogr:name>
      <ogr:bezeichnung>Schickes Bistro-Restaurant mit Toskana-Flair und unterschiedlich gestalteten Sitzgruppen. Montags erhalten Sie jede unserer Speisen und täglich ab 17 Uhr alle Cocktails, zum halben Preis! Jeden Fr- und Sa-Abend House und Disco Funk.</ogr:bezeichnung>
      <ogr:adresse>Eisenbahnstraße 68 79098 Freiburg</ogr:adresse>
      <ogr:url>www.baltino.de</ogr:url>
      <ogr:telefon>49761 2926888</ogr:telefon>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="293">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413596.653886,5316663.920504</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.293</ogr:gml_id>
      <ogr:ogc_fid>12</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>taxi</ogr:poityp>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="302">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413685.759141,5316833.534759</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.302</ogr:gml_id>
      <ogr:ogc_fid>12</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>parken</ogr:poityp>
      <ogr:kategorie>Tiefgarage</ogr:kategorie>
      <ogr:name>Parkhaus Bismarckallee</ogr:name>
      <ogr:bezeichnung>P4</ogr:bezeichnung>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="331">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413783.318552,5316984.459741</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.331</ogr:gml_id>
      <ogr:ogc_fid>13</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>parken</ogr:poityp>
      <ogr:kategorie>Parkplatz</ogr:kategorie>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="433">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413194.254023,5316752.967971</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.433</ogr:gml_id>
      <ogr:ogc_fid>19</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_kulinarik</ogr:poityp>
      <ogr:kategorie>Bar, Café, Restaurant, Terrasse</ogr:kategorie>
      <ogr:name>BRASIL</ogr:name>
      <ogr:bezeichnung>Wunderschönes Café, Restaurant, sehr schöne Bar mit gemütlicher Sofalounge &amp; abgetrenntem Raucherbereich. Nichtraucherlounge mit 120 Plätzen. Über 100 Coktails, die frisch zubereitet werden. Frühstück Mo-Fr 10.00- 12.00 Uhr, Sa &amp; So 10.00 - 15.00 Uhr. Warme Küche täglich 12.00 - 22.30 Uhr.</ogr:bezeichnung>
      <ogr:adresse>Wannerstraße 21 79106 Freiburg</ogr:adresse>
      <ogr:url>www.brasil-freiburg.de</ogr:url>
      <ogr:telefon>49761 289888</ogr:telefon>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="489">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413401.476195,5316559.0512</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.489</ogr:gml_id>
      <ogr:ogc_fid>22</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_touren</ogr:poityp>
      <ogr:kategorie>Rad- und Aktivführung</ogr:kategorie>
      <ogr:name>FREIBURG AKTIV &amp; eBIKES</ogr:name>
      <ogr:bezeichnung>Sie möchten mit FREIBURG AKTIV als Gruppe noch mehr Spaß + Kultur erleben? Für Ihren Aufenthalt in Freiburg können Sie bei uns eine der vielfältigen Stadtführungen buchen. Für Ihr Event oder Incentive organisieren wir Ihr Rahmenprogramm (Tagung, Schul-, Vereins- oder Betriebsausfl ug) in Freiburg und in die Regio. Ob mit dem Mountainbike zu Berge, per Rad durch die Stadt und/oder Rheinebene, mit dem Boot durch die Rheinauen, zur (Traktor)Weinprobe in den Kaiserstuhl… lassen Sie sich überraschen. Nix ist unmöglich. Mehr Freizeit geht nicht!</ogr:bezeichnung>
      <ogr:adresse>Wentzingerstr.  15 79106 Freiburg</ogr:adresse>
      <ogr:url>www.freiburg-aktiv.de</ogr:url>
      <ogr:telefon>0761 2023426</ogr:telefon>
      <ogr:ansprechpartner>Fernando Schüber</ogr:ansprechpartner>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="522">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413648.278939,5316712.19415</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.522</ogr:gml_id>
      <ogr:ogc_fid>24</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_sehensw</ogr:poityp>
      <ogr:kategorie>Green City Freiburg</ogr:kategorie>
      <ogr:name>Solartower am Hauptbahnhof</ogr:name>
      <ogr:bezeichnung>Die 240 Solarmodule erstrecken sich über 19 Stockwerke und machen den Turm zum höchsten Solarkraftwerk Süddeutschlands. Ausgezeichnet mit dem Photovoltaik-Architekturpreis Baden-Württemberg, produziert der Solartower jährlich ca. 24.000 kWh Strom.</ogr:bezeichnung>
      <ogr:adresse>Bismarckallee 9 79098 Freiburg</ogr:adresse>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="532">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413401.476195,5316559.0512</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.532</ogr:gml_id>
      <ogr:ogc_fid>25</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_sehensw</ogr:poityp>
      <ogr:kategorie>Green City Freiburg</ogr:kategorie>
      <ogr:name>Radstation am Hauptbahnhof</ogr:name>
      <ogr:bezeichnung>Die Radstation lockt nicht nur durch ihr unkonventionelles Äußeres: direkt am Bahnhof gelegen, vernetzt die multimodale Mobilitätszentrale den öffentlichen Personennah- und -fernverkehr mit Fuß-, Rad- und KFZ-Mobilität.</ogr:bezeichnung>
      <ogr:adresse>Wentzingerstraße 15 79098 Freiburg</ogr:adresse>
      <ogr:url>www.radstation-freiburg.de</ogr:url>
      <ogr:telefon>49761 21050</ogr:telefon>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="550">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413438.658894,5316801.553859</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.550</ogr:gml_id>
      <ogr:ogc_fid>27</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_kulinarik</ogr:poityp>
      <ogr:kategorie>Bar, Café, Restaurant</ogr:kategorie>
      <ogr:name>Café Einstein</ogr:name>
      <ogr:bezeichnung>Kaffee und Kuchen am Nachmittag und Speisen á la Carte am Abend - saisonal wie regional - laden zum Verweilen ein. Weiter wird eine Vielzahl renommierter Biersorten angeboten aber auch erfrischende Cocktails zu studentischen Preisen stehen auf unserer Getränkekarte.</ogr:bezeichnung>
      <ogr:adresse>Klarastraße 29 79106 Freiburg</ogr:adresse>
      <ogr:url>www.cafe-einstein.de</ogr:url>
      <ogr:telefon>49761 88530809</ogr:telefon>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="578">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413378.826728,5316773.256702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.578</ogr:gml_id>
      <ogr:ogc_fid>29</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:kategorie>VÖ</ogr:kategorie>
      <ogr:name>St. Klara</ogr:name>
      <ogr:bezeichnung>Kindergarten</ogr:bezeichnung>
      <ogr:adresse>Klarastraße 41  79106 Freiburg</ogr:adresse>
      <ogr:telefon>0761/273100</ogr:telefon>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:organisation>katholische Kirche</ogr:organisation>
      <ogr:stadtbezirk>Alt-Stühlinger</ogr:stadtbezirk>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="609">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413677.627949,5317043.952884</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.609</ogr:gml_id>
      <ogr:ogc_fid>32</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>spielplaetze</ogr:poityp>
      <ogr:kategorie>1</ogr:kategorie>
      <ogr:name>Spielplatz Kreuzstraße</ogr:name>
      <ogr:bezeichnung>&lt;p&gt;&quot;ein Drache für den Stühlinger&quot;

Ein großer Drache bewacht den Platz der kleinen &quot;Kreuz­ritter&quot;. Es gibt unterschiedliche Zonen für die verschiedenen Altersklassen. Im gut einsehbaren „Zwergenhausen“ können die Kleinsten in aller Ruhe in eine vielfältige Spielwelt eintauchen. Spielhäuschen, eine kleine Rutsche, nicht allzu schwere Kletterangebote sowie eine kleinkindgerechte Schaukel laden zum phantasievollen Spielen ein. Das vorhandene Wasserspiel wurde erneuert, so dass wieder nach Herzenslust gematscht und experimentiert werden kann. Kreuzstraße , 79106 Freiburg &lt;/p&gt;</ogr:bezeichnung>
      <ogr:url>http://www.freiburg.de/pb/,Lde/936803.html</ogr:url>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:orig_id>508</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="637">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413416.297486,5316574.965933</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.637</ogr:gml_id>
      <ogr:ogc_fid>35</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>parken</ogr:poityp>
      <ogr:kategorie>Fahrradstadtion</ogr:kategorie>
      <ogr:name>Fahrradstation mobile</ogr:name>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="703">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413237.557105,5316630.077052</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.703</ogr:gml_id>
      <ogr:ogc_fid>41</ogr:ogc_fid>
      <ogr:prioritaet>9</ogr:prioritaet>
      <ogr:drehwinkel>298</ogr:drehwinkel>
      <ogr:poityp>kirchen</ogr:poityp>
      <ogr:kategorie>Kirche</ogr:kategorie>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="725">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413220.542197,5316955.189791</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.725</ogr:gml_id>
      <ogr:ogc_fid>44</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_kulinarik</ogr:poityp>
      <ogr:kategorie>Kneipe, Restaurant</ogr:kategorie>
      <ogr:name>Egon 54</ogr:name>
      <ogr:bezeichnung>Traditionsreiche Studentenkneipe mit solidem, preiswertem Essen, interessanter Musik und gelegentlichen Konzerten.</ogr:bezeichnung>
      <ogr:adresse>Egonstraße 54 79106 Freiburg</ogr:adresse>
      <ogr:url>www.egon.54.ms</ogr:url>
      <ogr:telefon>49761 276646</ogr:telefon>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="739">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413200.322483,5316716.573061</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.739</ogr:gml_id>
      <ogr:ogc_fid>45</ogr:ogc_fid>
      <ogr:prioritaet>469</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>schulen</ogr:poityp>
      <ogr:kategorie>122667</ogr:kategorie>
      <ogr:name>Hansjakob-Realschule</ogr:name>
      <ogr:adresse>Wannerstraße 2, 79106 Freiburg im Breisgau</ogr:adresse>
      <ogr:orig_id>45</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="752">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413390.238854,5316902.178482</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.752</ogr:gml_id>
      <ogr:ogc_fid>46</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>brunnen</ogr:poityp>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="837">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413778.02976,5316662.51718</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.837</ogr:gml_id>
      <ogr:ogc_fid>57</ogr:ogc_fid>
      <ogr:prioritaet>1100</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>schulen</ogr:poityp>
      <ogr:kategorie>667815</ogr:kategorie>
      <ogr:name>St. Ursula-Gymnasium</ogr:name>
      <ogr:adresse>Eisenbahnstraße 45, 79098 Freiburg im Breisgau</ogr:adresse>
      <ogr:orig_id>57</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="965">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413502.33343,5317066.305514</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.965</ogr:gml_id>
      <ogr:ogc_fid>78</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:kategorie>GT, K1 ,agm.</ogr:kategorie>
      <ogr:name>Uni-Kita Zaubergarten</ogr:name>
      <ogr:bezeichnung>Kita/Krippe</ogr:bezeichnung>
      <ogr:adresse>Agnesenstraße 4  79106 Freiburg</ogr:adresse>
      <ogr:telefon>0761/274043</ogr:telefon>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:organisation>Familienservice der Uni</ogr:organisation>
      <ogr:stadtbezirk>Eschholz</ogr:stadtbezirk>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="987">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413603.295877,5317131.597516</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.987</ogr:gml_id>
      <ogr:ogc_fid>82</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>haltestellen</ogr:poityp>
      <ogr:kategorie>Bus</ogr:kategorie>
      <ogr:name>Freiburg Kreuzstraße</ogr:name>
      <ogr:orig_id>de:8311:30109:0:1</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="994">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413602.29627,5317135.595947</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.994</ogr:gml_id>
      <ogr:ogc_fid>83</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>haltestellen</ogr:poityp>
      <ogr:kategorie>Bus</ogr:kategorie>
      <ogr:name>Freiburg Kreuzstraße</ogr:name>
      <ogr:orig_id>de:8311:30109:0:2</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1004">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413738.14536,5316663.21655</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1004</ogr:gml_id>
      <ogr:ogc_fid>84</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_kulinarik</ogr:poityp>
      <ogr:kategorie>Restaurant</ogr:kategorie>
      <ogr:name>Hotel Rheingold</ogr:name>
      <ogr:bezeichnung>Regionale Gerichte sowie Fisch- und Steakgerichte laden zum Genießen ein. Menüs können individuell zusammengestellt werden.</ogr:bezeichnung>
      <ogr:adresse>Eisenbahnstraße 47 79098 Freiburg</ogr:adresse>
      <ogr:url>www.rheingold-freiburg.de</ogr:url>
      <ogr:telefon>49761 28210</ogr:telefon>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1045">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413601.598384,5316629.82738</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1045</ogr:gml_id>
      <ogr:ogc_fid>90</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_kulinarik</ogr:poityp>
      <ogr:kategorie>Bar, Restaurant</ogr:kategorie>
      <ogr:name>Inter City Hotel</ogr:name>
      <ogr:bezeichnung>Im Restaurant wird Ihnen eine große Auswahl an internationalen und nationalen Speisen geboten.</ogr:bezeichnung>
      <ogr:adresse>Bismarckallee 3 79098 Freiburg</ogr:adresse>
      <ogr:url>www.intercityhotel.com/freiburg</ogr:url>
      <ogr:telefon>49761 38000</ogr:telefon>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1070">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413648.278939,5316712.19415</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1070</ogr:gml_id>
      <ogr:ogc_fid>94</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>fwtm_kulinarik</ogr:poityp>
      <ogr:kategorie>Bar, Café</ogr:kategorie>
      <ogr:name>Kagan</ogr:name>
      <ogr:bezeichnung>Die Bezeichnung Café - Bar - Club - Lounge deutet es an: im Kagan kann man gemütlich Kaffee und Tee trinken, Cocktails genießen und schließlich die verschiedensten Spielarten des zeitgemäßen 'clubbing' pflegen.</ogr:bezeichnung>
      <ogr:adresse>Bismarckallee 9 79098 Freiburg</ogr:adresse>
      <ogr:url>www.kagan-freiburg.de/mobile/</ogr:url>
      <ogr:telefon>491514 0020560</ogr:telefon>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1107">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413746.239562,5316985.655517</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1107</ogr:gml_id>
      <ogr:ogc_fid>100</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>haltestellen</ogr:poityp>
      <ogr:kategorie>Bus</ogr:kategorie>
      <ogr:name>Freiburg Am Planetarium</ogr:name>
      <ogr:orig_id>de:8311:30126:0:1</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1139">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413570.663831,5316608.609879</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1139</ogr:gml_id>
      <ogr:ogc_fid>105</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>parken</ogr:poityp>
      <ogr:kategorie>Tiefgarage</ogr:kategorie>
      <ogr:name>Bahnhofsgarage</ogr:name>
      <ogr:bezeichnung>P1</ogr:bezeichnung>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1213">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413442.917391,5316882.056501</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1213</ogr:gml_id>
      <ogr:ogc_fid>117</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:kategorie>VÖ, Nachmittag</ogr:kategorie>
      <ogr:name>Klara-Kinder, Egonstraße</ogr:name>
      <ogr:bezeichnung>Krippe</ogr:bezeichnung>
      <ogr:adresse>Egonstraße 21  79106 Freiburg</ogr:adresse>
      <ogr:telefon>01795556659</ogr:telefon>
      <ogr:ansprechpartner>Frau Reich-Disch</ogr:ansprechpartner>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:organisation>Die Klara Kinder GbR</ogr:organisation>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1335">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413254.151081,5316564.394866</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1335</ogr:gml_id>
      <ogr:ogc_fid>143</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>spielplaetze</ogr:poityp>
      <ogr:kategorie>1</ogr:kategorie>
      <ogr:name>Spielplatz Stühlingerplatz</ogr:name>
      <ogr:bezeichnung>&lt;p&gt; Engelbergerstraße , 79106 Freiburg&lt;/p&gt;</ogr:bezeichnung>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:orig_id>502</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1491">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413384.088871,5316851.226149</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1491</ogr:gml_id>
      <ogr:ogc_fid>179</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:kategorie>GT</ogr:kategorie>
      <ogr:name>Klara-Kinder, Guntramstraße</ogr:name>
      <ogr:bezeichnung>Krippe</ogr:bezeichnung>
      <ogr:adresse>Guntramstraße 21  79106 Freiburg</ogr:adresse>
      <ogr:telefon>01795556659</ogr:telefon>
      <ogr:ansprechpartner>Frau Reich-Disch</ogr:ansprechpartner>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:organisation>Die Klara Kinder GbR</ogr:organisation>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1719">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413340.399526,5317074.61807</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1719</ogr:gml_id>
      <ogr:ogc_fid>502</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>haltestellen</ogr:poityp>
      <ogr:kategorie>Bus</ogr:kategorie>
      <ogr:name>Freiburg Lehener Straße</ogr:name>
      <ogr:orig_id>de:8311:30901:0:1</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1720">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413342.398738,5317073.618473</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1720</ogr:gml_id>
      <ogr:ogc_fid>503</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>haltestellen</ogr:poityp>
      <ogr:kategorie>Bus</ogr:kategorie>
      <ogr:name>Freiburg Lehener Straße</ogr:name>
      <ogr:orig_id>de:8311:30901:0:2</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1864">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413498.337743,5316572.81879</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1864</ogr:gml_id>
      <ogr:ogc_fid>684</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>haltestellen</ogr:poityp>
      <ogr:kategorie>Straßenbahn/Bus</ogr:kategorie>
      <ogr:name>Hauptbahnhof</ogr:name>
      <ogr:orig_id>Parent30107</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1865">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413185.460789,5316763.739696</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1865</ogr:gml_id>
      <ogr:ogc_fid>685</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>haltestellen</ogr:poityp>
      <ogr:kategorie>Straßenbahn/Bus</ogr:kategorie>
      <ogr:name>Eschholzstraße</ogr:name>
      <ogr:orig_id>Parent30108</ogr:orig_id>
    </ogr:pois>
  </gml:featureMember>
</ogr:FeatureCollection>
