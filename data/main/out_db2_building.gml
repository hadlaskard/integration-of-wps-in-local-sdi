<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ db_building_data_t2WSx4.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413316.54</gml:X><gml:Y>5316682.77</gml:Y></gml:coord>
      <gml:coord><gml:X>413640.22</gml:X><gml:Y>5317066.56</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                                              
  <gml:featureMember>
    <ogr:building fid="building.0">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413580.74,5316858.96 413571.0,5316864.13 413575.26,5316872.06 413584.99,5316866.92 413580.74,5316858.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7L</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>99</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.1">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413580.74,5316858.96 413576.39,5316850.82 413572.72,5316852.78 413572.61,5316852.56 413570.18,5316853.86 413570.3,5316854.08 413566.67,5316856.01 413568.6,5316859.62 413570.72,5316863.6 413571.0,5316864.13 413580.74,5316858.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7I</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.2">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413556.27,5316838.68 413547.82,5316843.19 413545.11,5316844.63 413540.53,5316836.18 413540.68,5316836.1 413538.99,5316832.99 413532.97,5316836.28 413544.71,5316857.6 413561.56,5316848.53 413556.27,5316838.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7t</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>303</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.3">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413504.62,5316756.73 413500.2,5316749.22 413497.42,5316751.1 413502.05,5316758.76 413504.62,5316756.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaG</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.4">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413556.27,5316838.68 413561.56,5316848.53 413561.85,5316849.07 413572.48,5316843.48 413562.37,5316824.56 413560.68,5316821.4 413560.58,5316821.21 413550.02,5316826.97 413550.11,5316827.14 413551.75,5316830.22 413556.27,5316838.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7u</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>303</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.5">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413600.68,5316923.82 413611.62,5316920.8 413612.55,5316918.64 413602.94,5316900.62 413594.73,5316905.01 413598.01,5316912.0 413599.18,5316911.22 413599.83,5316912.72 413600.06,5316912.62 413600.49,5316913.64 413598.03,5316915.44 413600.68,5316923.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7S</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>221</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.6">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413526.7,5316826.91 413537.21,5316821.32 413537.95,5316822.6 413541.06,5316820.84 413540.32,5316819.56 413544.79,5316817.2 413546.31,5316819.99 413544.99,5316820.71 413546.68,5316823.87 413548.01,5316823.17 413549.94,5316826.76 413550.02,5316826.97 413560.58,5316821.21 413560.5,5316821.0 413549.72,5316800.91 413520.96,5316816.3 413526.7,5316826.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7r</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>536</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.7">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413536.91,5316776.85 413527.25,5316781.83 413527.09,5316781.92 413527.18,5316782.09 413528.23,5316781.56 413529.17,5316781.07 413530.71,5316783.56 413531.89,5316785.47 413529.99,5316786.56 413527.82,5316787.81 413531.71,5316795.24 413543.39,5316788.98 413536.91,5316776.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tway</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>159</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.8">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413530.52,5316764.88 413520.75,5316769.93 413527.09,5316781.92 413527.25,5316781.83 413536.91,5316776.85 413530.52,5316764.88</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaz</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>150</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.9">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413524.16,5316753.08 413514.65,5316758.15 413519.12,5316766.78 413520.67,5316769.78 413520.75,5316769.93 413530.52,5316764.88 413530.46,5316764.77 413528.82,5316761.73 413524.16,5316753.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaC</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>145</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.10">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413524.16,5316753.08 413520.37,5316746.05 413516.61,5316743.34 413510.3,5316752.25 413512.2,5316753.36 413514.65,5316758.15 413524.16,5316753.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaE</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.11">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413516.61,5316743.34 413509.32,5316738.09 413507.89,5316740.06 413502.79,5316747.16 413510.3,5316752.25 413516.61,5316743.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaF</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>100</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.12">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413507.89,5316740.06 413494.24,5316730.45 413494.74,5316729.83 413481.1,5316720.28 413474.39,5316715.59 413473.8,5316716.4 413473.44,5316716.9 413472.86,5316717.72 413461.43,5316733.88 413459.41,5316736.94 413469.59,5316744.16 413471.67,5316741.17 413485.02,5316750.5 413487.05,5316747.74 413490.35,5316750.07 413492.55,5316746.96 413494.39,5316748.26 413497.41,5316744.03 413502.49,5316747.57 413502.79,5316747.16 413507.89,5316740.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaH</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>857</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.13">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413451.25,5316711.56 413451.29,5316711.59 413446.3,5316718.59 413456.6,5316726.33 413454.5,5316729.28 413461.43,5316733.88 413472.86,5316717.72 413473.44,5316716.9 413456.04,5316704.9 413455.45,5316705.72 413451.25,5316711.56</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaI</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>393</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.14">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413437.41,5316700.07 413437.77,5316700.67 413437.15,5316701.55 413440.93,5316704.2 413440.36,5316705.01 413442.04,5316706.2 413443.14,5316704.65 413446.13,5316706.77 413445.59,5316707.53 413447.47,5316708.82 413450.81,5316711.24 413450.9,5316711.34 413451.03,5316711.4 413451.25,5316711.56 413455.45,5316705.72 413456.04,5316704.9 413458.28,5316701.78 413458.08,5316701.63 413457.86,5316701.47 413454.38,5316699.0 413443.04,5316690.93 413434.74,5316695.39 413437.41,5316700.07</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaJ</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>243</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.15">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.68,5316682.77 413427.34,5316688.96 413431.63,5316697.06 413434.74,5316695.39 413443.04,5316690.93 413438.68,5316682.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaM</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>119</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.16">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413598.25,5316891.8 413588.24,5316897.08 413589.95,5316900.26 413593.0,5316905.94 413594.73,5316905.01 413602.94,5316900.62 413598.25,5316891.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7R</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>113</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.17">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413585.56,5316840.51 413598.2,5316864.16 413598.11,5316864.21 413598.51,5316864.95 413598.98,5316864.76 413598.93,5316864.68 413609.54,5316858.97 413596.08,5316833.92 413585.08,5316839.83 413585.48,5316840.56 413585.56,5316840.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYi</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>353</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.18">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413593.87,5316883.59 413583.84,5316888.89 413585.63,5316892.22 413588.24,5316897.08 413598.25,5316891.8 413593.87,5316883.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7O</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>105</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.19">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413565.37,5316802.67 413578.09,5316826.53 413578.0,5316826.58 413578.4,5316827.33 413589.52,5316821.38 413576.05,5316796.04 413575.52,5316796.32 413575.56,5316796.4 413565.47,5316801.77 413565.43,5316801.69 413564.89,5316801.98 413565.28,5316802.71 413565.37,5316802.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TvYh</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>359</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.20">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413593.87,5316883.59 413589.48,5316875.39 413579.75,5316880.54 413583.84,5316888.89 413593.87,5316883.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7N</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.21">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413583.55,5316899.54 413578.78,5316902.06 413581.72,5316906.74 413583.05,5316908.85 413587.14,5316906.74 413585.14,5316902.73 413583.55,5316899.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7P</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>40</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.22">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413589.95,5316900.26 413588.24,5316897.08 413583.55,5316899.54 413585.14,5316902.73 413589.95,5316900.26</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7Q</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.23">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413561.72,5316863.81 413557.86,5316856.67 413557.79,5316856.54 413552.17,5316859.56 413552.24,5316859.67 413556.52,5316866.51 413556.82,5316866.35 413561.72,5316863.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7J</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>50</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.24">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413561.72,5316863.81 413556.82,5316866.35 413558.2,5316868.88 413563.1,5316866.35 413561.75,5316863.8 413561.72,5316863.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7K</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.25">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413519.12,5316780.03 413521.37,5316778.93 413518.72,5316773.91 413516.46,5316775.28 413519.12,5316780.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaA</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>14</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.26">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413509.49,5316760.9 413504.76,5316763.43 413509.61,5316771.52 413514.14,5316769.26 413509.49,5316760.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaB</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>49</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.27">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413509.49,5316760.9 413506.13,5316755.53 413504.62,5316756.73 413502.05,5316758.76 413504.76,5316763.43 413509.49,5316760.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaD</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.28">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413429.17,5316721.73 413432.67,5316728.44 413439.19,5316724.55 413435.24,5316717.95 413435.16,5316718.0 413429.17,5316721.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaL</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>56</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.29">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413435.16,5316718.0 413427.4,5316705.52 413437.41,5316700.07 413434.74,5316695.39 413431.63,5316697.06 413426.71,5316699.71 413423.65,5316701.35 413415.7,5316705.54 413416.21,5316706.34 413418.4,5316709.8 413420.2,5316712.65 413423.02,5316717.09 413426.88,5316723.16 413429.17,5316721.73 413435.16,5316718.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaK</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>264</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.30">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413426.71,5316699.71 413422.5,5316691.6 413419.37,5316693.31 413423.65,5316701.35 413426.71,5316699.71</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwaN</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>32</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.31">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413430.98,5316761.0 413439.19,5316756.29 413435.95,5316750.0 413436.96,5316749.48 413436.28,5316748.15 413427.0,5316753.56 413430.98,5316761.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbf</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>82</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.32">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413440.6,5316748.88 413439.09,5316749.88 413442.11,5316754.62 413445.74,5316752.54 413442.59,5316747.57 413440.6,5316748.88</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbh</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.33">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413485.65,5316974.39 413479.6,5316963.44 413479.4,5316963.07 413471.62,5316967.25 413477.84,5316978.72 413485.65,5316974.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8U</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.34">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413584.99,5316866.92 413575.26,5316872.06 413579.75,5316880.54 413589.48,5316875.39 413584.99,5316866.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7M</ogr:gmlid>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>106</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.35">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413541.26,5317042.87 413543.33,5317041.74 413543.46,5317042.08 413552.81,5317036.84 413550.34,5317032.45 413549.96,5317032.71 413520.23,5316979.14 413520.49,5316978.99 413518.06,5316974.59 413508.46,5316979.87 413508.62,5316980.11 413506.61,5316981.23 413541.26,5317042.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7e</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>911</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.36">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413513.24,5317023.75 413503.32,5317029.27 413504.77,5317031.86 413498.78,5317035.13 413497.53,5317032.96 413495.92,5317033.86 413495.41,5317034.16 413496.61,5317036.33 413492.02,5317038.91 413497.01,5317047.25 413497.67,5317048.35 413519.91,5317036.01 413513.24,5317023.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8m</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>322</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.37">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413501.32,5317025.85 413503.32,5317029.27 413513.24,5317023.75 413511.27,5317020.22 413502.55,5317004.32 413498.76,5317006.42 413495.1,5317008.45 413494.78,5317008.62 413494.68,5317008.68 413492.07,5317010.13 413496.95,5317018.46 413501.32,5317025.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8k</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>259</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.38">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413496.95,5317018.46 413492.07,5317010.13 413486.4,5317013.28 413482.93,5317015.2 413479.12,5317017.31 413482.88,5317023.68 413484.2,5317025.88 413496.95,5317018.46</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8l</ogr:gmlid>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>145</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.39">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413582.13,5317051.92 413582.73,5317052.9 413587.3,5317051.33 413586.77,5317049.37 413595.06,5317044.62 413595.31,5317044.82 413595.69,5317044.59 413589.9,5317034.1 413589.49,5317034.28 413589.56,5317034.56 413590.23,5317035.8 413586.22,5317037.92 413585.67,5317036.8 413580.67,5317039.55 413577.19,5317041.54 413563.69,5317049.34 413560.34,5317051.24 413555.57,5317054.19 413556.26,5317055.44 413552.17,5317057.74 413551.47,5317056.55 413551.26,5317056.24 413550.96,5317056.43 413556.48,5317066.56 413556.91,5317066.34 413556.89,5317065.93 413565.06,5317061.47 413565.61,5317062.24 413570.13,5317060.76 413569.68,5317058.88 413582.13,5317051.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7f</ogr:gmlid>
      <ogr:street>Colmarer Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>511</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.40">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413500.03,5316812.23 413495.97,5316804.4 413488.08,5316808.93 413492.1,5316816.48 413500.03,5316812.23</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twao</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>79</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.41">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413492.1,5316816.48 413488.08,5316808.93 413482.65,5316812.19 413483.31,5316813.38 413482.43,5316813.98 413482.86,5316814.83 413480.54,5316816.04 413483.3,5316821.2 413492.1,5316816.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twan</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>76</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.42">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413467.06,5316857.18 413463.1,5316849.76 413460.09,5316848.82 413450.02,5316854.19 413454.26,5316862.12 413457.46,5316860.41 413458.09,5316861.61 413457.74,5316861.79 413457.9,5316862.09 413467.06,5316857.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9n</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>137</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.43">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413483.3,5316821.2 413480.54,5316816.04 413479.74,5316814.55 413478.5,5316812.22 413475.36,5316814.36 413477.19,5316817.38 413473.36,5316819.42 413472.89,5316818.62 413472.05,5316819.01 413470.94,5316817.13 413463.1,5316821.13 413466.81,5316828.2 413469.38,5316828.67 413483.3,5316821.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twam</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>154</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.44">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413450.02,5316854.19 413442.08,5316858.43 413441.83,5316858.58 413442.68,5316860.15 413446.93,5316867.97 413447.09,5316867.89 413448.29,5316867.25 413447.78,5316866.29 413454.12,5316862.81 413454.52,5316862.6 413454.26,5316862.12 413450.02,5316854.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9o</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>90</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.45">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413440.15,5316844.19 413453.77,5316836.93 413454.69,5316833.9 413450.53,5316826.12 413441.22,5316831.1 413440.55,5316831.46 413442.24,5316834.62 413436.64,5316837.6 413440.15,5316844.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbA</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>2081</ogr:use_id>
      <ogr:use>Gaststätte, Restaura</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>170</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.46">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.67,5316848.7 413440.15,5316844.19 413436.64,5316837.6 413435.15,5316834.8 413434.88,5316834.95 413431.07,5316837.07 413430.49,5316836.02 413429.23,5316836.65 413426.19,5316838.17 413431.67,5316848.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twde</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.47">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413436.04,5316889.16 413437.93,5316892.71 413436.61,5316893.4 413438.51,5316896.95 413438.07,5316897.19 413441.44,5316903.52 413441.88,5316903.28 413443.76,5316906.82 413445.06,5316906.12 413446.94,5316909.64 413447.38,5316909.41 413449.33,5316913.07 413448.47,5316915.9 413444.72,5316917.95 413444.49,5316917.51 413440.95,5316919.39 413440.24,5316918.05 413436.7,5316919.93 413436.46,5316919.49 413430.15,5316922.85 413430.38,5316923.29 413426.85,5316925.16 413427.56,5316926.49 413424.04,5316928.36 413429.14,5316937.86 413462.77,5316919.97 413464.25,5316919.18 413463.46,5316917.7 413445.56,5316884.13 413436.04,5316889.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8J</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>790</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.48">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.73,5316799.53 413531.71,5316795.24 413527.82,5316787.81 413527.29,5316786.76 413526.87,5316785.96 413522.44,5316788.26 413521.52,5316786.5 413517.86,5316788.4 413518.79,5316790.15 413523.73,5316799.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaw</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.49">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.67,5316848.7 413426.19,5316838.17 413422.49,5316840.15 413423.05,5316841.21 413418.95,5316843.4 413424.02,5316852.77 413431.67,5316848.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdc</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.50">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413434.12,5316862.67 413420.01,5316870.2 413420.86,5316871.78 413425.68,5316880.82 413439.78,5316873.29 413434.97,5316864.26 413434.12,5316862.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8K</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>192</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.51">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413417.02,5316856.51 413424.02,5316852.77 413418.95,5316843.4 413418.38,5316842.35 413416.42,5316843.41 413416.1,5316842.83 413414.35,5316843.76 413411.06,5316845.49 413411.66,5316846.6 413417.02,5316856.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd9</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.52">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413409.15,5316860.69 413417.02,5316856.51 413411.66,5316846.6 413409.35,5316847.85 413403.67,5316851.13 413409.15,5316860.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd8</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>101</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.53">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413397.4,5316866.95 413409.15,5316860.69 413403.67,5316851.13 413403.07,5316850.09 413401.05,5316846.41 413396.18,5316849.07 413397.63,5316851.83 413394.81,5316853.32 413395.39,5316854.42 413391.77,5316856.34 413397.4,5316866.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd7</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.54">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413416.96,5316887.89 413416.7,5316885.64 413425.68,5316880.82 413420.86,5316871.78 413420.01,5316870.2 413402.0,5316879.79 413402.089899998,5316879.96199998 413402.178599996,5316880.1346 413402.266099993,5316880.30779999 413402.352399991,5316880.48169999 413402.437499989,5316880.65609998 413402.521299987,5316880.83109998 413402.603899985,5316881.0068 413402.685299983,5316881.183 413402.765399981,5316881.35969999 413402.844299979,5316881.53699999 413402.921899977,5316881.71489998 413402.998299975,5316881.89329998 413403.073399998,5316882.0723 413403.147199996,5316882.25179999 413403.219799994,5316882.43169999 413403.291199993,5316882.61219998 413403.361199991,5316882.79319998 413403.429999989,5316882.97469997 413403.497499987,5316883.1567 413403.563699986,5316883.33909999 413403.628699984,5316883.52199999 413403.692299983,5316883.70529998 413403.754699981,5316883.88909998 413403.815799979,5316884.0733 413403.875499978,5316884.25789999 413403.934,5316884.443 413403.991199975,5316884.62849998 413404.047,5316884.81429998 413404.101599997,5316885.0006 413404.154799996,5316885.18719999 413404.206799995,5316885.37419999 413404.257399993,5316885.56149999 413404.306699992,5316885.74929998 413404.354699991,5316885.93729998 413404.40129999,5316886.1257 413404.446599989,5316886.31439999 413404.490599988,5316886.50339999 413404.533299987,5316886.69269998 413404.574699986,5316886.88239998 413404.614699984,5316887.0723 413404.653299983,5316887.26239999 413404.690699983,5316887.45289999 413404.726599982,5316887.64359998 413404.761299981,5316887.83459998 413404.79459998,5316888.0257 413404.826599979,5316888.21719999 413404.857199978,5316888.40879999 413404.886399978,5316888.60069998 413404.914399977,5316888.79269998 413404.940899976,5316888.98499998 413404.966099976,5316889.1774 413404.99,5316889.37 413406.86,5316889.17 413416.96,5316887.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8L</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>257</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.55">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413389.42,5316871.19 413397.4,5316866.95 413391.77,5316856.34 413391.3,5316855.46 413387.23,5316857.58 413387.81,5316858.69 413383.93,5316860.72 413389.42,5316871.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd6</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>112</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.56">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413382.28,5316874.99 413389.42,5316871.19 413383.93,5316860.72 413380.41,5316862.68 413381.19,5316864.09 413378.99,5316865.42 413377.65,5316866.14 413382.28,5316874.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd4</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>88</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.57">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413350.98,5316891.73 413346.43,5316883.19 413346.15,5316883.34 413344.44,5316884.25 413342.56,5316885.15 413342.25,5316884.6 413341.69,5316884.5 413338.92,5316886.06 413343.93,5316895.5 413350.98,5316891.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdg</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>81</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.58">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413355.62,5316904.55 413344.27,5316910.6 413332.19,5316917.05 413332.65,5316917.91 413338.31,5316928.52 413345.21,5316924.83 413344.65,5316923.77 413346.35,5316922.86 413346.92,5316923.91 413353.8,5316920.23 413352.37,5316917.56 413354.89,5316916.22 413355.09,5316916.59 413357.62,5316915.24 413357.42,5316914.87 413360.31,5316913.33 413360.29,5316913.29 413355.62,5316904.55</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9q</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>317</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.59">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413515.8,5316803.77 413523.73,5316799.53 413518.79,5316790.15 413514.42,5316792.45 413513.48,5316790.93 413509.97,5316792.8 413510.8,5316794.37 413515.8,5316803.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twau</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.60">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413517.54,5316831.75 413519.22,5316830.85 413519.79,5316831.92 413521.2,5316831.16 413520.63,5316830.1 413525.26,5316827.63 413526.37,5316827.03 413526.7,5316826.91 413520.96,5316816.3 413520.68,5316816.47 413511.85,5316821.19 413517.54,5316831.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7p</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>126</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.61">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413507.88,5316808.02 413515.8,5316803.77 413510.8,5316794.37 413506.5,5316796.63 413505.69,5316795.18 413502.05,5316797.04 413507.88,5316808.02</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twas</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.62">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413511.85,5316821.19 413503.03,5316825.92 413508.74,5316836.48 413514.45,5316833.41 413515.02,5316834.46 413516.43,5316833.7 413515.86,5316832.65 413517.54,5316831.75 413511.85,5316821.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7n</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>122</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.63">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413507.88,5316808.02 413502.05,5316797.04 413498.51,5316798.95 413499.26,5316800.51 413495.11,5316802.74 413495.97,5316804.4 413500.03,5316812.23 413507.88,5316808.02</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twaq</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.64">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413411.14,5316837.51 413414.77,5316835.67 413410.03,5316826.94 413403.01,5316830.62 413403.03,5316830.66 413407.68,5316839.26 413411.14,5316837.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twda</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>78</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.65">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413411.14,5316837.51 413407.68,5316839.26 413411.06,5316845.49 413414.35,5316843.76 413411.14,5316837.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdb</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>27</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.66">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413512.92,5316779.03 413504.99,5316783.43 413507.72,5316788.58 413515.72,5316784.34 413512.92,5316779.03</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twav</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>54</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.67">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413457.9,5316862.09 413457.74,5316861.79 413458.09,5316861.61 413457.46,5316860.41 413454.26,5316862.12 413454.52,5316862.6 413454.12,5316862.81 413452.64,5316867.04 413454.36,5316867.7 413456.0,5316868.33 413456.49,5316866.71 413457.9,5316862.09</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9m</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.68">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413375.68,5316845.16 413376.11,5316845.79 413378.77,5316850.86 413383.2,5316848.53 413385.75,5316853.33 413389.2,5316851.5 413383.64,5316840.96 413375.68,5316845.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd5</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>79</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.69">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413450.13,5316873.61 413453.0,5316872.09 413454.36,5316867.7 413452.64,5316867.04 413451.34,5316868.96 413448.47,5316870.48 413450.13,5316873.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9p</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.70">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413516.46,5316775.28 413515.45,5316773.48 413513.05,5316775.1 413516.61,5316781.42 413519.12,5316780.03 413516.46,5316775.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twax</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>21</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.71">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413526.11,5316840.1 413528.5,5316838.81 413527.32,5316836.61 413521.74,5316839.54 413522.96,5316841.8 413526.11,5316840.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7q</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.72">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413499.79,5316792.81 413502.3,5316791.39 413498.57,5316784.24 413496.05,5316785.77 413499.79,5316792.81</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twat</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>23</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.73">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413516.77,5316845.14 413513.54,5316839.49 413511.09,5316840.82 413514.18,5316846.54 413514.47,5316846.38 413516.77,5316845.14</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7o</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.74">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413496.05,5316785.77 413493.35,5316787.41 413497.02,5316794.28 413499.79,5316792.81 413496.05,5316785.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twar</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.75">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413418.32,5316943.61 413419.39,5316945.67 413423.23,5316943.64 413423.40189999,5316943.55929999 413423.576699985,5316943.48499999 413423.754099981,5316943.41729999 413423.933899976,5316943.35619999 413424.115799997,5316943.30189999 413424.299699992,5316943.25429999 413424.485099988,5316943.21369999 413424.671999983,5316943.1799 413424.859999978,5316943.1531 413425.048899999,5316943.1333 413425.238299994,5316943.1206 413425.428099989,5316943.1148 413425.617999984,5316943.1162 413425.80769998,5316943.1246 413425.997,5316943.14 413426.185599995,5316943.1624 413426.373199991,5316943.1919 413426.559599986,5316943.22829999 413426.744599981,5316943.27159999 413426.927699977,5316943.32179999 413427.108899997,5316943.37869999 413427.287899993,5316943.44239999 413427.464399988,5316943.51259999 413427.638099984,5316943.58939999 413427.80889998,5316943.67249998 413427.976399975,5316943.76199998 413428.140499996,5316943.85759998 413428.30099999,5316943.95919998 413428.457499988,5316944.0668 413428.61,5316944.18 413428.759899981,5316944.30969999 413428.904699977,5316944.44509999 413429.044099999,5316944.58599999 413429.177899996,5316944.73219998 413429.30599999,5316944.88349998 413429.428099989,5316945.0396 413429.543999986,5316945.20039999 413429.653699983,5316945.36549999 413429.756799981,5316945.53479999 413429.853299978,5316945.70799998 413429.942999976,5316945.88469998 413430.025699999,5316946.0648 413430.101399997,5316946.24799999 413430.17,5316946.434 413430.231299994,5316946.62249998 413430.285199993,5316946.81329998 413430.331699992,5316947.006 413430.370699991,5316947.2003 413430.40209999,5316947.39599999 413430.425899989,5316947.59279998 413430.441999989,5316947.79039998 413430.450399989,5316947.98839998 413430.451199989,5316948.1866 413430.444299989,5316948.38469999 413430.429599989,5316948.58239998 413430.40739999,5316948.77939998 413430.37749999,5316948.97539998 413430.34,5316949.17 413426.62,5316952.92 413426.75,5316953.25 413475.61,5316927.36 413453.06,5316884.9 413455.45,5316883.63 413446.93,5316867.97 413442.68,5316860.15 413434.97,5316864.26 413420.86,5316871.78 413404.41,5316880.55 413404.492999988,5316880.72719998 413404.574599985,5316880.90509998 413404.654999983,5316881.0835 413404.734199981,5316881.26239999 413404.811999979,5316881.44199999 413404.888599978,5316881.62209998 413404.963799976,5316881.80269998 413405.037799999,5316881.98389998 413405.110499997,5316882.1656 413405.181899995,5316882.34779999 413405.25199999,5316882.53049999 413405.320799992,5316882.71369998 413405.38819999,5316882.89739998 413405.454399989,5316883.0815 413405.519199987,5316883.26619999 413405.582699985,5316883.45129999 413405.644899984,5316883.63679998 413405.705699982,5316883.82279998 413405.765299981,5316884.0092 413405.823499979,5316884.19609999 413405.880299978,5316884.38329999 413405.935799976,5316884.57099999 413405.99,5316884.759 413406.042799999,5316884.94739998 413406.094299998,5316885.1362 413406.144399996,5316885.32539999 413406.193199995,5316885.51489999 413406.240599994,5316885.70479998 413406.286699993,5316885.89499998 413406.331399992,5316886.0855 413406.374799991,5316886.27629999 413406.416699989,5316886.46749999 413406.457299988,5316886.65889998 413406.496599987,5316886.85059998 413406.534399986,5316887.0426 413406.570899986,5316887.23489999 413406.606099985,5316887.42739999 413406.639799984,5316887.62019998 413406.672199983,5316887.81319998 413406.703199982,5316888.0064 413406.732799981,5316888.19989999 413406.760999981,5316888.39349999 413406.78779998,5316888.58739998 413406.813299979,5316888.78139998 413406.837299979,5316888.97559998 413406.86,5316889.17 413406.883199978,5316889.36399999 413406.905099977,5316889.55809999 413406.925699977,5316889.75229998 413406.945099976,5316889.94669998 413406.963099976,5316890.1413 413406.979799975,5316890.33589999 413406.995299975,5316890.53059999 413407.0094,5316890.72549998 413407.022299999,5316890.92039998 413407.033799999,5316891.1154 413407.044099999,5316891.31049999 413407.053,5316891.50569999 413407.060699998,5316891.70089998 413407.067,5316891.89609998 413407.072099998,5316892.0914 413407.075899998,5316892.28669999 413407.078399998,5316892.48199999 413407.079499998,5316892.67739998 413407.079399998,5316892.87269998 413407.078,5316893.0681 413407.075299998,5316893.26339999 413407.071199998,5316893.45869999 413407.065899998,5316893.65399998 413407.059299999,5316893.84929998 413407.051399999,5316894.0444 413407.042199999,5316894.23959999 413407.031699999,5316894.43469999 413407.019899999,5316894.62959998 413407.0068,5316894.82459998 413406.992399975,5316895.0194 413406.976699975,5316895.21409999 413406.959699976,5316895.40869999 413406.941399976,5316895.60319998 413406.921899977,5316895.79759998 413406.900999977,5316895.99179997 413406.878899978,5316896.1859 413406.855399978,5316896.37989999 413406.830699979,5316896.57359999 413406.80469998,5316896.76729998 413406.77739998,5316896.96069998 413406.748799981,5316897.1539 413406.719,5316897.347 413406.687899983,5316897.53989999 413406.655499983,5316897.73259998 413406.621699984,5316897.92499998 413406.586699985,5316898.1173 413406.550499986,5316898.30919999 413406.512899987,5316898.50099999 413406.474099988,5316898.69249998 413406.433999989,5316898.88369998 413406.39259999,5316899.0747 413406.35,5316899.26529999 413406.306099992,5316899.45569999 413406.26099999,5316899.64579998 413406.214499995,5316899.83559998 413406.166799996,5316900.0251 413406.117899997,5316900.21429999 413406.067699998,5316900.40309999 413406.0162,5316900.59159999 413405.963499976,5316900.77969998 413405.909599977,5316900.96749998 413405.854399978,5316901.1549 413405.79789998,5316901.34199999 413405.740199981,5316901.52869999 413405.681299983,5316901.71499998 413405.621199984,5316901.90089998 413405.559799986,5316902.0863 413405.497099987,5316902.27139999 413405.433299989,5316902.45609999 413405.368199991,5316902.64029998 413405.301899992,5316902.82409998 413405.234299994,5316903.0074 413405.165599996,5316903.1903 413405.095599998,5316903.37279999 413405.024499999,5316903.55469999 413404.952099976,5316903.73619998 413404.878499978,5316903.91719998 413404.80369998,5316904.0977 413404.727699982,5316904.27769999 413404.650499984,5316904.45719999 413404.572199986,5316904.63619998 413404.492599988,5316904.81469998 413404.41189999,5316904.99259998 413404.33,5316905.17 413404.246799994,5316905.34669999 413404.162399996,5316905.52279999 413404.076799998,5316905.69829998 413403.990099975,5316905.87329998 413403.902199977,5316906.0477 413403.813199979,5316906.22159999 413403.723099982,5316906.39479999 413403.631799984,5316906.56749999 413403.539299986,5316906.73949998 413403.445799989,5316906.91089998 413403.351099991,5316907.0817 413403.255199994,5316907.25189999 413403.158299996,5316907.42139999 413403.060199998,5316907.59029999 413402.961099976,5316907.75859998 413402.860799978,5316907.92619998 413402.759399981,5316908.0931 413402.656999983,5316908.25929999 413402.553399986,5316908.42489999 413402.448699989,5316908.58979999 413402.343,5316908.754 413402.236199994,5316908.91749998 413402.128299997,5316909.0803 413402.0193,5316909.24239999 413401.909199977,5316909.40379999 413401.79809998,5316909.56439999 413401.685899983,5316909.72439998 413401.572699986,5316909.88349998 413401.458399988,5316910.0419 413401.343099991,5316910.1996 413401.226799994,5316910.35649999 413401.109399997,5316910.51259999 413400.990999975,5316910.66799998 413400.871599978,5316910.82249998 413400.751099981,5316910.97629998 413400.629699984,5316911.1293 413400.507199987,5316911.28139999 413400.38369999,5316911.43279999 413400.259299993,5316911.58329999 413400.133799997,5316911.73309998 413400.0074,5316911.88189998 413399.88,5316912.03 413399.751899981,5316912.1768 413399.622799984,5316912.32269999 413399.492799988,5316912.46769999 413399.361799991,5316912.61189999 413399.229799994,5316912.75509998 413399.096799998,5316912.89749998 413398.962999976,5316913.039 413398.828099979,5316913.1797 413398.692399982,5316913.31939999 413398.555699986,5316913.45819999 413398.418099989,5316913.59599999 413398.279599993,5316913.73299998 413398.140099996,5316913.86899998 413397.999799975,5316914.0041 413397.858499978,5316914.1383 413397.716399982,5316914.27149999 413397.573399986,5316914.40379999 413397.429499989,5316914.53509999 413397.284699993,5316914.66539998 413397.139099996,5316914.79479998 413396.992599975,5316914.92319998 413396.845199979,5316915.0506 413396.697,5316915.177 413396.547999986,5316915.30239999 413396.39809999,5316915.42689999 413396.247399994,5316915.55029999 413396.095799998,5316915.67269998 413395.943499976,5316915.79409998 413395.79029998,5316915.91449998 413395.636399984,5316916.0339 413395.481599988,5316916.1522 413395.326099992,5316916.26949999 413395.169799996,5316916.38579999 413395.0127,5316916.50099999 413394.854899978,5316916.61509998 413394.696299982,5316916.72819998 413394.536899986,5316916.84029998 413394.37679999,5316916.95119998 413394.215899995,5316917.0611 413394.054399999,5316917.1699 413393.892099977,5316917.27769999 413393.729099982,5316917.38429999 413393.565299986,5316917.48989999 413393.40089999,5316917.59439998 413393.235799994,5316917.69769998 413393.07,5316917.8 413398.05,5316927.2 413401.79,5316934.26 413405.54,5316941.34 413409.29,5316948.41 413418.32,5316943.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8I</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>4399</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.76">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413526.7,5316826.91 413531.7,5316836.89 413549.94,5316826.76 413548.01,5316823.17 413546.68,5316823.87 413544.99,5316820.71 413546.31,5316819.99 413544.79,5316817.2 413540.32,5316819.56 413541.06,5316820.84 413537.95,5316822.6 413537.21,5316821.32 413526.7,5316826.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7s</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>217</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.77">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413487.53,5316803.72 413493.78,5316800.18 413490.39,5316793.65 413483.58,5316797.77 413485.09,5316800.05 413487.53,5316803.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twap</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>55</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.78">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413425.72,5316818.53 413418.09,5316822.61 413422.23,5316830.57 413427.09,5316827.96 413427.99,5316829.7 413427.46,5316829.98 413430.23,5316835.29 413428.9,5316836.01 413429.23,5316836.65 413430.49,5316836.02 413431.07,5316837.07 413434.88,5316834.95 413434.69,5316834.59 413430.91,5316827.82 413430.85,5316827.72 413430.92,5316827.68 413428.57,5316823.62 413428.67,5316823.57 413425.73,5316818.52 413425.72,5316818.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdd</ogr:gmlid>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>120</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.79">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413451.38,5317042.29 413444.55,5317046.31 413447.74,5317051.9 413449.89,5317055.67 413446.99,5317057.41 413448.79,5317060.53 413451.03,5317059.36 413451.92,5317061.04 413451.1,5317061.52 413451.86,5317062.88 413469.02,5317052.9 413469.26,5317053.21 413483.18,5317044.83 413483.48,5317045.32 413484.89,5317044.48 413482.84,5317041.4 413490.79,5317036.86 413495.41,5317034.16 413495.92,5317033.86 413490.12,5317024.18 413483.85,5317027.82 413481.76,5317024.34 413474.85,5317028.42 413466.79,5317033.18 413462.96,5317035.44 413459.04,5317037.75 413456.86,5317039.04 413451.38,5317042.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8p</ogr:gmlid>
      <ogr:street>Fedderstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>869</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.80">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413428.26,5316968.73 413420.3,5316972.99 413427.85,5316987.15 413429.12,5316986.48 413430.9,5316989.85 413432.33,5316989.09 413435.62,5316987.29 413433.85,5316983.94 413435.81,5316982.89 413428.26,5316968.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8E</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>165</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.81">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.95,5316965.02 413396.9,5316957.43 413388.55,5316961.93 413389.28,5316963.29 413385.18,5316965.5 413388.41,5316971.76 413400.95,5316965.02</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9C</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.82">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413398.51,5316910.83 413398.38179999,5316910.97669998 413398.252599994,5316911.1226 413398.122299997,5316911.26749999 413397.990999975,5316911.41149999 413397.858799978,5316911.55449999 413397.725499982,5316911.69669998 413397.591199985,5316911.83779998 413397.455899988,5316911.97809998 413397.319599992,5316912.1174 413397.182399995,5316912.25569999 413397.044099999,5316912.39299999 413396.904999977,5316912.52939999 413396.764799981,5316912.66469998 413396.623699984,5316912.79909998 413396.481699988,5316912.93249998 413396.338699991,5316913.0649 413396.194799995,5316913.1963 413396.05,5316913.32659999 413395.904199977,5316913.45589999 413395.757599981,5316913.58419999 413395.609999985,5316913.71149998 413395.461599988,5316913.83769998 413395.312199992,5316913.96289998 413395.162,5316914.087 413395.0109,5316914.21 413394.859,5316914.332 413394.706199982,5316914.45289999 413394.552499986,5316914.57279999 413394.39799999,5316914.69149998 413394.242599994,5316914.80919998 413394.086499998,5316914.92569998 413393.929499977,5316915.0412 413393.771599981,5316915.1556 413393.612999985,5316915.26879999 413393.453599989,5316915.38089999 413393.293399993,5316915.49189999 413393.132499997,5316915.60169998 413392.970699975,5316915.71039998 413392.80819998,5316915.81799998 413392.644999984,5316915.92439998 413392.480999988,5316916.0297 413392.316199992,5316916.1338 413392.150699996,5316916.23669999 413391.984499975,5316916.33849999 413391.817599979,5316916.43909999 413391.649999984,5316916.53849999 413391.481699988,5316916.63679998 413391.312699992,5316916.73379998 413391.143,5316916.82969998 413390.972699975,5316916.92429998 413390.80169998,5316917.0177 413390.63,5316917.11 413396.44,5316928.06 413396.46,5316928.05 413398.05,5316927.2 413407.04,5316922.43 413405.98,5316920.43 413406.115599997,5316920.28949999 413406.250499994,5316920.1484 413406.38469999,5316920.0065 413406.518199987,5316919.86399998 413406.650999984,5316919.72079998 413406.783,5316919.577 413406.914299977,5316919.43249999 413407.044899999,5316919.28729999 413407.174799996,5316919.1414 413407.303899992,5316918.99489998 413407.432299989,5316918.84779998 413407.56,5316918.7 413399.88,5316912.03 413398.51,5316910.83</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8O</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>160</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.83">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413396.9,5316957.43 413392.01,5316948.26 413391.93,5316948.3 413381.06,5316954.14 413377.93,5316955.83 413381.63,5316962.68 413387.29,5316959.6 413388.55,5316961.93 413396.9,5316957.43</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9D</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>149</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.84">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413361.59,5316862.79 413365.54,5316870.2 413373.25,5316866.26 413374.2,5316865.75 413374.18,5316865.24 413373.67,5316864.33 413372.36,5316861.54 413374.23,5316860.51 413372.27,5316856.95 413361.59,5316862.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd2</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>94</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.85">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413351.67,5316872.88 413342.96,5316877.53 413346.15,5316883.34 413346.43,5316883.19 413350.98,5316891.73 413357.95,5316888.01 413358.57,5316885.82 413351.67,5316872.88</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdf</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>156</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.86">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413407.56,5316918.7 413407.688899983,5316918.55059999 413407.816999979,5316918.40069999 413407.944399976,5316918.25 413408.071,5316918.0988 413408.196899995,5316917.94689998 413408.32199999,5316917.79439998 413408.446299989,5316917.64119998 413408.569899986,5316917.48749999 413408.692799982,5316917.33309999 413408.814799979,5316917.1781 413408.936099976,5316917.0226 413409.056599999,5316916.86639998 413409.176299996,5316916.70959998 413409.295299993,5316916.55219999 413409.41349999,5316916.39429999 413409.530799987,5316916.23569999 413409.647399984,5316916.0766 413409.763199981,5316915.91689998 413409.878199978,5316915.75659998 413409.992399975,5316915.59579999 413410.105799997,5316915.43439999 413410.218399994,5316915.27239999 413410.330199992,5316915.1099 413410.441199989,5316914.94679998 413410.551399986,5316914.78319998 413410.660699983,5316914.61899998 413410.769299981,5316914.45429999 413410.877,5316914.289 413410.983899975,5316914.1232 413411.09,5316913.95689998 413411.195199995,5316913.79 413411.299699992,5316913.62259998 413411.40319999,5316913.45469999 413411.505999987,5316913.28629999 413411.607899985,5316913.1174 413411.708999982,5316912.94799998 413411.80919998,5316912.77799998 413411.908599977,5316912.60759998 413412.0071,5316912.43669999 413412.104799997,5316912.26529999 413412.201599995,5316912.0934 413412.297599992,5316911.92099998 413412.39269999,5316911.74819998 413412.486899988,5316911.57489999 413412.580299985,5316911.40109999 413412.672799983,5316911.22689999 413412.764499981,5316911.0522 413412.855299978,5316910.87699998 413412.945199976,5316910.70139998 413413.034199999,5316910.52539999 413413.122399997,5316910.34889999 413413.209699995,5316910.172 413413.296099993,5316909.99459997 413413.38159999,5316909.81679998 413413.466199988,5316909.63859998 413413.55,5316909.46 413404.33,5316905.17 413402.67,5316904.4 413402.588399985,5316904.57389999 413402.505699987,5316904.74719998 413402.421699989,5316904.92 413402.336499991,5316905.0921 413402.250099994,5316905.26369999 413402.162599996,5316905.43469999 413402.073799998,5316905.60499998 413401.983899975,5316905.77469998 413401.892799977,5316905.94379998 413401.80049998,5316906.1123 413401.706999982,5316906.28009999 413401.612399985,5316906.44729999 413401.516599987,5316906.61379998 413401.419699989,5316906.77959998 413401.321599992,5316906.94469998 413401.222299994,5316907.1092 413401.121899997,5316907.27299999 413401.020399999,5316907.43599999 413400.917799977,5316907.59839998 413400.814,5316907.76 413400.709099982,5316907.92089998 413400.603099985,5316908.0811 413400.495999987,5316908.24049999 413400.38769999,5316908.39919999 413400.278399993,5316908.55709999 413400.168,5316908.71429998 413400.056499999,5316908.87059998 413399.943899976,5316909.0262 413399.830199979,5316909.1811 413399.715399982,5316909.33509999 413399.599599985,5316909.48829999 413399.482699988,5316909.64069998 413399.364799991,5316909.79229998 413399.245799994,5316909.94309998 413399.125699997,5316910.093 413399.0046,5316910.24209999 413398.882499978,5316910.39039999 413398.759399981,5316910.53779999 413398.635199984,5316910.68429998 413398.51,5316910.83 413399.88,5316912.03 413407.56,5316918.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8N</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>112</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.87">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413391.93,5316948.3 413389.01,5316942.88 413386.35,5316942.07 413379.03,5316946.01 413378.2,5316948.76 413381.06,5316954.14 413391.93,5316948.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9v</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.88">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413371.84,5316936.77 413374.75,5316942.25 413377.46,5316943.05 413384.78,5316939.13 413385.6,5316936.42 413382.72,5316930.99 413371.84,5316936.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9u</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.89">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413404.99,5316889.37 413405.0143,5316889.56599999 413405.037099999,5316889.76219998 413405.058499999,5316889.95859998 413405.078499998,5316890.1551 413405.097099998,5316890.35179999 413405.114299997,5316890.54859999 413405.130099997,5316890.74549998 413405.144399996,5316890.94249998 413405.157399996,5316891.1396 413405.168899996,5316891.33679999 413405.17899999,5316891.53409999 413405.187699995,5316891.73139998 413405.195,5316891.92879998 413405.200899995,5316892.1262 413405.205399995,5316892.32369999 413405.208399995,5316892.52119999 413405.21,5316892.71879998 413405.210199995,5316892.91629998 413405.209,5316893.1138 413405.206399995,5316893.31129999 413405.202399995,5316893.50879999 413405.196899995,5316893.70629998 413405.19,5316893.90369998 413405.181699995,5316894.101 413405.172,5316894.29829999 413405.160899996,5316894.49559999 413405.148399996,5316894.69269998 413405.134399997,5316894.88969998 413405.119099997,5316895.0867 413405.102299997,5316895.28349999 413405.084099998,5316895.48019999 413405.064499998,5316895.67669998 413405.043499999,5316895.87309998 413405.021099999,5316896.0694 413404.997299975,5316896.26549999 413404.972099975,5316896.46139999 413404.945399976,5316896.65709998 413404.917399977,5316896.85269998 413404.888,5316897.048 413404.857199978,5316897.24309999 413404.824999979,5316897.43799999 413404.79129998,5316897.63269998 413404.756299981,5316897.82709998 413404.719899982,5316898.0213 413404.682099983,5316898.21519999 413404.642899984,5316898.40879999 413404.602299985,5316898.60209998 413404.560299986,5316898.79519998 413404.516899987,5316898.98789997 413404.472199988,5316899.1803 413404.426099989,5316899.37239999 413404.37859999,5316899.56409999 413404.329699992,5316899.75559998 413404.279499993,5316899.94659998 413404.227899994,5316900.1373 413404.174899996,5316900.32759999 413404.120599997,5316900.51749999 413404.064899998,5316900.70709998 413404.0078,5316900.89619998 413403.949399976,5316901.0849 413403.889599978,5316901.27319999 413403.828499979,5316901.46109999 413403.765999981,5316901.64849998 413403.702199982,5316901.83539998 413403.637099984,5316902.0219 413403.570599986,5316902.20799999 413403.502799987,5316902.39349999 413403.433699989,5316902.57859999 413403.363199991,5316902.76309998 413403.291399993,5316902.94709998 413403.218299994,5316903.1307 413403.143899996,5316903.31369999 413403.068199998,5316903.49609999 413402.991099975,5316903.67799998 413402.912799977,5316903.85939998 413402.833099979,5316904.0402 413402.752199981,5316904.22039999 413402.67,5316904.4 413404.33,5316905.17 413413.55,5316909.46 413413.633799984,5316909.27909999 413413.716699982,5316909.0978 413413.79869998,5316908.91609998 413413.879799978,5316908.73389998 413413.959899976,5316908.55139999 413414.039199999,5316908.36849999 413414.117499997,5316908.1851 413414.194899995,5316908.0014 413414.271299993,5316907.81729998 413414.346899991,5316907.63279998 413414.421499989,5316907.44789999 413414.495099987,5316907.26269999 413414.567899986,5316907.0771 413414.639699984,5316906.89109998 413414.710599982,5316906.70469998 413414.78049998,5316906.51799999 413414.849499979,5316906.33099999 413414.917499977,5316906.1436 413414.984599975,5316905.95589998 413415.050799999,5316905.76779998 413415.116,5316905.57939999 413415.180199995,5316905.39069999 413415.243499994,5316905.20159999 413415.305899992,5316905.0123 413415.367299991,5316904.82259998 413415.427699989,5316904.63259998 413415.487199988,5316904.44239999 413415.545699986,5316904.25179999 413415.603299985,5316904.0609 413415.659899983,5316903.86979998 413415.715499982,5316903.67829998 413415.770199981,5316903.48659999 413415.823899979,5316903.29459999 413415.876599978,5316903.1023 413415.928399977,5316902.90979998 413415.979199975,5316902.71699998 413416.029,5316902.524 413416.077799998,5316902.33069999 413416.125699997,5316902.1372 413416.172599996,5316901.94349998 413416.218499994,5316901.74949998 413416.263499993,5316901.55539999 413416.307399992,5316901.36089999 413416.350399991,5316901.1663 413416.39239999,5316900.97139998 413416.433399989,5316900.77639998 413416.473499988,5316900.58109999 413416.512499987,5316900.38569999 413416.550599986,5316900.19 413416.587699985,5316899.99409997 413416.623799984,5316899.79809998 413416.658899983,5316899.60189999 413416.692999983,5316899.40549999 413416.726199982,5316899.209 413416.758299981,5316899.0122 413416.78949998,5316898.81539998 413416.819599979,5316898.61829998 413416.848799979,5316898.42109999 413416.876999978,5316898.22379999 413416.904199977,5316898.0264 413416.930399976,5316897.82879998 413416.955599976,5316897.63099998 413416.979799975,5316897.43319999 413417.003,5316897.23519999 413417.025199999,5316897.0371 413417.046399999,5316896.83889998 413417.066599998,5316896.64059998 413417.085799998,5316896.44219999 413417.104,5316896.24369999 413417.121199997,5316896.0451 413417.137399997,5316895.84649998 413417.152599996,5316895.64769998 413417.166799996,5316895.44889999 413417.18,5316895.25 413417.374399991,5316895.22899999 413417.565599986,5316895.1877 413417.751299981,5316895.1265 413417.929599977,5316895.046 413418.098299998,5316894.94709998 413418.255699994,5316894.83099998 413418.4,5316894.699 413418.529499987,5316894.55249999 413418.642899984,5316894.39319999 413418.738799981,5316894.22279999 413418.816199979,5316894.0432 413418.874299978,5316893.85649998 413418.912399977,5316893.66469998 413418.93,5316893.47 413418.926099977,5316893.27349999 413418.901199977,5316893.0786 413418.855799978,5316892.88749998 413418.79009998,5316892.70229998 413418.705099982,5316892.52509999 413418.601699985,5316892.35809999 413418.481,5316892.203 413418.344499991,5316892.0617 413418.193699995,5316891.93579998 413418.030299999,5316891.82659998 413417.856199978,5316891.73559998 413417.673399983,5316891.66359998 413417.483899988,5316891.61149998 413417.29,5316891.58 413417.282099993,5316891.39489999 413417.273199993,5316891.20979999 413417.263499993,5316891.0247 413417.252799994,5316890.83969998 413417.241299994,5316890.65479998 413417.228799994,5316890.46989999 413417.215499995,5316890.28509999 413417.201199995,5316890.1003 413417.186099995,5316889.91559998 413417.17,5316889.731 413417.153,5316889.54649999 413417.135199997,5316889.36199999 413417.116399997,5316889.1777 413417.096799998,5316888.99339997 413417.076199998,5316888.80919998 413417.054799999,5316888.62519998 413417.032399999,5316888.44119999 413417.0092,5316888.25729999 413416.984999975,5316888.0736 413416.96,5316887.89 413406.86,5316889.17 413404.99,5316889.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8M</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>230</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.90">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413382.77,5316930.96 413378.66,5316923.29 413368.75,5316928.53 413370.92,5316932.47 413369.89,5316933.09 413371.84,5316936.77 413382.72,5316930.99 413382.77,5316930.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9y</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.91">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413418.22,5316997.37 413412.63,5317000.49 413414.96,5317004.87 413413.27,5317005.78 413416.9,5317012.6 413424.24,5317008.62 413418.22,5316997.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9I</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.92">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413378.66,5316923.29 413373.34,5316913.3 413364.27,5316918.1 413364.72,5316919.09 413363.17,5316919.93 413364.14,5316921.73 413363.91,5316921.85 413365.19,5316924.22 413363.76,5316924.98 413366.22,5316929.72 413366.28,5316929.84 413367.02,5316929.44 413368.75,5316928.53 413378.66,5316923.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9x</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>146</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.93">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413365.54,5316870.2 413370.18,5316878.96 413373.15,5316879.86 413382.28,5316874.99 413377.65,5316866.14 413374.2,5316867.95 413373.25,5316866.26 413365.54,5316870.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd3</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>141</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.94">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413373.34,5316913.3 413366.59,5316900.67 413364.24,5316899.95 413355.62,5316904.55 413360.29,5316913.29 413361.96,5316912.4 413363.39,5316914.9 413362.79,5316915.3 413364.27,5316918.1 413373.34,5316913.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9w</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>2081</ogr:use_id>
      <ogr:use>Gaststätte, Restaura</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>173</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.95">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413357.35,5316854.83 413361.59,5316862.79 413372.27,5316856.95 413371.07,5316854.8 413373.67,5316853.3 413370.67,5316847.81 413368.63,5316848.89 413357.35,5316854.83</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd1</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>127</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.96">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413342.96,5316877.53 413351.67,5316872.88 413347.44,5316864.94 413336.22,5316870.91 413336.35,5316871.16 413337.49,5316870.56 413339.32,5316873.9 413338.18,5316874.5 413340.07,5316877.95 413341.99,5316876.94 413342.45,5316877.8 413342.96,5316877.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdh</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.97">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413353.12,5316846.9 413357.35,5316854.83 413368.63,5316848.89 413367.67,5316847.08 413371.22,5316845.22 413369.32,5316841.17 413365.82,5316843.02 413364.71,5316840.91 413364.61,5316840.96 413364.56,5316840.86 413363.5,5316841.42 413353.12,5316846.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twd0</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>134</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.98">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413336.22,5316870.91 413347.44,5316864.94 413343.4,5316857.32 413331.68,5316863.45 413331.81,5316863.7 413332.84,5316863.16 413334.72,5316866.93 413333.71,5316867.45 413335.73,5316871.17 413336.22,5316870.91</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdn</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>110</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.99">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413347.95,5316837.19 413353.12,5316846.9 413363.5,5316841.42 413358.35,5316831.7 413347.95,5316837.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcV</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>129</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.100">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413331.68,5316863.45 413343.4,5316857.32 413338.97,5316849.02 413327.26,5316855.03 413329.27,5316858.86 413330.26,5316858.35 413332.57,5316862.67 413331.56,5316863.2 413331.68,5316863.45</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twdp</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>119</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.101">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413342.03,5316826.22 413347.95,5316837.19 413358.25,5316831.74 413358.8,5316831.46 413356.5,5316827.11 413357.92,5316826.37 413354.46,5316819.75 413349.32,5316822.4 413349.23,5316822.23 413347.72,5316821.13 413346.0,5316822.01 413345.9,5316823.95 413345.99,5316824.13 413342.03,5316826.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcT</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>171</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.102">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413357.41,5316808.37 413365.44,5316823.65 413373.84,5316819.24 413370.54,5316812.95 413371.84,5316812.27 413371.97,5316811.82 413370.86,5316809.69 413370.4,5316809.55 413369.11,5316810.22 413365.81,5316803.95 413357.41,5316808.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcQ</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>33 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.103">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413365.81,5316803.95 413362.64,5316797.88 413363.94,5316797.2 413364.06,5316796.69 413363.12,5316794.86 413362.65,5316794.73 413361.34,5316795.41 413358.04,5316789.14 413349.63,5316793.55 413357.41,5316808.37 413365.81,5316803.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcP</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>164</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.104">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413328.3,5316800.3 413334.24,5316811.47 413338.18,5316809.39 413338.27,5316809.57 413339.94,5316810.57 413341.66,5316809.66 413341.73,5316807.73 413341.64,5316807.55 413346.73,5316804.84 413343.25,5316798.3 413341.82,5316799.05 413339.35,5316794.4 413338.96,5316794.61 413328.3,5316800.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcU</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>176</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.105">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413322.66,5316789.72 413327.91,5316799.59 413328.3,5316800.3 413338.96,5316794.61 413337.53,5316791.93 413334.49,5316786.19 413333.53,5316784.4 413322.66,5316789.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcL</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>142</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.106">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413418.22,5316997.37 413412.42,5316986.61 413405.42,5316990.4 413407.82,5316995.17 413409.38,5316994.38 413409.91,5316995.37 413412.63,5317000.49 413418.22,5316997.37</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9H</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>87</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.107">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413316.54,5316778.24 413322.66,5316789.72 413333.53,5316784.4 413331.0,5316779.64 413333.03,5316778.56 413330.79,5316774.34 413328.76,5316775.42 413327.4,5316772.86 413316.54,5316778.24</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcI</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.108">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413403.93,5316942.2 413407.68,5316949.26 413409.29,5316948.41 413418.32,5316943.61 413414.56,5316936.55 413405.54,5316941.34 413403.93,5316942.2</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8R</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.109">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413407.08,5316976.49 413397.47,5316981.6 413397.54,5316981.72 413399.14,5316984.65 413398.19,5316985.18 413398.25,5316985.3 413399.68,5316987.92 413400.64,5316987.39 413403.0,5316991.69 413403.37,5316991.5 413405.14,5316990.56 413405.42,5316990.4 413412.42,5316986.61 413412.51,5316986.64 413407.08,5316976.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9F</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>128</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.110">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.18,5316935.12 413403.93,5316942.2 413405.54,5316941.34 413414.56,5316936.55 413410.8,5316929.48 413401.79,5316934.26 413400.18,5316935.12</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8Q</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.111">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413407.08,5316976.49 413400.95,5316965.02 413388.41,5316971.76 413388.51,5316971.95 413389.01,5316971.68 413394.95,5316982.72 413395.11,5316983.01 413397.54,5316981.72 413397.47,5316981.6 413407.08,5316976.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9A</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>178</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.112">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413396.44,5316928.06 413400.18,5316935.12 413401.79,5316934.26 413410.8,5316929.48 413407.04,5316922.43 413398.05,5316927.2 413396.46,5316928.05 413396.44,5316928.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8P</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.113">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413365.21,5316833.43 413363.08,5316834.57 413366.02,5316840.09 413368.16,5316838.96 413365.21,5316833.43</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcX</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.114">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413372.32,5316827.11 413377.61,5316824.27 413376.43,5316822.04 413371.12,5316824.85 413372.32,5316827.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcZ</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.115">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413366.22,5316929.72 413360.56,5316932.7 413360.62,5316932.82 413363.17,5316937.54 413365.65,5316936.2 413364.25,5316933.81 413368.24,5316931.73 413367.02,5316929.44 413366.28,5316929.84 413366.22,5316929.72</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9z</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>27</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.116">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413334.49,5316786.19 413336.73,5316785.0 413337.15,5316785.79 413337.92,5316785.96 413338.46,5316785.67 413341.31,5316784.16 413339.64,5316781.08 413337.02,5316782.51 413333.53,5316784.4 413334.49,5316786.19</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcM</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>20</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.117">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413346.85,5316750.99 413346.53,5316751.16 413341.4,5316753.85 413343.17,5316756.99 413346.39,5316762.66 413351.76,5316759.78 413346.85,5316750.99</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcH</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>62</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.118">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413374.21,5316799.53 413370.74,5316793.44 413368.68,5316789.83 413365.9,5316791.34 413371.42,5316801.0 413374.21,5316799.53</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcR</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.119">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413371.42,5316801.0 413376.88,5316810.58 413379.66,5316809.08 413374.21,5316799.53 413371.42,5316801.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcS</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.120">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413346.39,5316762.66 413343.17,5316764.38 413344.61,5316767.07 413353.46,5316762.83 413351.76,5316759.78 413346.39,5316762.66</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcK</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>32</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.121">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413398.25,5316985.3 413398.19,5316985.18 413399.14,5316984.65 413397.54,5316981.72 413395.11,5316983.01 413394.95,5316982.72 413392.15,5316984.22 413393.83,5316987.39 413398.25,5316985.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9G</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>21</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.122">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.25,5316967.15 413428.26,5316968.73 413420.3,5316972.99 413427.85,5316987.15 413429.12,5316986.48 413430.9,5316989.85 413432.33,5316989.09 413435.62,5316987.29 413436.26,5316986.94 413440.79,5316984.43 413439.26,5316981.64 413431.25,5316967.15</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8F</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>244</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.123">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413345.18,5316778.08 413341.58,5316780.03 413343.78,5316784.08 413346.0,5316782.89 413345.77,5316782.46 413347.16,5316781.71 413345.18,5316778.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcN</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>18</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.124">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413384.58,5316817.85 413379.49,5316820.54 413384.9,5316830.12 413390.02,5316827.42 413389.4,5316826.2 413384.58,5316817.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcW</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>64</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.125">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413347.16,5316781.71 413350.84,5316788.47 413360.65,5316783.2 413355.0,5316772.77 413345.18,5316778.08 413347.16,5316781.71</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcO</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1131</ogr:use_id>
      <ogr:use>Wohn- und Betriebsge</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>132</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.126">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413368.16,5316838.96 413380.39,5316832.5 413377.48,5316826.91 413374.42,5316828.51 413374.16,5316828.02 413368.31,5316831.15 413368.13,5316831.24 413368.39,5316831.73 413365.21,5316833.43 413368.16,5316838.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcY</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.127">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413355.51,5316766.5 413334.52,5316777.87 413337.02,5316782.51 413339.64,5316781.08 413341.58,5316780.03 413345.18,5316778.08 413355.0,5316772.77 413358.08,5316771.1 413356.27,5316767.86 413355.51,5316766.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwcJ</ogr:gmlid>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>126</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.128">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413519.58,5316955.56 413513.95,5316944.99 413503.89,5316950.61 413498.3,5316953.74 413493.73,5316956.29 413499.54,5316966.68 413499.63,5316966.63 413519.58,5316955.56</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8Y</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>275</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.129">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413545.21,5316898.7 413554.92,5316893.47 413548.11,5316881.18 413538.43,5316886.48 413545.21,5316898.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7F</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>155</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.130">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413492.44,5316904.73 413483.41,5316909.54 413485.58,5316913.5 413484.47,5316914.11 413486.5,5316917.9 413496.6,5316912.5 413492.44,5316904.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw98</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>95</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.131">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413538.43,5316886.48 413548.11,5316881.18 413539.98,5316866.52 413530.32,5316871.88 413538.43,5316886.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7E</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>185</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.132">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413488.29,5316896.96 413478.14,5316902.37 413480.15,5316906.14 413481.27,5316905.54 413483.41,5316909.54 413492.44,5316904.73 413488.29,5316896.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9a</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.133">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.05,5316889.0 413475.02,5316893.82 413470.99,5316895.97 413467.63,5316897.77 413469.51,5316901.3 413474.56,5316898.6 413476.33,5316898.98 413478.14,5316902.37 413488.29,5316896.96 413484.05,5316889.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9d</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>133</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.134">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.05,5316889.0 413479.8,5316881.05 413469.63,5316886.48 413471.68,5316890.28 413472.8,5316889.68 413475.02,5316893.82 413484.05,5316889.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9f</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.135">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413503.03,5316825.92 413475.61,5316840.6 413474.81,5316843.17 413483.74,5316859.88 413485.24,5316859.08 413497.16,5316881.36 413506.56,5316876.33 413495.98,5316856.49 413490.65,5316846.49 413492.83,5316845.32 413502.08,5316840.39 413503.8,5316839.47 413509.05,5316849.31 413514.18,5316846.54 413511.09,5316840.82 413508.74,5316836.48 413503.03,5316825.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7i</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
      <ogr:use_id>1020</ogr:use_id>
      <ogr:use>Wohnheim</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>845</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.136">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413475.56,5316873.11 413466.46,5316877.98 413468.69,5316882.11 413467.59,5316882.71 413469.63,5316886.48 413479.8,5316881.05 413475.56,5316873.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9h</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.137">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.18,5316929.49 413528.39,5316926.73 413532.9,5316924.32 413525.0,5316909.53 413515.26,5316914.74 413523.18,5316929.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7B</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>185</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.138">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413458.87,5316813.22 413463.1,5316821.13 413470.94,5316817.13 413472.42,5316816.37 413471.31,5316814.41 413472.45,5316813.74 413471.75,5316812.57 413470.56,5316813.03 413468.17,5316808.39 413458.87,5316813.22</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbw</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.139">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.31,5316865.16 413461.02,5316870.68 413463.08,5316874.44 413464.18,5316873.83 413464.96,5316875.25 413466.46,5316877.98 413475.56,5316873.11 413471.31,5316865.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9j</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>99</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.140">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413455.04,5316806.05 413458.87,5316813.22 413468.17,5316808.39 413467.51,5316807.21 413468.54,5316806.56 413467.87,5316805.3 413466.81,5316805.91 413464.23,5316801.15 413455.04,5316806.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbu</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>87</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.141">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.31,5316865.16 413467.06,5316857.18 413457.9,5316862.09 413460.08,5316866.22 413458.98,5316866.91 413461.02,5316870.68 413471.31,5316865.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9l</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>100</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.142">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413451.01,5316798.51 413455.04,5316806.05 413464.23,5316801.15 413461.67,5316796.21 413462.82,5316795.62 413462.21,5316794.45 413461.04,5316795.07 413460.27,5316793.58 413451.01,5316798.51</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbs</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.143">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413446.81,5316819.17 413438.41,5316823.74 413438.98,5316824.79 413438.11,5316825.27 413439.64,5316828.14 413441.22,5316831.1 413450.53,5316826.12 413446.81,5316819.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbB</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>82</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.144">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413446.99,5316790.97 413451.01,5316798.51 413460.27,5316793.58 413459.75,5316792.38 413460.82,5316791.74 413460.21,5316790.57 413459.07,5316791.26 413456.0,5316786.16 413446.99,5316790.97</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbq</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.145">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413441.85,5316809.89 413430.51,5316815.96 413431.54,5316817.87 413434.46,5316816.31 413438.41,5316823.74 413446.81,5316819.17 413441.85,5316809.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbD</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.146">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413442.88,5316783.29 413446.99,5316790.97 413456.0,5316786.16 413457.16,5316785.55 413458.34,5316784.92 413455.17,5316779.59 413454.27,5316780.05 413453.83,5316779.17 413452.6,5316779.8 413451.92,5316778.46 413442.88,5316783.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbo</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>106</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.147">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413441.85,5316809.89 413436.89,5316800.61 413426.93,5316805.95 413424.37,5316807.32 413420.93,5316809.16 413420.79,5316809.24 413425.72,5316818.53 413425.73,5316818.52 413430.51,5316815.96 413441.85,5316809.89</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbF</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>192</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.148">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413509.24,5316936.17 413499.1,5316941.57 413503.89,5316950.61 413513.95,5316944.99 413509.24,5316936.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw90</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>116</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.149">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.79,5316775.64 413442.88,5316783.29 413451.92,5316778.46 413451.22,5316777.11 413452.37,5316776.52 413451.79,5316775.41 413450.64,5316775.99 413447.97,5316770.75 413447.1,5316771.21 413438.79,5316775.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbm</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.150">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413436.89,5316800.61 413431.97,5316791.56 413422.24,5316796.73 413421.84,5316796.94 413423.91,5316800.84 413424.08,5316800.75 413426.93,5316805.95 413436.89,5316800.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbH</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>118</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.151">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.79,5316775.64 413447.1,5316771.21 413443.84,5316764.85 413444.8,5316764.37 413444.13,5316763.06 413434.91,5316768.35 413438.79,5316775.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbk</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>81</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.152">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.97,5316791.56 413427.61,5316783.4 413416.64,5316789.26 413417.15,5316790.2 413418.41,5316789.52 413420.85,5316794.12 413417.91,5316795.68 413419.3,5316798.29 413421.84,5316796.94 413422.24,5316796.73 413431.97,5316791.56</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbK</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>113</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.153">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413430.98,5316761.0 413434.91,5316768.35 413444.13,5316763.06 413443.56,5316761.98 413442.56,5316762.52 413440.75,5316759.18 413439.19,5316756.29 413430.98,5316761.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbi</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>80</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.154">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413423.37,5316775.47 413414.77,5316780.01 413413.62,5316780.62 413417.27,5316787.49 413416.04,5316788.14 413416.64,5316789.26 413427.61,5316783.4 413423.37,5316775.47</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbO</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>101</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.155">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413399.28,5316792.92 413398.45,5316793.36 413394.44,5316785.84 413394.12,5316786.02 413392.01,5316782.06 413384.13,5316786.28 413381.61,5316787.63 413384.2,5316792.45 413385.86,5316791.56 413389.18,5316797.77 413387.51,5316798.66 413390.1,5316803.49 413400.55,5316797.89 413400.53,5316797.84 413401.68,5316797.23 413399.28,5316792.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbL</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>208</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.156">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413419.17,5316767.61 413410.63,5316772.14 413411.41,5316773.63 413410.34,5316774.19 413410.9,5316775.26 413411.97,5316774.7 413414.77,5316780.01 413423.37,5316775.47 413419.17,5316767.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbQ</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>88</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.157">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413422.95,5316745.98 413427.0,5316753.56 413436.28,5316748.15 413435.81,5316747.24 413434.84,5316747.74 413431.55,5316741.33 413422.95,5316745.98</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbd</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>83</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.158">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413419.17,5316767.61 413414.87,5316759.57 413406.35,5316764.11 413408.25,5316767.67 413409.26,5316769.57 413408.23,5316770.12 413408.8,5316771.19 413409.83,5316770.64 413410.63,5316772.14 413419.17,5316767.61</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbU</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>89</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.159">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413515.26,5316914.74 413525.0,5316909.53 413518.45,5316897.27 413508.72,5316902.48 413515.26,5316914.74</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7C</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>153</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.160">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413422.95,5316745.98 413431.55,5316741.33 413429.16,5316736.79 413428.28,5316735.08 413429.29,5316734.56 413428.64,5316733.27 413418.93,5316738.45 413422.95,5316745.98</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbb</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>85</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.161">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413414.87,5316759.57 413409.28,5316749.09 413400.03,5316753.95 413399.66,5316754.14 413400.91,5316756.27 413399.86,5316756.8 413401.74,5316760.44 413402.79,5316759.9 413405.28,5316764.68 413406.35,5316764.11 413414.87,5316759.57</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbZ</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>134</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.162">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413414.93,5316730.95 413418.93,5316738.45 413428.64,5316733.27 413428.02,5316732.09 413426.86,5316732.69 413426.02,5316731.08 413423.65,5316726.48 413414.93,5316730.95</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twba</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>84</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.163">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413405.02,5316741.11 413394.74,5316746.59 413395.33,5316747.7 413396.35,5316747.16 413400.03,5316753.95 413409.28,5316749.09 413405.02,5316741.11</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc1</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.164">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413410.88,5316723.38 413414.93,5316730.95 413423.65,5316726.48 413424.71,5316725.94 413424.06,5316724.68 413422.8,5316725.33 413419.52,5316718.95 413410.88,5316723.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb9</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>85</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.165">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.74,5316733.1 413392.32,5316737.63 413391.38,5316738.14 413395.17,5316744.93 413394.15,5316745.46 413394.74,5316746.59 413405.02,5316741.11 413400.74,5316733.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc3</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.166">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413410.88,5316723.38 413419.52,5316718.95 413416.22,5316712.52 413417.15,5316712.05 413417.61,5316712.95 413418.5,5316712.5 413418.91,5316713.31 413420.2,5316712.65 413418.4,5316709.8 413416.53,5316710.85 413406.9,5316715.92 413410.88,5316723.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb7</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>91</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.167">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413400.74,5316733.1 413396.02,5316724.27 413387.45,5316728.72 413392.32,5316737.63 413400.74,5316733.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc5</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.168">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413406.9,5316715.92 413416.53,5316710.85 413418.4,5316709.8 413416.21,5316706.34 413413.99,5316707.55 413413.22,5316706.13 413414.2,5316705.6 413412.76,5316702.99 413402.83,5316708.29 413406.9,5316715.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb6</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.169">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413391.39,5316715.59 413382.17,5316720.54 413381.68,5316720.8 413382.14,5316721.62 413383.25,5316721.01 413385.83,5316725.75 413387.45,5316728.72 413396.02,5316724.27 413391.39,5316715.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc9</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.170">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413509.24,5316936.17 413505.0,5316928.24 413494.88,5316933.63 413497.28,5316938.13 413499.1,5316941.57 413509.24,5316936.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw92</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.171">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413398.75,5316700.67 413402.83,5316708.29 413412.76,5316702.99 413413.72,5316702.47 413411.76,5316698.9 413410.89,5316699.38 413408.69,5316695.33 413398.75,5316700.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb5</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.172">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413391.39,5316715.59 413387.06,5316707.48 413377.7,5316712.54 413376.68,5316713.09 413377.13,5316713.92 413378.23,5316713.32 413379.5,5316715.65 413382.17,5316720.54 413391.39,5316715.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcb</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>98</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.173">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413370.91,5316726.57 413369.85,5316724.6 413368.42,5316725.37 413366.7,5316722.07 413351.52,5316730.22 413353.39,5316733.7 413357.49,5316731.5 413358.43,5316733.27 413370.91,5316726.57</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcc</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>51 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>97</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.174">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413394.68,5316693.04 413398.75,5316700.67 413408.69,5316695.33 413406.53,5316691.36 413407.44,5316690.86 413405.48,5316687.24 413404.52,5316687.76 413394.68,5316693.04</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twb3</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>52</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.175">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413382.95,5316699.77 413373.61,5316704.77 413369.82,5316706.79 413368.19,5316707.66 413369.84,5316710.76 413371.28,5316710.01 413372.35,5316709.45 413375.26,5316707.91 413377.7,5316712.54 413387.06,5316707.48 413382.95,5316699.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twcd</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>53</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>115</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.176">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413508.72,5316902.48 413518.45,5316897.27 413510.55,5316882.48 413500.83,5316887.69 413508.72,5316902.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7D</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>185</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.177">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413500.74,5316920.26 413491.75,5316925.04 413490.64,5316925.64 413493.1,5316930.27 413494.88,5316933.63 413505.0,5316928.24 413500.74,5316920.26</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw94</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>104</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.178">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413545.21,5316898.7 413553.35,5316913.42 413563.15,5316908.32 413554.92,5316893.47 413545.21,5316898.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7G</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>187</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.179">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413496.6,5316912.5 413486.5,5316917.9 413488.33,5316921.32 413488.47,5316921.58 413489.59,5316920.98 413491.75,5316925.04 413500.74,5316920.26 413496.6,5316912.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw97</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>95</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.180">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413455.57,5316883.8 413457.29,5316887.02 413464.34,5316883.25 413462.63,5316880.03 413461.6,5316880.58 413460.29,5316881.28 413455.57,5316883.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9g</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.181">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413429.16,5316736.79 413431.11,5316735.8 413433.42,5316740.31 413434.88,5316739.52 413436.83,5316738.46 413432.29,5316731.27 413428.64,5316733.27 413429.29,5316734.56 413428.28,5316735.08 413429.16,5316736.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbc</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.182">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.23,5316939.29 413487.27,5316937.67 413482.84,5316929.79 413479.82,5316931.4 413484.23,5316939.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw93</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.183">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.87,5316846.93 413521.53,5316842.57 413516.77,5316845.14 413514.47,5316846.38 413516.82,5316850.73 413523.87,5316846.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7k</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>40</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.184">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.5,5316804.8 413480.91,5316807.21 413480.62,5316806.86 413476.92,5316809.45 413478.69,5316812.09 413486.81,5316806.54 413488.59,5316805.32 413487.53,5316803.72 413484.94,5316805.46 413484.5,5316804.8</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twby</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>33</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.185">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413402.12,5316776.65 413394.03,5316780.98 413396.95,5316786.15 413404.99,5316781.73 413402.12,5316776.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbM</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>54</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.186">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413399.28,5316792.92 413401.68,5316797.23 413406.66,5316794.56 413404.21,5316790.17 413399.23,5316792.83 413399.28,5316792.92</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbN</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>28</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.187">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413402.65,5316766.08 413399.88,5316760.87 413394.66,5316763.67 413397.42,5316768.86 413402.65,5316766.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbY</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.188">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413356.27,5316767.86 413358.08,5316771.1 413358.34,5316771.56 413362.23,5316769.37 413365.4,5316775.35 413361.66,5316777.48 413363.83,5316781.33 413366.2,5316785.49 413368.98,5316784.03 413378.95,5316778.71 413380.64,5316777.81 413379.41,5316775.52 413379.51,5316775.46 413372.47,5316762.35 413372.37,5316762.41 413370.92,5316759.71 413356.27,5316767.86</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbV</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>3044</ogr:use_id>
      <ogr:use>Gemeindehaus, Küster</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>310</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.189">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413366.95,5316751.09 413356.96,5316756.79 413359.74,5316761.63 413354.44,5316764.58 413355.51,5316766.5 413356.27,5316767.86 413370.92,5316759.71 413372.46,5316758.85 413367.76,5316750.63 413366.95,5316751.09</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbW</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>3044</ogr:use_id>
      <ogr:use>Gemeindehaus, Küster</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>140</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.190">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413488.33,5316921.32 413484.43,5316923.75 413485.14,5316925.23 413483.52,5316926.1 413484.91,5316928.69 413488.83,5316926.6 413490.64,5316925.64 413491.75,5316925.04 413489.59,5316920.98 413488.47,5316921.58 413488.33,5316921.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw96</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>33</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.191">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413388.86,5316773.42 413380.45,5316757.64 413382.88,5316756.34 413376.05,5316743.57 413365.85,5316749.03 413366.95,5316751.09 413367.76,5316750.63 413372.46,5316758.85 413370.92,5316759.71 413372.37,5316762.41 413372.47,5316762.35 413379.51,5316775.46 413379.41,5316775.52 413380.64,5316777.81 413388.86,5316773.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbX</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>3065</ogr:use_id>
      <ogr:use>Kindergarten, Kinder</ogr:use>
      <ogr:class>Öffentlich</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>324</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.192">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413456.05,5316873.34 413459.18,5316871.66 413456.49,5316866.71 413456.0,5316868.33 413454.36,5316867.7 413453.0,5316872.09 413450.13,5316873.61 413451.33,5316875.86 413456.05,5316873.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9k</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>34</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.193">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413470.99,5316895.97 413469.46,5316893.09 413462.52,5316896.8 413464.05,5316899.68 413467.63,5316897.77 413470.99,5316895.97</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9e</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>26</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.194">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.87,5316846.93 413528.46,5316844.45 413526.11,5316840.1 413522.96,5316841.8 413521.53,5316842.57 413523.87,5316846.93</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7l</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>26</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.195">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413470.1,5316798.01 413469.57,5316798.3 413473.46,5316805.05 413476.26,5316803.03 413473.53,5316798.38 413473.7,5316798.28 413472.73,5316796.61 413470.1,5316798.01</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbv</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>26</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.196">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413465.96,5316790.54 413470.1,5316798.01 413472.73,5316796.61 413468.43,5316789.23 413465.96,5316790.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbt</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.197">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413465.96,5316790.54 413468.43,5316789.23 413464.14,5316781.82 413461.58,5316783.18 413465.96,5316790.54</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbr</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.198">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413418.88,5316816.17 413416.5,5316811.69 413409.38,5316815.47 413411.8,5316820.04 413418.88,5316816.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbG</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>41</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.199">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413452.63,5316768.27 413456.97,5316775.77 413459.56,5316774.39 413454.92,5316767.05 413452.63,5316768.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbn</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>24</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.200">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413416.07,5316800.0 413410.28,5316803.09 413410.23,5316803.12 413412.03,5316806.5 413417.87,5316803.39 413416.07,5316800.0</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbI</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.201">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413448.14,5316760.77 413452.63,5316768.27 413454.92,5316767.05 413450.2,5316759.59 413448.14,5316760.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbl</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>22</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.202">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413438.96,5316746.58 413440.6,5316748.88 413442.59,5316747.57 413441.16,5316745.3 413438.96,5316746.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbg</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>7</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.203">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413441.16,5316745.3 413436.83,5316738.46 413434.88,5316739.52 413438.96,5316746.58 413441.16,5316745.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbe</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>19</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.204">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413395.7,5316774.38 413396.4,5316775.68 413404.31,5316771.45 413403.61,5316770.15 413395.7,5316774.38</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbS</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>13</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.205">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413402.65,5316766.08 413397.42,5316768.86 413392.23,5316771.65 413392.87,5316772.81 413393.92,5316772.23 413395.24,5316774.62 413395.7,5316774.38 413403.61,5316770.15 413408.25,5316767.67 413406.35,5316764.11 413405.28,5316764.68 413402.65,5316766.08</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbT</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>61</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.206">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413391.4,5316758.48 413394.12,5316757.05 413389.89,5316749.17 413387.17,5316750.62 413391.4,5316758.48</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc0</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>27</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.207">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413389.89,5316749.17 413385.42,5316741.34 413382.68,5316742.82 413387.17,5316750.62 413389.89,5316749.17</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc2</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>28</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.208">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413383.66,5316726.96 413374.69,5316731.62 413376.22,5316734.58 413385.17,5316729.91 413383.66,5316726.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc7</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>34</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.209">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413385.83,5316725.75 413383.66,5316726.96 413385.17,5316729.91 413387.45,5316728.72 413385.83,5316725.75</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc8</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>8</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.210">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413368.36,5316717.58 413369.63,5316719.97 413376.17,5316716.49 413374.92,5316714.04 413368.36,5316717.58</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twca</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>20</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.211">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413493.24,5316944.69 413488.62,5316947.15 413489.61,5316948.92 413493.64,5316956.12 413493.73,5316956.29 413498.3,5316953.74 413493.24,5316944.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8Z</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>55</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.212">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413493.24,5316944.69 413496.95,5316942.71 413495.11,5316939.26 413490.48,5316941.67 413486.66,5316943.65 413488.62,5316947.15 413493.24,5316944.69</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw91</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>38</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.213">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413480.77,5316920.96 413480.99,5316920.84 413479.05,5316917.2 413477.59,5316917.95 413477.0,5316916.79 413473.45,5316918.6 413475.97,5316923.53 413480.77,5316920.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw99</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>29</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.214">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413442.11,5316754.62 413439.19,5316756.29 413440.75,5316759.18 413441.75,5316758.64 413442.04,5316759.17 413444.77,5316757.14 413446.87,5316759.81 413447.38,5316759.5 413448.14,5316760.77 413450.2,5316759.59 413445.74,5316752.54 413442.11,5316754.62</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbj</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2020</ogr:use_id>
      <ogr:use>Verwaltungs-, Büroge</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>41</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.215">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413483.58,5316797.77 413476.26,5316803.03 413473.46,5316805.05 413474.86,5316807.22 413477.31,5316805.54 413482.46,5316801.98 413485.09,5316800.05 413483.58,5316797.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbx</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>34</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.216">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413482.46,5316801.98 413483.06,5316802.81 413484.5,5316804.8 413484.94,5316805.46 413487.53,5316803.72 413485.09,5316800.05 413482.46,5316801.98</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbz</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>14</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.217">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413358.43,5316733.27 413360.85,5316737.77 413375.41,5316729.97 413373.0,5316725.45 413370.91,5316726.57 413358.43,5316733.27</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc6</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>85</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.218">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413469.51,5316901.3 413467.63,5316897.77 413464.05,5316899.68 413465.78,5316903.29 413469.51,5316901.3</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9c</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1122</ogr:use_id>
      <ogr:use>Wohn- und Bürogebäud</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>17</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.219">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413392.01,5316782.06 413393.36,5316781.34 413388.86,5316773.42 413380.64,5316777.81 413378.95,5316778.71 413382.46,5316785.32 413383.36,5316784.84 413384.13,5316786.28 413392.01,5316782.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbR</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.220">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.96,5316915.67 413477.08,5316912.94 413472.92,5316905.17 413472.89,5316905.13 413467.95,5316907.84 413471.96,5316915.67</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9b</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>51</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.221">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413540.09,5316851.39 413529.95,5316856.83 413532.74,5316862.14 413542.88,5316856.72 413540.09,5316851.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7m</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>69</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.222">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413456.05,5316873.34 413451.33,5316875.86 413455.57,5316883.8 413460.29,5316881.28 413456.05,5316873.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9i</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>48</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.223">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413439.64,5316828.14 413435.82,5316830.18 413433.67,5316826.14 413430.92,5316827.68 413430.85,5316827.72 413430.91,5316827.82 413434.69,5316834.59 413440.55,5316831.46 413441.22,5316831.1 413439.64,5316828.14</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbC</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>39</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.224">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413431.54,5316817.87 413430.51,5316815.96 413425.73,5316818.52 413428.67,5316823.57 413433.24,5316821.05 413431.54,5316817.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbE</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>31</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.225">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413461.58,5316783.18 413464.14,5316781.82 413459.56,5316774.39 413456.97,5316775.77 413461.58,5316783.18</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twbp</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>25</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.226">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413424.37,5316807.32 413419.3,5316798.29 413416.07,5316800.0 413417.87,5316803.39 413420.93,5316809.16 413424.37,5316807.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbJ</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>39</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.227">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413404.99,5316781.73 413406.49,5316784.38 413409.06,5316783.03 413407.86,5316780.75 413409.3,5316779.99 413406.27,5316774.45 413402.12,5316776.65 413404.99,5316781.73</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007TwbP</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>37</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.228">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413385.42,5316741.34 413389.29,5316739.26 413386.25,5316733.7 413379.53,5316737.37 413382.68,5316742.82 413385.42,5316741.34</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Twc4</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>48</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.229">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413480.77,5316920.96 413475.97,5316923.53 413478.22,5316927.93 413477.96,5316928.07 413478.86,5316929.68 413479.82,5316931.4 413482.84,5316929.79 413484.91,5316928.69 413483.52,5316926.1 413480.77,5316920.96</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw95</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>49</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.230">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413495.98,5316856.49 413509.12,5316849.44 413509.05,5316849.31 413503.8,5316839.47 413502.08,5316840.39 413504.24,5316844.46 413495.0,5316849.37 413492.83,5316845.32 413490.65,5316846.49 413495.98,5316856.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7j</ogr:gmlid>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1020</ogr:use_id>
      <ogr:use>Wohnheim</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>121</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.231">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413584.05,5317022.41 413545.56,5316975.73 413537.32,5316982.58 413540.43,5316986.38 413541.08,5316985.87 413542.67,5316984.63 413557.49,5317002.84 413555.93,5317004.12 413555.45,5317004.51 413557.68,5317007.18 413558.04,5317006.88 413559.68,5317005.52 413574.79,5317023.75 413573.15,5317025.08 413572.81,5317025.38 413575.68,5317029.07 413584.05,5317022.41</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7g</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>518</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.232">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413553.16,5316959.78 413556.47,5316967.7 413556.85,5316967.55 413562.49,5316974.38 413569.58,5316968.56 413565.73,5316963.94 413567.85,5316963.08 413570.83,5316961.86 413571.09,5316961.75 413566.97,5316951.66 413566.67,5316951.79 413563.7,5316953.02 413555.43,5316956.46 413552.31,5316957.75 413553.16,5316959.78</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw79</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>240</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.233">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413573.77,5316988.68 413581.96,5316981.93 413570.35,5316967.93 413569.58,5316968.56 413562.49,5316974.38 413562.19,5316974.63 413573.77,5316988.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7a</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>193</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.234">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413573.77,5316988.68 413582.71,5316999.51 413584.91,5316997.7 413592.36,5316991.56 413592.39,5316991.53 413589.48,5316988.14 413589.31,5316988.27 413587.99,5316989.28 413581.96,5316981.93 413573.77,5316988.68</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7b</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>157</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.235">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413584.91,5316997.7 413610.28,5317028.52 413617.7,5317022.15 413592.36,5316991.56 413584.91,5316997.7</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw72</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>387</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.236">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413586.28,5316968.06 413580.23,5316971.6 413581.41,5316973.62 413588.19,5316977.29 413590.98,5316975.62 413587.55,5316969.86 413587.44,5316969.93 413586.28,5316968.06</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7c</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>51</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.237">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413595.41,5316983.07 413594.84,5316982.12 413590.79,5316985.28 413588.88,5316987.62 413589.31,5316988.27 413589.48,5316988.14 413592.39,5316991.53 413598.96,5316989.03 413595.41,5316983.07</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7d</ogr:gmlid>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>51</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.238">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413559.94,5316936.94 413562.21,5316936.12 413570.27,5316933.26 413575.57,5316931.35 413572.1,5316920.89 413555.95,5316926.61 413559.94,5316936.94</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7x</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.239">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413544.68,5316943.36 413549.11,5316941.49 413556.87,5316938.22 413559.94,5316936.94 413555.95,5316926.61 413540.19,5316933.26 413544.68,5316943.36</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7y</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.240">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413581.83,5316956.9 413586.96,5316955.56 413586.8,5316954.96 413586.3,5316953.03 413609.09,5316947.09 413609.58,5316948.99 413609.53,5316949.0 413609.7,5316949.63 413612.69,5316948.85 413612.54,5316948.22 413612.48,5316948.23 413611.98,5316946.34 413634.82,5316940.38 413635.32,5316942.3 413635.48,5316942.91 413640.22,5316941.67 413637.57,5316931.64 413578.47,5316946.99 413581.83,5316956.9</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw71</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>509</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.241">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413528.16,5316938.87 413533.32,5316948.58 413541.36,5316944.92 413544.68,5316943.36 413540.19,5316933.26 413533.51,5316936.38 413528.16,5316938.87</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7A</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>142</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.242">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413566.97,5316951.66 413571.09,5316961.75 413577.59,5316959.03 413581.94,5316957.22 413581.83,5316956.9 413578.47,5316946.99 413578.43,5316946.88 413566.97,5316951.66</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw77</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>132</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.243">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413485.65,5316974.39 413496.35,5316968.45 413499.36,5316966.78 413499.45,5316966.73 413499.54,5316966.68 413493.73,5316956.29 413493.64,5316956.12 413493.44,5316956.22 413490.46,5316957.78 413479.6,5316963.44 413485.65,5316974.39</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8X</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17 a</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>195</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.244">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413465.27,5316955.64 413472.72,5316951.51 413473.23,5316952.42 413475.99,5316950.88 413475.49,5316949.98 413482.86,5316945.88 413477.99,5316937.08 413471.41,5316940.7 413470.34,5316939.62 413466.67,5316941.66 413467.03,5316943.14 413460.47,5316946.81 413461.57,5316948.77 413465.27,5316955.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8W</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17 b</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>212</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.245">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413469.01,5316983.63 413475.31,5316980.13 413477.59,5316978.86 413477.84,5316978.72 413471.62,5316967.25 413471.48,5316967.33 413469.12,5316968.65 413468.21,5316969.16 413468.95,5316970.55 413463.61,5316973.59 413469.01,5316983.63</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8T</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>122</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.246">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413469.01,5316983.63 413463.61,5316973.59 413441.54,5316985.79 413441.31,5316985.92 413446.74,5316995.99 413469.01,5316983.63</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8G</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>291</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.247">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413485.02,5316991.21 413490.5,5317000.88 413494.48,5316998.85 413498.94,5316996.4 413493.42,5316986.55 413485.02,5316991.21</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7Y</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>108</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.248">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413485.02,5316991.21 413476.67,5316995.95 413482.1,5317005.63 413490.5,5317000.88 413485.02,5316991.21</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw81</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.249">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413432.33,5316989.09 413433.32,5316991.0 413430.67,5316992.44 413435.37,5317000.89 413435.99,5317001.95 413437.13,5317001.32 413446.74,5316995.99 413441.31,5316985.92 413437.01,5316988.29 413436.26,5316986.94 413435.62,5316987.29 413432.33,5316989.09</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8D</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>147</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.250">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413476.62,5316995.86 413469.08,5317000.05 413474.52,5317009.07 413474.32,5317009.19 413474.53,5317009.55 413481.09,5317006.19 413481.85,5317007.54 413482.86,5317006.97 413482.1,5317005.63 413476.67,5316995.95 413476.62,5316995.86</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw82</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>96</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.251">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413416.92,5317012.64 413416.9,5317012.6 413413.27,5317005.78 413411.04,5317006.99 413409.72,5317004.57 413408.1,5317005.46 413407.26,5317003.91 413402.36,5317006.59 413403.2,5317008.14 413399.03,5317010.4 413404.02,5317019.72 413416.92,5317012.64</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9J</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>158</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.252">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413469.08,5317000.05 413461.22,5317004.42 413467.15,5317014.86 413471.07,5317012.7 413470.33,5317011.46 413471.75,5317010.65 413474.32,5317009.19 413474.52,5317009.07 413469.08,5317000.05</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw85</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>103</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.253">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413453.34,5317008.79 413458.45,5317018.05 413462.57,5317015.89 413463.13,5317016.96 413467.15,5317014.86 413461.22,5317004.42 413453.34,5317008.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw88</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.254">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413463.25,5317026.77 413465.91,5317031.59 413466.79,5317033.18 413474.85,5317028.42 413471.39,5317022.33 413463.25,5317026.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw89</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>32 a</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>67</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.255">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413445.46,5317013.16 413451.22,5317023.58 413455.09,5317021.27 413454.52,5317020.24 413458.45,5317018.05 413453.34,5317008.79 413445.46,5317013.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8a</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>101</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.256">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413445.46,5317013.16 413437.67,5317017.65 413443.52,5317028.17 413443.61,5317028.33 413447.85,5317025.95 413447.69,5317025.67 413451.22,5317023.58 413445.46,5317013.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8d</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>109</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.257">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413436.05,5317032.29 413441.06,5317029.53 413443.27,5317028.31 413443.52,5317028.17 413437.67,5317017.65 413437.42,5317017.79 413435.27,5317018.97 413430.24,5317021.76 413436.05,5317032.29</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8f</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>102</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.258">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413430.24,5317021.76 413417.91,5317028.55 413423.83,5317039.06 413424.88,5317038.48 413428.14,5317036.67 413436.05,5317032.29 413430.24,5317021.76</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8h</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>169</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.259">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413591.42,5316926.42 413600.68,5316923.82 413598.03,5316915.44 413597.46,5316913.65 413595.79,5316914.08 413595.77,5316914.05 413592.38,5316914.92 413592.0,5316913.48 413588.15,5316914.49 413588.24,5316914.86 413588.49,5316915.78 413591.42,5316926.42</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7U</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>107</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.260">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413573.72,5316964.78 413578.37,5316972.68 413580.23,5316971.6 413586.28,5316968.06 413584.17,5316964.18 413584.05,5316964.25 413584.53,5316965.13 413582.14,5316966.57 413581.62,5316965.71 413579.15,5316961.62 413573.72,5316964.78</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw78</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2112</ogr:use_id>
      <ogr:use>Betrieb</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>68</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.261">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413580.05,5316929.94 413591.42,5316926.42 413588.49,5316915.78 413576.79,5316919.43 413572.3,5316920.83 413572.1,5316920.89 413575.57,5316931.35 413575.82,5316931.27 413580.05,5316929.94</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7w</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
      <ogr:use_id>1123</ogr:use_id>
      <ogr:use>Wohn- und Geschäftsg</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>186</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.262">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413568.89,5316886.28 413563.79,5316878.12 413559.01,5316870.48 413556.52,5316866.51 413552.24,5316859.67 413552.17,5316859.56 413547.25,5316862.23 413572.97,5316903.23 413577.63,5316900.23 413573.83,5316894.17 413568.89,5316886.28</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7H</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>268</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.263">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413572.97,5316903.23 413577.09,5316909.8 413581.72,5316906.74 413578.78,5316902.06 413577.63,5316900.23 413572.97,5316903.23</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7v</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2463</ogr:use_id>
      <ogr:use>Garage</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>43</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.264">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413523.18,5316929.49 413523.22,5316929.57 413524.44,5316931.85 413528.16,5316938.87 413533.51,5316936.38 413529.65,5316929.1 413528.43,5316926.8 413528.39,5316926.73 413523.18,5316929.49</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7z</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>63</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.265">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413441.54,5316985.79 413463.61,5316973.59 413452.77,5316953.48 413448.69,5316945.89 413439.87,5316950.61 413439.74,5316950.27 413433.99,5316953.26 413436.57,5316957.95 413433.37,5316959.71 413433.09,5316959.22 413431.76,5316959.95 413431.67,5316959.83 413428.22,5316961.65 413431.25,5316967.15 413439.26,5316981.64 413440.79,5316984.43 413441.54,5316985.79</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8H</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>764</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.266">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.75,5317010.65 413475.5,5317017.16 413478.15,5317015.67 413477.46,5317014.5 413474.53,5317009.55 413474.32,5317009.19 413471.75,5317010.65</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw86</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2050</ogr:use_id>
      <ogr:use>Handel, Geschäft</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>23</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.267">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413463.61,5316973.59 413468.95,5316970.55 413468.21,5316969.16 413466.74,5316966.39 413470.16,5316964.64 413468.03,5316960.82 413465.27,5316955.64 413461.57,5316948.77 413452.77,5316953.48 413463.61,5316973.59</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8S</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2140</ogr:use_id>
      <ogr:use>Lager</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>209</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.268">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413484.62,5317010.1 413481.12,5317011.99 413481.28,5317012.26 413482.93,5317015.2 413486.4,5317013.28 413484.62,5317010.1</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw83</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.269">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413477.46,5317014.5 413478.15,5317015.67 413479.12,5317017.31 413482.93,5317015.2 413481.28,5317012.26 413477.46,5317014.5</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw84</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>15</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.270">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413462.96,5317035.44 413461.38,5317032.76 413457.52,5317034.99 413459.04,5317037.75 413462.96,5317035.44</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8b</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>14</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.271">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413583.05,5316908.85 413584.38,5316910.96 413590.97,5316907.21 413590.88,5316907.07 413589.71,5316905.41 413587.14,5316906.74 413583.05,5316908.85</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7T</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2723</ogr:use_id>
      <ogr:use>Schuppen</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>18</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.272">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413468.03,5316960.82 413482.87,5316952.66 413480.42,5316947.99 413484.11,5316945.88 413483.13,5316944.13 413486.04,5316942.55 413478.8,5316929.92 413457.31,5316941.04 413460.47,5316946.81 413467.03,5316943.14 413466.67,5316941.66 413470.34,5316939.62 413471.41,5316940.7 413477.99,5316937.08 413482.86,5316945.88 413475.49,5316949.98 413475.99,5316950.88 413473.23,5316952.42 413472.72,5316951.51 413465.27,5316955.64 413468.03,5316960.82</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8V</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2465</ogr:use_id>
      <ogr:use>Tiefgarage</ogr:use>
      <ogr:class>Unterirdisch</ogr:class>
      <ogr:quality>Katastervermessung</ogr:quality>
      <ogr:area>282</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.273">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413495.1,5317008.45 413498.76,5317006.42 413497.11,5317003.5 413493.22,5317005.71 413493.11,5317005.76 413493.01,5317005.82 413494.68,5317008.68 413494.78,5317008.62 413495.1,5317008.45</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw80</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 2 &gt;1:2000</ogr:quality>
      <ogr:area>16</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.274">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413463.25,5317026.77 413460.79,5317022.31 413457.93,5317023.88 413463.05,5317033.17 413465.91,5317031.59 413463.25,5317026.77</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw8c</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>2120</ogr:use_id>
      <ogr:use>Werkstatt</ogr:use>
      <ogr:class>Sonstige</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>35</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.275">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413502.55,5317004.32 413503.18,5317003.97 413498.94,5316996.4 413494.48,5316998.85 413497.11,5317003.5 413498.76,5317006.42 413502.55,5317004.32</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw7Z</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>44</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.276">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413396.98,5317002.16 413406.51,5316997.13 413403.37,5316991.5 413403.0,5316991.69 413392.04,5316997.51 413393.3,5316999.85 413393.11,5316999.95 413394.89,5317003.26 413395.18,5317003.1 413396.98,5317002.16</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw9L</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>84</ogr:area>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="building.277">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:25832"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413471.39,5317022.33 413474.85,5317028.42 413481.76,5317024.34 413482.88,5317023.68 413479.12,5317017.31 413478.15,5317015.67 413475.5,5317017.16 413474.75,5317017.58 413475.88,5317019.72 413471.39,5317022.33</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:gmlid>DEBWL0010007Tw87</ogr:gmlid>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>None</ogr:house_nr>
      <ogr:use_id>1010</ogr:use_id>
      <ogr:use>Wohnhaus</ogr:use>
      <ogr:class>Wohnen</ogr:class>
      <ogr:quality>Stufe 1 &gt;1:1000</ogr:quality>
      <ogr:area>74</ogr:area>
    </ogr:building>
  </gml:featureMember>
</ogr:FeatureCollection>
