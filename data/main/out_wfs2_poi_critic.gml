<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ wfs_pois_data_AsuX4Q.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413378.826728</gml:X><gml:Y>5316773.256702</gml:Y></gml:coord>
      <gml:coord><gml:X>413442.917391</gml:X><gml:Y>5316882.056501</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                              
  <gml:featureMember>
    <ogr:pois fid="578">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413378.826728,5316773.256702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.578</ogr:gml_id>
      <ogr:ogc_fid>29</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:name>St. Klara</ogr:name>
      <ogr:bezeichnung>Kindergarten</ogr:bezeichnung>
      <ogr:kategorie>VÖ</ogr:kategorie>
      <ogr:adresse>Klarastraße 41  79106 Freiburg</ogr:adresse>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:telefon>0761/273100</ogr:telefon>
      <ogr:organisation>katholische Kirche</ogr:organisation>
      <ogr:stadtbezirk>Alt-Stühlinger</ogr:stadtbezirk>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1213">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413442.917391,5316882.056501</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1213</ogr:gml_id>
      <ogr:ogc_fid>117</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:name>Klara-Kinder, Egonstraße</ogr:name>
      <ogr:bezeichnung>Krippe</ogr:bezeichnung>
      <ogr:kategorie>VÖ, Nachmittag</ogr:kategorie>
      <ogr:adresse>Egonstraße 21  79106 Freiburg</ogr:adresse>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:telefon>01795556659</ogr:telefon>
      <ogr:organisation>Die Klara Kinder GbR</ogr:organisation>
      <ogr:ansprechpartner>Frau Reich-Disch</ogr:ansprechpartner>
    </ogr:pois>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:pois fid="1491">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413384.088871,5316851.226149</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:gml_id>pois.1491</ogr:gml_id>
      <ogr:ogc_fid>179</ogr:ogc_fid>
      <ogr:prioritaet>0</ogr:prioritaet>
      <ogr:drehwinkel>0</ogr:drehwinkel>
      <ogr:poityp>kita</ogr:poityp>
      <ogr:name>Klara-Kinder, Guntramstraße</ogr:name>
      <ogr:bezeichnung>Krippe</ogr:bezeichnung>
      <ogr:kategorie>GT</ogr:kategorie>
      <ogr:adresse>Guntramstraße 21  79106 Freiburg</ogr:adresse>
      <ogr:stadtteil>Stühlinger</ogr:stadtteil>
      <ogr:telefon>01795556659</ogr:telefon>
      <ogr:organisation>Die Klara Kinder GbR</ogr:organisation>
      <ogr:ansprechpartner>Frau Reich-Disch</ogr:ansprechpartner>
    </ogr:pois>
  </gml:featureMember>
</ogr:FeatureCollection>
