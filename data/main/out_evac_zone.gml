<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ evac_zone_f4_floatgl_.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413331.833396789</gml:X><gml:Y>5316695.20279075</gml:Y></gml:coord>
      <gml:coord><gml:X>413600.772653543</gml:X><gml:Y>5317044.47371799</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                    
  <gml:featureMember>
    <ogr:evac_zone fid="evac_zone.0">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:25832"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>413435.029469541,5316695.20279075 413388.72697642,5316702.1367024 413386.969342195,5316703.09101992 413331.833396789,5316779.68101616 413347.788043725,5316907.56553141 413348.742361246,5316909.32316564 413411.977975136,5317019.50146267 413419.787672429,5317025.50218164 413421.620807063,5317026.7826571 413573.462917048,5317044.47371799 413586.645173738,5317037.31633658 413587.046832091,5317035.96036071 413600.772653543,5316912.44274324 413575.377519591,5316796.51135331 413573.46888455,5316792.99608486 413511.543965375,5316748.10394317 413507.877696108,5316745.54299224 413506.044561475,5316744.26251678 413435.029469541,5316695.20279075</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:corr_buff>1.4142135623731</ogr:corr_buff>
    </ogr:evac_zone>
  </gml:featureMember>
</ogr:FeatureCollection>
