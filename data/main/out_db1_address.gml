<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ db_address_data_NhOBFk.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>413333.77</gml:X><gml:Y>5316703.45</gml:Y></gml:coord>
      <gml:coord><gml:X>413597.76</gml:X><gml:Y>5317031.24</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                                              
  <gml:featureMember>
    <ogr:address fid="address.0">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413519.0,5316997.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.1">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413534.28,5317024.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.2">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413515.39,5317031.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.3">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413504.2,5317013.93</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Agnesenstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.4">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413494.3,5316812.0</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.5">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413487.26,5316816.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.6">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413457.27,5316852.98</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.7">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413475.21,5316822.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.8">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413447.62,5316858.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.9">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413447.02,5316839.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.10">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413441.73,5316887.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.11">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413435.13,5316845.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.12">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413454.92,5316908.27</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.13">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413527.39,5316795.7</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.14">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413427.13,5316849.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.15">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413432.31,5316868.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.16">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413420.04,5316853.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.17">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413425.87,5316871.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.18">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413411.24,5316856.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.19">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413416.49,5316877.06</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.20">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413402.74,5316861.99</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413408.95,5316881.13</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413392.65,5316866.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413537.68,5316811.0</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413385.25,5316871.49</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413351.4,5316909.27</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413346.65,5316892.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413519.11,5316800.07</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413518.89,5316820.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413511.19,5316804.15</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413509.3,5316825.79</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413503.29,5316808.65</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Egonstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413426.23,5316978.94</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413396.44,5316963.05</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413396.45,5316921.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413391.85,5316954.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413403.79,5316910.53</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413387.75,5316946.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413407.84,5316900.68</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413383.45,5316934.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413409.24,5316892.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413379.35,5316927.37</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413419.63,5317003.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413374.65,5316918.67</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413370.36,5316874.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413366.36,5316904.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413365.56,5316865.69</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413353.47,5316879.31</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413361.66,5316857.89</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413348.46,5316869.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413356.25,5316850.26</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413342.77,5316862.79</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413352.76,5316840.9</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413428.23,5316932.66</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413339.91,5316854.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413346.44,5316830.93</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413365.36,5316819.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413360.86,5316811.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>33 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413357.2,5316804.38</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413352.76,5316796.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413333.77,5316804.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413414.24,5316992.44</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413408.7,5316944.11</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413408.54,5316981.74</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413404.8,5316937.09</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413401.64,5316971.95</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413401.48,5316929.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Guntramstraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413514.5,5316950.75</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413542.89,5316891.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413492.01,5316909.76</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413537.12,5316878.74</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413488.91,5316901.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413483.71,5316894.07</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413479.81,5316886.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413492.11,5316866.18</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413475.41,5316878.28</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413522.4,5316920.56</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413462.92,5316816.5</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413470.72,5316870.78</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413458.2,5316808.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413466.83,5316862.45</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413454.55,5316801.51</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.82">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413445.42,5316823.6</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.83">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413451.37,5316793.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.84">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413442.82,5316814.8</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.85">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413447.27,5316785.86</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.86">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413438.03,5316805.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.87">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413510.11,5316941.05</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>3</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.88">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413443.17,5316778.01</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.89">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413433.23,5316796.61</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>31</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.90">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413438.92,5316770.72</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.91">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413428.43,5316787.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>33</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.92">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413434.43,5316763.62</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.93">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413422.13,5316780.22</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>35</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.94">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413394.21,5316790.75</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>35 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.95">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413430.23,5316756.62</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.96">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413418.33,5316772.92</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>37</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.97">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413426.39,5316748.97</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.98">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413413.93,5316764.92</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>39</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.99">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413513.1,5316907.36</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.100">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413422.39,5316741.77</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.101">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413410.74,5316754.63</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>41</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.102">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413418.48,5316734.15</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>42</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.103">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413405.84,5316745.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>43</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.104">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413414.56,5316726.29</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>44</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.105">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413400.34,5316738.73</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>45</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.106">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413410.63,5316718.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>46</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.107">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413397.04,5316728.94</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>47</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.108">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413406.58,5316711.56</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>48</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.109">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413391.79,5316719.8</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>49</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.110">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413505.81,5316932.85</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.111">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413403.04,5316703.45</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>50</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.112">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413387.74,5316712.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>51</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.113">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413505.9,5316894.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.114">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413501.31,5316924.66</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.115">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413552.38,5316904.76</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.116">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413496.51,5316917.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Klarastraße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.117">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413563.8,5317000.46</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>1</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.118">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413559.31,5316968.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>2</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.119">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413571.67,5316980.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.120">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413580.15,5316993.32</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Kreuzstraße</ogr:street>
      <ogr:house_nr>6</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.121">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413567.48,5316930.95</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>11</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.122">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413551.59,5316938.85</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>13</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.123">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413538.2,5316944.85</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>15</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.124">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413573.16,5316951.06</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.125">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413508.91,5316959.35</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.126">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413489.81,5316969.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.127">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413474.22,5316947.65</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>17 b</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.128">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413480.91,5316974.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>19</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.129">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413472.52,5316979.64</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>21</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.130">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413463.62,5316984.84</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>23</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.131">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413490.19,5316990.27</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.132">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413451.32,5316991.04</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>25</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.133">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413481.74,5316995.02</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.134">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413439.33,5316996.89</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>27</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.135">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413474.04,5317000.21</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>28</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.136">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413409.75,5317014.78</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>29</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.137">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413467.01,5317004.33</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>30</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.138">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413458.82,5317009.23</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>32</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.139">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413468.81,5317027.16</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>32 a</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.140">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413450.92,5317013.13</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.141">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413442.39,5317017.24</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>36</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.142">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413433.86,5317021.44</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>38</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.143">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413425.73,5317027.53</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>40</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.144">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413595.68,5316923.94</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>5</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.145">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413583.87,5316925.16</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Lehener Straße</ogr:street>
      <ogr:house_nr>9</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.146">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413585.76,5316871.57</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>10</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.147">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413580.57,5316864.88</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>12</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.148">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413577.17,5316855.38</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>14</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.149">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413557.62,5316844.71</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>16</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.150">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413564.07,5316834.39</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>18</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.151">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413552.94,5316810.89</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>20</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.152">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413538.18,5316783.41</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>22</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.153">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413532.54,5316771.55</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>24</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.154">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413524.29,5316759.91</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>26</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.155">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413461.81,5316712.04</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>34</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.156">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413597.76,5316897.76</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>4</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.157">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413574.87,5316812.59</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>7</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:address fid="address.158">
      <ogr:geometryProperty><gml:Point srsName="EPSG:25832"><gml:coordinates>413589.16,5316881.17</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:street>Wentzingerstraße</ogr:street>
      <ogr:house_nr>8</ogr:house_nr>
    </ogr:address>
  </gml:featureMember>
</ogr:FeatureCollection>
