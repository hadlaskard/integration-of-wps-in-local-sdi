#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The library is used to parse the XML of WPS response documents and
    supports synchronous, asynchronous, single use and chained processes.
"""

# libs
import logging
import lxml.etree
import time
import requests

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")
VERSION = "1.0.0"
NAMESPACES = {
    'xlink': "http://www.w3.org/1999/xlink",
    'wps': "http://www.opengis.net/wps/{wps_version}",
    'ows': "http://www.opengis.net/ows/{ows_version}",
    'gml': "http://www.opengis.net/gml",
    'xsi': "http://www.w3.org/2001/XMLSchema-instance"
}

namespaces100 = {k: NAMESPACES[k].format(wps_version="1.0.0", ows_version="1.1") for k in NAMESPACES}
namespaces200 = {k: NAMESPACES[k].format(wps_version="2.0", ows_version="2.0") for k in NAMESPACES}


# return xpath namespace for given element and xpath
def get_xpath_ns(version):
    def xpath_ns(ele, path):
        if version == "1.0.0":
            nsp = namespaces100
        elif version == "2.0.0":
            nsp = namespaces200
        return ele.xpath(path, namespaces=nsp)

    return xpath_ns


# get xpath namespace
xpath_ns = get_xpath_ns(VERSION)


# return progress / result of the status response
def get_output(doc):
    process_succeeded = xpath_ns(doc, '/wps:ExecuteResponse/wps:Status/wps:ProcessSucceeded')
    process_accepted = xpath_ns(doc, '/wps:ExecuteResponse/wps:Status/wps:ProcessAccepted')
    process_status_url = xpath_ns(doc, '/wps:ExecuteResponse')
    process_status_url = process_status_url[0].attrib['statusLocation']

    LOGGER.debug('Status Reference Process Succeeded:' + str(process_succeeded))
    LOGGER.debug('Status Reference Process Accepted:' + str(process_accepted))
    LOGGER.debug('Status Reference statusLocation:' + str(process_status_url))

    # loop until statusLocation is final process result
    while not process_succeeded:
        # wait interval in seconds
        time.sleep(5)

        # reload doc from process_status_url
        r = requests.get(process_status_url, verify=False)
        doc_new = r.content

        # look for ProcessSucceeded status element
        doc = lxml.etree.fromstring(doc_new)
        process_succeeded = xpath_ns(doc, '/wps:ExecuteResponse/wps:Status/wps:ProcessSucceeded')

    result = get_output_data(doc)

    LOGGER.debug('Status Reference Result:' + str(result))

    return result


# return the content of LiteralData, Reference or ComplexData
def get_output_data(doc):
    output = {}
    for output_el in xpath_ns(doc, '/wps:ExecuteResponse'
                                   '/wps:ProcessOutputs/wps:Output'):
        [identifier_el] = xpath_ns(output_el, './ows:Identifier')

        lit_el = xpath_ns(output_el, './wps:Data/wps:LiteralData')
        if lit_el != []:
            output[identifier_el.text] = lit_el[0].text

        ref_el = xpath_ns(output_el, './wps:Reference')
        if ref_el != []:
            LOGGER.debug('Reference XPATH:' + str(ref_el[0].attrib))
            output[identifier_el.text] = ref_el[0].attrib['{' + NAMESPACES['xlink'] + '}href']

        data_el = xpath_ns(output_el, './wps:Data/wps:ComplexData')
        if data_el != []:
            if data_el[0].text:
                output[identifier_el.text] = data_el[0].text
            else:  # XML children
                ch = list(data_el[0])[0]
                output[identifier_el.text] = lxml.etree.tostring(ch)

        # looking for BoundingBoxData
        bbox_el = xpath_ns(output_el, './ows:BoundingBox')
        if bbox_el != []:
            LOGGER.debug('BBox XPATH:' + lxml.etree.tostring(bbox_el[0]))

            output[identifier_el.text] = lxml.etree.tostring(bbox_el[0])

    return output