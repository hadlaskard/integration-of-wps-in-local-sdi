#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The library is used for methods like database handling
    or spatial reference transformations.
"""

# libs
import logging
import tempfile
import psycopg2
import psycopg2.extras
from psycopg2 import sql
from pyproj import Proj, transform
from osgeo import ogr
from osgeo import osr

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# transform projection
def geo_transform(x1, y1, epsg1, epsg2):
    proj1 = Proj(init='epsg:'+str(epsg1))
    proj2 = Proj(init='epsg:'+str(epsg2))

    x2, y2 = transform(proj1, proj2, x1, y1)

    return x2, y2


# calculation of threshold distance for given solid and tnt mass
def damage_dist_threshold(tnt, solid):
    solid_dist = 0.

    # distance of float glass threshold at 3 kPa peak overpressure: R/M^(1/3)=52
    if solid == 0:
        solid_dist = 52.

    # distance of eardrum rupture threshold at 17 kPa peak overpressure: R/M^(1/3)=12.5
    if solid == 1:
        solid_dist = 12.5

    # taken from "Explosive Shocks in Air" by Graham and Kinney (Springer), derived by A. Klomfass, Fraunhofer EMI
    threshold = solid_dist * (tnt ** (1. / 3.))

    return threshold


# export spatial data from database intersected by a given geometry
def pg_export(subject, area, epsg):
    # unique geometry column identifier for general method use
    col_geom = 'geometry'

    # spatial reference
    sref = osr.SpatialReference()
    sref.ImportFromEPSG(epsg)

    # set vector format definition
    data_path = tempfile.mkstemp(prefix='db_' + subject + '_data_', suffix='.gml')[1]
    data_src = ogr.GetDriverByName("GML").CreateDataSource(data_path)
    data_lyr = data_src.CreateLayer(subject, srs=sref)

    # open database connection, using .pgpass for authentication
    if subject in ('address', 'parcel'):
        db_conn = psycopg2.connect("host=geodb port=5432 dbname=postnas_freiburg user=postgres")
    else:
        db_conn = psycopg2.connect("host=geodb port=5432 dbname=geo1 user=postgres")

    # check connection
    if db_conn is None:
        LOGGER.debug('PG connection refused.')

    # open cursor to perform database operations
    db_cur = db_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    # sql query with placeholders and execute command, using templating mechanism for better security
    if subject == 'address':
        query = sql.SQL("SELECT st.strname AS street, ad.ha_nr AS house_nr, ST_AsText(wkb_geometry) AS {g} "
                        "FROM {ad} ad LEFT JOIN {st} st ON st.strshl = ad.strshl "
                        "WHERE ST_Intersects(ST_SetSRID(ST_PolygonFromText(%s), %s), wkb_geometry);")
        db_cur.execute(query.format(g=sql.Identifier(col_geom),
                                    ad=sql.Identifier('gdm_mat_v_haeuser'),
                                    st=sql.Identifier('str_shl')),
                       [area.ExportToWkt(), epsg])
    elif subject == 'building':
        query = sql.SQL("SELECT gmlid, lagename AS street, hausnr AS house_nr, gfk AS use_id, nutzung AS use, "
                        "klasse AS class, qualitaet AS quality, area, ST_AsText(the_geom) AS {g} "
                        "FROM {sch}.{tbl} WHERE ST_Intersects(ST_SetSRID(ST_PolygonFromText(%s), %s), the_geom);")
        db_cur.execute(query.format(g=sql.Identifier(col_geom),
                                    sch=sql.Identifier('alkis'),
                                    tbl=sql.Identifier('gebaeude')),
                       [area.ExportToWkt(), epsg])
    elif subject == 'parcel':
        query = sql.SQL("SELECT gml_id, gemarkungsnummer AS subdistrict, zaehler AS enum, nenner AS denum, "
                        "flstkz AS code, amtlicheflaeche AS area, ST_AsText(wkb_geometry) AS {g} "
                        "FROM (SELECT *, $$08$$ || {d} || $$-000-$$ || lpad({z}::text, 5, $$0$$) ||$$/$$ || "
                        "lpad(coalesce({n}, $$0$$)::text, 4, $$0$$) AS flstkz FROM {tbl} "
                        "WHERE ST_Intersects(ST_SetSRID(ST_PolygonFromText(%s), %s), wkb_geometry)) AS foo;")
        db_cur.execute(query.format(g=sql.Identifier(col_geom),
                                    d=sql.Identifier('gemarkungsnummer'),
                                    z=sql.Identifier('zaehler'),
                                    n=sql.Identifier('nenner'),
                                    tbl=sql.Identifier('ax_flurstueck')),
                       [area.ExportToWkt(), epsg])
    elif subject == 'local_plan':
        query = sql.SQL("SELECT nummer AS nr, plannr, planbez AS name, aktiv AS legal, bpplan_uid AS uid, "
                        "aenderung_von AS revision, in_kraft_datum AS date, ST_AsText(the_geom) AS {g} "
                        "FROM {sch}.{tbl} WHERE ST_Intersects(ST_SetSRID(ST_PolygonFromText(%s), %s), the_geom);")
        db_cur.execute(query.format(g=sql.Identifier(col_geom),
                                    sch=sql.Identifier('bplan'),
                                    tbl=sql.Identifier('geltungsbereich')),
                       [area.ExportToWkt(), epsg])
    elif subject == 'poi':
        query = sql.SQL("SELECT poityp, name, bezeichnung AS description, kategorie AS category, adresse AS address, "
                        "url, mail, telefon AS phone, ansprechpartner AS contact, ST_AsText(the_geom) AS {g} "
                        "FROM {sch}.{tbl} WHERE ST_Intersects(ST_SetSRID(ST_PolygonFromText(%s), %s), the_geom);")
        db_cur.execute(query.format(g=sql.Identifier(col_geom), sch=sql.Identifier('poi'), tbl=sql.Identifier('pois')),
                       [area.ExportToWkt(), epsg])

    # process query result data
    names = [desc[0] for desc in db_cur.description]
    names.remove(col_geom)
    for name in names:
        field = ogr.FieldDefn(name, ogr.OFTString)
        data_lyr.CreateField(field)

    rows = db_cur.fetchall()
    for row in rows:
        data_lyr_def = data_lyr.GetLayerDefn()
        feat = ogr.Feature(data_lyr_def)
        feat_geom = ogr.CreateGeometryFromWkt(row[col_geom])
        feat.SetGeometry(feat_geom)

        for name in names:
            feat.SetField(name, str(row[name]))

        data_lyr.CreateFeature(feat)

        # free and reassign
        feat = None

    # make the changes to the database persistent
    db_conn.commit()

    # close communication with the database
    db_cur.close()
    db_conn.close()

    # free and reassign
    db_conn = None

    return data_path