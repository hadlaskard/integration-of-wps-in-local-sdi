#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The process returns intersected area of each input feature.
"""

# libs
import logging
import requests
import tempfile
from pywps import Process, ComplexInput, ComplexOutput, Format
from pywps.app.Common import Metadata
from pywps.validator.mode import MODE
from pywps.validator import complexvalidator
from osgeo import ogr
from osgeo import osr
from lxml import etree
from lib import varlib

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# process returns intersected area of each input feature
class VectIntersect(Process):
    def __init__(self):
        in_geom_a = ComplexInput(
            'in_geom_a',
            'Input Geometry A [gml]',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml),
                               Format(mime_type='application/gml+xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml)],
            mode=MODE.NONE
        )

        in_geom_b = ComplexInput(
            'in_geom_b',
            'Input Geometry B [gml]',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml),
                               Format(mime_type='application/gml+xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml)],
            mode=MODE.NONE
        )

        out_intersect = ComplexOutput(
            'out_intersect',
            'Intersected Geometry',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      encoding='UTF-8', validate=None)]
        )

        inputs = [in_geom_a, in_geom_b]
        outputs = [out_intersect]

        super(VectIntersect, self).__init__(
            self._handler,
            identifier='vect_intersect',
            version='1.0',
            title='Vector Intersection Process',
            abstract='The process returns intersected area of each input feature.',
            metadata=[Metadata('The process returns intersected area of each input feature.',
                               'http://geodev:8080/geonetwork/srv/ger/catalog.search?service=CSW&version=2.0.2'
                               '&request=GetRecordById&id=c850b578-8561-42fb-88d1-1ac9e3314cf4#/metadata/'
                               'c850b578-8561-42fb-88d1-1ac9e3314cf4')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )

    # handler method obtains request object and response object
    # @staticmethod  # only for static methods, no 'self' applicable
    def _handler(self, request, response):
        # check if data is given by reference
        if request.inputs['in_geom_a'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_geom_a'][0].method == 'GET':
                # obtain input with identifier as file name
                in_geom_a = request.inputs['in_geom_a'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_geom_a'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_geom_a'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, w: write in text mode
                filename = tempfile.mkstemp(prefix='geom_a_', suffix='.gml')[1]
                with open(filename, 'w') as fp:
                    fp.write(data)
                    fp.close()

                in_geom_a = filename
        else:
            # obtain input with identifier as file name
            in_geom_a = request.inputs['in_geom_a'][0].file

        # check if data is given by reference
        if request.inputs['in_geom_b'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_geom_b'][0].method == 'GET':
                # obtain input with identifier as file name
                in_geom_b = request.inputs['in_geom_b'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_geom_b'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_geom_b'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, w: write in text mode
                filename = tempfile.mkstemp(prefix='geom_b_', suffix='.gml')[1]
                with open(filename, 'w') as fp:
                    fp.write(data)
                    fp.close()

                in_geom_b = filename
        else:
            # obtain input with identifier as file name
            in_geom_b = request.inputs['in_geom_b'][0].file

        # open file and layer of input a
        in_src_a = ogr.Open(in_geom_a)
        in_lyr_a = in_src_a.GetLayer()
        lyr_name_a = in_lyr_a.GetName()

        # open file and layer of input b
        in_src_b = ogr.Open(in_geom_b)
        in_lyr_b = in_src_b.GetLayer()
        lyr_name_b = in_lyr_b.GetName()

        # get and set output spatial reference
        epsg = int(in_lyr_a.GetSpatialRef().GetAttrValue('AUTHORITY', 1))
        sref = osr.SpatialReference()
        sref.ImportFromEPSG(epsg)

        # create output file
        driver = ogr.GetDriverByName('GML')
        out_src = driver.CreateDataSource(lyr_name_a)
        out_lyr = out_src.CreateLayer(lyr_name_a+'_'+lyr_name_b, sref, ogr.wkbGeometryCollection)

        # create geometry collection of input a
        collect_a = ogr.Geometry(ogr.wkbGeometryCollection)
        for feat in in_lyr_a:
            collect_a.AddGeometry(feat.GetGeometryRef())

        # create geometry collection of input b
        collect_b = ogr.Geometry(ogr.wkbGeometryCollection)
        for feat in in_lyr_b:
            collect_b.AddGeometry(feat.GetGeometryRef())

        # calculate intersection
        intersect_geom = collect_a.Intersection(collect_b)

        # create output feature to the file
        out_feat = ogr.Feature(feature_def=out_lyr.GetLayerDefn())
        out_feat.SetGeometry(intersect_geom)
        out_lyr.CreateFeature(out_feat)

        # free and reassign
        out_feat = None
        out_src = None

        # set output format and file name
        response.outputs['out_intersect'].output_format = Format(mime_type='text/xml', extension='.gml',
                                                                 schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                                                 encoding='UTF-8', validate=None)
        response.outputs['out_intersect'].file = lyr_name_a

        return response