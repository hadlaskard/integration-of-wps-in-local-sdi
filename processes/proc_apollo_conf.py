#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The process is part of the explosive ordnance disposal workflow
    and returns APOLLO configuration data for SIRIUS interface.
"""

# libs
import logging
import tempfile
import json
from pywps import Process, LiteralInput, ComplexInput, ComplexOutput, Format
from pywps.app.Common import Metadata
from pywps.validator.mode import MODE
from pywps.validator import complexvalidator
from pywps.validator.allowed_value import RANGECLOSURETYPE, ALLOWEDVALUETYPE
from pywps.inout.literaltypes import AllowedValue
from easydict import EasyDict
from osgeo import ogr
from lib import geolib

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# process returns APOLLO configuration data for SIRIUS interface
class ApolloConf(Process):
    # static class variables
    epsg = 25832  # local spatial reference code
    epsg2 = 4326  # spatial reference code for WGS84
    srv_url = 'https://www.cadfem.de/apollo/'  # url provided by the SIRIUS project team

    def __init__(self):
        in_geom = ComplexInput(
            'in_geom',
            'Exact Location [gml]',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml)],
            # validation mode unable to use due incompatibilities between mimetype library and QGIS wps client
            mode=MODE.NONE
        )

        in_precision = LiteralInput(
            'in_precision',
            'Precision [m]',
            abstract='Precision used by APOLLO simulation. Supported values are: 0.5, 1.0, 2.5, 5.0, 10.0',
            data_type='float',
            allowed_values=(0.5, 1.0, 2.5, 5.0, 10.0)
        )

        in_height = LiteralInput(
            'in_height',
            'Relative Height [m]',
            data_type='float',
            default='-2.5'
        )

        in_tnt = LiteralInput(
            'in_tnt',
            'Exact TNT Blast Power [kg]',
            data_type='integer',
            # spacing unable to use due incompatibilities between QGIS wps client
            # allowed_values=(range(50, 2000+1, 50)),
            allowed_values=[AllowedValue(minval=1, maxval=5000,  # spacing=50,
                                         allowed_type=ALLOWEDVALUETYPE.RANGE,
                                         range_closure=RANGECLOSURETYPE.OPEN)]
        )

        in_heading = LiteralInput(
            'in_heading',
            'Bomb Azimuth Angle [deg]',
            data_type='float',
            min_occurs=0,
            default='0.0'
        )

        in_pitch = LiteralInput(
            'in_pitch',
            'Bomb Tilt Angle [deg]',
            data_type='float',
            min_occurs=0,
            default='0.0'
        )

        in_type = LiteralInput(
            'in_type',
            'Bomb Type',
            abstract='Type of the bomb after classification. Supported values are: N/A, GP100, GP250',
            data_type='string',
            allowed_values=('N/A', 'GP100', 'GP250'),
            min_occurs=0
        )

        in_detonator = LiteralInput(
            'in_detonator',
            'Detonator Position',
            abstract='Position of detonator after classification. Supported values are: N/A, Front, Rear, Top, Bottom',
            data_type='string',
            allowed_values=('N/A', 'Front', 'Rear', 'Top', 'Bottom'),
            min_occurs=0
        )

        in_site_desc = LiteralInput(
            'in_site_desc',
            'Site Description',
            abstract='Description of the bomb find location. Supported values are: Surface, Cavern',
            data_type='string',
            allowed_values=('Surface', 'Cavern'),
            min_occurs=0
        )

        in_site_rad = LiteralInput(
            'in_site_rad',
            'Site Radius [m]',
            data_type='float',
            min_occurs=0,
            default='0.0'
        )

        in_hidden = LiteralInput(
            'in_hidden',
            'Hidden Objects [gml:id1 gml:id2]',
            abstract='List of 3D city model objects that will be ignored by the simulation. '
                     'Supported values are GML identification strings.',
            data_type='string',
            min_occurs=0
        )

        out_conf = ComplexOutput(
            'out_conf',
            'APOLLO Configuration Data',
            supported_formats=[Format(mime_type='application/json', extension='.json',
                                      validate=complexvalidator.validategeojson,
                                      encoding='UTF-8', schema='json')]
        )

        inputs = [in_geom, in_precision, in_height, in_tnt, in_heading, in_pitch,
                  in_type, in_detonator, in_site_desc, in_site_rad, in_hidden]

        outputs = [out_conf]

        super(ApolloConf, self).__init__(
            self._handler,
            identifier='apollo_conf',
            version='1.0',
            title='APOLLO Configuration Process',
            abstract='The process returns APOLLO configuration data for SIRIUS interface.',
            metadata=[Metadata('The process is part of the explosive ordnance disposal workflow '
                               'and returns APOLLO configuration data for SIRIUS interface.',
                               'http://geodev:8080/geonetwork/srv/ger/catalog.search?service=CSW&version=2.0.2'
                               '&request=GetRecordById&id=c850b578-8561-42fb-88d1-1ac9e3314cf4#/metadata/'
                               'c850b578-8561-42fb-88d1-1ac9e3314cf4')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )

    # handler method obtains request object and response object
    # @staticmethod  # only for static methods, no 'self' applicable
    def _handler(self, request, response):
        # obtain input with identifier as file name
        in_file = request.inputs['in_geom'][0].file

        # possible request attributes: 'abstract', 'as_reference', 'base64', 'clone', 'crs', 'crss', 'data',
        # 'describe_xml', 'dimensions', 'execute_xml', 'file', 'get_base64', 'get_data', 'get_file',
        # 'get_memory_object', 'get_stream', 'get_workdir', 'identifier', 'json', 'll', 'max_occurs', 'memory_object',
        # 'metadata', 'min_occurs', 'set_base64', 'set_data', 'set_file', 'set_memory_object', 'set_stream',
        # 'set_workdir', 'source', 'source_type', 'stream', 'title', 'ur', 'valid_mode', 'validator', 'workdir'

        # default parameter values
        bomb_type, detonator, site_desc, hidden = '', '', '', ''
        precision, height, tnt, heading, pitch, site_rad = 0., 0., 0, 0., 0., 2.

        # check and obtain input with identifier as data directly
        if 'in_precision' in request.inputs:
            precision = request.inputs['in_precision'][0].data
        if 'in_height' in request.inputs:
            height = request.inputs['in_height'][0].data
        if 'in_tnt' in request.inputs:
            tnt = request.inputs['in_tnt'][0].data
        if 'in_heading' in request.inputs:
            heading = request.inputs['in_heading'][0].data
        if 'in_pitch' in request.inputs:
            pitch = request.inputs['in_pitch'][0].data
        if 'in_type' in request.inputs:
            bomb_type = request.inputs['in_type'][0].data
        if 'in_detonator' in request.inputs:
            detonator = request.inputs['in_detonator'][0].data
        if 'in_site_desc' in request.inputs:
            site_desc = request.inputs['in_site_desc'][0].data
        if 'in_site_rad' in request.inputs:
            site_rad = request.inputs['in_site_rad'][0].data
        if 'in_hidden' in request.inputs:
            hidden = (request.inputs['in_hidden'][0].data).split()

        # open file and layer
        in_src = ogr.Open(in_file)
        in_lyr = in_src.GetLayer()

        # only one single input feature and valid tnt blast power
        if in_lyr.GetFeatureCount() == 1 and tnt > 0:
            # conservative calculation for float glass
            dist_threshold = geolib.damage_dist_threshold(tnt, 0)

            LOGGER.debug('Threshold:' + str(dist_threshold))

            # get the feature geometry
            in_feat = in_lyr.GetNextFeature()
            in_geom = in_feat.GetGeometryRef()

            # get SRID of geometry and make sure location is a point
            epsg0 = int(in_geom.GetSpatialReference().GetAttrValue('AUTHORITY', 1))
            x0, y0 = in_geom.Centroid().GetX(), in_geom.Centroid().GetY()

            # harmonization of spatial reference
            if epsg0 != self.epsg:
                # transform position to local spatial reference
                x2, y2 = geolib.geo_transform(x0, y0, epsg0, self.epsg)
            else:
                x2, y2 = x0, y0

            # calculate bounding box
            bbx1 = x2 - dist_threshold
            bby1 = y2 - dist_threshold
            bbx2 = x2 + dist_threshold
            bby2 = y2 + dist_threshold

            # transform position to WGS84
            x_wgs, y_wgs = geolib.geo_transform(x2, y2, self.epsg, self.epsg2)

            LOGGER.debug('Coordinates in ' + str(self.epsg) + ':' + str(x2) + '/' + str(y2))
            LOGGER.debug('Coordinates in ' + str(self.epsg2) + ':' + str(x_wgs) + '/' + str(y_wgs))

            # create location geometry
            location = ogr.Geometry(ogr.wkbPoint)
            location.AddPoint(x2, y2)

            LOGGER.debug('Location as WKT:' + location.ExportToWkt())

            # create output data
            conf_data = EasyDict({'bomb': {'tnt': tnt, 'type': bomb_type, 'detonator': detonator},
                                  'domain': {'name': 'Ultimo', 'zroi': 100, 'droi': dist_threshold},
                                  'mode': {'name': 'Ultimo', 't': 50, 'precision': precision},
                                  'site': {'type': site_desc, 'radius': site_rad},
                                  'geometry': {'crs': self.epsg2, 'position': [x_wgs, y_wgs], 'depth': (-1) * height},
                                  'crs': self.epsg,
                                  'position': [x2, y2],
                                  'height': height,
                                  'heading': heading,
                                  'pitch': pitch,
                                  'extent': [bbx1, bby1, bbx2, bby2],
                                  'hiddenObjects': hidden,
                                  'service': {'url': self.srv_url, 'resultFile': 'effects_' + str(self.uuid) + '.zip'}
                                  })

            conf_json = json.dumps(conf_data)

            # create file, w: write in text mode
            conf_path = tempfile.mkstemp(prefix='conf_', suffix='.json')[1]
            with open(conf_path, 'w') as fp:
                fp.write(conf_json)
                fp.close()

            # set output format and file name
            response.outputs['out_conf'].output_format = Format(mime_type='application/json', extension='.json',
                                                                validate=complexvalidator.validategeojson,
                                                                encoding='UTF-8', schema='json')
            response.outputs['out_conf'].file = conf_path
        else:
            # remove output from response
            del response.outputs['out_conf']

            LOGGER.debug('Only one single input feature allowed. ' +
                         str(in_lyr.GetFeatureCount()) + ' detected!')

        # free and reassign
        in_src = None
        in_lyr = None

        # possible response attributes: 'abstract', 'as_reference', 'base64', 'crs', 'crss', 'data', 'describe_xml',
        # 'dimensions', 'execute_xml', 'file', 'get_base64', 'get_data', 'get_file', 'get_memory_object', 'get_stream',
        # 'get_workdir', 'identifier', 'json', 'll', 'max_occurs', 'memory_object', 'metadata', 'min_occurs',
        # 'set_base64', 'set_data', 'set_file', 'set_memory_object', 'set_stream', 'set_workdir', 'source',
        # 'source_type', 'stream', 'title', 'ur', 'valid_mode', 'validator', 'workdir'

        return response