#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The process returns 3D related spatial data selected by input geometry.
"""

# libs
import logging
import tempfile
import requests
import owslib.util
import psycopg2
from psycopg2 import sql
from pywps import Process, ComplexInput, ComplexOutput, Format
from pywps.app.Common import Metadata
from pywps.validator.mode import MODE
from pywps.validator import complexvalidator
from owslib.wcs import WebCoverageService
from osgeo import ogr
from osgeo import osr
from lxml import etree
from lib import geolib
from lib import varlib

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# process returns 3D related spatial data selected by input geometry
class Export3dData(Process):
    # static class variables
    epsg = 25832  # local spatial reference code
    epsg3 = 31467  # outdated local spatial reference code

    def __init__(self):
        in_geom = ComplexInput(
            'in_geom',
            'Selection Geometry [gml]',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml)],
            mode=MODE.NONE
        )

        out_dem = ComplexOutput(
            'out_dem',
            'Digital Elevation Model',
            supported_formats=[Format(mime_type='image/geotiff', extension='.tif')]
        )

        out_city = ComplexOutput(
            'out_city',
            '3D City Model',
            supported_formats=[Format(mime_type='text/xml', extension='.x3d',
                                      schema='http://www.web3d.org/specifications/x3d-3.3.xsd',
                                      validate=None, encoding='UTF-8')]
        )

        inputs = [in_geom]

        outputs = [out_dem, out_city]

        super(Export3dData, self).__init__(
            self._handler,
            identifier='export_3d_data',
            version='1.0',
            title='Export 3D Related Spatial Data Process',
            abstract='The process returns 3D related spatial data selected by input geometry. Supported outputs are: '
                     'Digital Elevation Model [out_dem]; 3D City Model [out_city]',
            metadata=[Metadata('The process returns 3D related spatial data selected by input geometry.',
                               'http://geodev:8080/geonetwork/srv/ger/catalog.search?service=CSW&version=2.0.2'
                               '&request=GetRecordById&id=c850b578-8561-42fb-88d1-1ac9e3314cf4#/metadata/'
                               'c850b578-8561-42fb-88d1-1ac9e3314cf4')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )

    # handler method obtains request object and response object
    # @staticmethod  # only for static methods, no 'self' applicable
    def _handler(self, request, response):
        # check if data is given by reference
        if request.inputs['in_geom'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_geom'][0].method == 'GET':
                # obtain input with identifier as file name
                in_geom = request.inputs['in_geom'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_geom'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_geom'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, w: write in text mode
                filename = tempfile.mkstemp(prefix='input_', suffix='.gml')[1]
                with open(filename, 'w') as fp:
                    fp.write(data)
                    fp.close()

                in_geom = filename
        else:
            # obtain input with identifier as file name
            in_geom = request.inputs['in_geom'][0].file

        # open file and layer
        in_src = ogr.Open(in_geom)
        in_lyr = in_src.GetLayer()

        # get spatial reference
        epsg0 = int(in_lyr.GetSpatialRef().GetAttrValue('AUTHORITY', 1))

        # only one single input feature
        if in_lyr.GetFeatureCount() == 1:
            # get geometry and extent
            feat = in_lyr.GetNextFeature()
            geom = feat.GetGeometryRef()
            extent = geom.GetEnvelope()

            # harmonization of spatial reference
            if epsg0 != self.epsg:
                # transform extent to local spatial reference
                bbx1, bby1 = geolib.geo_transform(extent[0], extent[2], epsg0, self.epsg)
                bbx2, bby2 = geolib.geo_transform(extent[1], extent[3], epsg0, self.epsg)
            else:
                bbx1, bby1 = extent[0], extent[2]
                bbx2, bby2 = extent[1], extent[3]

            LOGGER.debug('Input BBox in ' + str(self.epsg) + ':' + str(bbx1) +
                         ',' + str(bby1) + ',' + str(bbx2) + ',' + str(bby2))

            # DEM PART ##################################################

            if 'out_dem' in request.outputs.keys():
                # WCS request
                url = "http://mapbender/wcs7/verma_hoehen/verma_dgm?"
                wcs = WebCoverageService(url, version="1.0.0")

                # list all coverages
                LOGGER.debug(','.join(wcs.contents))

                # get a certain coverage
                dem = wcs['dgm1']

                # list all attributes of the coverage
                LOGGER.debug(dir(dem))

                # list all bbox
                for bb in dem.boundingboxes:
                    LOGGER.debug('DEM BBox:' + str(bb) + '_' +
                                 str(dem.boundingboxes[1]['nativeSrs']) + '_' +
                                 str(dem.boundingboxes[1]['bbox']))

                # list all time positions
                for tp in dem.timepositions:
                    LOGGER.debug('DEM TPos:' + str(tp))

                # list all supported formats
                for sf in dem.supportedFormats:
                    LOGGER.debug('DEM Formats:' + str(sf))

                # request parameters
                bbox = (bbx1, bby1, bbx2, bby2)
                crs = 'EPSG:' + str(self.epsg)
                file_type = 'GEOTIFF_16'  # GEOTIFF_16, AAIGRID, GTiff
                resx, resy = 1, 1  # max. available resolution of DEM data

                try:
                    # get coverage request
                    gc = wcs.getCoverage(identifier=dem.id, bbox=bbox, format=file_type, crs=crs, resx=resx, resy=resy)

                    LOGGER.debug('Get Coverage URL:' + gc.geturl())

                    # create file, wb: write in binary mode
                    dem_path = tempfile.mkstemp(prefix='dem_', suffix='.tif')[1]
                    with open(dem_path, 'wb') as fp:
                        fp.write(gc.read())
                        fp.close()
                except owslib.util.ServiceException as se:
                    dem_path = ''
                    LOGGER.debug('WCS ServiceException:' + str(se))

                # set output format and file name
                response.outputs['out_dem'].output_format = Format(mime_type='image/geotiff', extension='.tif')
                response.outputs['out_dem'].file = dem_path
            else:
                # remove output from response
                del response.outputs['out_dem']

            # 3D CITY MODEL PART ##################################################

            if 'out_city' in request.outputs.keys():
                # harmonization of spatial reference
                if epsg0 != self.epsg3:
                    # transform selection geometry to spatial reference of 3D city model
                    sref0 = osr.SpatialReference()
                    sref0.ImportFromEPSG(epsg0)
                    sref3 = osr.SpatialReference()
                    sref3.ImportFromEPSG(self.epsg3)
                    transform = osr.CoordinateTransformation(sref0, sref3)
                    geom.Transform(transform)

                LOGGER.debug('Input Geometry in ' + str(self.epsg3) + ':' + geom.ExportToWkt())

                # open database connection, using .pgpass for authentication
                db_conn = psycopg2.connect("host=geodb port=5432 dbname=citydb_v4 user=postgres")

                # check connection
                if db_conn is None:
                    LOGGER.debug('PG connection refused.')

                # open cursor to perform database operations
                db_cur = db_conn.cursor()

                # sql query with placeholders, transformation to local spatial reference
                query = sql.SQL("SELECT ST_AsX3D(ST_Transform(ST_SetSRID(sg.geometry, %s), %s), 3, 0) AS geom_3d "
                                "FROM {tbl} sg LEFT JOIN thematic_surface ts ON ts.lod2_multi_surface_id = sg.root_id "
                                "LEFT JOIN building b ON ts.building_id = b.building_root_id "
                                "WHERE sg.geometry IS NOT NULL AND ts.lod2_multi_surface_id IS NOT NULL "
                                "AND ST_Intersects(ST_SetSRID(ST_PolygonFromText(%s), %s), sg.geometry);")

                # execute command, using templating mechanism for better security
                db_cur.execute(query.format(tbl=sql.Identifier('surface_geometry')),
                               [self.epsg3, self.epsg, geom.ExportToWkt(), self.epsg3])

                # process query result data
                city_data = '<?xml version="1.0" encoding="UTF-8"?>\n' \
                            '<!DOCTYPE X3D PUBLIC "ISO//Web3D//DTD X3D 3.3//EN"\n' \
                            '  "http://www.web3d.org/specifications/x3d-3.3.dtd">\n\n' \
                            '<X3D profile="Interchange" version="3.3"\n' \
                            '     xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance"\n' \
                            '     xsd:noNamespaceSchemaLocation="http://www.web3d.org/specifications/x3d-3.3.xsd">\n' \
                            '<Scene>'

                for city_geom in db_cur:
                    city_data += '\n  <Shape>\n    ' + str(city_geom)[2:-3] + '\n  </Shape>'

                city_data += '\n</Scene>\n</X3D>'

                # create file, w: write in text mode
                city_path = tempfile.mkstemp(prefix='city_', suffix='.x3d')[1]
                with open(city_path, 'w') as fp:
                    fp.write(city_data)
                    fp.close()

                # make the changes to the database persistent
                db_conn.commit()

                # close communication with the database
                db_cur.close()
                db_conn.close()

                # free and reassign
                db_conn = None

                # set output format and file name
                response.outputs['out_city'].output_format = Format(mime_type='text/xml', extension='.x3d',
                                                                    schema='http://www.web3d.org/specifications/x3d-3.3.xsd',
                                                                    validate=None, encoding='UTF-8')
                response.outputs['out_city'].file = city_path
            else:
                # remove output from response
                del response.outputs['out_city']
        else:
            LOGGER.debug('Only one single input feature allowed. ' +
                         str(in_lyr.GetFeatureCount()) + ' detected!')

        LOGGER.debug(request.outputs.keys())
        LOGGER.debug(response.outputs.keys())

        return response