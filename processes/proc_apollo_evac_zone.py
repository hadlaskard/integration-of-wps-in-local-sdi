#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The process is part of the explosive ordnance disposal workflow
    and returns evacuation zone around blast affected area.
"""

# libs
import numpy as np
import logging
import tempfile
import requests
import math
import os
import json
import zipfile
import shutil
from pywps import Process, LiteralInput, ComplexInput, ComplexOutput, Format
from pywps.app.Common import Metadata
from pywps.validator.mode import MODE
from pywps.validator import complexvalidator
from osgeo import ogr
from osgeo import osr
from osgeo import gdal
from lxml import etree
from lib import varlib

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# process returns evacuation zone around blast affected area
class ApolloEvacZone(Process):
    # static class variables
    rot_deg = 28.5  # z axis rotation used by APOLLO, case study only, will be 0.0 in productive use

    def __init__(self):
        in_conf = ComplexInput(
            'in_conf',
            'APOLLO Configuration Data [json]',
            supported_formats=[Format(mime_type='application/json', extension='.json',
                                      validate=complexvalidator.validategeojson,
                                      encoding='UTF-8', schema='json')],
            mode=MODE.NONE
        )

        in_effects = ComplexInput(
            'in_effects',
            'APOLLO Effects Result [zip|dat]',
            supported_formats=[Format(mime_type='application/octet-stream', extension='.zip')],
            mode=MODE.NONE
        )

        in_dmg_lvl = LiteralInput(
            'in_dmg_lvl',
            'Damage Level',
            abstract='Level of damage the evacuation zone will be calculated for: '
                     '0 = Float Glass, '
                     '1 = Hardened Glass, '
                     '2 = Safety Glass, '
                     '3 = Masonry, '
                     '4 = Eardrum Rupture, '
                     '5 = Injury, '
                     '6 = Lethal Injury',
            data_type='integer',
            allowed_values=(0, 1, 2, 3, 4, 5, 6),
            min_occurs=0
        )

        out_evac_zone = ComplexOutput(
            'out_evac_zone',
            'Evacuation Zone',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      encoding='UTF-8', validate=None)]
        )

        out_raster = ComplexOutput(
            'out_raster',
            'Evacuation Raster',
            supported_formats=[Format(mime_type='image/geotiff', extension='.tif')]
        )

        inputs = [in_conf, in_effects, in_dmg_lvl]

        outputs = [out_evac_zone, out_raster]

        super(ApolloEvacZone, self).__init__(
            self._handler,
            identifier='apollo_evac_zone',
            version='1.0',
            title='APOLLO Evacuation Zone Process',
            abstract='The process returns evacuation zone around blast affected area.',
            metadata=[Metadata('The process is part of the explosive ordnance disposal workflow '
                               'and returns evacuation zone around blast affected area.',
                               'http://geodev:8080/geonetwork/srv/ger/catalog.search?service=CSW&version=2.0.2'
                               '&request=GetRecordById&id=c850b578-8561-42fb-88d1-1ac9e3314cf4#/metadata/'
                               'c850b578-8561-42fb-88d1-1ac9e3314cf4')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )

    # handler method obtains request object and response object
    # @staticmethod  # only for static methods, no 'self' applicable
    def _handler(self, request, response):
        # IN_CONF PART ##################################################

        # check if data is given by reference
        if request.inputs['in_conf'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_conf'][0].method == 'GET':
                # obtain input with identifier as file name
                in_conf = request.inputs['in_conf'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_conf'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_conf'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, w: write in text mode
                filename = tempfile.mkstemp(prefix='conf_', suffix='.json')[1]
                with open(filename, 'w') as fp:
                    fp.write(data)
                    fp.close()

                in_conf = filename
        else:
            # obtain input with identifier as file name
            in_conf = request.inputs['in_conf'][0].file

        # IN_EFFECTS PART ##################################################

        # check if data is given by reference
        if request.inputs['in_effects'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_effects'][0].method == 'GET':
                # obtain input with identifier as file name
                in_effects = request.inputs['in_effects'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_effects'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_effects'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, wb: write in binary mode
                filename = tempfile.mkstemp(prefix='effects_', suffix='.zip')[1]
                with open(filename, 'wb') as fp:
                    fp.write(data)
                    fp.close()

                in_effects = filename
        else:
            # obtain input with identifier as file name
            in_effects = request.inputs['in_effects'][0].file

        # IN DAMAGE LEVEL PART ##################################################

        dmg_lvl = 'F4_FloatGl'  # default level of damage

        # check and obtain input with identifier as data directly
        if 'in_dmg_lvl' in request.inputs:
            lvl = request.inputs['in_dmg_lvl'][0].data

            if lvl == 1:
                dmg_lvl = 'F5_HardGl'
            if lvl == 2:
                dmg_lvl = 'F6_SafeGl'
            if lvl == 3:
                dmg_lvl = 'F7_Masonry'
            if lvl == 4:
                dmg_lvl = 'F10_Eardrum'
            if lvl == 5:
                dmg_lvl = 'F11_Injury'
            if lvl == 6:
                dmg_lvl = 'F12_Lethal'

        # CONFIG PART ##################################################

        LOGGER.debug('Config path:' + in_conf)
        LOGGER.debug('Effects path:' + in_effects)

        # open configuration file
        with open(in_conf, 'r') as fp:
            conf_data = json.load(fp)

        # check and obtain input with identifier as data directly
        if 'crs' in conf_data:
            epsg = conf_data['crs']
        if 'position' in conf_data:
            if len(conf_data['position']) > 1:
                x = conf_data['position'][0]
                y = conf_data['position'][1]
        if 'mode' in conf_data:
            if 'precision' in conf_data['mode']:
                precision = conf_data['mode']['precision']

        # check necessary parameter
        try:
            x, y, epsg, precision
            LOGGER.debug('Parameter:' + str(x) + '/' + str(y) + '/' + str(epsg) + '/' + str(precision))
        except NameError:
            LOGGER.debug('Input value error in APOLLO configuration.')

        # APOLLO EFFECTS PART ##################################################

        in_effects_dat = in_effects

        # zip archive handling for APOLLO effects file
        if zipfile.is_zipfile(in_effects):
            with zipfile.ZipFile(in_effects) as my_zip:
                # get name of files with *.dat extension
                cont_match = filter(lambda s: '.dat' in s, my_zip.namelist())

                # set new name for APOLLO effects file
                in_effects_dat = os.path.join(os.path.dirname(in_effects), cont_match[0])

                # extract first *.dat file
                with my_zip.open(cont_match[0]) as zf, open(in_effects_dat, 'wb') as f:
                    shutil.copyfileobj(zf, f)

        LOGGER.debug('APOLLO effects file:' + in_effects_dat)

        # build dtype array structure for APOLLO effects file
        dt = np.dtype({'names': ['I', 'J', 'K', 'Dir', 'N', 'Obj',
                                 'F1_MaxOP', 'F2_MaxOP-Imp', 'F3_OP-Imp', 'F4_FloatGl', 'F5_HardGl', 'F6_SafeGl',
                                 'F7_Masonry', 'F8_RC30-01', 'F9_RC30-06', 'F10_Eardrum', 'F11_Injury', 'F12_Lethal'],
                       'formats': ['int', 'int', 'int', 'int', 'int', 'int', 'float', 'float', 'float', 'float',
                                   'float', 'float', 'float', 'float', 'float', 'float', 'float', 'float']})

        # read APOLLO effects file
        data = np.loadtxt(in_effects_dat, skiprows=19, dtype=dt, ndmin=2)

        # get dimensions (I=512 J=512 K=76)
        size_i = np.amax(data['I']) - np.amin(data['I']) + 1
        size_j = np.amax(data['J']) - np.amin(data['J']) + 1
        # size_k = np.amax(data['K']) - np.amin(data['K']) + 1

        # get delta of translation to positive quarter
        delta_i = abs(np.amin(data['I']))
        # delta_j = abs(np.amin(data['J']))
        # delta_k = abs(np.amin(data['K']))

        # max values, no abs, needed for iterations
        # max_i = np.amax(data['I'])
        max_j = np.amax(data['J'])
        # max_k = np.amax(data['K'])

        LOGGER.debug('Dimensions:sizeI=' + str(size_i) + '/sizeJ=' + str(size_j) +
                     '/deltaI=' + str(delta_i) + '/maxJ=' + str(max_j))

        # empty array with size of ground surface
        target = np.zeros((size_j, size_i))

        # make data flat
        for row in np.nditer(data):
            # save value only if greater than previous value in K direction
            if row[dmg_lvl] > target[max_j - row['J']][delta_i + row['I']]:
                # save n-dimensional values
                # target[max_j - row['J']][delta_i + row['I']] = [row['F1_MaxOP'], row['F2_MaxOP-Imp'],
                #                                                 row['F3_OP-Imp'], row[dmg_lvl]]
                # save 1-dimensional value
                target[max_j - row['J']][delta_i + row['I']] = row[dmg_lvl]

        # free and reassign
        data = None

        # RASTER PART ##################################################

        # file path for raster
        raster_path = os.path.splitext(in_effects_dat)[0] + '_' + dmg_lvl.lower() + '_.tif'

        # set spatial reference and export projection to wkt
        sref = osr.SpatialReference()
        sref.ImportFromEPSG(epsg)
        wkt_proj = sref.ExportToWkt()

        # number of pixels in x and y, and size of one pixel
        pixel_x = size_i
        pixel_y = size_j
        pixel_size = precision

        # transform location coordinates to upper left base point used in GTiff
        rot_rad = math.radians(-1 * self.rot_deg)
        size_i2 = size_i / 2.0
        size_j2 = size_j / 2.0
        delta_x = (size_i2 * precision) * math.cos(rot_rad) + (size_j2 * precision) * math.sin(rot_rad)
        delta_y = -(size_i2 * precision) * math.sin(rot_rad) + (size_j2 * precision) * math.cos(rot_rad)
        min_x = x - delta_x
        max_y = y + delta_y

        LOGGER.debug('Coordinates:' + str(min_x) + '/' + str(max_y) + '/' + str(delta_x) + '/' + str(delta_y))
        LOGGER.debug('Rotation:' + str(math.cos(rot_rad) * pixel_size) + '/' + str(math.sin(rot_rad)))

        # set raster format definition
        raster = gdal.GetDriverByName('GTiff').Create(
            raster_path,  # file path
            pixel_x,  # width in pixels
            pixel_y,  # height in pixels
            1,  # number of bands
            gdal.GDT_Float32  # type of raster
        )

        # set transformation from pixel to projected coordinates
        raster.SetGeoTransform((
            min_x,  # x value at top left
            math.cos(rot_rad) * pixel_size,  # transform pixel size in west-east
            math.sin(rot_rad),  # rotation factor 1
            max_y,  # y value at top left
            math.sin(rot_rad),  # rotation factor 2
            -math.cos(rot_rad) * pixel_size  # transform pixel size in north-south
        ))

        # set projection for transformed coordinates
        raster.SetProjection(wkt_proj)

        # write simulated data to band 1
        raster.GetRasterBand(1).WriteArray(target)

        # flush all write cached data to disk
        raster.FlushCache()

        # free and reassign
        raster = None
        target = None

        # RASTER MASK PART ##################################################

        # file path for raster mask
        raster_mask_path = os.path.splitext(in_effects_dat)[0] + '_' + dmg_lvl.lower() + '_mask_.tif'

        # import raster
        ds_r = gdal.Open(raster_path)
        ds_r_val = ds_r.ReadAsArray()

        # spatial reference
        proj = ds_r.GetProjection()
        proj_gt = ds_r.GetGeoTransform()

        # overwrite pixel values with 0/1 regarding their threshold value
        r_mask_data = (ds_r_val >= 0.5).astype(int)

        LOGGER.debug('Projection:' + str(proj) + '/' + 'GeoTransform:' + str(proj_gt))
        LOGGER.debug('Pixel value corner/center:' + str(r_mask_data[0, 0]) + '/' + str(r_mask_data[256, 256]))

        # set raster format definition
        raster_mask = gdal.GetDriverByName('GTiff').Create(
            raster_mask_path,  # file path
            len(r_mask_data[0]),  # width in pixels
            len(r_mask_data),  # height in pixels
            1,  # number of bands
            gdal.GDT_Float32  # type of raster
        )

        # set transformation from pixel to projected coordinates
        raster_mask.SetGeoTransform(proj_gt)

        # set projection for transformed coordinates
        raster_mask.SetProjection(proj)

        # set nodata value
        raster_mask.GetRasterBand(1).DeleteNoDataValue()
        raster_mask.GetRasterBand(1).SetNoDataValue(0)

        # write data to band 1
        raster_mask.GetRasterBand(1).WriteArray(r_mask_data)

        # flush all write cached data to disk
        raster_mask.FlushCache()

        # free and reassign
        raster_mask = None
        r_mask_data = None

        # POLYGONIZE PART ##################################################

        # file path for polygonize result
        evac_polygons_path = os.path.join(os.path.dirname(in_effects), 'evac_polygons_' + dmg_lvl.lower() + '_.gml')

        # import raster
        ds_r_mask = gdal.Open(raster_mask_path)
        ds_r_mask_band = ds_r_mask.GetRasterBand(1)

        # spatial reference
        proj = ds_r_mask.GetProjection()
        sref = osr.SpatialReference(wkt=proj)

        # set vector format definition
        src_poly = ogr.GetDriverByName("GML").CreateDataSource(evac_polygons_path)
        src_poly_lyr = src_poly.CreateLayer("evac_zone", srs=sref)

        # create polygons at pixel value 1, nodata at pixel value 0
        gdal.Polygonize(ds_r_mask_band, ds_r_mask_band, src_poly_lyr, -1, [], callback=None)

        # free and reassign
        src_poly = None
        src_poly_lyr = None

        # EVACUATION ZONE PART ##################################################

        # correction buffer because of pixel error, based on used APOLLO precision
        corr_buff = float(math.sqrt(precision ** 2 + precision ** 2))

        LOGGER.debug('Correction Buffer:' + str(corr_buff))

        # file path for evacuation zone
        evac_zone_path = os.path.join(os.path.dirname(in_effects), 'evac_zone_' + dmg_lvl.lower() + '_.gml')

        # import polygons
        src_poly = ogr.GetDriverByName("GML").Open(evac_polygons_path)
        src_poly_lyr = src_poly.GetLayer()

        # spatial reference
        sref = osr.SpatialReference()
        sref.ImportFromEPSG(epsg)

        # collect all polygons
        geom_collect = ogr.Geometry(ogr.wkbGeometryCollection)
        for feat in src_poly_lyr:
            geom_collect.AddGeometry(feat.GetGeometryRef())

        # create convex hull
        conv_hull = geom_collect.ConvexHull()
        conv_hull.AssignSpatialReference(sref)

        LOGGER.debug('Centroid as WKT:' + str(conv_hull.Centroid().ExportToWkt()))

        # set vector format definition
        src_zone = ogr.GetDriverByName("GML").CreateDataSource(evac_zone_path)
        src_zone_lyr = src_zone.CreateLayer("evac_zone", srs=sref)

        # add data to file
        field_corr_buff = ogr.FieldDefn("corr_buff", ogr.OFTReal)
        src_zone_lyr.CreateField(field_corr_buff)
        src_zone_lyr_def = src_zone_lyr.GetLayerDefn()
        conv_hull_feat = ogr.Feature(src_zone_lyr_def)
        conv_hull_feat.SetGeometry(conv_hull)
        conv_hull_feat.SetField("corr_buff", corr_buff)
        src_zone_lyr.CreateFeature(conv_hull_feat)

        # free and reassign
        conv_hull_feat = None
        src_poly = None
        src_poly_lyr = None
        src_zone = None
        src_zone_lyr = None

        # set output format and file name
        response.outputs['out_evac_zone'].output_format = Format(mime_type='text/xml', extension='.gml',
                                                                 schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                                                 encoding='UTF-8', validate=None)
        response.outputs['out_evac_zone'].file = evac_zone_path

        response.outputs['out_raster'].output_format = Format(mime_type='image/geotiff', extension='.tif')
        response.outputs['out_raster'].file = raster_path

        return response