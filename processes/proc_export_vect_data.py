#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The process returns a subset of given or fixed spatial data selected by geometry.
"""

# libs
import logging
import tempfile
import requests
import owslib.util
from pywps import Process, LiteralInput, ComplexInput, ComplexOutput, Format
from pywps.app.Common import Metadata
from pywps.validator.mode import MODE
from pywps.validator import complexvalidator
from owslib.wms import WebMapService
from osgeo import ogr
from osgeo import osr
from lxml import etree
from lib import varlib
from lib import geolib

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# process returns a subset of given or fixed spatial data selected by geometry
class ExportVectData(Process):
    # static class variables
    epsg = 25832  # local spatial reference code

    def __init__(self):
        in_geom = ComplexInput(
            'in_geom',
            'Selection Geometry [gml]',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml)],
            mode=MODE.NONE
        )

        in_wfs1 = ComplexInput(
            'in_wfs1',
            'WFS Request 1 [gml]',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml)],
            mode=MODE.NONE,
            min_occurs=0
        )

        in_wfs2 = ComplexInput(
            'in_wfs2',
            'WFS Request 2 [gml]',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml)],
            mode=MODE.NONE,
            min_occurs=0
        )

        in_db1 = LiteralInput(
            'in_db1',
            'Database Spatial Data Name 1',
            abstract='Supported spatial data is defined by the following names: '
                     'address, building, parcel, local_plan, poi',
            data_type='string',
            allowed_values=('address', 'building', 'parcel', 'local_plan', 'poi'),
            min_occurs=0
        )

        in_db2 = LiteralInput(
            'in_db2',
            'Database Spatial Data Name 2',
            abstract='Supported spatial data is defined by the following names: '
                     'address, building, parcel, local_plan, poi',
            data_type='string',
            allowed_values=('address', 'building', 'parcel', 'local_plan', 'poi'),
            min_occurs=0
        )

        out_wfs1 = ComplexOutput(
            'out_wfs1',
            'WFS Request 1 Subset',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      encoding='UTF-8', validate=None)]
        )

        out_wfs2 = ComplexOutput(
            'out_wfs2',
            'WFS Request 2 Subset',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      encoding='UTF-8', validate=None)]
        )

        out_db1 = ComplexOutput(
            'out_db1',
            'Database Spatial Data 1 Subset',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      encoding='UTF-8', validate=None)]
        )

        out_db2 = ComplexOutput(
            'out_db2',
            'Database Spatial Data 2 Subset',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      encoding='UTF-8', validate=None)]
        )

        out_bound = ComplexOutput(
            'out_bound',
            'Selection Boundary',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      encoding='UTF-8', validate=None)]
        )

        out_map = ComplexOutput(
            'out_map',
            'Output Data Overview Map',
            supported_formats=[Format(mime_type='image/geotiff', extension='.tif')]
        )

        inputs = [in_geom, in_wfs1, in_wfs2, in_db1, in_db2]

        outputs = [out_wfs1, out_wfs2, out_db1, out_db2, out_bound, out_map]

        super(ExportVectData, self).__init__(
            self._handler,
            identifier='export_vect_data',
            version='1.0',
            title='Export Vector Data Process',
            abstract='The process returns a subset of given or fixed spatial data selected by geometry.',
            metadata=[Metadata('The process returns a subset of given or fixed spatial data selected by geometry.',
                               'http://geodev:8080/geonetwork/srv/ger/catalog.search?service=CSW&version=2.0.2'
                               '&request=GetRecordById&id=c850b578-8561-42fb-88d1-1ac9e3314cf4#/metadata/'
                               'c850b578-8561-42fb-88d1-1ac9e3314cf4')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )

    # handler method obtains request object and response object
    # @staticmethod  # only for static methods, no 'self' applicable
    def _handler(self, request, response):
        # check if data is given by reference
        if request.inputs['in_geom'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_geom'][0].method == 'GET':
                # obtain input with identifier as file name
                in_geom = request.inputs['in_geom'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_geom'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_geom'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, w: write in text mode
                filename = tempfile.mkstemp(prefix='input_', suffix='.gml')[1]
                with open(filename, 'w') as fp:
                    fp.write(data)
                    fp.close()

                in_geom = filename
        else:
            # obtain input with identifier as file name
            in_geom = request.inputs['in_geom'][0].file

        # open file and layer
        in_src = ogr.Open(in_geom)
        in_lyr = in_src.GetLayer()

        # get spatial reference
        epsg0 = int(in_lyr.GetSpatialRef().GetAttrValue('AUTHORITY', 1))

        # get geometry
        feat = in_lyr.GetNextFeature()
        geom = feat.GetGeometryRef()

        # only one single polygon input feature
        if in_lyr.GetFeatureCount() == 1 and geom.GetGeometryName() == 'POLYGON':
            # harmonization of spatial reference
            if epsg0 != self.epsg:
                # transform selection geometry to spatial reference of 3D city model
                sref0 = osr.SpatialReference()
                sref0.ImportFromEPSG(epsg0)
                sref = osr.SpatialReference()
                sref.ImportFromEPSG(self.epsg)
                transform = osr.CoordinateTransformation(sref0, sref)
                geom.Transform(transform)

            LOGGER.debug('Input Geometry of Type ' + str(geom.GetGeometryName()) +
                         ' in ' + str(self.epsg) + ':' + geom.ExportToWkt())

            # WFS PART ##################################################

            if 'out_wfs1' in request.outputs.keys():
                # check and obtain input with identifier as data directly
                if 'in_wfs1' in request.inputs:
                    wfs1 = request.inputs['in_wfs1'][0].data

                    # create file, w: write in text mode
                    in_path = tempfile.mkstemp(prefix='wfs1_data_', suffix='.gml')[1]
                    with open(in_path, 'w') as fp:
                        fp.write(wfs1)
                        fp.close()

                    LOGGER.debug('WFS1 Data String:' + str(wfs1[0:1000]))

                    # open file and layer
                    wfs1_src = ogr.Open(in_path)
                    wfs1_lyr = wfs1_src.GetLayer()

                    # get spatial reference
                    wfs_epsg = int(wfs1_lyr.GetSpatialRef().GetAttrValue('AUTHORITY', 1))

                    LOGGER.debug('WFS1 Feature Count in ' + str(wfs_epsg) + ':' + str(wfs1_lyr.GetFeatureCount()))

                    # check spatial reference
                    if wfs_epsg == self.epsg:
                        wfs1_lyr.SetSpatialFilter(geom)
                    else:
                        LOGGER.debug('Incompatible Spatial Reference of WFS1 and Selection Geometry.')

                    LOGGER.debug('WFS1 Feature Count After Filter:' + str(wfs1_lyr.GetFeatureCount()))

                    # set output format definition
                    out_path = tempfile.mkstemp(prefix='wfs_' + wfs1_lyr.GetName() + '_data_', suffix='.gml')[1]
                    out_src = ogr.GetDriverByName("GML").CreateDataSource(out_path)
                    out_src.CopyLayer(wfs1_lyr, wfs1_lyr.GetName())

                    # set output format and file name
                    response.outputs['out_wfs1'].output_format = Format(mime_type='text/xml', extension='.gml',
                                                                       schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                                                       encoding='UTF-8', validate=None)
                    response.outputs['out_wfs1'].file = out_path
            else:
                # remove output from response
                del response.outputs['out_wfs1']

            if 'out_wfs2' in request.outputs.keys():
                # check and obtain input with identifier as data directly
                if 'in_wfs2' in request.inputs:
                    wfs2 = request.inputs['in_wfs2'][0].data

                    # create file, w: write in text mode
                    in_path = tempfile.mkstemp(prefix='wfs2_data_', suffix='.gml')[1]
                    with open(in_path, 'w') as fp:
                        fp.write(wfs2)
                        fp.close()

                    LOGGER.debug('WFS2 Data String:' + str(wfs2[0:1000]))

                    # open file and layer
                    wfs2_src = ogr.Open(in_path)
                    wfs2_lyr = wfs2_src.GetLayer()

                    # get spatial reference
                    wfs_epsg = int(wfs2_lyr.GetSpatialRef().GetAttrValue('AUTHORITY', 1))

                    LOGGER.debug('WFS2 Feature Count in ' + str(wfs_epsg) + ':' + str(wfs2_lyr.GetFeatureCount()))

                    # check spatial reference
                    if wfs_epsg == self.epsg:
                        wfs2_lyr.SetSpatialFilter(geom)
                    else:
                        LOGGER.debug('Incompatible Spatial Reference of WFS2 and Selection Geometry.')

                    LOGGER.debug('WFS2 Feature Count After Filter:' + str(wfs2_lyr.GetFeatureCount()))

                    # set output format definition
                    out_path = tempfile.mkstemp(prefix='wfs_' + wfs2_lyr.GetName() + '_data_', suffix='.gml')[1]
                    out_src = ogr.GetDriverByName("GML").CreateDataSource(out_path)
                    out_src.CopyLayer(wfs2_lyr, wfs2_lyr.GetName())

                    # set output format and file name
                    response.outputs['out_wfs2'].output_format = Format(mime_type='text/xml', extension='.gml',
                                                                        schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                                                        encoding='UTF-8', validate=None)
                    response.outputs['out_wfs2'].file = out_path
            else:
                # remove output from response
                del response.outputs['out_wfs2']

            # DATABASE PART ##################################################

            if 'out_db1' in request.outputs.keys():
                # check and obtain input with identifier as data directly
                if 'in_db1' in request.inputs:
                    db1 = str(request.inputs['in_db1'][0].data)

                    LOGGER.debug('DB1 Data Request:' + db1)

                    # call spatial data export methods
                    if db1 == 'poi':
                        db1_data = geolib.pg_export(db1, geom, self.epsg)
                    elif db1 == 'local_plan':
                        db1_data = geolib.pg_export(db1, geom, self.epsg)
                    elif db1 == 'parcel':
                        db1_data = geolib.pg_export(db1, geom, self.epsg)
                    elif db1 == 'building':
                        db1_data = geolib.pg_export(db1, geom, self.epsg)
                    elif db1 == 'address':
                        db1_data = geolib.pg_export(db1, geom, self.epsg)
                    else:
                        db1_data = None

                    # set output format and file name
                    response.outputs['out_db1'].output_format = Format(mime_type='text/xml', extension='.gml',
                                                                       schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                                                       encoding='UTF-8', validate=None)
                    response.outputs['out_db1'].file = db1_data
            else:
                # remove output from response
                del response.outputs['out_db1']

            if 'out_db2' in request.outputs.keys():
                # check and obtain input with identifier as data directly
                if 'in_db2' in request.inputs:
                    db2 = str(request.inputs['in_db2'][0].data)

                    LOGGER.debug('DB2 Data Request:' + db2)

                    # call spatial data export methods
                    if db2 == 'poi':
                        db2_data = geolib.pg_export(db2, geom, self.epsg)
                    elif db2 == 'local_plan':
                        db2_data = geolib.pg_export(db2, geom, self.epsg)
                    elif db2 == 'parcel':
                        db2_data = geolib.pg_export(db2, geom, self.epsg)
                    elif db2 == 'building':
                        db2_data = geolib.pg_export(db2, geom, self.epsg)
                    elif db2 == 'address':
                        db2_data = geolib.pg_export(db2, geom, self.epsg)
                    else:
                        db2_data = None

                    # set output format and file name
                    response.outputs['out_db2'].output_format = Format(mime_type='text/xml', extension='.gml',
                                                                       schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                                                       encoding='UTF-8', validate=None)
                    response.outputs['out_db2'].file = db2_data
            else:
                # remove output from response
                del response.outputs['out_db2']

            # OVERVIEW MAP AND SELECTION GEOMETRY ##################################################

            if 'out_bound' in request.outputs.keys():
                # set output format and file name
                response.outputs['out_bound'].output_format = Format(mime_type='text/xml', extension='.gml',
                                                                    schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                                                    encoding='UTF-8', validate=None)
                response.outputs['out_bound'].file = in_geom
            else:
                # remove output from response
                del response.outputs['out_bound']

            if 'out_map' in request.outputs.keys():
                # WMS request
                url = "http://mapbender/wms7/gdm_atkis/gdm_atkis?"
                wms = WebMapService(url, version="1.1.1")

                # get extent and bounding box
                extent = geom.GetEnvelope()
                bbx1, bby1 = extent[0], extent[2]
                bbx2, bby2 = extent[1], extent[3]

                # image ratio values
                x_diff = bbx2 - bbx1
                y_diff = bby2 - bby1
                width = 1280

                # request parameters
                bbox = (bbx1, bby1, bbx2, bby2)
                size = (x_diff/y_diff*width, width)
                srs = 'EPSG:' + str(self.epsg)
                file_type = 'image/tiff'

                try:
                    # get map request
                    gm = wms.getmap(layers=['atkis1'], bbox=bbox, size=size, format=file_type, srs=srs, transparent=True)

                    LOGGER.debug('Get Map URL:' + gm.geturl())

                    # create file, wb: write in binary mode
                    ov_map_path = tempfile.mkstemp(prefix='ov_map_', suffix='.tif')[1]
                    with open(ov_map_path, 'wb') as fp:
                        fp.write(gm.read())
                        fp.close()
                except owslib.util.ServiceException as se:
                    ov_map_path = ''
                    LOGGER.debug('WMS ServiceException:' + str(se))

                # set output format and file name
                response.outputs['out_map'].output_format = Format(mime_type='image/geotiff', extension='.tif')
                response.outputs['out_map'].file = ov_map_path
            else:
                # remove output from response
                del response.outputs['out_map']
        else:
            LOGGER.debug('Only one single polygon input feature allowed. ' + str(in_lyr.GetFeatureCount()) +
                         ' features of type ' + str(geom.GetGeometryName()) + ' detected!')

        return response