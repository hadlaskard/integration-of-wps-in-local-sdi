#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The process is part of the explosive ordnance disposal workflow
    and returns rough danger distance based on given solid and tnt mass.
"""

# libs
import logging
from pywps import Process, LiteralInput, LiteralOutput
from pywps.app.Common import Metadata
from pywps.validator.allowed_value import RANGECLOSURETYPE, ALLOWEDVALUETYPE
from pywps.inout.literaltypes import AllowedValue
from lib import geolib

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# process returns rough danger distance based on given solid and tnt mass
class ApolloRoughDist(Process):
    def __init__(self):
        in_tnt = LiteralInput(
            'in_tnt',
            'Rough TNT Blast Power [kg]',
            data_type='integer',
            # spacing unable to use due incompatibilities between QGIS wps client
            # allowed_values=(range(50, 2000+1, 50)),
            allowed_values=[AllowedValue(minval=1, maxval=5000,  # spacing=50,
                                         allowed_type=ALLOWEDVALUETYPE.RANGE,
                                         range_closure=RANGECLOSURETYPE.OPEN)]
        )

        in_solid = LiteralInput(
            'in_solid',
            'Solid Type',
            abstract='Type of material the damage distance threshold will be calculated for: '
                     '0 = Float Glass, 1 = Eardrum Rupture',
            data_type='integer',
            allowed_values=(0, 1),
            min_occurs=0
        )

        out_rough_dist = LiteralOutput(
            'out_rough_dist',
            'Rough Danger Distance',
            data_type='string'  # use of string instead float as workaround for bug in PyWPS
        )

        inputs = [in_tnt, in_solid]

        outputs = [out_rough_dist]

        super(ApolloRoughDist, self).__init__(
            self._handler,
            identifier='apollo_rough_dist',
            version='1.0',
            title='APOLLO Rough Danger Distance Process',
            abstract='The process returns rough danger distance based on given solid and tnt mass.',
            metadata=[Metadata('The process is part of the explosive ordnance disposal workflow '
                               'and returns rough danger distance based on given solid and tnt mass.',
                               'http://geodev:8080/geonetwork/srv/ger/catalog.search?service=CSW&version=2.0.2'
                               '&request=GetRecordById&id=c850b578-8561-42fb-88d1-1ac9e3314cf4#/metadata/'
                               'c850b578-8561-42fb-88d1-1ac9e3314cf4')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )

    # handler method obtains request object and response object
    # @staticmethod  # only for static methods, no 'self' applicable
    def _handler(self, request, response):
        # default parameter values
        tnt, solid = 0, 0

        # check and obtain input with identifier as data directly
        if 'in_tnt' in request.inputs:
            tnt = request.inputs['in_tnt'][0].data
        if 'in_solid' in request.inputs:
            solid = request.inputs['in_solid'][0].data

        # calculation of threshold distance
        dist_threshold = geolib.damage_dist_threshold(tnt, solid)

        # set output format and file name
        response.outputs['out_rough_dist'].data = str(dist_threshold)

        return response