#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The process is part of the explosive ordnance disposal workflow
    and executes APOLLO via SIRIUS and returns blast effects result.
"""

# libs
import logging
import tempfile
import requests
import json
from pywps import Process, ComplexInput, ComplexOutput, Format
from pywps.app.Common import Metadata
from pywps.validator.mode import MODE
from pywps.validator import complexvalidator
from lxml import etree
from lib import varlib

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# process executes APOLLO via SIRIUS and returns blast effects result
class ApolloExecute(Process):
    def __init__(self):
        in_conf = ComplexInput(
            'in_conf',
            'APOLLO Configuration Data [json]',
            supported_formats=[Format(mime_type='application/json', extension='.json',
                                      validate=complexvalidator.validategeojson,
                                      encoding='UTF-8', schema='json')],
            mode=MODE.NONE
        )

        in_dem = ComplexInput(
            'in_dem',
            'Digital Elevation Model [tif]',
            supported_formats=[Format(mime_type='image/geotiff', extension='.tif')],
            mode=MODE.NONE
        )

        in_city = ComplexInput(
            'in_city',
            '3D City Model [x3d]',
            supported_formats=[Format(mime_type='text/xml', extension='.x3d',
                                      schema='http://www.web3d.org/specifications/x3d-3.3.xsd',
                                      validate=None, encoding='UTF-8')],
            mode=MODE.NONE
        )

        out_effects = ComplexOutput(
            'out_effects',
            'APOLLO Effects Result',
            supported_formats=[Format(mime_type='application/octet-stream')]
        )

        inputs = [in_conf, in_dem, in_city]

        outputs = [out_effects]

        super(ApolloExecute, self).__init__(
            self._handler,
            identifier='apollo_execute',
            version='1.0',
            title='APOLLO Execute Process',
            abstract='The process executes APOLLO via SIRIUS and returns blast effects result.',
            metadata=[Metadata('The process is part of the explosive ordnance disposal workflow '
                               'and executes APOLLO via SIRIUS and returns blast effects result.',
                               'http://geodev:8080/geonetwork/srv/ger/catalog.search?service=CSW&version=2.0.2'
                               '&request=GetRecordById&id=c850b578-8561-42fb-88d1-1ac9e3314cf4#/metadata/'
                               'c850b578-8561-42fb-88d1-1ac9e3314cf4')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )

    # handler method obtains request object and response object
    # @staticmethod  # only for static methods, no 'self' applicable
    def _handler(self, request, response):
        # IN_CONF PART ##################################################

        # check if data is given by reference
        if request.inputs['in_conf'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_conf'][0].method == 'GET':
                # obtain input with identifier as file name
                in_conf = request.inputs['in_conf'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_conf'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_conf'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, w: write in text mode
                filename = tempfile.mkstemp(prefix='conf_', suffix='.json')[1]
                with open(filename, 'w') as fp:
                    fp.write(data)
                    fp.close()

                in_conf = filename
        else:
            # obtain input with identifier as file name
            in_conf = request.inputs['in_conf'][0].file

        # IN_DEM PART ##################################################

        # check if data is given by reference
        if request.inputs['in_dem'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_dem'][0].method == 'GET':
                # obtain input with identifier as file name
                in_dem = request.inputs['in_dem'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_dem'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_dem'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, wb: write in binary mode
                filename = tempfile.mkstemp(prefix='dem_', suffix='.tif')[1]
                with open(filename, 'wb') as fp:
                    fp.write(data)
                    fp.close()

                in_dem = filename
        else:
            # obtain input with identifier as file name
            in_dem = request.inputs['in_dem'][0].file

        # IN_CITY PART ##################################################

        # check if data is given by reference
        if request.inputs['in_city'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_city'][0].method == 'GET':
                # obtain input with identifier as file name
                in_city = request.inputs['in_city'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_city'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_city'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, w: write in text mode
                filename = tempfile.mkstemp(prefix='city_', suffix='.x3d')[1]
                with open(filename, 'w') as fp:
                    fp.write(data)
                    fp.close()

                in_city = filename
        else:
            # obtain input with identifier as file name
            in_city = request.inputs['in_city'][0].file

        # EXECUTE PART ##################################################

        LOGGER.debug('Config path:' + in_conf)
        LOGGER.debug('DEM path:' + in_dem)
        LOGGER.debug('City path:' + in_city)

        # open configuration file
        with open(in_conf, 'r') as fp:
            conf_data = json.load(fp)

        # read url for APOLLO service and result data
        if 'service' in conf_data:
            srv_url = conf_data['service']['url']
            result_file = conf_data['service']['resultFile']
            srv_url_result = srv_url + result_file
            LOGGER.debug('Service URL:' + srv_url_result)

        # NON-PRODUCTIVE ONLY -> overwrite result data url because simulation of working SIRIUS / APOLLO server
        srv_url_result = 'https://geodev2/apollo_result/apollo_effects.zip'

        # reveal input data, execute APOLLO and calculate effects result
        # r_exe = requests.get(srv_url, verify=False)

        # effects result file checker
        while not requests.head(srv_url_result, verify=False).status_code == requests.codes.ok:
            response.update_status('APOLLO Execute Process Still In Progress', 0)
            LOGGER.debug('Resource File Status Code:' + str(requests.head(srv_url_result, verify=False).status_code))

        # get effects result file when APOLLO is ready
        r = requests.get(srv_url_result, verify=False)
        data = r.content

        # create file, wb: write in binary mode
        result_file = tempfile.mkstemp(prefix='effects_', suffix='.zip')[1]
        with open(result_file, 'wb') as fp:
            fp.write(data)
            fp.close()

        # set output format and file name
        response.outputs['out_effects'].output_format = Format(mime_type='application/octet-stream')
        response.outputs['out_effects'].file = result_file

        return response