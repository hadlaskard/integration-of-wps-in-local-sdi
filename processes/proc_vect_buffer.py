#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" The process returns buffer around each input feature.
"""

# libs
import logging
import requests
import tempfile
from pywps import Process, LiteralInput, ComplexInput, ComplexOutput, Format
from pywps.app.Common import Metadata
from pywps.validator.mode import MODE
from pywps.validator import complexvalidator
from osgeo import ogr
from osgeo import osr
from lxml import etree
from lib import varlib

# authorship information
__author__ = "Gunnar Ströer"
__copyright__ = "Copyright 2019, integration of wps in local sdi"
__version__ = "1.0"
__maintainer__ = "Gunnar Ströer"
__email__ = "gunnar.stroeer@yahoo.de"
__status__ = "Development"

# global variables
LOGGER = logging.getLogger("PYWPS")


# process process returns buffer around each input feature
class VectBuffer(Process):
    def __init__(self):
        in_geom = ComplexInput(
            'in_geom',
            'Input Geometry [gml]',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml),
                               Format(mime_type='application/gml+xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      validate=complexvalidator.validategml)],
            # validation mode unable to use due incompatibilities between mimetype library and QGIS wps client
            mode=MODE.NONE
        )

        in_size_ref = ComplexInput(
            'in_size_ref',
            'Buffer Size Reference',
            abstract='Buffer size calculated by previous process only chainable as reference.',
            supported_formats=[Format(mime_type='text/plain')],
            min_occurs=0
        )

        in_size = LiteralInput(
            'in_size',
            'Buffer Size [m]',
            data_type='string',  # use of string instead float as workaround
            min_occurs=0
        )

        in_size_field = LiteralInput(
            'in_size_field',
            'Buffer Size Field Name',
            abstract='Name of input geometry attribute field which value will be used for buffer size.',
            data_type='string',
            min_occurs=0
        )

        out_buff = ComplexOutput(
            'out_buff',
            'Buffer Geometry',
            supported_formats=[Format(mime_type='text/xml', extension='.gml',
                                      schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                      encoding='UTF-8', validate=None)]
        )

        inputs = [in_geom, in_size_ref, in_size, in_size_field]
        outputs = [out_buff]

        super(VectBuffer, self).__init__(
            self._handler,
            identifier='vect_buffer',
            version='1.0',
            title='Vector Buffer Process',
            abstract='The process returns buffer around each input feature.',
            metadata=[Metadata('The process returns buffer around each input feature.',
                               'http://geodev:8080/geonetwork/srv/ger/catalog.search?service=CSW&version=2.0.2'
                               '&request=GetRecordById&id=c850b578-8561-42fb-88d1-1ac9e3314cf4#/metadata/'
                               'c850b578-8561-42fb-88d1-1ac9e3314cf4')],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True
        )

    # handler method obtains request object and response object
    # @staticmethod  # only for static methods, no 'self' applicable
    def _handler(self, request, response):
        # check if data is given by reference
        if request.inputs['in_geom'][0].as_reference:
            # check if GET method is used
            if request.inputs['in_geom'][0].method == 'GET':
                # obtain input with identifier as file name
                in_geom = request.inputs['in_geom'][0].file
            # check if POST method is used - whole response has to be parsed (chaining)
            elif request.inputs['in_geom'][0].method == 'POST':
                # obtain whole response XML with identifier as data directly
                in_response = request.inputs['in_geom'][0].data

                LOGGER.debug('XML Response:' + in_response)

                # get content of LiteralData, Reference or ComplexData
                ref_url = varlib.get_output(etree.fromstring(in_response))

                # get GML file as reference
                r = requests.get(ref_url[ref_url.keys()[0]], verify=False)
                data = r.content

                # create file, w: write in text mode
                filename = tempfile.mkstemp(prefix='geom_', suffix='.gml')[1]
                with open(filename, 'w') as fp:
                    fp.write(data)
                    fp.close()

                in_geom = filename
        else:
            # obtain input with identifier as file name
            in_geom = request.inputs['in_geom'][0].file

        # default parameter values
        size, size_field = 0, ''

        # check and obtain input with identifier as data directly
        if 'in_size' in request.inputs:
            size = request.inputs['in_size'][0].data
        if 'in_size_field' in request.inputs:
            size_field = request.inputs['in_size_field'][0].data
        if 'in_size_ref' in request.inputs:
            size_ref = request.inputs['in_size_ref'][0].data

            # buffer size priority by reference
            if float(size_ref):
                size = float(size_ref)

        # open file and layer
        in_src = ogr.Open(in_geom)
        in_lyr = in_src.GetLayer()

        # get layer name
        lyr_name = in_lyr.GetName()

        # get all field names of input layer
        field_names = [field.name for field in in_lyr.schema]

        # get and set output spatial reference
        epsg = int(in_lyr.GetSpatialRef().GetAttrValue('AUTHORITY', 1))
        sref = osr.SpatialReference()
        sref.ImportFromEPSG(epsg)

        # create output file
        driver = ogr.GetDriverByName('GML')
        out_src = driver.CreateDataSource(lyr_name)
        out_lyr = out_src.CreateLayer(lyr_name+'_buff', sref, ogr.wkbPolygon)

        # get feature count
        count = in_lyr.GetFeatureCount()
        index = 0

        # make buffer for each feature
        while index < count:
            # get the geometry
            in_feat = in_lyr.GetNextFeature()
            in_geom = in_feat.GetGeometryRef()

            # check if size attribute exists
            if size_field in field_names:
                size_val = in_feat.GetField(size_field)
                if isinstance(size_val, int) or isinstance(size_val, float):
                    size = size_val
                else:
                    size = 0

            LOGGER.debug('Buffer Size:' + str(size))

            # make the buffer
            buff_geom = in_geom.Buffer(float(size))

            # create output feature to the file
            out_feat = ogr.Feature(feature_def=out_lyr.GetLayerDefn())
            out_feat.SetGeometry(buff_geom)
            out_lyr.CreateFeature(out_feat)

            # free and reassign
            out_feat = None

            index += 1

        # free and reassign
        out_src = None

        # set output format and file name
        response.outputs['out_buff'].output_format = Format(mime_type='text/xml', extension='.gml',
                                                            schema='http://schemas.opengis.net/gml/3.1.1/base/gml.xsd',
                                                            encoding='UTF-8', validate=None)
        response.outputs['out_buff'].file = lyr_name

        return response