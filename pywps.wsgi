#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" PyWPS WSGI instance - A wrapper around the PyWPS server with
    a list of processes and configuration files passed as arguments.
"""

# libs
import sys
from pywps.app import Service

# processes need to be installed in PYTHON_PATH
sys.path.append('/srv/www/wps/')

from processes.proc_apollo_conf import ApolloConf
from processes.proc_apollo_evac_zone import ApolloEvacZone
from processes.proc_apollo_execute import ApolloExecute
from processes.proc_apollo_rough_dist import ApolloRoughDist
from processes.proc_export_3d_data import Export3dData
from processes.proc_export_vect_data import ExportVectData
from processes.proc_vect_buffer import VectBuffer
from processes.proc_vect_intersect import VectIntersect

processes = [
    ApolloConf(),
    ApolloEvacZone(),
    ApolloExecute(),
    ApolloRoughDist(),
    Export3dData(),
    ExportVectData(),
    VectBuffer(),
    VectIntersect()
]

# for the process list on the home page
process_descriptor = {}
for process in processes:
    abstract = process.abstract
    identifier = process.identifier
    process_descriptor[identifier] = abstract

# Service accepts list of process instances and list of configuration files
application = Service(processes, ['/srv/www/wps/pywps.cfg'])